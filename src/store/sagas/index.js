import { all } from 'redux-saga/effects';
import chatSaga from './chat';


export default function* rootSaga(getState) {
    yield all([
        chatSaga()
    ]);
}