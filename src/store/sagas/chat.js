import { all, call, fork, put, takeEvery, takeLatest } from "redux-saga/effects"
import { APIClient } from "../../helpers/apiClient"
import {
  apiError,
  getGroupsSuccess,
  createGroupSuccess,
  getChatsSuccess,
  sendMessageSuccess,
  getNotificationSuccess,
  markAsReadSuccess,
  getChats as getAllChats,
  createChatSuccess,
} from "../action/index"
import {
  CREATE_CHAT,
  CREATE_GROUP,
  GET_CHATS,
  GET_GROUPS,
  GET_NOT,
  MARK_AS_READ,
  SEND_MESSAGE,
} from "../reducer/chat/constants"

const get = new APIClient().get
const create = new APIClient().create
const putData = new APIClient().put

// const profile = useProfile()

function* getGroups({ payload: { userId } }) {
  try {
    const response = yield call(get, `/users/${userId}/groups`)
    yield put(getGroupsSuccess(response.groups))
  } catch (error) {
    yield put(apiError(error))
  }
}

function* markAsRead({ payload: { userId, roomId } }) {
  try {
    const response = yield call(putData, `room/${roomId}/mark-read`, {
      ecodeemId: userId,
    })

    yield put(markAsReadSuccess(response.data.n))
  } catch (error) {
    yield put(apiError(error))
  }
}

function* getChats({ payload: { userId } }) {
  try {
    const response = yield call(get, `/users/${userId}/all`)
    yield put(getChatsSuccess(response.conversations))
  } catch (error) {
    yield put(apiError(error))
  }
}

function* getNotification({ payload: { userId } }) {
  try {
    const response = yield call(get, `/users/${userId}/unread`)
    yield put(getNotificationSuccess(response.unread))
  } catch (error) {
    yield put(apiError(error))
  }
}

function* createGroup({ payload: { groupData: group } }) {
  try {
    const response = yield call(create, "/room/initiate-group", {
      groupname: group.groupname,
      groupdescription: group.groupdescription,
      ecodeemId: group.ecodeemId,
      type: group.type,
      userId: [...group.userIds],
    })
    yield put(createGroupSuccess(response.chatRoom.room))
    yield put(getAllChats(group.ecodeemId))
  } catch (error) {
    yield put(apiError(error))
  }
}

function* createChat({ payload: { chatData } }) {
  try {
    const response = yield call(create, "/room/initiate-chat", {
      ecodeemId: chatData.ecodeemId,
      type: "private",
      userId: chatData.userId,
    })

    if (response.chatRoom.isNew) {
      yield put(getAllChats(chatDat.ecodeemId))
      yield put(createChatSuccess(response.chatRoom.room._id))
    } else {
      yield put(createChatSuccess(response.chatRoom.room._id))
    }
  } catch (error) {
    yield put(apiError(error))
  }
}

function* sendMessage({ payload: { message } }) {
  const { roomId, ...rest } = message
  try {
    const response = yield call(create, `room/${message.roomId}/message`, {
      ...rest,
    })
    yield put(sendMessageSuccess(response.post))
  } catch (error) {
    yield put(apiError(error))
  }
}

export function* watchGetGroup() {
  yield takeEvery(GET_GROUPS, getGroups)
}

export function* watchCreateGroup() {
  yield takeEvery(CREATE_GROUP, createGroup)
}

export function* watchMarkAsRead() {
  yield takeEvery(MARK_AS_READ, markAsRead)
}

export function* watchGetChats() {
  yield takeEvery(GET_CHATS, getChats)
}

export function* watchSendMessage() {
  yield takeEvery(SEND_MESSAGE, sendMessage)
}

export function* watchGetNot() {
  yield takeEvery(GET_NOT, getNotification)
}

export function* watchCreateChat() {
  yield takeEvery(CREATE_CHAT, createChat)
}

function* chatSaga() {
  yield all([
    fork(watchGetGroup),
    fork(watchCreateGroup),
    fork(watchGetChats),
    fork(watchSendMessage),
    fork(watchGetNot),
    fork(watchMarkAsRead),
    fork(watchCreateChat),
  ])
}

export default chatSaga
