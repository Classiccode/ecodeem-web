import {
  CHAT_USER, ACTIVE_USER, FULL_USER, ADD_LOGGED_USER, CREATE_GROUP, GET_CHATS, GET_GROUPS_SUCCESS, GET_GROUPS, CREATE_GROUP_SUCCESS, GET_CHATS_SUCCESS, SEND_MESSAGE, SEND_MESSAGE_SUCCESS, GET_NOT_SUCCESS, GET_NOT, MARK_AS_READ, MARK_AS_READ_SUCCESS, ON_NEW_SEND_MESSAGE, CREATE_CHAT, CREATE_CHAT_SUCCESS, TYPING_MESSAGE, API_FAILED
} from '../reducer/chat/constants';


export const chatUser = () => ({
  type: CHAT_USER
});

export const activeUser = (userId) => ({
  type: ACTIVE_USER,
  payload: userId
});

export const setFullUser = (fullUser) => ({
  type: FULL_USER,
  payload: fullUser
});

export const addLoggedinUser = (userData) => ({
  type: ADD_LOGGED_USER,
  payload: userData
});

export const createGroup = (groupData) => ({
  type: CREATE_GROUP,
  payload: { groupData }
})

export const createGroupSuccess = (group) => ({
  type: CREATE_GROUP_SUCCESS,
  payload: group
})

export const createChat = (chatData) => ({
  type: CREATE_CHAT,
  payload: { chatData }
})

export const createChatSuccess = (chat) => ({
  type: CREATE_CHAT_SUCCESS,
  payload: chat
})

export const sendMessage = (message) => ({
  type: SEND_MESSAGE,
  payload: { message }
})

export const sendMessageSuccess = (message) => ({
  type: SEND_MESSAGE_SUCCESS,
  payload: message
})

export const getGroups = (userId) => ({
  type: GET_GROUPS,
  payload: { userId }
})

export const getGroupsSuccess = (groups) => ({
  type: GET_GROUPS_SUCCESS,
  payload: groups
})


export const getChats = (userId) => ({
  type: GET_CHATS,
  payload: { userId }
})

export const getChatsSuccess = (chats) => ({
  type: GET_CHATS_SUCCESS,
  payload: chats
})

export const markAsRead = (userId, roomId) => ({
  type: MARK_AS_READ,
  payload: { userId, roomId }
})

export const markAsReadSuccess = (count) => ({
  type: MARK_AS_READ_SUCCESS,
  payload: count
})


export const getNotification = (userId) => ({
  type: GET_NOT,
  payload: { userId }
})

export const getNotificationSuccess = (unread) => ({
  type: GET_NOT_SUCCESS,
  payload: unread
})

export const onNewMessage = (message) => ({
  type: ON_NEW_SEND_MESSAGE,
  payload: message
})

export const onTyping = (typingUsers) => ({
  type: TYPING_MESSAGE,
  payload: typingUsers
})

export const apiError = (error) => ({
  type: API_FAILED,
  payload: error
});
