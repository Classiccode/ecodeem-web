import { combineReducers, createStore, applyMiddleware } from "redux"
import thunk from "redux-thunk"
import { eventReducer } from "./reducer"
import { dataReducer } from "./reducer/data"
import { chatReducer } from "./reducer/chat"
import createSagaMiddleware from "redux-saga"
import rootSaga from "./sagas"
import friendSlice from "./slice/friendSlice"
import productSlice from "./slice/productSlice"
import cartSlice from "./slice/cartSlice"
import Layout from "./reducer/layout"
import blogSlice from "./slice/blogSlice"
import deemSlice from "./slice/deemSlice"
import { persistReducer } from "redux-persist"
import storage from "redux-persist/lib/storage"
import persistStore from "redux-persist/lib/persistStore"
import { themeReducer } from "./reducer/theme"
import orderSlice from "./slice/orderSlice"

const authPersistConfig = {
  key: "auth",
  storage: storage,
}

const themePersistConfig = {
  key: "theme",
  storage: storage,
}
const sagaMiddleware = createSagaMiddleware()
const middleware = [sagaMiddleware, thunk]
const mainReducer = combineReducers({
  event: eventReducer,
  data: persistReducer(authPersistConfig, dataReducer),
  Chat: chatReducer,
  Layout,
  friends: friendSlice,
  blog: blogSlice,
  deem: deemSlice,
  product: productSlice,
  cart: cartSlice,
  orders: orderSlice,
  theme: persistReducer(themePersistConfig, themeReducer),
})
export const store = createStore(mainReducer, applyMiddleware(...middleware))
export type RootState = ReturnType<typeof mainReducer>
export const persistor = persistStore(store)

sagaMiddleware.run(rootSaga)
