import { createSlice } from "@reduxjs/toolkit"

interface CartProps {
  cart: any[]
  addedItems: any[]
  removedItems: any[]
  decrementItems: any[]
  shippingType: any[]
  totalPrice: number
}
const initialState = {
  cart: [],
  addedItems: [],
  removedItems: [],
  decrementItems: [],
  shippingType: [],
  totalPrice: 0,
} as CartProps

const CartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    getCart: (state, action) => {
      state.cart = action.payload
    },
    addToCart: (state, action) => {
      state.addedItems = action.payload
    },
    removeFromCart: (state, action) => {
      state.removedItems = action.payload
    },
    decrementFromCart: (state, action) => {
      state.decrementItems = action.payload
    },
    clearCart: (state, action) => {
      state.cart = action.payload
    },
    getShippingType: (state, action) => {
      state.shippingType = action.payload
    },
    getTotalPrice: (state) => {
      if (state.cart.length >= 1) {
        state.totalPrice = state.cart.reduce(
          (accumulator, currentValue) => accumulator + currentValue.total,
          0
        )
      }
    },
  },
})

export const {
  getCart,
  addToCart,
  removeFromCart,
  clearCart,
  getTotalPrice,
  decrementFromCart,
  getShippingType,
} = CartSlice.actions

export default CartSlice.reducer
