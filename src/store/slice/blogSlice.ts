import { toast } from "react-toastify"
/* eslint-disable @typescript-eslint/no-explicit-any */
import { PostType } from "../../core/type"
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axios from "axios"
import Config from "../../config"

// const profile = useProfile()

interface IgetDeemProps {
  id?: string
}

interface IBase {
  postId?: string
  commentId?: string
  replyId?: string
  token?: string
}

export const getInterestedDeems = createAsyncThunk(
  "fetchAllDeems",
  async () => {
    const response = await axios.get(`${Config.apiUrl}/post/timeline`)
    return response.data.timelinePosts
  }
)

export const getPerInterestDeems = createAsyncThunk(
  "fetchPerInterestDeems",
  async (interest: string) => {
    const response = await axios.get(
      `${Config.apiUrl}/interest/single/${interest}`
    )

    return response.data.interestPosts
  }
)

export const sharePost = createAsyncThunk(
  "sharePost",
  async (apiData: IBase) => {
    const response: any = await axios.patch(
      `${Config.apiUrl}/post/share?postId=${apiData.postId}`
    )

    const { data, status } = response
    return data
  }
)

// export const createPost = createAsyncThunk('post', async (postData: PostApiData) => {

//     // const response: any = await axios.post(Config.REST_ENDPOINT + NewPost, { ...Apidata });

//     // const { data, status } = response;

//     // return data;

// })

interface BlogState {
  posts: PostType[]
  post: PostType | undefined
  status: "idle" | "loading" | "succeeded" | "failed"
  message: any
  action: any

  // commentStatus: 'idle' | 'loading' | 'succeeded' | 'failed',
  // commentMessage: any,
  // comment: commentType | null
  // deleteCommentStatus: 'idle' | 'loading' | 'succeeded' | 'failed',
  // deleteCommentMessage: any,
  // replyMessage:any,
  // replyStatus:'idle' | 'loading' | 'succeeded' | 'failed',
  // sharePostErrorStatus: 'idle' | 'loading' | 'succeeded' | 'failed',
  // sharePostError: any,

  postModalState: boolean
}

const initialState = {
  posts: [],
  post: undefined,
  status: "idle",
  message: null,
  action: null,

  // commentMessage: '',
  // commentStatus: 'idle',
  // replyMessage:'',
  // comment: null,
  // replyStatus:'idle',

  postModalState: false,

  // deleteCommentStatus: 'idle',
  // deleteCommentMessage: '',
  // sharePostErrorStatus: 'idle',
  // sharePostError: '',
} as BlogState

// type statusType = {
//     status: 'idle' | 'loading' | 'succeeded' | 'failed',
// }

interface ActionType {
  type: string
  payload: PostType
}

interface ReactionActionType {
  type: string
  payload: {
    commentId?: string
    postId?: string
    token: string
    userId?: string
  }
}

const blogSlice = createSlice({
  name: "blog",
  initialState,

  reducers: {
    addPost(state, action) {
      state.posts.unshift(action.payload)
    },

    removePost(state, action: ActionType) {
      const removeIndex = state.posts
        .map(function (item: PostType) {
          return item._id
        })
        .indexOf(action.payload._id)

      // remove object

      state.posts.splice(removeIndex, 1)
    },

    togglePost(state) {
      state.postModalState = !state.postModalState
    },
    likeDeem(state, action: ReactionActionType) {
      const prevState: PostType[] = state.posts

      const index = prevState
        .map(function (e) {
          return e._id
        })
        .indexOf(action.payload.postId!)

      if (index !== -1) {
        state.posts[index].counts!.likesCount++
        state.posts[index].likes!.push(action.payload.userId!)
      }

      const config = {
        headers: { Authorization: `Bearer ${action.payload.token}` },
      }

      axios.patch(
        `${Config.apiUrl}/post/like?postId=${action.payload.postId}`,
        {},
        config
      )
    },

    unlikeDeem(state, action: ReactionActionType) {
      const prevState: any = state.posts

      const index = prevState
        .map(function (e) {
          return e._id
        })
        .indexOf(action.payload.postId)

      if (index !== -1) {
        state.posts[index].counts!.likesCount--
        const idx = state.posts[index].likes!.indexOf(action.payload.userId!)

        state.posts[index].likes!.splice(idx, 1)
      }

      const config = {
        headers: { Authorization: `Bearer ${action.payload.token}` },
      }

      axios.patch(
        `${Config.apiUrl}/post/like?postId=${action.payload.postId}`,
        {},
        config
      )
    },
  },

  extraReducers: {
    [getInterestedDeems.pending.toString()]: (state, action) => {
      state.status = "loading"
    },
    [getInterestedDeems.fulfilled.toString()]: (state, action) => {
      state.status = "succeeded"

      // Add any fetched posts to the array

      const newArray = action.payload.filter(
        (value) => Object.keys(value).length !== 0 && value.postedBy !== null
      )
      state.posts = [...newArray]
    },

    [getInterestedDeems.rejected.toString()]: (state, action) => {
      state.status = "failed"
      state.message = action.error.message
    },

    [getPerInterestDeems.pending.toString()]: (state, action) => {
      state.status = "loading"
    },
    [getPerInterestDeems.fulfilled.toString()]: (state, action) => {
      state.status = "succeeded"

      // Add any fetched posts to the array

      state.posts = action.payload
    },
    [getPerInterestDeems.rejected.toString()]: (state, action) => {
      state.status = "failed"
      state.message = action.error.message
    },

    [sharePost.rejected.toString()]: (state, action) => {
      state.status = "failed"

      // console.log(action);

      state.message = action.error.message
    },

    [sharePost.fulfilled.toString()]: (state, action) => {
      toast.success("post shared to your timeline", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      })
    },
    [sharePost.rejected.toString()]: (state, action) => {
      toast.error(action.error.message, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      })
    },
  },
})

export default blogSlice.reducer

export const selectAllPosts = (state: { posts: PostType[] }) => {
  return state.posts
}
export const { likeDeem, unlikeDeem, addPost, togglePost, removePost } =
  blogSlice.actions
