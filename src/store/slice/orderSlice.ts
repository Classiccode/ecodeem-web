import { createSlice } from "@reduxjs/toolkit"

interface OrderProps {
  orders: any[]
  cancelOrder: any[]
  track: any[]
  create: any[]
}
const initialState = {
  orders: [],
  cancelOrder: [],
  track: [],
  create: [],
} as OrderProps

const OrderSlice = createSlice({
  name: "orders",
  initialState,
  reducers: {
    getOrders: (state, action) => {
      state.orders = action.payload
    },
    cancelOrder: (state, action) => {
      state.cancelOrder = action.payload
    },
    trackOrder: (state, action) => {
      state.track = action.payload
    },
    createOrder: (state, action) => {
      state.create = action.payload
    },
  },
})

export const { getOrders, createOrder, cancelOrder, trackOrder } =
  OrderSlice.actions

export default OrderSlice.reducer
