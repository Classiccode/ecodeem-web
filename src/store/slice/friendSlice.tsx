import { FollowerType, FollowingType } from "../../core/type"
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axios, { AxiosResponse } from "axios"
import Config from "../../config"
import { toast } from "react-toastify"

interface IBase {
  userId?: string
}

export const getFollowers = createAsyncThunk("fetchFollowers", async () => {
  const resp = await axios.get(`${Config.apiUrl}/follow/followers`)

  return resp.data
})

export const getFollowing = createAsyncThunk("fetchFollowing", async () => {
  const resp = await axios.get(`${Config.apiUrl}/follow/following`)

  return resp.data
})

export const follow = createAsyncThunk("follow", async (props: IBase) => {
  const resp = await axios.post(
    `${Config.apiUrl}/follow/follow-user?userId=${props.userId}`,
    {}
  )

  return resp.data
})

export const followRes = async ({
  token,
  userID,
}: {
  userID: string
  token: string
}): Promise<AxiosResponse> => {
  return await axios.post(
    `${Config.apiUrl}/follow/follow-user?userId=${userID}`,
    {},
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  )
}

export const unFollowRes = async ({
  userID,
  token,
}: {
  userID: string
  token: string
}): Promise<AxiosResponse> => {
  return await axios.post(
    `${Config.apiUrl}/follow/unFollow-user?userId=${userID}`,
    {},
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  )
}

export const unfollow = createAsyncThunk("unfollow", async (props: IBase) => {
  const resp = await axios.post(
    `${Config.apiUrl}/follow/unFollow-user?userId=${props.userId}`,
    {}
  )
  return resp.data
})

type FriendState = {
  followers: Array<FollowerType>
  following: Array<FollowingType>
  status: "idle" | "loading" | "succeeded" | "failed"
  followingState: "idle" | "loading" | "succeeded" | "failed"
  message: any
  action: any
}

const initialState = {
  followers: [],
  following: [],
  status: "idle",
  followingState: "idle",
  message: null,
  action: null,
} as FriendState

const friendSlice = createSlice({
  name: "friends",
  initialState,

  reducers: {},

  extraReducers: {
    [getFollowers.pending.toString()]: (state, action) => {
      state.status = "loading"
    },
    [getFollowers.fulfilled.toString()]: (state, action) => {
      state.status = "succeeded"
      state.followers = [...action.payload]
    },
    [getFollowers.rejected.toString()]: (state, action) => {
      state.status = "failed"
      state.message = action.error.message
    },

    [getFollowing.pending.toString()]: (state, action) => {
      state.status = "loading"
    },
    [getFollowing.fulfilled.toString()]: (state, action) => {
      state.status = "succeeded"
      state.following = [...action.payload]
    },
    [getFollowing.rejected.toString()]: (state, action) => {
      state.status = "failed"
      state.message = action.error.message
    },

    [follow.fulfilled.toString()]: (state, action) => {
      state.followingState = "succeeded"
      state.following.push({ user: { ...action.payload.data.user } })
      toast.success(
        `you have successfully followed ${action.payload.data.user.username}`,
        {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        }
      )
    },

    [follow.pending.toString()]: (state, action) => {
      state.followingState = "loading"
    },

    [follow.rejected.toString()]: (state, action) => {
      state.followingState = "failed"
      toast.error(action.error.message, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      })

      state.message = action.error.message
    },

    [unfollow.fulfilled.toString()]: (state, action) => {
      state.followingState = "succeeded"
      const index = state.following.findIndex(
        (e) => e.user._id == action.payload._id
      )

      const newData = state.following.filter(
        (e) => e.user._id !== action.payload.data.user._id
      )

      state.following = [...newData]

      toast.success(
        `you have successfully unfollow ${action.payload.data.user.username}`,
        {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        }
      )
    },

    [unfollow.pending.toString()]: (state, action) => {
      state.followingState = "loading"
    },

    [unfollow.rejected.toString()]: (state, action) => {
      state.status = "failed"
      toast.error(action.error.message, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      })
      state.message = action.error.message
    },
  },
})

export default friendSlice.reducer

export type { FriendState }
