import Config from "./../../config"
import { openInNewTab } from "../../utitly"
import { PostType } from "../../core/type"
import { createSlice, createAsyncThunk } from "@reduxjs/toolkit"
import axios from "axios"
import { Deemtype } from "../../core/type"

interface CommentApiData {
  postId?: string
  comment: string
}
interface ReactionActionType {
  type: string
  payload: {
    commentId?: string
    postId: string
  }
}

interface IReply {
  postId?: string
  commentId?: string
  reply: string
}

interface IBase {
  postId?: string
  commentId?: string
  replyId?: string
}

interface PostApiData {
  postData: {
    description: string
    mediaType?: string
    userID: string
    amountExpected?: number
    amountReceived?: number
    closeDate?: Date
    fundable?: boolean
  }
  imageFile: File
}

interface IgetDeemProps {
  id?: string
}

interface IDonate {
  amount: number
  postId: string
}

export const getPost = createAsyncThunk(
  "fetchADeem",
  async (props: IgetDeemProps) => {
    const resp = await axios.get(`${Config.apiUrl}/post/view/${props.id}`)

    return resp.data
  }
)

export const addComment = createAsyncThunk(
  "addComment",
  async (apiData: CommentApiData) => {
    const Apidata = {
      comment: apiData.comment,
    }
    const response: any = await axios.patch(
      `${Config.apiUrl}/post/comment?postId=${apiData.postId}`,
      { ...Apidata }
    )

    const { data, status } = response

    return response
  }
)

export const addReply = createAsyncThunk(
  "addReply",
  async (apiData: IReply) => {
    const Apidata = {
      reply: apiData.reply,
    }
    const response: any = await axios.patch(
      `${Config.apiUrl}/post/reply-com?postId=${apiData.postId}&commentId=${apiData.commentId}`,
      { ...Apidata }
    )

    const { data, status } = response

    return response
  }
)

export const deleteComment = createAsyncThunk(
  "deleteComment",
  async (apiData: IBase) => {
    const response: any = await axios.patch(
      `${Config.apiUrl}/post/delete-com?postId=${apiData.postId}&commentId=${apiData.commentId}`,
      {}
    )

    const { data, status } = response

    return apiData.commentId
  }
)

export const deleteReply = createAsyncThunk(
  "deleteRely",
  async (apiData: IBase) => {
    const response: any = await axios.patch(
      `${Config.apiUrl}/post/delete-reply?postId=${apiData.postId}&commentId=${apiData.commentId}&replyId=${apiData.replyId}`,
      {}
    )

    const { data, status } = response

    return data
  }
)

export const donate = createAsyncThunk("donate", async (apiData: IDonate) => {
  const amount = apiData.amount
  const postId = apiData.postId

  const response: any = await axios.post(
    `${Config.apiUrl}/donation/donate-web`,
    {
      amount,
      postId,
    }
  )

  const { data, status } = response

  return data.data.link
})

interface DeemStateData {
  deem: PostType | null
  status: "idle" | "loading" | "succeeded" | "failed"
  donating: "idle" | "loading" | "succeeded" | "failed"
  message: any
  action: any
}

const initialState = {
  deem: null,
  status: "idle",
  donating: "idle",
  message: null,
  action: null,
} as DeemStateData

const deemSlice = createSlice({
  name: "deem",
  initialState,

  reducers: {
    likeAndUnlikeComment(state, action: ReactionActionType) {
      const prevState = state.deem

      // const deemAddComInst = prevState.find((inst: any) => inst._id == action.payload.deemId);

      // const userLikeId = deemAddComInst.likesLog.findIndex((like: any) => like._id == action.payload.profile.userPost._id)

      // const DecreaseDeemCom = { ...deemAddComInst, likesCount: deemAddComInst ? deemAddComInst.likesCount-- : null, likesLog: [...deemAddComInst.likesLog.splice(userLikeId, 1)] };

      // state.posts = [...prevState, { ...DecreaseDeemCom }];

      // const options = {
      //     userID: action.payload.profile.userPost._id
      // }

      // axios.post(Config.REST_ENDPOINT + LikeAndRemoveAPost(action.payload.deemId), { ...options });

      axios
        .patch(
          `${Config.apiUrl}/post/like-com?postId=${action.payload.postId}&commentId=${action.payload.commentId}`,
          {}
        )
        .then(({ data }) => {
          // prevState!.counts!.likesCount = data[0].likesCount
        })
    },
  },

  extraReducers: {
    [addComment.pending.toString()]: (state, action) => {
      // state.status = "loading"
    },
    [addComment.fulfilled.toString()]: (state, action) => {
      // state.status = "succeeded"

      state.deem!.counts!.commentCount = state.deem!.counts!.commentCount + 1

      state.message = action.payload.message
      const reCom = action.payload.data.comments
      state.deem!.comments = [...reCom]
    },

    [addComment.rejected.toString()]: (state, action) => {
      // state.status = "failed"

      // console.log(action);

      state.message = action.error.message
    },

    [addReply.fulfilled.toString()]: (state, action) => {
      state.message = action.payload.message

      const commentId = action.payload.data._id

      const newArray = action.payload.data.replies.reverse()

      const index = state
        .deem!.comments!.map(function (e) {
          return e._id
        })
        .indexOf(commentId)
      state.deem!.comments![index].replies = [...newArray]
    },
    [addReply.rejected.toString()]: (state, action) => {
      state.message = action.error.message
    },

    [deleteReply.pending.toString()]: (state, action) => {
      // state.status = "loading"
    },
    [deleteReply.fulfilled.toString()]: (state, action) => {
      state.status = "succeeded"

      state.message = action.payload.message

      const commentId = action.payload._id

      const index = state
        .deem!.comments!.map(function (e) {
          return e._id
        })
        .indexOf(commentId)

      // const newComments = state.deem!.comments[index].replies.filter(e =>  e._id !==  commentId)

      state.deem!.comments![index].replies = [...action.payload.replies]
    },
    [deleteReply.rejected.toString()]: (state, action) => {
      state.status = "failed"

      state.message = action.error.message
    },

    [deleteComment.pending.toString()]: (state, action) => {
      // state.status = "loading"
    },
    [deleteComment.fulfilled.toString()]: (state, action) => {
      state.status = "succeeded"

      state.message = action.payload.message

      state.deem!.counts!.commentCount = state.deem!.counts!.commentCount - 1

      // Add any fetched posts to the array

      const p = state.deem!.comments!.findIndex(
        (item) => item._id === action.payload
      )

      state.deem!.comments?.splice(p, 1)

      // state.deem!.comments = [...action.payload.data.comments]

      // state.commentError = action
      // const deemAddComInst = prevState.find((inst: any) => inst._id == action.payload.deemId);
    },

    [deleteComment.rejected.toString()]: (state, action) => {
      state.status = "failed"

      state.message = action.error.message
    },

    [getPost.pending.toString()]: (state, action) => {
      state.status = "loading"

      // console.log(action);

      // state.message = action.error.message
    },

    [getPost.fulfilled.toString()]: (state, action) => {
      state.status = "succeeded"

      // Add any fetched posts to the array

      state.deem = action.payload

      state.message = "successful"
    },
    [getPost.rejected.toString()]: (state, action) => {
      state.status = "failed"

      // console.log(action);

      // state.message = action.
    },

    [donate.pending.toString()]: (state, action) => {
      state.donating = "loading"
    },

    [donate.fulfilled.toString()]: (state, action) => {
      state.donating = "succeeded"

      openInNewTab(action.payload)
      state.action = action.payload
    },
    [donate.rejected.toString()]: (state, action) => {
      state.donating = "failed"
    },
  },
})

export default deemSlice.reducer

export const { likeAndUnlikeComment } = deemSlice.actions
