import { createSlice } from "@reduxjs/toolkit"

const ProductSlice = createSlice({
  name: "product",
  initialState: {
    pages: 0,
    allProducts: [],
    newArrivals: [],
    topSelling: [],
    topRated: [],
    categories: [],
    singleProduct: [],
    productsByCategory: [],
    searchResult: [],
    priceResult: [],
  },
  reducers: {
    getAllProducts: (state, action) => {
      state.allProducts = action.payload
    },

    getSingleProduct: (state, action) => {
      state.singleProduct = action.payload
    },

    getNewArrivals: (state, action) => {
      state.newArrivals = action.payload
    },

    getTopSelling: (state, action) => {
      state.topSelling = action.payload
    },

    getTopRated: (state, action) => {
      state.topRated = action.payload
    },

    getCategory: (state, action) => {
      state.categories = action.payload
    },

    getProductsByCategory: (state, action) => {
      state.productsByCategory = action.payload
    },

    getSearchResult: (state, action) => {
      state.searchResult = action.payload
    },

    getPriceResult: (state, action) => {
      state.priceResult = action.payload
    },
  },
})

export const {
  getAllProducts,
  getNewArrivals,
  getTopRated,
  getTopSelling,
  getCategory,
  getSingleProduct,
  getProductsByCategory,
  getSearchResult,
  getPriceResult,
} = ProductSlice.actions

export default ProductSlice.reducer
