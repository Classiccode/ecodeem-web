import { DataReducerType, EvenReducerType } from "./reducer"

export interface ReducerType {
  event: EvenReducerType
  data: DataReducerType
}
