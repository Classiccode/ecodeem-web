import {
  ChatRoomType,
  FollowerType,
  TypingUserType,
} from "./../../../core/type"
import {
  ChatMessageType,
  ChatUserType,
  MessageTypeEnum,
} from "../../../core/type"
import { LoginReturnType } from "../../../function/auth"
import { StoreType } from "../../reducer"
import { ProductReturnType } from "../../../function"
export interface EvenReducerType {
  isPostWindowOpen: boolean
  isNavOpen: boolean
}

export interface ActionType<T = unknown> {
  type: StoreType
  value: T
}

export interface ThemeActionType<T = unknown> {
  type: string
  value: T
}

export interface CategoryReturnType {
  createdAt: Date
  description: string
  _id: string
  image: string
  title: string
  updatedAt: Date
}

export interface SectionalCategoriesReturnType {
  _id: string
  title: string
  description: string
  category: CategoryReturnType
  createdAt: Date
  updatedAt: Date
  __v: 0
}

export interface SubCategoriesReturnType {
  _id: string
  image: string
  views: number
  title: string
  description: string
  sectionalCategory: SectionalCategoriesReturnType
  createdAt: Date
  updatedAt: Date
}
export interface DataReducerType {
  loginUserDetails?: LoginReturnType
  categories: CategoryReturnType[]
  sectionalCategories: SectionalCategoriesReturnType[]
  subCategories: SubCategoriesReturnType[]
  currentProductView?: ProductReturnType
  cartCount: number
}

export interface ChatType {
  _id: string
  users: Array<ChatUserType>
  unread: number
  lastMessage: ChatMessageType
  recentMessages: Array<ChatMessageType>
  type: MessageTypeEnum
  name?: string
  avatar?: string
  description?: string
}

export interface ChatReducerType {
  active_user: number
  notification: number
  loading: boolean
  loadingChats: boolean
  loadingRecentConversation: boolean
  typingUsers: Array<TypingUserType>
  groups: Array<ChatRoomType>
  contacts: Array<FollowerType>
  chats: Array<ChatType>
  error: unknown
}

export interface ChatActionType<T = unknown> {
  type: ChatActionTypeEnum
  payload?: T
}

export enum ChatActionTypeEnum {
  CHAT_USER,
  ACTIVE_USER,
  FULL_USER,
  ADD_LOGGED_USER,
  CREATE_GROUP,
  GET_CONTACTS,
  GET_CHATS_SUCCESS,
  GET_CONTACTS_SUCCESS,
  GET_CHATS,
  GET_GROUPS,
  GET_GROUPS_SUCCESS,
  CREATE_GROUP_SUCCESS,
  SEND_MESSAGE,
  SEND_MESSAGE_SUCCESS,
  GET_NOT,
  GET_NOT_SUCCESS,
  MARK_AS_READ,
  MARK_AS_READ_SUCCESS,
  ON_NEW_SEND_MESSAGE,
  CREATE_CHAT,
  CREATE_CHAT_SUCCESS,
  TYPING_MESSAGE,
  API_FAILED,
}
