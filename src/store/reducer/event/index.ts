import { ActionType, EvenReducerType } from "../../type/reducer"
import { StoreType } from "../data"

/* eslint-disable indent */
const init: EvenReducerType = {
  isPostWindowOpen: false,
  isNavOpen: false,
}

export const eventReducer = (
  store = init,
  action: ActionType
): EvenReducerType => {
  switch (action.type) {
    case StoreType.updateLoginDetail:
      return { ...store }
    case StoreType.toggleNav:
      return { ...store, isNavOpen: !store.isNavOpen }
    case StoreType.changeNavState:
      return { ...store, isNavOpen: action.value as boolean }
    default:
      return { ...store }
  }
}
