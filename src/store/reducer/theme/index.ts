import { ThemeActionType } from "../../type/reducer"

/* eslint-disable indent */
const init = {
  isDarkMode: false,
} as {
  isDarkMode: boolean
}

export const themeReducer = (
  store = init,
  action: ThemeActionType
): { isDarkMode: boolean } => {
  switch (action.type) {
    case "TOGGLE_DARK":
      return { ...store, isDarkMode: !store.isDarkMode }
    default:
      return { ...store }
  }
}
