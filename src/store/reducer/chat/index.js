import {
  CHAT_USER,
  ACTIVE_USER,
  FULL_USER,
  ADD_LOGGED_USER,
  CREATE_GROUP,
  GET_CONTACTS,
  GET_CHATS_SUCCESS,
  GET_CONTACTS_SUCCESS,
  GET_CHATS,
  GET_GROUPS,
  GET_GROUPS_SUCCESS,
  CREATE_GROUP_SUCCESS,
  SEND_MESSAGE,
  SEND_MESSAGE_SUCCESS,
  GET_NOT,
  GET_NOT_SUCCESS,
  MARK_AS_READ,
  MARK_AS_READ_SUCCESS,
  ON_NEW_SEND_MESSAGE,
  CREATE_CHAT,
  CREATE_CHAT_SUCCESS,
  START_TYPING_MESSAGE,
  STOP_TYPING_MESSAGE,
  TYPING_MESSAGE,
  API_FAILED,
} from "./constants"
import update from "immutability-helper"

const INIT_STATE = {
  active_user: 0,
  notification: 0,
  loading: false,
  loadingChats: false,
  loadingRecentConversation: true,
  typingUsers: [],
  users: [],
  groups: [],
  contacts: [],
  chats: [],
}

export const chatReducer = (state = INIT_STATE, action) => {
  switch (action.type) {
    case CHAT_USER:
      return { ...state }

    case ACTIVE_USER:
      return {
        ...state,
        active_user: action.payload,
      }

    case FULL_USER:
      return {
        ...state,
        users: action.payload,
      }

    case ADD_LOGGED_USER:
      const newUser = action.payload
      return {
        ...state,
        users: [...state.users, newUser],
      }

    case CREATE_GROUP:
      return { ...state }

    case CREATE_GROUP_SUCCESS:
      return {
        ...state,
        groups: [...state.groups, { ...action.payload, isNew: true }],
      }

    case CREATE_CHAT:
      return { ...state }

    case CREATE_CHAT_SUCCESS:
      const index = state.chats.findIndex((item) => item._id === action.payload)

      return {
        ...state,
        active_user: index,
      }

    case SEND_MESSAGE:
      return { ...state }

    case SEND_MESSAGE_SUCCESS:
      let objIndex = state.chats.findIndex(
        (obj) => obj._id == action.payload.chatRoomId
      )

      // return update(state, {
      //     chats:{[objIndex]:{recentMessages:{$push:[action.payload]}, LastMessage:{$set: action.payload}}}
      //  })
      return state

    case ON_NEW_SEND_MESSAGE:
      let newMessageIndex = state.chats.findIndex(
        (obj) => obj._id == action.payload.chatRoomId
      )

      // return update(state, {
      //     chats:{[newMessageIndex]:{LastMessage:{$set: action.payload}, Unread:{$apply: function(x) {return x + 1;}}}}
      //  })
      return update(state, {
        chats: {
          [newMessageIndex]: {
            recentMessages: { $push: [action.payload] },
            lastMessage: { $set: action.payload },
            unread: {
              $apply: function (x) {
                return x + 1
              },
            },
          },
        },
      })

    case GET_CONTACTS:
      return { ...state, loading: true }
    case GET_CONTACTS_SUCCESS:
      return {
        ...state,
        contacts: [...state.contacts, action.payload],
        loading: false,
        error: null,
      }

    case GET_CHATS:
      return { ...state, loadingRecentConversation: true }
    case GET_CHATS_SUCCESS:
      return {
        ...state,
        chats: [...action.payload],
        error: null,
        loadingRecentConversation: false,
      }

    case GET_NOT:
      return { ...state }
    case GET_NOT_SUCCESS:
      return { ...state, notification: action.payload, error: null }

    case GET_GROUPS:
      return { ...state, loadingChats: true }
    case GET_GROUPS_SUCCESS:
      return {
        ...state,
        groups: [...action.payload],
        loadingChats: false,
        error: null,
      }

    case MARK_AS_READ:
      return { ...state }
    case MARK_AS_READ_SUCCESS:
      return { ...state, notification: state.notification - action.payload }

    case TYPING_MESSAGE:
      return { ...state, typingUsers: [...action.payload] }

    case API_FAILED:
      return { ...state, loading: false, error: action.payload }

    default:
      return { ...state }
  }
}
