/* eslint-disable indent */
import { ProductReturnType } from "../../../function"
import { LoginReturnType } from "../../../function/auth"
import type {
  ActionType,
  CategoryReturnType,
  DataReducerType,
  SectionalCategoriesReturnType,
  SubCategoriesReturnType,
} from "../../type/reducer"

interface editType {
  bio: string
  firstName: string
  lastName: string
  phoneNumber: string
}
const init: DataReducerType = {
  loginUserDetails: undefined,
  categories: [],
  sectionalCategories: [],
  subCategories: [],
  currentProductView: undefined,
  cartCount: 0,
}

export const dataReducer = (
  store = init,
  action: ActionType
): DataReducerType => {
  switch (action.type) {
    case StoreType.updateFullCategoryList:
      return {
        ...store,
        categories: (
          action.value as {
            categories: CategoryReturnType[]
            subCategories: SubCategoriesReturnType[]
            sectionalCategories: SectionalCategoriesReturnType[]
          }
        ).categories,
        sectionalCategories: (
          action.value as {
            categories: CategoryReturnType[]
            subCategories: SubCategoriesReturnType[]
            sectionalCategories: SectionalCategoriesReturnType[]
          }
        ).sectionalCategories,
        subCategories: (
          action.value as {
            categories: CategoryReturnType[]
            subCategories: SubCategoriesReturnType[]
            sectionalCategories: SectionalCategoriesReturnType[]
          }
        ).subCategories,
      }
    case StoreType.updateLoginDetail:
      return { ...store, loginUserDetails: action.value as LoginReturnType }
    case StoreType.cartCount:
      return { ...store, cartCount: action.value as number }
    case StoreType.setProductInView:
      return {
        ...store,
        currentProductView: action.value as ProductReturnType | undefined,
      }
    case StoreType.updateAfterEdit:
      return {
        ...store,
        loginUserDetails: {
          ...store.loginUserDetails!,
          user: {
            ...store.loginUserDetails!.user,
            ...(action.value as editType),
          },
        },
      }
    case StoreType.updatePic:
      return {
        ...store,
        loginUserDetails: {
          ...store.loginUserDetails!,
          user: {
            ...store.loginUserDetails!.user,
            avatar: action.value as string,
          },
        },
      }
    default:
      return { ...store }
  }
}

export enum StoreType {
  updateLoginDetail,
  updatePic,
  updateAfterEdit,
  toggleNav,
  changeNavState,
  updateFullCategoryList,
  setProductInView,
  cartCount,
}
