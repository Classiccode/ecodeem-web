import { render } from "react-dom"
import Ecodeem from "./App"
import { BrowserRouter } from "react-router-dom"
import reportWebVitals from "./reportWebVitals"
import { Provider } from "react-redux"
import { persistor, store } from "./store"
import { QueryClient, QueryClientProvider } from "react-query"
import { PersistGate } from "redux-persist/es/integration/react"
import * as serviceWorker from "./serviceWorker"

// import {} from "react-query"
// import { subscribeUser } from "./subscription"

const queryClient = new QueryClient()
render(
  <Provider store={store}>
    <BrowserRouter>
      <QueryClientProvider client={queryClient}>
        <PersistGate loading={null} persistor={persistor}>
          <Ecodeem />
        </PersistGate>
      </QueryClientProvider>
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
)
reportWebVitals()
serviceWorker.register()

// subscribeUser()
