import axios, { AxiosError } from "axios"
import Config from "../../config"
import { BaseReturnType, DonationType } from "../../core/type"
import { AllDonorsReturnDataType, DonateReturnDataType } from "./type"

export async function donateFuncHandler(
  amount: number,
  postId: string
): Promise<BaseReturnType<DonateReturnDataType>> {
  try {
    const response = await axios.post(`${Config.apiUrl}/donation/donate-web`, {
      amount,
      postId,
    })

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function verifyDonateFuncHandler(
  transaction_id: string
): Promise<BaseReturnType<string>> {
  try {
    const response = await axios.post(
      `${Config.apiUrl}/donation/verify-web-donation?transaction_id=${transaction_id}`
    )

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function getDonorsForAPostFuncHandler(
  postId: string
): Promise<BaseReturnType<Array<DonationType>>> {
  try {
    const response = await axios.get(
      `${Config.apiUrl}/donation/donors-post?postId=${postId}`
    )

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function getAllDonorsFuncHandler(
  pageNumber = 1
): Promise<BaseReturnType<Array<AllDonorsReturnDataType>>> {
  try {
    const response = await axios.get(
      `${Config.apiUrl}/donation/donors?pageNumber=${pageNumber}`
    )

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}
