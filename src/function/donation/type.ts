export interface DonateReturnDataType {
  status: string
  message: string
  data: {
    link: string
  }
}

interface DonationTypeWithPost {
  _id: string
  donationPost: {
    _id: string
    postDesc: string
  }
  amountDonated: number
  dateDonated: string
}

export interface AllDonorsReturnDataType {
  page: number
  pages: number
  donorsCount: number
  allDonors: Array<{
    _id: string
    userId: {
      avatar: string
      _id: string
      firstName: string
      lastName: string
      username: string
    }
    donations: Array<DonationTypeWithPost>
    createdAt: string
  }>
}
