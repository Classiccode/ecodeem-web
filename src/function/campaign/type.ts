import { PostType } from "../../core/type"

export type AllCampaignsReturnType = {
  page: number
  pages: number
  campCount: number
  allCamps: Array<PostType>
}

export type MyCampaignsReturnType = {
  page: number
  pages: number
  campCount: number
  allCamps: Array<PostType>
}
