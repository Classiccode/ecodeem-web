import axios, { AxiosError, AxiosResponse } from "axios"
import Config from "../../config"
import { BaseReturnType } from "../../core/type"
import { AllCampaignsReturnType, MyCampaignsReturnType } from "./type"

export async function getAllCampaignsFuncHandler(
  interestId?: "",
  pageNumber = 1
): Promise<AxiosResponse<AllCampaignsReturnType>> {
  try {
    const response = await axios.get(
      `${Config.apiUrl}/post/all-camps?pageNumber=${pageNumber}`
    )
    return response
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function getMyCampaignsFuncHandler(
  pageNumber = 1
): Promise<BaseReturnType<MyCampaignsReturnType>> {
  try {
    const response = await axios.get(
      `${Config.apiUrl}/post/my-camps?pageNumber=${pageNumber}`
    )

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}
