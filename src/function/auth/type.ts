export interface AuthParamType {
  email?: string
  password?: string
  username?: string
  firstName?: string
  lastName?: string
  phoneNumber?: string
  code?: string
  token?: string
  interests?: Array<string>
  avatar?: File
  canLoginWithBio?: boolean
  canPush?: boolean
  rememberLogin?: boolean
  lowData?: boolean
  backgroundMode?: boolean
  bio?: string
}

export interface UserType {
  accountConfirm: boolean
  appID: string
  avatar: string
  confirmationCode: string
  country: string
  createdAt: string
  date: string
  email: string
  firstName: string
  ipAddress: string
  isActive: boolean
  isVerified: boolean
  lastName: string
  phoneNumber: string
  referralCode: string
  state: string
  updatedAt: string
  username: string
  bio: string
  __v: number
  _id: string
  interests: []
  settings: {
    canLoginWithBio: boolean
    canPush: boolean
    rememberLogin: boolean
    lowData: boolean
    backgroundMode: boolean
  }
  reward?: {
    level: number
    title: string
  }
}

export type LoginReturnType = {
  token: string
  user: UserType
}

export type VerifyReturnType = {
  token: string
  verifyUser: UserType
}
