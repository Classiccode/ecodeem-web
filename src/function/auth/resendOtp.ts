import axios, { AxiosError } from "axios"
import Config from "../../config"

export async function resendOtp({ email }: { email: string }): Promise<void> {
  try {
    axios
      .post(Config.apiUrl, {
        email,
      })
      .catch((error: AxiosError) => {
        throw error.response?.data.message ?? "Network Error."
      })
  } catch (e) {
    throw "Network Error."
  }
}
