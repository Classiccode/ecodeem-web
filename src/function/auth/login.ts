import { LoginReturnType } from "./type"
import { BaseReturnType } from "../../core/type"
import axios, { AxiosError } from "axios"
import { AuthParamType } from "./"
import Config from "../../config"
import { setAuthorization } from "../../config/axios"

/***
 *
 * This is a function to login
 *
 * @param email This is a string of the user's mail
 * @param password This is a string of the user password
 * @returns A promise of BaseReturnType<LoginReturnType>
 * @example await loginFuncHandler({ email: string, password: string })
 *
 */

export async function loginFuncHandler({
  email,
  password,
}: AuthParamType): Promise<BaseReturnType<LoginReturnType>> {
  if (!email || !password) {
    throw new Error("Password and email are required")
  }
  try {
    const response = await axios.post(`${Config.apiUrl}/user/signin`, {
      email,
      password,
    })
    setAuthorization(undefined, response.data.token)
    localStorage.setItem("userID", response.data.user._id) //whatever you do, do not touch this line, the marketplace might break if you do
    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message ?? (error as string)
    )
  }
}
