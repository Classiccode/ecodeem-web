import Config from "./../../config"
import { VerifyReturnType } from "./type"
import { BaseReturnType } from "../../core/type"
import axios, { AxiosError, AxiosResponse } from "axios"
import { AuthParamType } from "./"
import { setAuthorization } from "../../config/axios"

export async function registrationFuncHandler({
  email,
  password,
  username,
  firstName,
  lastName,
  interests,
  phoneNumber,
}: AuthParamType): Promise<BaseReturnType<null>> {
  if (
    !email ||
    !password ||
    !username ||
    !firstName ||
    !lastName ||
    !interests ||
    !phoneNumber
  ) {
    throw new Error("Fill all required parameter.")
  }
  if (interests.length <= 0) {
    throw new Error("Fill all required parameter.")
  }
  try {
    const resp = await axios.post(`${Config.apiUrl}/user/signup`, {
      email,
      password,
      username,
      firstName,
      lastName,
      interests,
      phoneNumber,
    })

    return resp.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function sendVerificationCodeFuncHandler({
  email,
}: AuthParamType): Promise<BaseReturnType<null>> {
  if (!email) {
    throw new Error("email is required")
  }
  try {
    const resp = await axios.post(`${Config.apiUrl}/user/resend-otp`, { email })

    return resp.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function verifyEmailFuncHandler({
  email,
  code,
}: AuthParamType): Promise<AxiosResponse<VerifyReturnType>> {
  if (!email || !code) {
    throw new Error("email and code are required")
  }
  try {
    const resp = await axios.post(`${Config.apiUrl}/user/verify`, {
      email,
      confirmCode: code,
    })

    setAuthorization(undefined, resp.data.token)

    return resp
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}
