import axios, { AxiosError } from "axios"
import Config from "../../config"
import { BaseReturnType } from "../../core/type"
import { AuthParamType } from "./"

export async function sendPasswordResetMailFuncHandler({
  email,
}: AuthParamType): Promise<BaseReturnType<null>> {
  try {
    if (!email) {
      throw new Error("email is required")
    }
    const resp = await axios.post(`${Config.apiUrl}/user/recover_password`, {
      email,
    })

    return resp.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function reSendPasswordResetMailFuncHandler({
  email,
}: AuthParamType): Promise<BaseReturnType<null>> {
  if (!email) {
    throw new Error("email is required")
  }
  try {
    const resp = await axios.post(
      `${Config.apiUrl}/user/re-send-restPassword`,
      {
        email,
      }
    )

    return resp.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function resetPasswordFuncHandler({
  password,
  token,
}: AuthParamType): Promise<BaseReturnType<null>> {
  if (!token || !password) {
    throw new Error("token and password are required")
  }
  try {
    const resp = await axios.post(
      `${Config.apiUrl}/user/reset_password/${token}`,
      {
        password,
      }
    )

    return resp.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}
