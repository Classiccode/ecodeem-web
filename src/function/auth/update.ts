import { UserType } from "./type"
import { BaseReturnType, InterestType, SettingsType } from "../../core/type"
import axios, { AxiosError, AxiosResponse } from "axios"
import { AuthParamType } from "."
import Config from "../../config"

export async function profilePicFuncHandler(
  avatar: File | undefined
): Promise<AxiosResponse<UserType>> {
  if (!avatar) {
    throw new Error("avatar is required")
  }

  const formData = new FormData()
  formData.append("avatar", avatar)
  let resp: any
  try {
    formData.append("file", avatar)
    formData.append("upload_preset", "preset1")

    resp = await axios.post(
      "https://api.cloudinary.com/v1_1/dsxddxoeg/image/upload",
      formData,
      {
        transformRequest: [
          (data, headers) => {
            delete headers.common.Authorization
            return data
          },
        ],
        headers: {
          "Content-Type": "multipart/form-data",
        },
      }
    )

    const response = await axios.patch(`${Config.apiUrl}/user/uploadPics`, {
      avatar: resp.secure_url,
    })

    return response
  } catch (error: unknown) {
    console.error(error)
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function profileUpdateFuncHandler({
  username,
  firstName,
  lastName,
  phoneNumber,
  bio,
}: AuthParamType): Promise<AxiosResponse<UserType>> {
  try {
    const response = await axios.put(`${Config.apiUrl}/user/update`, {
      username,
      firstName,
      lastName,
      phoneNumber,
      bio,
    })

    return response
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function passwordUpdateFuncHandler({
  password,
}: AuthParamType): Promise<AxiosResponse<null>> {
  try {
    const response = await axios.patch(`${Config.apiUrl}/user/updatePassword`, {
      password,
    })

    return response
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function settingsUpdateFuncHandler({
  canLoginWithBio,
  canPush,
  rememberLogin,
  lowData,
  backgroundMode,
}: AuthParamType): Promise<BaseReturnType<SettingsType>> {
  try {
    const response = await axios.patch(`${Config.apiUrl}/user/settings`, {
      canLoginWithBio,
      canPush,
      rememberLogin,
      lowData,
      backgroundMode,
    })

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function interestsUpdateFuncHandler({
  interests,
}: AuthParamType): Promise<AxiosResponse<Array<InterestType>>> {
  try {
    const response = await axios.patch(
      `${Config.apiUrl}/user/update-my-interests`,
      {
        interests,
      }
    )

    return response
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}
