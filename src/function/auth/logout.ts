import { BaseReturnType } from "../../core/type"
import axios, { AxiosError } from "axios"
import Config from "../../config"

export async function logoutCurrentDeviceFuncHandler(): Promise<
  BaseReturnType<null>
  // eslint-disable-next-line indent
> {
  let url
  try {
    const response = await axios.post(`${url}/user/logout`)
    localStorage.clear()
    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function logoutAllDeviceFuncHandler(): Promise<
  BaseReturnType<null>
  // eslint-disable-next-line indent
> {
  try {
    const response = await axios.post(`${Config.apiUrl}/user/logout_all`)
    localStorage.clear()
    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}
