import { AxiosError } from "axios"
import { toast } from "react-toastify"
import Config from "../../config"

export async function CancelOrder({
  userID,
  orderID,
}: {
  userID: string
  orderID: string
}): Promise<string> {
  return Config.apiMarketPlace
    .patch(
      `/order/cancel/${orderID}`,
      {},
      {
        params: {
          userID,
        },
      }
    )
    .then((res) => {
      toast.success(res.data.message)
      return res.data.message as string
    })
    .catch((error) => {
      throw (
        (
          error as AxiosError<{
            status: string
            message: string
          }>
        ).response?.data.message ?? "Network Error"
      )
    })
}
