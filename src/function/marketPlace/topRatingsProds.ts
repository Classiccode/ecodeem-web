/* eslint-disable indent */
import { ProductReturnType } from "."
import Config from "../../config"

export async function TopRatingProducts(): Promise<ProductReturnType[]> {
  return Config.apiMarketPlace
    .get("/product/top")
    .then((res) => {
      return res.data.data.products as ProductReturnType[]
    })
    .catch((error) => {
      throw error
    })
}
