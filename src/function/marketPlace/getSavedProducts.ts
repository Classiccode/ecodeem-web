import { CartType } from "."
import Config from "../../config"

export async function increaseCartItem({
  userID,
}: {
  userID: string
}): Promise<CartType[]> {
  return Config.apiMarketPlace
    .post("/customer/mySavedProd", {
      params: {
        userID,
      },
    })
    .then((res) => {
      return res.data.data.cart as CartType[]
    })
    .catch((error) => {
      throw error
    })
}
