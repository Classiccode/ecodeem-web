import { AxiosError } from "axios"
import { CartType } from "."
import Config from "../../config"

export async function decrementCartItem({
  productID,
  userID,
}: {
  productID: string
  userID: string
}): Promise<CartType[]> {
  return Config.apiMarketPlace
    .patch(
      "/customer/decrementFromCart",
      {},
      {
        params: {
          productId: productID,
          userID: userID,
        },
      }
    )
    .then((res) => {
      return res.data.data.cart as CartType[]
    })
    .catch((error) => {
      throw (
        (
          error as AxiosError<{
            status: string
            message: string
          }>
        ).response?.data.message ?? "Network Error"
      )
    })
}
export async function removeFromCartItem({
  productID,
  userID,
}: {
  productID: string
  userID: string
}): Promise<CartType[]> {
  return Config.apiMarketPlace
    .patch(
      "/customer/removeFromCart",
      {},
      {
        params: {
          productId: productID,
          userID: userID,
        },
      }
    )
    .then((res) => {
      return res.data.data.cart as CartType[]
    })
    .catch((error) => {
      throw (
        (
          error as AxiosError<{
            status: string
            message: string
          }>
        ).response?.data.message ?? "Network Error"
      )
    })
}
