import { AxiosError, AxiosResponse } from "axios"
import Config from "../../config"

export async function payNow({
  userID,
  orderId,
}: {
  userID: string
  orderId: string
}): Promise<string> {
  return Config.apiMarketPlace
    .post(
      "/payment/payWeb",
      {},
      {
        params: {
          userID,
          orderId,
        },
      }
    )
    .then((res: AxiosResponse<{ data: PayNowResponse }>) => {
      return res.data.data.data.link
    })
    .catch((error) => {
      throw (
        (
          error as AxiosError<{
            status: string
            message: string
          }>
        ).response?.data.message ?? "Network Error"
      )
    })
}

export interface PayNowResponse {
  status: string
  message: string
  data: {
    link: string
  }
}
