import type { AxiosError } from "axios"
import { toast } from "react-toastify"
import Config from "../../config"

export async function trackMyOrder({
  orderID,
}: {
  orderID: string
}): Promise<string> {
  return Config.apiMarketPlace
    .get("/order/track", { params: { orderID }, data: { orderID } })
    .then((res) => {
      toast.dark(res.data.data.title)
      return res.data.data.title as string
    })
    .catch((error) => {
      throw (
        (
          error as AxiosError<{
            status: string
            message: string
          }>
        ).response?.data.message ?? "Network Error"
      )
    })
}
