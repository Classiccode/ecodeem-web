import { AxiosError, AxiosResponse } from "axios"
import Config from "../../config"

export async function reviewProduct({
  productId,
  rating,
  userID,
  comment,
}: {
  productId: string
  rating: number
  comment: string
  userID: string
}): Promise<{
  productReviews: {
    _id: string
    name: string
    rating: number
    comment: string
    customer: string
    reviewedOn: string
  }[]
  productRating: number
}> {
  try {
    const { data } = await Config.apiMarketPlace.post(
      "/product/reviews",
      {
        productId,
        rating,
        comment,
      },
      {
        params: {
          userID: userID,
        },
      }
    )
    return {
      productReviews: data.data.productReviews,
      productRating: data.data.productRating,
    }
  } catch (error: unknown) {
    throw (
      (
        error as AxiosError<{
          status: string
          message: string
        }>
      ).response?.data.message ?? "Network Error"
    )
  }
}
