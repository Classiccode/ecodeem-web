import Config from "../../config"

export async function getShippingTypes(): Promise<ShippingType[]> {
  return Config.apiMarketPlace
    .get("/shipping/customerAll")
    .then((res) => {
      return res.data.data as ShippingType[]
    })
    .catch((error) => {
      throw error
    })
}

export interface ShippingType {
  _id: string
  title: string
  location: string
  price: number
  timeline: string
  createdAt: string
  updatedAt: string
  __v: 0
}
