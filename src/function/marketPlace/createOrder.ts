import { AxiosError, AxiosResponse } from "axios"
import { CartType } from "."
import Config from "../../config"

export async function createShippingOrder({
  userID,
  shippingAddress,
  shippingType,
}: {
  userID: string
  shippingType: string
  shippingAddress: {
    address: string
    city: string
    state: string
    postalCode: number
    country: string
  }
}): Promise<CreateOrderResponse> {
  return Config.apiMarketPlace
    .post(
      "/order/checkout",
      {
        shippingType,
        shippingAddress,
      },
      {
        params: {
          userID,
        },
      }
    )
    .then((res: AxiosResponse<CreateOrderResponse>) => {
      return res.data
    })
    .catch((error) => {
      throw (
        (
          error as AxiosError<{
            status: string
            message: string
          }>
        ).response?.data.message ?? "Network Error"
      )
    })
}

export interface CreateOrderResponse {
  data: {
    shippingAddress: {
      postalCode: string
      address: string
      city: string
      country: string
      state: string
    }
    orderDetails: {
      itemPrice: number
      shippingPrice: number
      totalPrice: number
    }
    status: string
    isPaid: boolean
    _id: string
    shippingType: string
    orderItems: [
      {
        _id: string
        quantity: number
        name: string
        price: number
        image: string
        productId: string
        seller: null
        total: number
      }
    ]
    orderID: string
    customer: string
    dateOrdered: string
    createdAt: string
    updatedAt: string
    __v: 0
  }
  status: string
}
