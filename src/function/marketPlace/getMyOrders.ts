import { CartType } from "."
import Config from "../../config"

export async function getMyOrder({
  userID,
}: {
  userID: string
}): Promise<MyOrdersResponse[]> {
  return Config.apiMarketPlace
    .get("/order/myOrders", {
      params: {
        userID,
      },
    })
    .then((res) => {
      return res.data.data.orders as MyOrdersResponse[]
    })
    .catch((error) => {
      throw error
    })
}

export interface MyOrdersResponse {
  shippingAddress: {
    postalCode: string
    address: string
    city: string
    country: string
    state: string
  }
  orderDetails: {
    itemPrice: number
    shippingPrice: number
    totalPrice: number
  }
  status: string
  isPaid: true
  _id: string
  shippingType: string
  orderItems: [
    {
      _id: string
      quantity: number
      name: string
      price: number
      image: string
      productId: string
      seller: null
      total: number
    }
  ]
  orderID: string
  customer: string
  dateOrdered: string
  createdAt: string
  updatedAt: string
  __v: number
}
