export interface ProductReturnType {
  discount: {
    active: boolean
    amount: number
  }
  description: string
  variations: string[]
  seller: null
  status: string
  available: boolean
  views: number
  viewers: string[]
  disabled: boolean
  brand: string
  rating: number
  numReviews: number
  numSold: number
  _id: string
  name: string
  price: number
  subCategory: {
    image: string
    views: number
    _id: string
    title: string
    description: string
    sectionalCategory: string
    createdAt: string
    updatedAt: string
    __v: string
  }
  image: string
  addedBy: string
  dateAdded: string
  reviews: any[]
  createdAt: string
  updatedAt: string
  __v: 1
}

export interface CartType {
  quantity: number
  _id: string
  name: string
  price: number
  image: number
  productId: string
  seller: null
  date: string
  total: number
}
