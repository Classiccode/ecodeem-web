import { CartType } from "."
import Config from "../../config"

export async function removeCartItem({
  productID,
  userID,
}: {
  productID: string
  userID: string
}): Promise<CartType[]> {
  return Config.apiMarketPlace
    .patch(
      "/customer/removeFromCart",
      {},
      {
        params: {
          productId: productID,
          userID: userID,
        },
      }
    )
    .then((res) => {
      return res.data.data.cart as CartType[]
    })
    .catch((error) => {
      throw error
    })
}
