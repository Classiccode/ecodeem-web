/* eslint-disable indent */
import { ProductReturnType } from "."
import Config from "../../config"

export async function MonthlyDeal(): Promise<ProductReturnType[]> {
  return Config.apiMarketPlace
    .get("/product/m-deal")
    .then((res) => {
      return res.data.data.products as ProductReturnType[]
    })
    .catch((error) => {
      throw error
    })
}
