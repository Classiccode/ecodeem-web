/* eslint-disable indent */
import { ProductReturnType } from "."
import Config from "../../config"

export async function WeeksDeal(): Promise<ProductReturnType[]> {
  return Config.apiMarketPlace
    .get("/product/w-deal")
    .then((res) => {
      return res.data.data.products as ProductReturnType[]
    })
    .catch((error) => {
      throw error
    })
}
