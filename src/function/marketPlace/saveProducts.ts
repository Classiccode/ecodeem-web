import { CartType } from "."
import Config from "../../config"

export async function increaseCartItem({
  productID,
  userID,
}: {
  productID: string
  userID: string
}): Promise<CartType[]> {
  return Config.apiMarketPlace
    .post(
      "/customer/saveProduct",
      {
        productId: productID,
      },
      {
        params: {
          userID,
        },
      }
    )
    .then((res) => {
      return res.data.data.cart as CartType[]
    })
    .catch((error) => {
      throw error
    })
}
