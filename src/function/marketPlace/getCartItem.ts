import { CartType } from "."
import Config from "../../config"

export async function getCartItems({
  userID,
}: {
  userID: string
}): Promise<CartType[]> {
  return Config.apiMarketPlace
    .get("/customer/myCart", {
      params: {
        userID: userID,
      },
    })
    .then((res) => {
      return res.data.data.myCart as CartType[]
    })
    .catch((error) => {
      throw error
    })
}
