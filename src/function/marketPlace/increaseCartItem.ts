import Config from "../../config"
import { CartType } from "."

export async function addCartItem({
  productID,
  userID,
}: {
  productID: string
  userID: string
}): Promise<CartType[]> {
  return Config.apiMarketPlace
    .post(`/customer/addTocart?userID=${userID}`, {
      productId: productID,
    })
    .then((res) => {
      return res.data.data.cart as CartType[]
    })
    .catch((error) => {
      throw error
    })
}
