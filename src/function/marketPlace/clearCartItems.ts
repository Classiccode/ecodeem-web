import { CartType } from "."
import Config from "../../config"

export async function clearCartItem({
  userID,
}: {
  userID: string
}): Promise<boolean> {
  return Config.apiMarketPlace
    .patch(
      "/customer/clearCart",
      {},
      {
        params: {
          userID: userID,
        },
      }
    )
    .then(() => {
      return true
    })
    .catch((error) => {
      throw error
    })
}
