/* eslint-disable indent */
import { ProductReturnType } from "."
import Config from "../../config"

export async function NewArrival(): Promise<ProductReturnType[]> {
  return Config.apiMarketPlace
    .get("/product/newArrival")
    .then((res) => {
      return res.data.data.newProducts as ProductReturnType[]
    })
    .catch((error) => {
      throw error
    })
}
