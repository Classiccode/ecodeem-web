/* eslint-disable indent */
import { AxiosResponse } from "axios"
import { ProductReturnType } from "."
import Config from "../../config"

export async function TopSellingProducts(): Promise<ProductReturnType[]> {
  return Config.apiMarketPlace
    .get("/product/topSelling")
    .then((res) => {
      return res.data.data.topSellingProd as ProductReturnType[]
    })
    .catch((error) => {
      throw error
    })
}
