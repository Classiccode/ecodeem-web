import { ProductReturnType } from "."
import Config from "../../config"

export function GetSingleProduct({
  id,
  userID,
}: {
  id: string
  userID: string
}): Promise<ProductReturnType[]> {
  return Config.apiMarketPlace
    .get(`/product/prod/${id}?userID=${userID}`)
    .then((res) => {
      return res.data.data as ProductReturnType[]
    })
    .catch((error) => {
      throw error
    })
}
