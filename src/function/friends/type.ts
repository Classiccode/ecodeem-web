import { FollowerType } from "./../../core/type"
export interface FollowReurnType {
  followers: Array<FollowerType>
  _id: string
  user: FollowerType
  createdAt: string
  updatedAt: string
}
