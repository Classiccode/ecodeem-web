import axios, { AxiosError } from "axios"
import { FollowerType } from "./../../core/type"
import { BaseReturnType } from "../../core/type"
import Config from "../../config"

export async function getFollowersFuncHandler(
  limit?: number,
  skip?: number
): Promise<BaseReturnType<Array<FollowerType>>> {
  try {
    const response = await axios.get(
      `${Config.apiUrl}/follow/followers?limit=${limit}&skip=${skip}`
    )

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}
