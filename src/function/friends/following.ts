import { FollowingType } from "./../../core/type"
import axios, { AxiosError } from "axios"
import { BaseReturnType } from "../../core/type"
import { FollowReurnType } from "./type"
import Config from "../../config"

export async function followUserFuncHandler(
  userId: string
): Promise<BaseReturnType<FollowReurnType>> {
  try {
    const response = await axios.post(
      `${Config.apiUrl}/follow/follow-user?userId=${userId}`
    )

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function unFollowUserFuncHandler(
  userId: string
): Promise<BaseReturnType<FollowReurnType>> {
  try {
    const response = await axios.post(
      `${Config.apiUrl}/follow/unFollow-user?userId=${userId}`
    )

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function getFollowingsFuncHandler(
  limit?: number,
  skip?: number
): Promise<BaseReturnType<Array<FollowingType>>> {
  try {
    const response = await axios.get(
      `${Config.apiUrl}/follow/following?limit=${limit}&skip=${skip}`
    )

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}
