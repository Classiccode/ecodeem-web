import Config from "./../../config"
import { NotificationType } from "./../../core/type"
import axios, { AxiosError, AxiosResponse } from "axios"
import { BaseReturnType } from "../../core/type"

export async function getNotificationsFuncHandler(
  skip?: string,
  limit = 20
): Promise<AxiosResponse<NotificationType[]>> {
  try {
    const response = await axios.get(
      `${Config.apiUrl}/notification/my-notifications?limit=${limit}&skip=${skip}`
    )

    return response
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function readNotificationFuncHandler(
  notificatioonId: string
): Promise<AxiosResponse<NotificationType>> {
  try {
    const response = await axios.patch(
      `${Config.apiUrl}/notification/read-notification/${notificatioonId}`
    )

    return response
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function getChatCountNotificationFuncHandler(
  userId: string
): Promise<{ success: boolean; unread: number }> {
  try {
    const response = await axios.get(
      `${Config.chatApiUrl}/users/${userId}/unread`
    )

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function deleteNotificationFuncHandler(
  notificatioonId: string
): Promise<BaseReturnType<Array<null>>> {
  try {
    const response = await axios.delete(
      `${Config.apiUrl}/notification/delete-notification/${notificatioonId}`
    )

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}
