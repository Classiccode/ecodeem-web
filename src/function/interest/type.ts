import { InterestType, PostType } from "./../../core/type"
export interface InterestsReturnType {
  interestCount: number
  interests: Array<InterestType>
}

export interface InterestReturnType {
  interests: InterestType
  interestPosts: Array<PostType>
}
