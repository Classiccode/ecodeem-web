import axios, { AxiosError, AxiosResponse } from "axios"
import { find } from "lodash"
import Config from "../../config"
import { BaseReturnType, InterestType } from "../../core/type"
import { InterestReturnType, InterestsReturnType } from "./type"

export async function getInterestsFuncHandler(): Promise<
  AxiosResponse<InterestsReturnType>
  // eslint-disable-next-line indent
> {
  try {
    const response = await axios.get(`${Config.apiUrl}/interest/user-all`)
    return response
  } catch (error: unknown) {
    console.error(error)
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}
export async function getInterestFuncHandler(
  interestId: string
): Promise<BaseReturnType<InterestReturnType>> {
  try {
    const response = await axios.get(
      `${Config.apiUrl}/interest/single/${interestId}`
    )

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export function getInterestsId(interests: InterestType[]): string[] {
  const value: string[] = []
  for (const interest of interests) {
    value.push(interest._id)
  }
  return value
}

export function getInterestsById(
  interests: InterestType[],
  interestIDs: string[]
): InterestType[] {
  const value: InterestType[] = []
  for (const id of interestIDs) {
    const _v = find(interests, { _id: id })
    if (_v !== undefined) {
      value.push(_v)
    }
  }
  return value
}

export function getInterestById(
  interests: InterestType[],
  interestID: string
): InterestType {
  return (
    find(interests, { _id: interestID }) ?? {
      _id: "string",
      interest: "string",
      image: "string",
      createdAt: "string",
      updatedAt: "string",
    }
  )
}
