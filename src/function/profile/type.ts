import { InterestType, PostType } from "./../../core/type"
import { RewardType, SettingsType } from "../../core/type"

export interface ProfileParamType {
  email?: string
  phone?: string
  username?: string
  interest?: string
  name?: string
  page?: number
  order?: "asc" | "desc"
}

export interface ProfileReturnType {
  _id: string
  reward: RewardType
  ipAddress?: string
  firstName: string
  lastName: string
  username: string
  email: string
  avatar: string
  bio: string
  phoneNumber: string
  interest: Array<InterestType>
  country: string
  state: string
  isActive: boolean
  isVerified: boolean
  accountConfirm: boolean
  appID: string
  referralCode: string
  date: string
  following: number
  followers: number
  settings?: SettingsType
  posts?: Array<PostType>
  postCount?: number
}

export type UserSearchUserObjectType = {
  _id: string
  name: string
  email: string
  username: string
  interest: Array<InterestType>
  phoneNumber: string
  avatar: string
}

export type UserSearchReturnType = {
  page: number
  pages: number
  userCount: number
  users: Array<UserSearchUserObjectType>
}
