import { BaseReturnType } from "./../../core/type"
import axios, { AxiosError, AxiosResponse } from "axios"
import {
  ProfileParamType,
  ProfileReturnType,
  UserSearchReturnType,
} from "./type"
import Config from "../../config"

export async function getMyProfile(): Promise<
  AxiosResponse<ProfileReturnType>
  // eslint-disable-next-line indent
> {
  try {
    const response = await axios.get(`${Config.apiUrl}/user/profile/me`)
    return response
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function searchUserProfile({
  name,
  phone,
  email,
  username,
  interest,
  page,
  order,
}: ProfileParamType): Promise<AxiosResponse<UserSearchReturnType>> {
  try {
    // const response = await axios.get(
    //   `${Config.apiUrl}/user/search-users?${email && "searchByEmail=" + email}&${
    //     phone && "searchByPhoneNumber=" + phone
    //   }&${username && "searchByUsername=" + username}&${
    //     interest && "searchByInterest=" + interest
    //   }&${page && "pageNumber=" + page}&${order && "order=" + order}&${
    //     name && "searchByName=" + name
    //   }`
    // )

    const response = await axios.get(
      `${Config.apiUrl}/user/search-users?searchByName=${name}`
    )

    return response
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function getUserProfile(
  id: string
): Promise<AxiosResponse<ProfileReturnType>> {
  try {
    const response = await axios.get(`${Config.apiUrl}/user/profile/${id}`)
    return response
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}
