import axios from "axios"
import { apiUrl } from "../../config/api"

/**
 * This is a function to save the users settings
 * Note: This function make an api call to do so.
 * @type ({token, settings})=>Promise.<void>
 * @property {string} token this is the users JWT token to authenticate the user at the backend
 * @property {string} settings this is the users settings to be saved on the system.
 * @return {Promise<void>}
 */

export async function SaveSettings({
  token,
  settings,
}: {
  token: string
  settings: {
    canLoginWithBio: boolean
    canPush: boolean
    rememberLogin: boolean
    lowData: boolean
    backgroundMode: boolean
  }
}): Promise<void> {
  console.log(token, settings)
  try {
    await axios({
      method: "PATCH",
      url: `${apiUrl}/user/settings`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
      data: {
        ...settings,
      },
    })
  } catch (error) {
    throw new Error("Network Error")
  }
}
