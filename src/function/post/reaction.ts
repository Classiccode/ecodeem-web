import axios, { AxiosError } from "axios"
import Config from "../../config"
import { BaseReturnType } from "../../core/type"
import { SharePostReturnType } from "./type"

export async function sharePostFuncHandler(
  postId: string
): Promise<BaseReturnType<SharePostReturnType>> {
  try {
    const response = await axios.patch(
      `${Config.apiUrl}/post/share?postId==${postId}`
    )

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function likePostFuncHandler(
  postId: string
): Promise<BaseReturnType<null>> {
  try {
    const response = await axios.patch(
      `${Config.apiUrl}/post/like?postId==${postId}`
    )

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function unlikePostFuncHandler(
  postId: string
): Promise<BaseReturnType<null>> {
  try {
    const response = await axios.patch(
      `${Config.apiUrl}/post/like?postId==${postId}`
    )

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}
