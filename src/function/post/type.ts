import {
  CommentType,
  PostType,
  ReactionCountType,
  ReplyType,
  ShareType,
} from "./../../core/type"
export type PostParamType = {
  postId: string
  postType: "video" | "image" | "text" | "raiseFund"
  postDesc: string
  interest: string
  file?: {
    blob: string
    type: "video" | "image"
  }
  minAmount?: number
  maxAmount?: number
  endDate?: string
}

export type AllPostReturnType = {
  page: number
  pages: number
  postCount: number
  myPosts: Array<PostType>
}

export type GeneralPostReturnType = {
  page: number
  pages: number
  postCount: number
  allPosts: Array<PostType>
}

export type SharedPostReturnType = {
  page: number
  pages: number
  sharedPostCount: number
  sharedPosts: Array<PostType>
}

export type SharePostReturnType = {
  shares: Array<ShareType>
  counts: ReactionCountType
}

export type GetTimelinePostReturnType = {
  timelinePostCount: number
  timelinePosts: Array<PostType>
}

export type CommentReturnType = {
  newComment: {
    userId: string
    comment: string
    avatar: string
    fullName: string
    dateCommented: string
  }
  postId: string
  postDesc: string
  postedBy: string
  comments: Array<CommentType>
  counts: ReactionCountType
}

export type DeleteCommentReturnType = {
  postDesc: string
  postedBy: string
  comments: Array<CommentType>
  counts: ReactionCountType
}

export type ReplyToCommentReturnType = {
  replies: Array<ReplyType>
}
