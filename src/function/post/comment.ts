import { CommentType } from "./../../core/type"
import axios, { AxiosError } from "axios"
import Config from "../../config"
import { BaseReturnType } from "../../core/type"
import { CommentReturnType, DeleteCommentReturnType } from "./type"

export async function commentFuncHandler(
  postId: string,
  comment: string
): Promise<BaseReturnType<CommentReturnType>> {
  try {
    const response = await axios.patch(
      `${Config.apiUrl}/post/comment?postId=${postId}`,
      { comment }
    )

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function likeCommentFuncHandler(
  postId: string,
  commentId: string
): Promise<BaseReturnType<Array<CommentType>>> {
  try {
    const response = await axios.patch(
      `${Config.apiUrl}/post/like-com?postId=${postId}&commentId=${commentId}`
    )

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function dislikeCommentFuncHandler(
  postId: string,
  commentId: string
): Promise<BaseReturnType<Array<CommentType>>> {
  try {
    const response = await axios.patch(
      `${Config.apiUrl}/post/like-com?postId=${postId}&commentId=${commentId}`
    )

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function deleteCommentFuncHandler(
  postId: string,
  commentId: string
): Promise<BaseReturnType<DeleteCommentReturnType>> {
  try {
    const response = await axios.patch(
      `${Config.apiUrl}/post/delete-com?postId=${postId}&commentId=${commentId}`
    )

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}
