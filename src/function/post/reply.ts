import { BaseReturnType } from "./../../core/type"
import axios, { AxiosError } from "axios"
import Config from "../../config"
import { ReplyToCommentReturnType } from "./type"

export async function replyFuncHandler(
  postId: string,
  commentId: string,
  reply: string
): Promise<BaseReturnType<ReplyToCommentReturnType>> {
  try {
    const response = await axios.patch(
      `${Config.apiUrl}/post/reply-com?postId=${postId}&commentId=${commentId}`,
      { reply }
    )

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function deleteReplyFuncHandler(
  postId: string,
  commentId: string,
  replyId: string
): Promise<BaseReturnType<ReplyToCommentReturnType>> {
  try {
    const response = await axios.patch(
      `${Config.apiUrl}/post/reply-com?postId=${postId}&commentId=${commentId}&replyId=${replyId}`
    )

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}
