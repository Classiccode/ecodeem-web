import axios, { AxiosError } from "axios"
import Config from "../../config"
import { BaseReturnType, PostType } from "../../core/type"
import {
  AllPostReturnType,
  GetTimelinePostReturnType,
  PostParamType,
  SharedPostReturnType,
  GeneralPostReturnType,
} from "./type"

export async function createPostFuncHandler({
  postType,
  postDesc,
  interest,
  file,
  maxAmount,
  minAmount,
  endDate,
}: PostParamType): Promise<
  BaseReturnType<PostType>
  // eslint-disable-next-line indent
> {
  let data
  let resp:
    | {
        secure_url: string
      }
    | undefined

  if (file) {
    try {
      const formData = new FormData()
      formData.append("file", file.blob)
      formData.append("upload_preset", "preset1")

      resp = await axios.post(
        `https://api.cloudinary.com/v1_1/dsxddxoeg/${file.type}/upload`,
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      )
    } catch (error: unknown) {
      throw new Error(
        (
          error as AxiosError<{
            message: string
          }>
        ).response?.data.message
      )
    }
  }

  if (postType == "text") {
    data = {
      postDesc,
      interest,
      postType: "text",
    }
  }

  if (postType == "image") {
    data = {
      postDesc,
      interest,
      postType: "image",
      mediaUrl: resp?.secure_url,
    }
  }

  if (postType == "video") {
    data = {
      postDesc,
      interest,
      postType: "video",
      mediaUrl: resp?.secure_url,
    }
  }

  if (postType == "raiseFund") {
    data = {
      postDesc,
      interest,
      postType: "raiseFund",
      ...(file && { mediaUrl: resp?.secure_url }),
      fundRaise: {
        minAmount,
        maxAmount,
        endDate,
      },
    }
  }

  try {
    const response = await axios.post(`${Config.apiUrl}/post/create`, data)

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function editPostFuncHandler({
  postId,
  postDesc,
  interest,
}: PostParamType): Promise<BaseReturnType<PostType>> {
  try {
    const response = await axios.put(`${Config.apiUrl}/post/edit/${postId}`, {
      postDesc,
      interest,
    })

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function deletePostFuncHandler(
  postId: string
): Promise<BaseReturnType<PostType>> {
  try {
    const response = await axios.delete(`${Config.apiUrl}/post/delete${postId}`)

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function getAllMyPostFuncHandler(
  pageNumber = 1
): Promise<BaseReturnType<AllPostReturnType>> {
  try {
    const response = await axios.get(
      `${Config.apiUrl}/post/my-posts?pageNumber=${pageNumber}`
    )

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function getGeneralPostFuncHandler(
  pageNumber = 1,
  interest?: string,
  postType?: string
): Promise<BaseReturnType<GeneralPostReturnType>> {
  try {
    const response = await axios.get(
      `${Config.apiUrl}/post/all-posts?pageNumber=${pageNumber}&filterByInterest=${interest}&filterByPostType=${postType}`
    )

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function getMyTimelinePostsFuncHandler(): Promise<
  BaseReturnType<GetTimelinePostReturnType>
  // eslint-disable-next-line indent
> {
  try {
    const response = await axios.get(`${Config.apiUrl}/post/timeline`)

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function getPostFuncHandler(
  postId: string
): Promise<BaseReturnType<PostType>> {
  try {
    const response = await axios.get(`${Config.apiUrl}/post/view/${postId}`)

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}

export async function getSharedPostFuncHandler(
  pageNumber = 1
): Promise<BaseReturnType<SharedPostReturnType>> {
  try {
    const response = await axios.get(
      `${Config.apiUrl}/post/shared?pageNumber=${pageNumber}`
    )

    return response.data
  } catch (error: unknown) {
    throw new Error(
      (
        error as AxiosError<{
          message: string
        }>
      ).response?.data.message
    )
  }
}
