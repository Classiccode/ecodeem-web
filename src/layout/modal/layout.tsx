import { IconButton } from "@material-ui/core"
import { AnimatePresence } from "framer-motion"
import CloseIcon from "remixicon-react/CloseLineIcon"

export function ModalLayout({
  children,
  title,
}: {
  title: string
  children: JSX.Element
}): JSX.Element {
  return (
    <AnimatePresence>
      <section className="Modal">
        <div className="container">
          <IconButton className="close_btn">
            <CloseIcon />
          </IconButton>
          <h3 className="title">{title}</h3>
          {children}
        </div>
      </section>
    </AnimatePresence>
  )
}
