import React, { useRef, useState, useEffect } from "react"
import { Input, InputGroup } from "reactstrap"
import { Link } from "react-router-dom"
import { connect } from "react-redux"
import Moment from "react-moment"
import moment from "moment"

//simplebar
import SimpleBar from "simplebar-react"

//actions
import {
  setconversationNameInOpenChat,
  activeUser,
  getChats,
} from "../../../redux/actions"

//components
import OnlineUsers from "./OnlineUsers"
// import { getChatter } from '../../../components/utilities';
import { getChatter } from "../../../../../features/Chat/helpers/common"

function Chats(props) {
  const [searchChat, setSearchChat] = useState("")
  const [recentChatList, setRecentChatList] = useState(props.chats)

  useEffect(() => {
    var li = document.getElementById("conversation" + props.active_user)
    if (li) {
      li.classList.add("active")
    }
    props.getChats(props.user._id)
  }, [])

  useEffect(() => {
    setRecentChatList(props.recentChatList)
  }, [props.recentChatList])

  const scrolltoBottom = () => {
    scrollRef.current?.scrollIntoView({ behavior: "smooth" })
  }

  const handleChange = (e) => {
    setSearchChat(e.target.value)
    var search = e.target.value
    let conversation = recentChatList
    let filteredArray = []

    //find conversation name from array
    for (let i = 0; i < conversation.length; i++) {
      if (
        conversation[i].name.toLowerCase().includes(search) ||
        conversation[i].name.toUpperCase().includes(search)
      )
        filteredArray.push(conversation[i])
    }

    //set filtered items to state
    setRecentChatList(filteredArray)

    //if input value is blanck then assign whole recent chatlist to array
    if (search === "") setRecentChatList(props.recentChatList)
  }

  const openUserChat = (e, chat) => {
    e.preventDefault()

    //find index of current chat in array
    var index = props.chats.indexOf(chat)

    // set activeUser
    props.activeUser(index)

    var chatList = document.getElementById("chat-list")
    var clickedItem = e.target
    var currentli = null

    if (chatList) {
      var li = chatList.getElementsByTagName("li")
      //remove coversation user
      for (var i = 0; i < li.length; ++i) {
        if (li[i].classList.contains("active")) {
          li[i].classList.remove("active")
        }
      }
      //find clicked coversation user
      for (var k = 0; k < li.length; ++k) {
        if (li[k].contains(clickedItem)) {
          currentli = li[k]
          break
        }
      }
    }

    //activation of clicked coversation user
    if (currentli) {
      currentli.classList.add("active")
    }

    var userChat = document.getElementsByClassName("user-chat")

    if (userChat) {
      userChat[0].classList.add("user-chat-show")
    }

    //removes unread badge if user clicks
    var unread = document.getElementById("unRead" + chat._id)
    if (unread) {
      unread.style.display = "none"
    }
  }

  return (
    <React.Fragment>
      <div>
        <div className="px-4 pt-4">
          <h4 className="mb-4">Chats</h4>
          <div className="search-box chat-search-box">
            <InputGroup size="lg" className="mb-3 rounded-lg">
              <span
                className="input-group-text text-muted bg-light pe-1 ps-3"
                id="basic-addon1"
              >
                <i className="ri-search-line search-icon font-size-18"></i>
              </span>
              <Input
                type="text"
                value={searchChat}
                onChange={(e) => handleChange(e)}
                className="form-control bg-light"
                placeholder="Search messages or users"
              />
            </InputGroup>
          </div>
          {/* Search Box */}
        </div>

        {/* online users */}
        {/* <OnlineUsers /> */}

        {/* Start chat-message-list  */}
        <div className="px-2">
          <h5 className="mb-3 px-3 font-size-16">Recent</h5>
          <SimpleBar
            style={{ maxHeight: "100%" }}
            className="chat-message-list"
          >
            <ul
              className="list-unstyled chat-list chat-user-list"
              id="chat-list"
            >
              {props.loadingRecentConversation ? (
                "loading"
              ) : props.chats.length < 1 ? (
                <p className="text-center">
                  you do not have any recent conversation
                </p>
              ) : (
                props.chats.map((chat, key) => (
                  <li
                    key={key}
                    id={"conversation" + key}
                    className={
                      chat.unRead
                        ? "unread"
                        : props.typingUsers.filter(function (e) {
                            return e.conversationId === chat._id
                          }).length > 0
                        ? "typing"
                        : key === props.active_user
                        ? "active"
                        : ""
                    }
                  >
                    <Link to="#" onClick={(e) => openUserChat(e, chat)}>
                      <div className="d-flex">
                        {chat.type == "private" ? (
                          !chat.users.avatar ? (
                            <div
                              className={
                                "chat-user-img " +
                                chat.status +
                                " align-self-center me-3 ms-0"
                              }
                            >
                              <div className="avatar-xs">
                                <span className="avatar-title rounded-circle bg-soft-primary text-primary">
                                  {getChatter(
                                    chat.users,
                                    props.user
                                  ).firstName.charAt(0)}
                                </span>
                              </div>
                              {/* {
                                                                    chat.status && <span className="user-status"></span>
                                                                } */}
                            </div>
                          ) : (
                            <div
                              className={
                                "chat-user-img " +
                                chat.status +
                                " align-self-center me-3 ms-0"
                              }
                            >
                              <img
                                src={getChatter(chat.users, props.user).avatar}
                                className="rounded-circle avatar-xs"
                                alt="chatvia"
                              />
                              {/* {
                                                                    chat.status && <span className="user-status"></span>
                                                                } */}
                            </div>
                          )
                        ) : !chat.avatar ? (
                          <div
                            className={
                              "chat-user-img " +
                              chat.status +
                              " align-self-center me-3 ms-0"
                            }
                          >
                            <div className="avatar-xs">
                              <span className="avatar-title rounded-circle bg-soft-primary text-primary">
                                {chat.name.charAt(0)}
                              </span>
                            </div>
                            {/* {
                                                                    chat.status && <span className="user-status"></span>
                                                                } */}
                          </div>
                        ) : (
                          <div
                            className={
                              "chat-user-img " +
                              chat.status +
                              " align-self-center me-3 ms-0"
                            }
                          >
                            <img
                              src={chat.avatar}
                              className="rounded-circle avatar-xs"
                              alt="chatvia"
                            />
                            {/* {
                                                                    chat.status && <span className="user-status"></span>
                                                                } */}
                          </div>
                        )}

                        <div className="flex-1 overflow-hidden">
                          {chat.type == "private" ? (
                            <h5 className="text-truncate font-size-15 mb-1">{`${
                              getChatter(chat.users, props.user).firstName
                            } ${
                              getChatter(chat.users, props.user).lastName
                            }`}</h5>
                          ) : (
                            <h5 className="text-truncate font-size-15 mb-1">
                              {chat.name}
                            </h5>
                          )}

                          <p className="chat-user-message text-truncate mb-0">
                            {props.typingUsers.filter(function (e) {
                              return e.conversationId === chat._id
                            }).length > 0 ? (
                              <>
                                typing
                                <span className="animate-typing">
                                  <span className="dot ms-1"></span>
                                  <span className="dot ms-1"></span>
                                  <span className="dot ms-1"></span>
                                </span>
                              </>
                            ) : (
                              <>
                                {chat.LastMessage &&
                                chat.LastMessage.type === "image" ? (
                                  <i className="ri-image-fill align-middle me-1"></i>
                                ) : null}
                                {chat.LastMessage &&
                                chat.LastMessage.type === "file" ? (
                                  <i className="ri-file-text-fill align-middle me-1"></i>
                                ) : null}
                                {chat.LastMessage &&
                                chat.LastMessage.type === "video" ? (
                                  <i className="ri-video-chat-fill align-middle me-1"></i>
                                ) : null}
                                {chat.LastMessage
                                  ? chat.LastMessage.message
                                  : null}
                              </>
                            )}
                          </p>
                        </div>
                        <div className="font-size-11">
                          {chat.LastMessage
                            ? moment(chat.LastMessage.createdAt).fromNow()
                            : null}
                        </div>
                        {chat.Unread < 1 ? null : (
                          <div
                            className="unread-message"
                            id={"unRead" + chat._id}
                          >
                            {/* <span className="badge badge-soft-danger rounded-pill">{chat.messages && chat.messages.length > 0 ? chat.unRead >= 20 ? chat.unRead + "+" : chat.unRead : ""}</span> */}
                            <span className="badge badge-soft-danger rounded-pill">
                              {chat.Unread}
                            </span>
                          </div>
                        )}
                      </div>
                    </Link>
                  </li>
                ))
              )}
            </ul>
          </SimpleBar>
        </div>
        {/* End chat-message-list */}
      </div>
    </React.Fragment>
  )
}

const mapStateToProps = (state) => {
  const { chats, active_user, loadingRecentConversation, typingUsers } =
    state.Chat
  const { user } = state.account
  return { active_user, chats, loadingRecentConversation, user, typingUsers }
}

export default connect(mapStateToProps, {
  setconversationNameInOpenChat,
  activeUser,
  getChats,
})(Chats)
