import { Button } from "@material-ui/core"
import { ChevronForwardOutline, PersonSharp } from "react-ionicons"
import { useHistory } from "react-router-dom"
import Logo from "../../assets/brand/eco-logo-2.png"
import { motion } from "framer-motion"

export function AuthPageLayout({
  children,
}: {
  children: JSX.Element
}): JSX.Element {
  const { push } = useHistory()
  return (
    <>
      <motion.section
        className="AuthLayout"
        initial={{ opacity: 0, scale: 1 }}
        exit={{ opacity: 0, scale: 1.2 }}
        animate={{ opacity: 1, scale: 1 }}
        transition={{ duration: 0.4, type: "spring" }}
      >
        <div className="side contentDesc">
          <div className="container">
            <span className="image-desc"> </span>
            <p>
              get started with your account today as you buy premium products
              with fast delivery
            </p>
          </div>
        </div>
        <div className="side contentForm">
          <Button
            className="btn_float"
            onClick={() => {
              push("/market")
            }}
          >
            SKIP TO SHOP NOW <ChevronForwardOutline />
          </Button>
          <div className="mobile-logo d-none text-center d-flex align-items-center">
            <img src={Logo} alt="logo" />
          </div>
          <div className="avatar">
            <PersonSharp />
          </div>
          {children}
        </div>
      </motion.section>
    </>
  )
}
