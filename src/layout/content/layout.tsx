import { memo } from "react"
import { useSelector } from "react-redux"
import { ContentHeader, LeftSidePanel, RightSidePanel } from "../../components"
import Post from "../../components/Post/post"
import { RootState } from "../../store"
import { motion } from "framer-motion"
import { useLocation } from "react-router"

function ContentLayoutComp({
  children,
}: {
  children: JSX.Element
}): JSX.Element {
  const { pathname } = useLocation()
  const isChat = pathname.includes("/chat")
  const postModalState = useSelector(
    (state: RootState) => state.blog.postModalState
  )
  return (
    <section className="ContentLayout">
      {postModalState && <Post />}
      <ContentHeader />
      {!isChat && <LeftSidePanel isAuthView={true} />}
      {isChat ? (
        <motion.div
          className="chat-middle-content"
          initial={{ opacity: 0, scale: 1 }}
          exit={{ opacity: 0, scale: 1.2 }}
          animate={{ opacity: 1, scale: 1 }}
          transition={{ duration: 0.4, type: "spring" }}
        >
          {children}
        </motion.div>
      ) : (
        <motion.div
          className="middle-content"
          initial={{ opacity: 0, scale: 1 }}
          exit={{ opacity: 0, scale: 1.2 }}
          animate={{ opacity: 1, scale: 1 }}
          transition={{ duration: 0.4, type: "spring" }}
        >
          {children}
        </motion.div>
      )}
      <RightSidePanel />
    </section>
  )
}

export const ContentLayout = memo(ContentLayoutComp)
