import { UnAuthLeft, UnAuthHeader } from "../../components"
import { motion } from "framer-motion"

export function UnAuthLayout({
  children,
}: {
  children: JSX.Element
}): JSX.Element {
  return (
    <section className="ContentLayout unAuth">
      <UnAuthHeader />
      <UnAuthLeft />
      <motion.div
        className=""
        initial={{ opacity: 0, scale: 1 }}
        exit={{ opacity: 0, scale: 1.2 }}
        animate={{ opacity: 1, scale: 1 }}
        transition={{ duration: 0.4, type: "spring" }}
      >
        {children}
      </motion.div>
    </section>
  )
}
