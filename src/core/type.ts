export interface BaseReturnType<Type> {
  data?: Type
  message?: string
  status: "success" | "error"
}

export interface RewardType {
  level: number
  title: string
}

export type Deemtype = {
  id: number
  avatar: string
  userName: string
  interest: string
  mediaUrl: string
  likes: number
  comments: number
  description: string
  timePosted: string
}

export interface SettingsType {
  canLoginWithBio: boolean
  canPush: boolean
  rememberLogin: boolean
  lowData: boolean
  backgroundMode: boolean
}

export type InterestType = {
  _id: string
  interest: string
  image: string
  createdAt?: string
  updatedAt?: string
}
export interface ReplyType {
  _id: string
  userId: string
  reply: string
  avatar: string
  fullName: string
  dateReplied: string
}

export type CommentType = {
  avatar: string
  comment: string
  dateCommented: string
  fullName: string
  likes: Array<string>
  likesCount: number
  replies: Array<ReplyType>
  repliesCount: number
  userId: string
  _id: string
}
export interface PostType {
  _id: string
  disabled: boolean
  postedBy: {
    avatar: string
    firstName: string
    lastName: string
    username: string
    _id: string
  }
  postDesc: string
  interest: {
    _id: string
    interest: string
  }
  postType: string
  mediaUrl?: string
  fundRaise?: {
    minAmount: number
    maxAmount: number
    endDate: string
    startDate: string
    daysLeft: number
    totalReceived: number
    donationPercentage: number
    fundRaiseExpired: boolean
    fundRaiseCompleted: boolean
  }
  counts?: {
    likesCount: number
    commentCount: number
    shareCount: number
    donorsCount: number
    viewsCount: number
  }
  likes?: Array<string>
  shares?: Array<string>
  views?: Array<string>
  donors?: Array<string>
  comments?: Array<CommentType>
}

export type FollowerType = {
  _id: string
  avatar: string
  firstName: string
  lastName: string
  username: string
}

export type FollowingType = {
  user: {
    _id: string
    avatar: string
    firstName: string
    lastName: string
    username: string
  }
}

export interface ShareType {
  _id: string
  sharedBy: string
  dateShared: string
}

export interface ReactionCountType {
  likesCount: number
  commentCount: number
  shareCount: number
  donorsCount: number
  viewsCount: number
}

export interface NotificationType {
  isRead: boolean
  _id: string
  sender: {
    avatar: string
    firstName: string
    lastName: string
    _id: string
  }
  receiver: {
    avatar: string
    firstName: string
    lastName: string
    _id: string
  }
  content: string
  action: string
  type: string
  createdAt: string
  updatedAt: string
}

export interface DonationType {
  _id: string
  userID: string
  amountDonated: number
  dateDonated: string
  avatar: string
  username: string
}

export interface ChatUserType {
  username: string
  firstName: string
  lastName: string
  avatar?: string
  ecodeemId: string
}

export interface ChatMessageType {
  chatRoomId: string
  message?: string
  link?: string
  type: MessageTypeEnum
  postedBy: ChatUserType
}

export interface ChatRoomType {
  isNew?: boolean
  userIds: Array<string>
  chatType: string
  chatInitiator: string
  name?: string
  avatar?: string
  description?: string
}

export interface TypingUserType {
  conversationId: string
  name: string
  id: string
}

export enum MessageTypeEnum {
  text,
  image,
  video,
  document,
}

export interface AuthUser {
  token: string
  user: {
    accountConfirm: boolean
    appID: string
    avatar: string
    confirmationCode: string | null
    country: string
    createdAt: string
    date: string
    email: string
    firstName: string
    ipAddress: string
    isActive: boolean
    isVerified: boolean
    lastName: string
    phoneNumber: string
    referralCode: string
    state: string
    updatedAt: string
    username: string
    __v: number
    _id: string
    reward: {
      level: number
      title: string
    }
    following: number
    followers: number
    posts?: PostType[]
  }
}

export type ProductType = {
  description: string
  seller: SellerType
  images: string
  available: boolean
  views: number
  disabled: boolean
  brand: string
  rating: number
  numReviews: number
  numSold: number
  _id: string
  name: string
  price: string | number
  category: CategoryType
  image: string
  addedBy: string
  dateAdded: string
  createdAt: string
  updatedAt: string
  __v: string
  reviews: ReviewType[]
}

export type CategoryType = {
  _id: string
  title: string
  description: string
  dateAdded: string
  createdAt: string
  updatedAt: string
  __v: string | number
  image: string
}

export type ProductInitialState = {
  pages: number
  allProducts: ProductType[]
  categories: CategoryType[] | undefined
  topRated: ProductType[] | undefined
  topSelling: ProductType[] | undefined
  newArrivals: ProductType[] | undefined
}

export type ProductItemProps = {
  link?: string
  productName: string
  price: number | string
  category: string
  productID: string
  img: string
  onCartClick?: any
  rating: number
  isAuth?: boolean
}

export type SellerType = {
  avatar: string
  _id: string
  firstname: string
  lastname: string
  sellerID: string
  email: string
}

export type ReviewType = {
  _id: string
  name: string
  rating: number
  comment: string
  customer: string
  reviewOn: string
}
