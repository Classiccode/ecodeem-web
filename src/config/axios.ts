import axios from "axios"

export const setAuthorization = (
  push?: (path: string, state?: unknown) => void,
  token?: string
): void => {
  if (token) {
    localStorage.setItem("token", token)
  }
  const localToken = localStorage.getItem("token")
  if (!token && !localToken) {
    push ? push("/auth/login") : window.location.assign("/auth/login")
  }
  axios.defaults.headers.common["Authorization"] =
    "Bearer " + localToken ?? token
}
