import axios, { AxiosInstance } from "axios"

const Config = {
  get apiUrl(): string {
    return "https://ecodeem-auth-dev.herokuapp.com/api/v1"
  },
  get token(): string {
    return localStorage.getItem("token") ?? ""
  },
  get apiMicroBlog(): AxiosInstance {
    return axios.create({
      baseURL: Config.apiUrl,
      headers: {
        Authorization: `Bearer ${Config.token}`,
      },
    })
  },
  get apiMarketPlace(): AxiosInstance {
    return axios.create({
      baseURL: Config.marketplaceApiUrl,
      headers: {
        Authorization: `Bearer ${Config.token}`,
      },
    })
  },
  get nairaSign(): string {
    return "₦"
  },
  get isDev(): boolean {
    return process.env.NODE_ENV === "development"
  },
  get marketplaceApiUrl(): string {
    return "https://eco-market-place.herokuapp.com/api/v1"
  },
  get baseURL(): string {
    return !Config.isDev ? "https://www.ecodeem.com" : "http://localhost:3000"
  },
  get chatApiUrl(): string {
    return "https://echodeem-chat.herokuapp.com"
  },
}

export default Config
