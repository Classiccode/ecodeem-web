export const SellerRegister = (): string => {
  return "/seller/register"
}

export const SellerLogin = (): string => {
  return "/seller/login"
}

export const GetSellerProfile = (): string => {
  return "/seller/myProfile"
}

export const GetAllProducts = (): string => {
  return "/product/shop"
}

export const GetSingleProduct = (ProductID: string): string => {
  return `/product/prod/${ProductID}`
}

export const GetTopRatedProducts = (): string => {
  return "/product/top"
}

export const GetTopSellingProducts = (): string => {
  return "/product/topSelling"
}

export const GetNewArrivalProducts = (): string => {
  return "/product/newArrival"
}

export const ReviewProduct = (UserID: string): string => {
  return `/product/reviews?userID=${UserID}`
}

export const GetCategories = (): string => {
  return "/category/customerAll"
}

export const GetProductsByCategories = (CategoryID: string): string => {
  return `/product/shop?filterByCategory=${CategoryID}`
}

export const AddProductToCart = (UserID: string): string => {
  return `/customer/addTocart?userID=${UserID}`
}

export const GetProductsInCart = (UserID: string): string => {
  return `/customer/myCart?userID=${UserID}`
}

export const RemoveProductFromCart = (
  ProductID: string,
  UserID: string
): string => {
  return `/customer/removeFromCart?productId=${ProductID}&userID=${UserID}`
}

export const DecrementFromCart = (
  ProductID: string,
  UserID: string
): string => {
  return `/customer/decrementFromCart?productId=${ProductID}&userID=${UserID}`
}

export const ClearCart = (userID: string): string => {
  return `/customer/clearCart/?userID=${userID}`
}

export const CreateOrder = (userID: string): string => {
  return `/order/checkout?userID=${userID}`
}

export const CancelOrder = (orderID: string, userID: string): string => {
  return `/order/cancel/${orderID}?userID=${userID}`
}

export const TrackOrder = (): string => {
  return "/order/track"
}

export const GetOrder = (userID: string): string => {
  return `/order/myOrders?userID=${userID}`
}

export const MakePayment = (userID: string, orderID: string): string => {
  return `/payment/payWeb?userID=${userID}&orderId=${orderID}`
}

export const VerifyPayment = (
  userID: string,
  transactionID: string
): string => {
  return `/payment/verifyWeb?userID=${userID}&transaction_id=${transactionID}`
}

export const GetShippingType = (): string => {
  return "/shipping/customerAll"
}

export const ProductFilters = (
  sortField?: string,
  order?: string,
  pageNumber?: string,
  productName?: string,
  brand?: string,
  category?: string,
  startPrice?: string,
  endPrice?: string
): string => {
  return `/product/shop?sortBy=${sortField}&order=${order}&pageNumber=${pageNumber}&filterByProductName=${productName}&filterByBrand=${brand}&filterByCategory=${category}&startPrice=${startPrice}&endPrice=${endPrice}`
}

export const PriceFilters = (
  category: string,
  startPrice?: string | number,
  endPrice?: string | number
): string => {
  return `/product/shop?filterByCategory=${category}&startPrice=${startPrice}&endPrice=${endPrice}`
}
export const SearchProduct = (productName: string): string => {
  return `/product/shop?filterByProductName=${productName}`
}

export const GetSavedProducts = (userID: string): string => {
  return `/customer/mySavedProd?userID=${userID}`
}

export const RemoveSaveProduct = (
  productID: string,
  userID: string
): string => {
  return `/customer/removeFromSaved?productId=${productID}&userID=${userID}`
}

export const SaveProduct = (userID: string): string => {
  return `/customer/saveProduct?userID=${userID}`
}

export const ClearSavedProducts = (userID: string): string => {
  return `/customer/clearSavedProd/?userID=${userID}`
}
