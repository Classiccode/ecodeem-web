export const isDev = process.env.NODE_ENV === "development"

export const apiUrl = !isDev
  ? "https://ecodeem-auth-dev.herokuapp.com/api/v1"
  : "https://ecodeem-auth-dev.herokuapp.com/api/v1" // "https://api.ecodeem.com"
export const chatApiUrl = !isDev
  ? "http://localhost:4000"
  : "https://echodeem-chat.herokuapp.com"
export const baseURL = !isDev
  ? "https://www.ecodeem.com"
  : "http://localhost:3000"
export const marketplaceApiUrl = !isDev
  ? "https://eco-market-place.herokuapp.com/api/v1"
  : "https://eco-market-place.herokuapp.com/api/v1"
