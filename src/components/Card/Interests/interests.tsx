import React, { Fragment, useState, FC, MouseEvent, useEffect } from "react"
import ScrollContainer from "react-indiana-drag-scroll"
import Plus from "@material-ui/icons/AddOutlined"
import { truncateWords } from "../../../utitly"
import Card from "../card"
import "./interests.scss"
import { ApiloadStateMain } from "../../../components/loaders"
import Config from "../../../config"

type Interestprops = {
  type: string
  onClick: (event: MouseEvent<HTMLDivElement>, value: string) => void
}

interface Iinterest {
  _id: string
  interest: string
  image: string
}

const Interests = ({ type, onClick }: Interestprops): JSX.Element => {
  const [interests, setInterests] = useState<Array<Iinterest>>([])
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState({ status: false, message: "" })

  function getInterests() {
    setLoading(true)
    const requestOptions = {
      method: "GET",
    }
    fetch(`${Config.apiUrl}/interest/user-all`, requestOptions)
      .then((response) => response.json())
      .then(({ data: { interests } }) => {
        setInterests([...interests])
        setLoading(false)
        setError({ status: false, message: "" })
      })
      .catch((error) => {
        setError({ status: true, message: error })
        setLoading(false)
      })
  }

  useEffect(() => {
    getInterests()
  }, [setInterests, setLoading])

  return (
    <Fragment>
      <div>
        <div className="d-flex flex-row align-items-center">
          <span className="red-dot mr-5"></span>
          <span>Interest</span>
        </div>
        <ScrollContainer className="Interests">
          <div className="Interests__Interest">
            <div
              onClick={(e) => onClick(e, "timeline")}
              className="Interests__Interest__Active Interests__Interest__Add"
            >
              <Plus fontSize="large" />
            </div>
            <h6>My Timeline</h6>
          </div>
          {loading ? (
            <div className="Interests__Interest">
              <div>
                <ApiloadStateMain />
              </div>
              <h6>loading...</h6>
            </div>
          ) : error.status ? (
            <p>
              error loading interests{" "}
              <span
                style={{ cursor: "pointer", color: "#8CC64B" }}
                onClick={() => getInterests()}
              >
                refresh
              </span>{" "}
            </p>
          ) : (
            interests.map((interest, idx) => (
              <div key={`interest-list-${idx}`} className="Interests__Interest">
                <div
                  onClick={(e) => onClick(e, interest._id)}
                  className={
                    type == interest._id
                      ? "Interests__Interest__Active"
                      : "Interests__Interest__Inactive"
                  }
                >
                  <img src={interest.image} />
                </div>
                <h6>{truncateWords(interest.interest, 12)}</h6>
              </div>
            ))
          )}
        </ScrollContainer>
      </div>
    </Fragment>
  )
}

export default Interests
