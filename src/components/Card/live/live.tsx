import React, { Fragment, useState, FC, MouseEvent } from "react"
import ScrollContainer from "react-indiana-drag-scroll"
import Plus from "@material-ui/icons/AddOutlined"
import { truncateWords } from "../../../utitly"
import Card from "../../Card/card"
import "../../Card/Interests/interests.scss"

interface IFeed {
  imageUrl: string
  alt: string
  text: string
}

const Live: FC = () => {
  const feed: Array<IFeed> = [
    {
      imageUrl:
        "https://avatars.dicebear.com/v2/avataaars/0b1158377e302f048d34e2e360b947e9.svg",
      alt: "lekan kodunmi",
      text: "lekan kodunmi",
    },
    {
      imageUrl:
        "https://avatars.dicebear.com/v2/avataaars/0b1158377e302f048d34e2e360b947e9.svg",
      alt: "lekan kodunmi",
      text: "lekan kodunmi",
    },
    {
      imageUrl:
        "https://avatars.dicebear.com/v2/avataaars/0b1158377e302f048d34e2e360b947e9.svg",
      alt: "lekan kodunmi",
      text: "lekan kodunmi",
    },
    {
      imageUrl:
        "https://avatars.dicebear.com/v2/avataaars/0b1158377e302f048d34e2e360b947e9.svg",
      alt: "lekan kodunmi",
      text: "lekan kodunmi",
    },
    {
      imageUrl:
        "https://avatars.dicebear.com/v2/avataaars/0b1158377e302f048d34e2e360b947e9.svg",
      alt: "lekan kodunmi",
      text: "lekan kodunmi",
    },
    {
      imageUrl:
        "https://avatars.dicebear.com/v2/avataaars/0b1158377e302f048d34e2e360b947e9.svg",
      alt: "lekan kodunmi",
      text: "lekan kodunmi",
    },
    {
      imageUrl:
        "https://avatars.dicebear.com/v2/avataaars/0b1158377e302f048d34e2e360b947e9.svg",
      alt: "lekan kodunmi",
      text: "lekan kodunmi",
    },
    {
      imageUrl:
        "https://avatars.dicebear.com/v2/avataaars/0b1158377e302f048d34e2e360b947e9.svg",
      alt: "lekan kodunmi",
      text: "lekan kodunmi",
    },
    {
      imageUrl:
        "https://avatars.dicebear.com/v2/avataaars/0b1158377e302f048d34e2e360b947e9.svg",
      alt: "lekan kodunmi",
      text: "lekan kodunmi",
    },
    {
      imageUrl:
        "https://avatars.dicebear.com/v2/avataaars/0b1158377e302f048d34e2e360b947e9.svg",
      alt: "lekan kodunmi",
      text: "lekan kodunmi",
    },
    {
      imageUrl:
        "https://avatars.dicebear.com/v2/avataaars/0b1158377e302f048d34e2e360b947e9.svg",
      alt: "lekan kodunmi",
      text: "lekan kodunmi",
    },
    {
      imageUrl:
        "https://avatars.dicebear.com/v2/avataaars/0b1158377e302f048d34e2e360b947e9.svg",
      alt: "lekan kodunmi",
      text: "lekan kodunmi",
    },
    {
      imageUrl:
        "https://avatars.dicebear.com/v2/avataaars/0b1158377e302f048d34e2e360b947e9.svg",
      alt: "lekan kodunmi",
      text: "lekan kodunmi",
    },
    {
      imageUrl:
        "https://avatars.dicebear.com/v2/avataaars/0b1158377e302f048d34e2e360b947e9.svg",
      alt: "lekan kodunmi",
      text: "lekan kodunmi",
    },
    {
      imageUrl:
        "https://avatars.dicebear.com/v2/avataaars/0b1158377e302f048d34e2e360b947e9.svg",
      alt: "lekan kodunmi",
      text: "lekan kodunmi",
    },
    {
      imageUrl:
        "https://avatars.dicebear.com/v2/avataaars/0b1158377e302f048d34e2e360b947e9.svg",
      alt: "lekan kodunmi",
      text: "lekan kodunmi",
    },
  ]

  return (
    <Fragment>
      <div>
        <div className="d-flex flex-row align-items-center">
          <span className="red-dot mr-5"></span>
          <span>Live Feed</span>
        </div>
        <ScrollContainer className="Interests">
          <div className="Interests__Interest">
            <div className="Interests__Interest__Active Interests__Interest__Add">
              <Plus fontSize="large" />
            </div>
            <h6>Go Live</h6>
          </div>
          {feed.map((interest, idx) => (
            <div key={`live-list${idx}`} className="Interests__Interest">
              <div className="Interests__Interest__Inactive">
                <img src={`${interest.imageUrl}`} />
              </div>
              <h6>{truncateWords(interest.text, 12)}</h6>
            </div>
          ))}
        </ScrollContainer>
      </div>
    </Fragment>
  )
}

export default Live
