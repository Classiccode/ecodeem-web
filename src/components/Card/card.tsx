import React, { Fragment } from "react"
import "./card.scss"

interface CardProps {
  rounded?: boolean
  className?: string
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  children?: any
}

const Card = ({ children, rounded, className }: CardProps): JSX.Element => {
  return (
    <Fragment>
      <div className={`Card ${rounded && "rounded-4"} ${className}`}>
        {children}
      </div>
    </Fragment>
  )
}

export default Card
