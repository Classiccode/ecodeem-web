import React, { FC, useState, Fragment, MouseEvent } from "react"
import { withRouter, useHistory } from "react-router-dom"
import { Link } from "react-router-dom"
import PersonOutlineIcon from "@material-ui/icons/PersonOutline"
import SettingsOutlinedIcon from "@material-ui/icons/SettingsOutlined"
import ExitToAppIcon from "@material-ui/icons/ExitToApp"
import Money from "@material-ui/icons/Money"
import Moon from "@material-ui/icons/MonochromePhotos"
import KeyboardBackspaceOutlinedIcon from "@material-ui/icons/KeyboardBackspaceOutlined"

import Card from "../card"
import "./profile.scss"
import { useProfile } from "../../../utitly"
import { useDispatch } from "react-redux"
import { togglePost } from "../../../store"

const BASE_URL =
  process.env.NODE_ENV === "development"
    ? process.env.USE_BASE_URL_ENV
    : process.env.USE_BASE_URL

type ProfileAsideType = {
  toogleAlert?: (event: MouseEvent) => void
}

const ProfileAside = ({ toogleAlert }: ProfileAsideType): JSX.Element => {
  const History = useHistory()

  const [calloutsettings, setCalloutsettings] = useState(false)
  const { profile } = useProfile()
  const [mode, setMode] = useState(false)
  const dispatch = useDispatch()

  const handleCallout = () => {
    setCalloutsettings(!calloutsettings)
  }

  const toggleMode = () => {
    setMode(!mode)
  }

  return (
    <Fragment>
      <Card>
        <div className="ProfileAside">
          <div className="ProfileAside__Top">
            <div className="ProfileAside__Top__Belt"></div>
            <div className="ProfileAside__Top__Avatar">
              <img src={profile.user?.avatar} />
            </div>
          </div>
          <div className="ProfileAside__Middle">
            <h3>{profile.user?.username}</h3>
            <h4>{profile.user?.email}</h4>
          </div>
          <div className="ProfileAside__Bottom">
            <table>
              <tbody>
                <tr className="ProfileAside__Bottom__Profile">
                  <td style={{ width: "25px" }}>
                    <Link to={`/u/${profile.user?._id}`}>
                      <div className="ProfileAside__Bottom__Icon">
                        <PersonOutlineIcon fontSize="inherit" />
                      </div>
                    </Link>
                  </td>
                  <td>
                    <Link to={`/u/${profile.user?._id}`}>
                      <span className="ProfileAside__Bottom__Text">
                        My Profile
                      </span>
                    </Link>
                  </td>
                </tr>

                <tr
                  onClick={() => handleCallout()}
                  className="ProfileAside__Bottom__Save"
                >
                  <td style={{ width: "25px" }}>
                    <div className="ProfileAside__Bottom__Icon">
                      <SettingsOutlinedIcon fontSize="inherit" />
                    </div>
                  </td>
                  <td>
                    <span className="ProfileAside__Bottom__Text">
                      My Account
                    </span>
                  </td>
                </tr>
                <tr className="ProfileAside__Bottom__Save">
                  <td style={{ width: "25px" }}>
                    <div className="ProfileAside__Bottom__Icon">
                      <Money fontSize="inherit" />
                    </div>
                  </td>
                  <td>
                    <span className="ProfileAside__Bottom__Text">Funding</span>
                  </td>
                </tr>
                <tr
                  onClick={() => toggleMode()}
                  className="ProfileAside__Bottom__Save"
                >
                  <td style={{ width: "25px" }}>
                    <div className="ProfileAside__Bottom__Icon">
                      <Moon fontSize="inherit" />
                    </div>
                  </td>
                  <td>
                    <span className="ProfileAside__Bottom__Text">
                      Change Mode
                    </span>
                  </td>
                </tr>
                <tr
                  onClick={toogleAlert}
                  className="ProfileAside__Bottom__Logout"
                >
                  <td style={{ width: "25px" }}>
                    <div className="ProfileAside__Bottom__Icon">
                      <ExitToAppIcon fontSize="inherit" />
                    </div>
                  </td>
                  <td>
                    <span className="ProfileAside__Bottom__Text">Logout</span>
                  </td>
                </tr>
                {calloutsettings && (
                  <div className="ProfileAside__Bottom__Callout">
                    <Card>
                      <div className="ProfileAside__Bottom__Callout__Header">
                        <table>
                          <tbody>
                            <tr>
                              <td>
                                <span onClick={() => handleCallout()}>
                                  <KeyboardBackspaceOutlinedIcon fontSize="inherit" />
                                </span>
                              </td>
                              <td> Account & Settings</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <Link to="/account/edit-profile">
                        <div className="ProfileAside__Bottom__Callout__Item">
                          Edit Profile
                        </div>
                      </Link>
                      <Link to="/u/Mauvaun">
                        <div className="ProfileAside__Bottom__Callout__Item">
                          Saved Posts
                        </div>
                      </Link>
                      <Link to="/account/update-interests">
                        <div className="ProfileAside__Bottom__Callout__Item">
                          Update my interests
                        </div>
                      </Link>
                      <Link to="/account/change-password">
                        <div className="ProfileAside__Bottom__Callout__Item">
                          Change password
                        </div>
                      </Link>
                      <div className="ProfileAside__Bottom__Callout__Item"></div>
                    </Card>
                  </div>
                )}

                {mode && (
                  <div className="ProfileAside__Bottom__Callout">
                    <Card>
                      <div className="ProfileAside__Bottom__Callout__Header">
                        <table>
                          <tbody>
                            <tr>
                              <td>
                                <span onClick={() => dispatch(toggleMode())}>
                                  <KeyboardBackspaceOutlinedIcon fontSize="inherit" />
                                </span>
                              </td>
                              <td> Theme</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div
                        onClick={() =>
                          document.documentElement.setAttribute(
                            "data-theme",
                            "light"
                          )
                        }
                        className="ProfileAside__Bottom__Callout__Item"
                      >
                        default
                      </div>

                      <div
                        onClick={() =>
                          document.documentElement.setAttribute(
                            "data-theme",
                            "deem"
                          )
                        }
                        className="ProfileAside__Bottom__Callout__Item"
                      >
                        deem
                      </div>

                      <div
                        onClick={() =>
                          document.documentElement.setAttribute(
                            "data-theme",
                            "dark"
                          )
                        }
                        className="ProfileAside__Bottom__Callout__Item"
                      >
                        dark
                      </div>
                    </Card>
                  </div>
                )}
              </tbody>
            </table>
          </div>
        </div>
      </Card>
    </Fragment>
  )
}

export default ProfileAside
