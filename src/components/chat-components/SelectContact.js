import React, { Component } from "react"
import { Input, Label, Button } from "reactstrap"
import { connect } from "react-redux"
import Config from "../../config"

// import { ApiloadStateMain } from "../../../components/loaders"
// import { Button } from '../../../components/Button/button'
import { getFollowers } from "../../store/slice/friendSlice"

//use sortedContacts variable as global variable to sort contacts
let sortedContacts = [
  {
    group: "A",
    children: [{ id: 0, name: "Demo" }],
  },
]

class SelectContact extends Component {
  constructor(props) {
    super(props)
    this.state = {
      contacts: this.props.contacts,
      followers: [],
      loading: true,
    }
    this.sortContact = this.sortContact.bind(this)
  }

  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) {
      this.setState({
        contacts: this.props.contacts,
      })
    }
  }

  sortContact() {
    let data = this.state.contacts.reduce((r, e) => {
      try {
        // get first letter of name of current element
        let group = e.name[0]
        // if there is no property in accumulator with this letter create it
        if (!r[group]) r[group] = { group, children: [e] }
        // if there is push current element to children array for that letter
        else r[group].children.push(e)
      } catch (error) {
        return sortedContacts
      }
      // return accumulator
      return r
    }, {})

    // since data at this point is an object, to get array of values
    // we use Object.values method
    let result = Object.values(data)
    this.setState({ contacts: result })
    sortedContacts = result
    return result
  }

  render() {
    return (
      <React.Fragment>
        {
          <ul className="list-unstyled contact-list">
            {this.props.friends.status == "succeeded" ? (
              this.props.friends.followers.length > 0 ? (
                this.props.friends.followers.map(
                  (
                    { avatar, firstName, lastName, username, _id },
                    keyChild
                  ) => (
                    <li key={keyChild}>
                      <div className="form-check">
                        <Input
                          type="checkbox"
                          className="form-check-input"
                          onChange={(e) => this.props.handleCheck(e, _id)}
                          id={"memberCheck" + _id}
                          value={_id}
                        />
                        <Label
                          className="form-check-label"
                          htmlFor={"memberCheck" + _id}
                        >{`${firstName} ${lastName}`}</Label>
                      </div>
                    </li>
                  )
                )
              ) : (
                <p className="mt-5 text-center">No one is following you yet</p>
              )
            ) : this.props.friends.status == "failed" ? (
              <div class="text-center">
                <p style={{ margin: "20px" }}>
                  Looks like you lost your connection. Please check it and try
                  again.{" "}
                </p>
                <Button
                  onClick={() =>
                    this.props.getFollowers({ token: this.props.token })
                  }
                  className="text-decoration-none text-center text-muted pr-1"
                  type="button"
                >
                  refresh
                </Button>

                <br />
              </div>
            ) : (
              <p>loading...</p>
            )}
          </ul>
        }
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state) => {
  const { contacts } = state.Chat
  const { token } = state.data.loginUserDetails || {}
  const friends = state.friends
  return { contacts, token, friends }
}

export default connect(mapStateToProps, {})(SelectContact)
