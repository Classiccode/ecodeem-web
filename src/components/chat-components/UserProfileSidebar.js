import React, { useEffect, useState } from "react"
import { connect } from "react-redux"
import { Button, Card, Badge } from "reactstrap"

//Simple bar
import SimpleBar from "simplebar-react"

//components
import AttachedFiles from "./AttachedFiles"
import CustomCollapse from "./CustomCollapse"

//actions
import { closeUserSidebar } from "../../store/action/index"

//i18n
import { useTranslation } from "react-i18next"

import { getChatter } from "../../helpers/common"

function UserProfileSidebar(props) {
  const [isOpen1, setIsOpen1] = useState(true)
  const [isOpen2, setIsOpen2] = useState(false)
  const [isOpen3, setIsOpen3] = useState(false)
  const [conversation, setConversation] = useState()
  const [files, setFiles] = useState([])
  // const [files] = useState([
  //     { name: "Admin-A.zip", size: "12.5 MB", thumbnail: "ri-file-text-fill" },
  //     { name: "Image-1.jpg", size: "4.2 MB", thumbnail: "ri-image-fill" },
  //     { name: "Image-2.jpg", size: "3.1 MB", thumbnail: "ri-image-fill" },
  //     { name: "Landing-A.zip", size: "6.7 MB", thumbnail: "ri-file-text-fill" },
  // ]);

  /* intilize t variable for multi language implementation */
  const { t } = useTranslation()

  useEffect(() => {
    setConversation(props.conversation)

    let fileMessage = props.conversation.recentMessages.filter(
      (e) => e.type == "image" || e.type == "document"
    )
    setFiles([...fileMessage])
  }, [props.conversation])

  const toggleCollapse1 = () => {
    setIsOpen1(!isOpen1)
    setIsOpen2(false)
    setIsOpen3(false)
  }

  const toggleCollapse2 = () => {
    setIsOpen2(!isOpen2)
    setIsOpen1(false)
    setIsOpen3(false)
  }

  const toggleCollapse3 = () => {
    setIsOpen3(!isOpen3)
    setIsOpen1(false)
    setIsOpen2(false)
  }

  // closes sidebar
  const closeuserSidebar = () => {
    props.closeUserSidebar()
  }
  // style={{display: props.userSidebar  ? "block" : "none"}}

  return (
    <React.Fragment>
      {conversation && (
        <div
          style={{ display: props.userSidebar === true ? "block" : "none" }}
          className="user-profile-sidebar"
        >
          <div className="px-3 px-lg-4 pt-3 pt-lg-4">
            <div className="user-chat-nav  text-end">
              <Button
                color="none"
                type="button"
                onClick={closeuserSidebar}
                className="nav-btn"
                id="user-profile-hide"
              >
                <i className="ri-close-line"></i>
              </Button>
            </div>
          </div>

          <div className="text-center p-4 border-bottom">
            <div className="mb-4 d-flex justify-content-center">
              {conversation.type == "private" ? (
                !getChatter(conversation.users, props.user).avatar ? (
                  <div className="avatar-lg">
                    <span className="avatar-title rounded-circle bg-soft-primary text-primary font-size-24">
                      {getChatter(
                        conversation.users,
                        props.user
                      ).firstName.charAt(0)}
                    </span>
                  </div>
                ) : (
                  <img
                    src={getChatter(conversation.users, props.user).avatar}
                    className="rounded-circle avatar-lg img-thumbnail"
                    alt="chatvia"
                  />
                )
              ) : !conversation.avatar ? (
                <div className="avatar-lg">
                  <span className="avatar-title rounded-circle bg-soft-primary text-primary font-size-24">
                    {conversation.name.charAt(0)}
                  </span>
                </div>
              ) : (
                <img
                  src={conversation.avatar}
                  className="rounded-circle avatar-lg img-thumbnail"
                  alt="chatvia"
                />
              )}
            </div>

            {conversation.type == "private" ? (
              <h5 className="font-size-16 mb-1 text-truncate">{`${
                getChatter(conversation.users, props.user).firstName
              } ${getChatter(conversation.users, props.user).lastName}`}</h5>
            ) : (
              <h5 className="font-size-16 mb-1 text-truncate">
                {conversation.name}
              </h5>
            )}
          </div>
          {/* End profile user */}

          {/* Start user-profile-desc */}
          <SimpleBar
            style={{ maxHeight: "100%" }}
            className="p-4 user-profile-desc"
          >
            <div className="text-muted">
              <p className="mb-4">{t(conversation.description)}</p>
            </div>

            <div id="profile-user-accordion" className="custom-accordion">
              {conversation.type == "private" && (
                <Card className="shadow-none border mb-2">
                  {/* import collaps */}
                  <CustomCollapse
                    title="About"
                    iconClass="ri-user-2-line"
                    isOpen={isOpen1}
                    toggleCollapse={toggleCollapse1}
                  >
                    <div>
                      <p className="text-muted mb-1">{t("Username")}</p>
                      <h5 className="font-size-14">
                        {getChatter(conversation.users, props.user).username}
                      </h5>
                    </div>
                    <div>
                      <p className="text-muted mb-1">{t("First Name")}</p>
                      <h5 className="font-size-14">
                        {getChatter(conversation.users, props.user).firstName}
                      </h5>
                    </div>
                    <div>
                      <p className="text-muted mb-1">{t("Last Name")}</p>
                      <h5 className="font-size-14">
                        {getChatter(conversation.users, props.user).lastName}
                      </h5>
                    </div>
                  </CustomCollapse>
                </Card>
              )}

              {conversation.type == "group" && (
                <Card className="mb-1 shadow-none border">
                  <CustomCollapse
                    title="Members"
                    iconClass="ri-group-line"
                    isOpen={isOpen3}
                    toggleCollapse={toggleCollapse3}
                  >
                    {conversation.users.map(
                      ({ avatar, firstName, lastName }) => (
                        <Card className="p-2 mb-2">
                          <div className="d-flex align-items-center">
                            <div className="chat-user-img align-self-center me-3">
                              {avatar ? (
                                <div className="chat-avatar">
                                  <img
                                    src={avatar}
                                    className="rounded-circle chat-user-img avatar-xs me-3"
                                    alt="chatvia"
                                  />
                                </div>
                              ) : (
                                <div className="avatar-xs">
                                  <span className="avatar-title rounded-circle bg-soft-primary text-primary">
                                    {firstName.charAt(0)}
                                  </span>
                                </div>
                              )}
                            </div>
                            <div>
                              <div className="text-left">
                                <h5 className="font-size-14 mb-1">
                                  {t(`${firstName} ${lastName}`)}
                                  <Badge
                                    color="danger"
                                    className="badge-soft-danger float-end"
                                  >
                                    {t("Member")}
                                  </Badge>
                                </h5>
                              </div>
                            </div>
                          </div>
                        </Card>
                      )
                    )}
                  </CustomCollapse>
                </Card>
              )}

              <Card className="mb-1 shadow-none border">
                <CustomCollapse
                  title="Attached Files"
                  iconClass="ri-attachment-line"
                  isOpen={isOpen2}
                  toggleCollapse={toggleCollapse2}
                >
                  {}

                  <AttachedFiles files={files} />
                </CustomCollapse>
              </Card>
            </div>
          </SimpleBar>
          {/* end user-profile-desc */}
        </div>
      )}
    </React.Fragment>
  )
}

const mapStateToProps = (state) => {
  const { users, active_user } = state.Chat
  const { userSidebar } = state.Layout
  return { users, active_user, userSidebar }
}

export default connect(mapStateToProps, { closeUserSidebar })(
  UserProfileSidebar
)
