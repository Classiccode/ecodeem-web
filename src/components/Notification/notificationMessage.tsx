import React, { FunctionComponent } from 'react'
import { useHistory } from 'react-router-dom'
// import Button from "../Button/button";
import './notification.scss'


interface Notprops{
    image?: string
    title:string
    text?:string
    url:string
}

const NotificationMessage:FunctionComponent<Notprops> = ({image,title,text,url}) => {
  const  history = useHistory()
    return (
        <>
            <div className='notificationMessage'>
                <div className="left">
                    <img src={image} alt=""/>
                    <p className='ml-3'>{title} </p>
                    <p> {text}</p>
                </div>
                <div className="right">
                    <button onClick={() => history.push(`/${url}`)} >view</button>
                </div>
            </div>
           
        </>
    )
}

export default NotificationMessage
