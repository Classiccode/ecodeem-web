import React, { MouseEvent } from "react"
import { NavLink, Link } from "react-router-dom"
import "./button.scss"
import { SvgIconProps } from "@material-ui/core"

interface Buttonprops {
  text?: string
  type:
    | "primary"
    | "primaryalt"
    | "secondary"
    | "cancel"
    | "monochrome"
    | "nav"
    | "gray"
    | "dark"
    | "navy"
  floating?: boolean
  icon?: SvgIconProps
  disable?: boolean
  onClick?: (event: MouseEvent) => void
  submit?: "button" | "submit" | "reset" | undefined
  count?: number | undefined
  className?: string
}

const Button = ({
  text,
  type,
  onClick,
  floating,
  icon,
  disable,
  submit,
  count,
  className,
}: Buttonprops): JSX.Element => {
  return (
    <>
      {disable ? (
        <button
          style={{
            backgroundColor: "#cccccc",
          }}
          disabled={disable}
          className={`Button__${type} ${
            floating ? "floating" : ""
          } ${className}`}
        >
          {`${text} `} {icon}
        </button>
      ) : (
        <button
          type={submit}
          onClick={onClick}
          className={`Button__${type} ${floating ? "floating" : ""}`}
        >
          {`${text ? text : ""} `} {icon}{" "}
          {count && <span className="Button__count">{count}</span>}
        </button>
      )}
    </>
  )
}

export default Button
