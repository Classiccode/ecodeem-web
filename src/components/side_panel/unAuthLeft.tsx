import { useSelector } from "react-redux"
import { RootState } from "../../store"
import { SubCategory } from "."

export function UnAuthLeft(): JSX.Element {
  const { categories, sectionalCategories } = useSelector(
    (state: RootState) => {
      return {
        categories: state.data.categories,
        sectionalCategories: state.data.sectionalCategories,
      }
    }
  )
  return (
    <nav className="side_panel left" style={{ position: "relative" }}>
      <h4 className="title_txt">Quick shop</h4>
      <div className="content_list">
        {categories.map((category) => {
          return (
            <SubCategory
              category={category}
              isAuthView={false}
              sectionalCategories={sectionalCategories.filter(
                (s) => s.category._id === category._id
              )}
              key={category._id}
            />
          )
        })}
      </div>
    </nav>
  )
}
