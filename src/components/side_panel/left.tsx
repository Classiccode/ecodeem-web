import { useSelector } from "react-redux"
import type {
  CategoryReturnType,
  RootState,
  SectionalCategoriesReturnType,
} from "../../store"
import { Link } from "react-router-dom"
import { ArrowForwardIos } from "@material-ui/icons"
import { useCallback } from "react"
import Config from "../../config"

export function LeftSidePanel({
  isAuthView = false,
}: {
  isAuthView?: boolean
}): JSX.Element {
  const { categories, profile, sectionalCategories } = useSelector(
    (state: RootState) => {
      return {
        categories: state.data.categories,
        sectionalCategories: state.data.sectionalCategories,
        profile: state.data.loginUserDetails,
      }
    }
  )
  return (
    <nav className="side_panel left">
      <h4 className="title_txt">Quick shop</h4>
      <div className="content_list">
        {categories.map((category) => {
          return (
            <SubCategory
              category={category}
              isAuthView={isAuthView}
              sectionalCategories={sectionalCategories.filter(
                (s) => s.category._id === category._id
              )}
              key={category._id}
            />
          )
        })}
      </div>
      <h4 className="title_txt">MY ACHIEVEMENT</h4>
      {profile?.user?.reward ? (
        <div className="achievement">
          <img src={`/rewards/${profile.user.reward.level + 1}.png`} alt="" />
        </div>
      ) : (
        <></>
      )}
      {profile?.user?.reward && (
        <p className="mt-5 text-center">{profile.user.reward.title}</p>
      )}
    </nav>
  )
}

export function SubCategory({
  category,
  isAuthView,
  sectionalCategories,
}: {
  category: CategoryReturnType
  isAuthView: boolean
  sectionalCategories: SectionalCategoriesReturnType[]
}): JSX.Element {
  return (
    <>
      <div className="category_list">
        <div className="icon">
          <span style={{ backgroundImage: `url(${category.image})` }} />
        </div>
        <p>{category.title}</p>
        <ArrowForwardIos className="custom_icon" />
        <div className="container_layout">
          <h3 className="title">The Sub Categories</h3>
          {sectionalCategories.map((sectional) => {
            return (
              <Link
                to={
                  isAuthView
                    ? `/content/market/sub_category/${sectional._id}`
                    : `/market/sub_category/${sectional._id}`
                }
                className="sub_list"
                key={sectional._id}
              >
                {sectional.title}
              </Link>
            )
          })}
        </div>
      </div>
    </>
  )
}
