import { LoginReturnType } from "../../function"
import { BulletListShimmer } from "react-content-shimmer"
import { setAuthorization } from "../../config/axios"
import { useDispatch, useSelector } from "react-redux"
import {
  FriendState,
  getFollowers,
  getFollowing,
  ReducerType,
  RootState,
} from "../../store"
import { useCallback, useEffect, memo } from "react"
import Friend from "../Friends/friend"
import { Button } from "@material-ui/core"

function RightSidePanelComp(): JSX.Element {
  const dispatch = useDispatch()
  setAuthorization()
  const setFollowers = useCallback(() => {
    dispatch(getFollowers())
    dispatch(getFollowing())
  }, [])
  useEffect(() => {
    setFollowers()
  }, [setFollowers])
  const { loginDetail } = useSelector<
    ReducerType,
    {
      loginDetail?: LoginReturnType
    }
  >((state: ReducerType) => {
    return { loginDetail: state.data.loginUserDetails }
  })
  const friends: FriendState = useSelector((state: RootState) => state.friends)
  return (
    <nav className="side_panel right">
      <div className="account_details">
        {loginDetail?.user.avatar ? (
          <img
            height="50px"
            width="50px"
            style={{ borderRadius: "50%", margin: "5px 10px" }}
            src={loginDetail?.user.avatar}
            alt=""
          />
        ) : (
          <span className="avatar" />
        )}

        <div className="col">
          <span className="name">{`${loginDetail?.user.firstName ?? ""} ${
            loginDetail?.user.lastName ?? ""
          }`}</span>
          <span className="username">{loginDetail?.user.username ?? ""}</span>
        </div>
      </div>
      <h4 className="title_txt">FOLLOWERS (FRIENDS)</h4>
      {friends.status == "succeeded" ? (
        friends.followers.length > 0 ? (
          friends.followers.map(
            ({ avatar, firstName, lastName, username, _id }) => (
              <div className="mt-3 d-flex mx-2" key={`hvxdxx ${_id}`}>
                <Friend
                  avatar={avatar}
                  username={username}
                  firstname={firstName}
                  lastname={lastName}
                  _id={_id}
                />
              </div>
            )
          )
        ) : (
          <ul>
            <li>
              <span className="name">No follower yet!</span>
            </li>
          </ul>
        )
      ) : friends.status == "failed" ? (
        <div>
          <p style={{ margin: "20px" }}>
            Looks like you lost your connection. Please check it and try again.
          </p>
          <div style={{ width: "50%", margin: "auto", height: "40px" }}>
            <Button
              size="small"
              variant="outlined"
              onClick={() => dispatch(getFollowers())}
            >
              reload
            </Button>
          </div>
          <br />
        </div>
      ) : (
        <>
          <BulletListShimmer
            rows={1}
            background="var(--background)"
            foreground="var(--background-2)"
          />
        </>
      )}
    </nav>
  )
}

export const RightSidePanel = memo(RightSidePanelComp)
