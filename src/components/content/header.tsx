/* eslint-disable prettier/prettier */
import {
  Avatar,
  Button,
  InputAdornment,
  ListItem,
  ListItemAvatar,
  ListItemText,
  TextField,
} from "@material-ui/core"
import { useEffect, useState } from "react"
import { NavLink, useHistory } from "react-router-dom"
import SearchIcon from "remixicon-react/Search2LineIcon"
import HomeIcon from "remixicon-react/Home4LineIcon"
import MarketPlaceIcon from "remixicon-react/Store3LineIcon"
import ChatIcon from "remixicon-react/Message3LineIcon"
import NotificationIcon from "remixicon-react/Notification2LineIcon"
import ProfileIcon from "remixicon-react/User6LineIcon"
import CartIcon from "remixicon-react/ShoppingCartLineIcon"
import MenuIcon from "remixicon-react/Menu2LineIcon"
import CloseIcon from "remixicon-react/CloseLineIcon"
import { useDispatch, useSelector } from "react-redux"
import { ReducerType, StoreType } from "../../store"
import { AnimatePresence, motion } from "framer-motion"
import { useProfile } from "../../utitly"
import { useNotification } from "../../screens"
import { useQuery } from "react-query"
import {
  getChatCountNotificationFuncHandler,
  searchUserProfile,
  UserSearchReturnType,
} from "../../function"
import { FixedSizeList, ListChildComponentProps } from "react-window"
import { RootState } from "../../store"
import { useLocation } from "react-router-dom"

export function ContentHeader(): JSX.Element {
  const [searchText, setSearchText] = useState<string>("")
  const [result, setResult] = useState<UserSearchReturnType>()
  const [show, setShow] = useState(false)
  const history = useHistory()
  const { isNavOpen } = useSelector<ReducerType, { isNavOpen: boolean }>(
    (state) => ({
      isNavOpen: state.event.isNavOpen,
    })
  )
  const cart = useSelector((state: RootState) => state.cart.cart)
  const dispatch = useDispatch()
  const { profile } = useProfile()
  const { data, isLoading, isError } = useNotification()
  const {
    data: chatno,
    isLoading: loadinchatno,
    isError: chatError,
  } = useQuery("chatCount", async () => {
    return (await getChatCountNotificationFuncHandler(profile.user._id)).unread
  })

  const { push } = useHistory()

  function renderRow(props: ListChildComponentProps) {
    const { index, style } = props

    return (
      <ListItem
        onClick={() => push(`/content/profile/${result?.users[index]._id}`)}
        button
        style={style}
        key={index}
      >
        <ListItemAvatar>
          <Avatar
            alt={result?.users[index].username}
            src={`${result?.users[index].avatar}`}
          />
        </ListItemAvatar>
        <ListItemText
          key={`Item ${index + 1}`}
          primary={result?.users[index].username}
        />
      </ListItem>
    )
  }

  const getResult = async (val: string) => {
    try {
      const resp = await searchUserProfile({ name: val })
      setResult(resp.data)
    } catch (error) {
      console.error(error)
    }
  }

  useEffect(() => {
    if (searchText.length > 0) {
      getResult(searchText)
    }
  }, [searchText])

  const closeSearch = () => {
    setShow(false)
  }
  const routes: {
    link: string
    name: string
    icon: JSX.Element
  }[] = [
    {
      name: "Home",
      link: "/content/post",
      icon: <HomeIcon />,
    },
    {
      name: "Market Place",
      link: "/content/market",
      icon: <MarketPlaceIcon />,
    },
    {
      name: "Chat",
      link: "/content/chat",
      icon: <ChatIcon />,
    },
    {
      name: "Notification",
      link: "/content/notification",
      icon: <NotificationIcon />,
    },
    {
      name: "Profile",
      link: `/content/profile/${profile.user._id}`,
      icon: <ProfileIcon />,
    },
  ]

  const { pathname } = useLocation()
  const { cartCount } = useSelector((state: RootState) => {
    return {
      cartCount: state.data.cartCount,
    }
  })
  return (
    <>
      <header className="ContentHeader">
        <div className="title_group">
          <Button
            className="nav_btn font-color"
            onClick={() => {
              dispatch({
                type: StoreType.toggleNav,
              })
            }}
          >
            {!isNavOpen ? <MenuIcon /> : <CloseIcon />}
          </Button>
          <span
            className="logo"
            role="a"
            style={{ cursor: "pointer" }}
            onClick={() => {
              push("/content/post")
            }}
          ></span>
          <TextField
            onFocus={() => {
              setShow(true)
            }}
            onBlur={() => {
              setTimeout(closeSearch, 500)
            }}
            className="inputBox"
            variant="outlined"
            value={searchText}
            onChange={(e) => {
              setSearchText(e.target.value)
            }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <SearchIcon />
                </InputAdornment>
              ),
            }}
            placeholder="search"
          />
        </div>
        <div className="links">
          {routes.map((route, index): JSX.Element => {
            return (
              <NavLink
                to={route.link}
                key={index}
                className={`nav ${
                  pathname.includes(`/${route.link.split("/")[2]}`)
                    ? "active"
                    : ""
                }`}
              >
                {route.icon}
                {route.name}
                {route.name == "Notification" && (
                  <span className="badge">
                    {!isLoading &&
                      !isError &&
                      data?.filter(function (element) {
                        return element.isRead == false
                      }).length}
                  </span>
                )}
                {route.name == "Chat" && (
                  <span className="badge">
                    {!loadinchatno && !chatError && chatno}
                  </span>
                )}
              </NavLink>
            )
          })}
        </div>
        <div className="action">
          <Button
            onClick={() => history.push("/content/notification")}
            className="cart_btn min"
          >
            <span className="badge">
              {!isLoading &&
                !isError &&
                data?.filter(function (element) {
                  return element.isRead == false
                }).length}
            </span>
            <NotificationIcon />
          </Button>

          <Button
            onClick={() => history.push("/content/market/cart")}
            className="cart_btn"
          >
            <span className="badge">{Math.max(cart?.length, cartCount)}</span>
            <CartIcon />
          </Button>
        </div>
        <AnimatePresence>
          {isNavOpen && (
            <motion.div
              className="cover"
              exit={{ opacity: 0, y: 20 }}
              initial={{ opacity: 0, y: 20 }}
              animate={{ opacity: 1, y: 0 }}
              onClick={() => {
                dispatch({
                  type: StoreType.changeNavState,
                  value: false,
                })
              }}
            >
              <div className="min_links">
                {routes.map((route, index): JSX.Element => {
                  return (
                    <NavLink to={route.link} key={index} className="nav">
                      {route.icon}
                      {route.name}
                    </NavLink>
                  )
                })}
              </div>
            </motion.div>
          )}
        </AnimatePresence>
      </header>
      {show && (
        <div className="searchResult">
          {result && (
            <FixedSizeList
              height={400}
              width="100%"
              itemSize={55}
              itemCount={result.userCount}
            >
              {renderRow}
            </FixedSizeList>
          )}
        </div>
      )}
    </>
  )
}
