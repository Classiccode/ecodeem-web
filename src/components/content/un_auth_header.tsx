/* eslint-disable react/display-name */
import { Button, InputAdornment, TextField } from "@material-ui/core"
import { useState } from "react"
import { useHistory } from "react-router"
import SearchIcon from "remixicon-react/Search2LineIcon"
import CartIcon from "remixicon-react/ShoppingCartLineIcon"

export function UnAuthHeader(): JSX.Element {
  const { push } = useHistory()
  const [searchText, setSearchText] = useState<string>("")
  return (
    <header
      className="ContentHeader"
      style={{ justifyContent: "space-between" }}
    >
      <div className="title_group">
        <span
          className="logo"
          role="button"
          onClick={() => {
            push("/market")
          }}
        ></span>
        <TextField
          className="inputBox"
          variant="outlined"
          value={searchText}
          onChange={(e) => {
            setSearchText(e.target.value)
          }}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SearchIcon />
              </InputAdornment>
            ),
          }}
          placeholder="search"
        />
      </div>
      <div className="action">
        <Button className="cart_btn">
          <CartIcon />
        </Button>
      </div>
    </header>
  )
}
