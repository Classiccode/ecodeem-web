import React from "react"
import "./index.scss"
import CloseOutlinedIcon from "@material-ui/icons/CloseOutlined"

interface ModalProps {
  children: JSX.Element
  title: string
  closeModal: () => void
}

const Modal: React.FC<ModalProps> = ({
  children,
  title,
  closeModal,
}: {
  children: JSX.Element
  title: string
  closeModal: () => void
}) => {
  return (
    <>
      <div className="Modal" onClick={closeModal}>
        <div
          className="Modal__Content"
          onClick={(e: React.ChangeEvent<any>) => e.stopPropagation()}
        >
          <div className="Modal__Content__Header">
            <p className="Modal__Content__Header__Title">{title}</p>
            <CloseOutlinedIcon
              onClick={closeModal}
              className="Modal__Content__Header__Icon"
              fontSize="large"
            />
          </div>
          {children}
        </div>
      </div>
    </>
  )
}
export default Modal
