import React, { ReactNode } from "react"
import "./overlay.scss"

export interface OverlayProps {
  children: ReactNode
}
const Overlay = (props: OverlayProps): JSX.Element => {
  return <div className="Overlay">{props.children}</div>
}

export default Overlay
