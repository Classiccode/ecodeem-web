import React from "react"
import "./apiloadStateMain.scss"

const Default = (): JSX.Element => {
  return (
    <React.Fragment>
      <div className="ApiLoadStateMain">
        <div className="ApiLoadStateMain__content"></div>
      </div>
    </React.Fragment>
  )
}

export default Default
