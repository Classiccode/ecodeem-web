import React from "react"
import "./apiloadState.scss"

const Default = (): JSX.Element => {
  return (
    <React.Fragment>
      <div className="ApiLoadState">
        <div className="ApiLoadState__content"></div>
      </div>
    </React.Fragment>
  )
}

export default Default
