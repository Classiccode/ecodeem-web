/* eslint-disable indent */
import React, {
  useContext,
  useReducer,
  useState,
  FC,
  MouseEvent,
  ReactNode,
  ChangeEvent,
  useEffect,
} from "react"
import { useDispatch, useSelector } from "react-redux"
import { addPost, getInterestedDeems, togglePost, RootState } from "../../store"
import { useProfile } from "../../utitly"
import axios from "axios"
import Config from "../../config"
import { toast } from "react-toastify"

type Inputprops = {
  fundgoal: boolean
  showInterests: boolean
  interest: string
  media: string
  mediaType: string
  min: number
  max: number
  target: string
  counter: number
  interests: Iinterest[]
  loading: boolean
  tab: "campaign" | "fund"
  showError: { status: boolean; message: string }
}
type ChildrenProps = {
  input: Inputprops
  toogleFundgoal: (event: MouseEvent) => void
  toogleInterests: (event: MouseEvent) => void
  selectInterest: (value: Iinterest, event: MouseEvent) => void
  handleChange: (
    event: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
  ) => void
  handleFileChange: (selectorFiles: FileList | null) => void
  removeMedia: (event: MouseEvent) => void
  handleSubmit: (event: MouseEvent) => void
  setTab: any
}

interface IBlogprops {
  children(_: ChildrenProps): ReactNode
}

interface Iinterest {
  _id: string
  interest: string
  image: string
}

const PostProvider = ({ children }: IBlogprops) => {
  // const {cachedDeems,deems,dispatch} = useDeems();
  // const post = useSelector((state: RootState) => state.blog.posts)

  const profile = useProfile()
  const dispatch = useDispatch()
  const [fundgoal, setFundgoal] = useState(false)
  const [showInterests, setShowInterests] = useState(false)
  const [message, setMesage] = useState("")
  const [interest, setInterest] = useState("SELECT INTEREST")
  const [min, setMin] = useState(0)
  const [max, setMax] = useState(0)
  const [target, setTarget] = useState("")
  const [media, setMedia] = useState("")
  const [mediaFile, setMediaFile] = useState<Blob | string>()
  const [mediaType, setMediaType] = useState("")
  const [counter, setCounter] = useState(0)
  const [showError, setError] = useState<{ status: boolean; message: string }>({
    status: false,
    message: "",
  })
  const [intId, setIntId] = useState<string | undefined>()
  const [loading, setLoading] = useState(false)
  const [interests, setInterests] = useState<Array<Iinterest>>([])
  const [tab, setTab] = useState<"campaign" | "fund">("campaign")

  const toogleFundgoal = (event: MouseEvent) => {
    event.preventDefault()
    setFundgoal(!fundgoal)
  }

  const toogleInterests = (event: MouseEvent) => {
    event.preventDefault()
    setShowInterests(!showInterests)
  }

  const selectInterest = (value: Iinterest, event: MouseEvent): void => {
    event.preventDefault()
    setInterest(value.interest)
    setIntId(value._id)
    setShowInterests(!showInterests)
  }

  useEffect(() => {
    const requestOptions = {
      method: "GET",
    }
    fetch(`${Config.apiUrl}/interest/user-all`, requestOptions)
      .then((response) => response.json())
      .then(({ data: { interests } }) => {
        setInterests([...interests])
      })
      .catch((error) => {
        console.log("error", error)
      })
  }, [setInterests])

  const handleChange = (
    event: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
  ): void => {
    event.preventDefault()

    const { name, value } = event.target

    switch (name) {
      case "message":
        value.length <= 700 && setMesage(value || "")
        value.length <= 700 && setCounter(value.length)
        break
      case "min":
        setMin(+value)
        break
      case "max":
        setMax(+value)
        break
      case "target":
        setTarget(value || "")
        break
    }
  }

  const handleFileChange = (selectorFiles: FileList | null): void => {
    if (selectorFiles) {
      if (
        selectorFiles[0].type.replace(/\/.*/, "") == "video" &&
        selectorFiles[0].size > 100000000
      ) {
        setMedia("")
        setError({ status: true, message: "video cannot be more than 100mb" })
        return
      }

      if (
        selectorFiles[0].type.replace(/\/.*/, "") == "image" &&
        selectorFiles[0].size > 10000000
      ) {
        setMedia("")
        setError({ status: true, message: "image cannot be more than 10mb" })
        return
      }

      if (
        !selectorFiles[0].type.match("image.*") &&
        !selectorFiles[0].type.match("video.*")
      ) {
        setMedia("")
        setError({
          status: true,
          message: "file can only be image or video type",
        })
        return
      }

      setMedia(URL.createObjectURL(selectorFiles[0]))
      setMediaFile(selectorFiles[0])

      // setMedia(URL.createObjectURL(selectorFiles[0]));
      // console.log(URL.createObjectURL(selectorFiles[0]) instanceof Blob);

      setMediaType(selectorFiles[0].type.replace(/\/.*/, ""))
    }
  }

  const removeMedia = (event: MouseEvent): void => {
    setMedia("")
    setMediaType("")
  }

  const handleSubmit = async (event: MouseEvent) => {
    event.preventDefault()

    // if (interest == "SELECT INTEREST") {
    //   setError({ status: true, message: "Select an interest" })
    //   return
    // }

    if (!message && !media) {
      setError({ status: true, message: "Describe or upload a media to deem" })
      return
    }

    if (message.length < 1) {
      setError({ status: true, message: "Description is required" })
      return
    }

    if (max < 1 && tab == "fund") {
      setError({ status: true, message: "Maxmum amount cannot be less than 1" })
      return
    }

    if (!media && tab == "fund") {
      setError({
        status: true,
        message: "For campaign you must upload a medie",
      })
      return
    }

    if (!intId || intId === "") {
      setError({ status: true, message: "Please select an interest" })
      return
    }

    if (target.length < 1 && tab == "fund") {
      setError({ status: true, message: "What is your target date" })
      return
    }

    setLoading(true)

    const formData = new FormData()

    let resp: any
    if (mediaFile) {
      formData.append("file", mediaFile)
      formData.append("upload_preset", "preset1")

      try {
        resp = await axios.post(
          `https://api.cloudinary.com/v1_1/dsxddxoeg/${mediaType}/upload`,
          formData,
          {
            transformRequest: [
              (data, headers) => {
                delete headers.common.Authorization
                return data
              },
            ],
            headers: {
              "Content-Type": "multipart/form-data",
            },
          }
        )
      } catch (error) {
        console.error(error)
        setLoading(false)
        toast.error("sorry we could not upload your media", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        })

        return
      }
    }

    // console.log(resp.data.secure_url);

    let data2: any

    if (tab == "campaign") {
      data2 = {
        postDesc: message,
        ...(intId && { interest: intId }),
        postType: mediaFile ? mediaType : "text",
        ...(mediaFile && { mediaUrl: resp.secure_url }),
      }
    }

    if (tab == "fund") {
      data2 = {
        postDesc: message,
        ...(intId && { interest: intId }),
        postType: "raiseFund",
        ...(mediaFile && { mediaUrl: resp.secure_url }),
        fundRaise: {
          minAmount: min,
          maxAmount: max,
          endDate: target,
        },
      }
    }

    await axios
      .post(`${Config.apiUrl}/post/create`, data2)
      .then(({ data }) => {
        // console.log(data);

        dispatch(getInterestedDeems())
        dispatch(togglePost())
        toast.success("post created successfully", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        })
      })
      .catch((error) => {
        toast.error("error creating post", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        })
      })

    setLoading(false)
  }

  const input = {
    fundgoal,
    interests,
    showInterests,
    interest,
    media,
    mediaType,
    min,
    max,
    target,
    counter,
    showError,
    loading,
    tab,
  }

  return (
    <>
      {children({
        input,
        toogleFundgoal,
        toogleInterests,
        selectInterest,
        handleChange,
        handleFileChange,
        removeMedia,
        handleSubmit,
        setTab,
      })}
    </>
  )
}

export default PostProvider
