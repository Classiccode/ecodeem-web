import React, { MouseEvent, Fragment } from "react"
import ScrollContainer from "react-indiana-drag-scroll"
import KeyboardBackspaceOutlinedIcon from "@material-ui/icons/KeyboardBackspaceOutlined"
import TextareaAutosize from "@material-ui/core/TextareaAutosize"
import { PermMediaOutlined, VideoLibraryRounded } from "@material-ui/icons/"
import MonetizationOnIcon from "@material-ui/icons/MonetizationOn"
import ExpandMoreOutlinedIcon from "@material-ui/icons/ExpandMoreOutlined"
import CloseIcon from "@material-ui/icons/Close"
import HighlightOffIcon from "@material-ui/icons/HighlightOff"

import PostProvider from "./provider"
import Card from "../Card/card"
import Button from "../Button/button"
import { truncateWords } from "../../utitly"
import "./post.scss"
import Video from "../Video/video"
import { ApiloadStateMain } from "../../components/loaders"
import { togglePost } from "../../store"
import { useDispatch } from "react-redux"
import ProfileCard from "../Chat/Profile/profileCard"
import { useProfile } from "../../utitly"
import Overlay from "../Overlay/overlay"
import VideoIcon from "remixicon-react/VideoAddFillIcon"
import ImageIcon from "remixicon-react/ImageAddFillIcon"

const Post: React.FC = () => {
  const dispatch = useDispatch()
  const profile = useProfile()

  return (
    <PostProvider>
      {({
        input,
        setTab,
        toogleFundgoal,
        toogleInterests,
        selectInterest,
        handleChange,
        handleFileChange,
        removeMedia,
        handleSubmit,
      }) => (
        <Fragment>
          <Overlay>
            <div className="Post">
              <form>
                <div className="Post__Wrapper">
                  <Card rounded={true}>
                    <div className="Post__Wrapper__Content">
                      <header className="Post__Wrapper__Content__Header">
                        <p>Create Deem</p>
                        {input.showError.status && (
                          <h4 className="text-danger">
                            {input.showError.message}
                          </h4>
                        )}
                        <div className="Post__Wrapper__Content__Header__Close">
                          <div onClick={() => dispatch(togglePost())}>
                            <HighlightOffIcon fontSize="small" />
                          </div>
                        </div>
                      </header>
                      <div className="Post__Wrapper__Content__scroll">
                        <ProfileCard
                          image={profile.profile.user.avatar}
                          title={`${profile.profile.user.firstName} ${profile.profile.user.lastName}`}
                        />
                        <div className="Post__Wrapper__Content__Body">
                          <ul>
                            <li
                              className={
                                input.tab == "campaign" ? "active" : ""
                              }
                              onClick={() => setTab("campaign")}
                            >
                              Make your Move
                            </li>
                            <li
                              className={input.tab == "fund" ? "active" : ""}
                              onClick={() => setTab("fund")}
                            >
                              Raise Fund
                            </li>
                          </ul>

                          <TextareaAutosize
                            className="bg-color"
                            style={{
                              borderRadius: "10px",
                            }}
                            rowsMin={4}
                            rowsMax={7}
                            maxLength={350}
                            name="message"
                            onChange={handleChange}
                            placeholder="What's going on?"
                          />
                        </div>

                        <div className="Post__Wrapper__Content__Footer">
                          <div className="Post__Wrapper__Content__Footer__Counter">
                            {input.counter}/356
                          </div>
                          {input.media && (
                            <div className="Post__Wrapper__Content__Footer__Media">
                              <span
                                onClick={removeMedia}
                                className="Post__Wrapper__Content__Footer__Media__Close"
                              >
                                <HighlightOffIcon fontSize="inherit" />
                              </span>
                              {input.mediaType == "image" ? (
                                <img src={input.media} />
                              ) : (
                                <div className="Post__Wrapper__Content__Footer__Media__Video">
                                  <Video src={input.media} />
                                </div>
                              )}
                            </div>
                          )}

                          <div className="Post__Wrapper__Content__Footer__Attribute">
                            <div className="Post__Wrapper__Content__Footer__Attribute__Media">
                              <section>
                                <label htmlFor="video-input">
                                  <div
                                    className="mr-5"
                                    style={{ marginRight: "20px" }}
                                  >
                                    <VideoIcon fontSize="large" />
                                  </div>
                                </label>
                                <label htmlFor="image-input">
                                  <div>
                                    <ImageIcon fontSize="large" />
                                  </div>
                                </label>
                                <input
                                  className="bg-color"
                                  id="video-input"
                                  accept="video/mp4,video/x-m4v,video/*"
                                  name="file-input"
                                  type="file"
                                  onChange={(e) => {
                                    handleFileChange(e.target.files)
                                    e.target.value = ""
                                  }}
                                />
                                <input
                                  className="bg-color"
                                  id="image-input"
                                  accept="image/x-png,image/gif,image/jpeg"
                                  name="file-input"
                                  type="file"
                                  onChange={(e) => {
                                    handleFileChange(e.target.files)
                                    e.target.value = ""
                                  }}
                                />
                              </section>
                            </div>

                            <div className="Post__Wrapper__Content__Footer__Attribute__Interest">
                              <section onClick={toogleInterests}>
                                {" "}
                                {input.interest}{" "}
                                <span>
                                  <ExpandMoreOutlinedIcon fontSize="inherit" />
                                </span>{" "}
                              </section>
                            </div>
                          </div>
                          {input.tab == "fund" && (
                            <div className="d-flex justify-content-around">
                              <h6>Min: {input.min ? input.min : null}</h6>
                              <h6>Max: {input.max ? input.max : null}</h6>
                              <h6></h6>
                              <h6></h6>
                            </div>
                          )}
                        </div>

                        {input.tab == "fund" && (
                          <div className="Post__Wrapper__Picker">
                            <div className="Post__Wrapper__Picker__Content">
                              <section>
                                <div className="Post__Wrapper__Picker__Content__Amount">
                                  <label htmlFor="min-amount"></label>
                                  <input
                                    id="min-amount"
                                    required={input.tab == "fund"}
                                    className="Post__Wrapper__Picker__Content__Input bg-color font-color"
                                    type="number"
                                    name="min"
                                    onChange={handleChange}
                                    placeholder="Min"
                                  />
                                </div>
                                <div className="Post__Wrapper__Picker__Content__Amount">
                                  <label htmlFor="max-amount"></label>
                                  <input
                                    id="max-amount"
                                    required={input.tab == "fund"}
                                    className="Post__Wrapper__Picker__Content__Input bg-color font-color"
                                    type="number"
                                    name="max"
                                    onChange={handleChange}
                                    placeholder="Max"
                                  />
                                </div>
                                <div className="Post__Wrapper__Picker__Content__Target">
                                  <label htmlFor="target">End date</label>
                                  <input
                                    id="target"
                                    className="Post__Wrapper__Picker__Content__Input bg-color font-color"
                                    type="date"
                                    name="target"
                                    onChange={handleChange}
                                    placeholder="Funding target (e.g 30 days)"
                                  />
                                </div>
                              </section>
                            </div>
                          </div>
                        )}
                        {input.showInterests && (
                          <div className="Post__Wrapper__Picker">
                            <div className="Post__Wrapper__Picker__Content">
                              <p>Select interest category</p>
                              <span
                                onClick={toogleInterests}
                                className="Post__Wrapper__Picker__Content__Close"
                              >
                                <CloseIcon fontSize="inherit" />
                              </span>

                              <ScrollContainer className="Interests">
                                {input.interests.map((interest, idx) => (
                                  <div
                                    key={idx}
                                    onClick={(e) => selectInterest(interest, e)}
                                    className="Interests__Interest"
                                  >
                                    <div>
                                      <img src={interest.image} alt="charity" />
                                    </div>
                                    <h6>
                                      {truncateWords(interest.interest, 6)}
                                    </h6>
                                  </div>
                                ))}
                              </ScrollContainer>
                            </div>
                          </div>
                        )}
                        {input.loading && <ApiloadStateMain />}
                        {!input.loading && (
                          <div className="mt-5 text-center m-auto">
                            <Button
                              disable={input.loading}
                              onClick={handleSubmit}
                              text="post"
                              type="dark"
                            />
                          </div>
                        )}
                      </div>
                    </div>
                  </Card>
                </div>
              </form>
            </div>
          </Overlay>
        </Fragment>
      )}
    </PostProvider>
  )
}

export default Post
