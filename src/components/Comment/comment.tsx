import { MouseEvent, useState } from "react"

import { CommentType } from "../../core/type"

import "./comment.scss"
import CommentDetail from "./commentDetail"

interface ICommentprops {
  title?: string
  postId: string
  contents: CommentType[]
  navigation?: (event: MouseEvent) => void
}

const Comment = ({ contents, postId }: ICommentprops): JSX.Element => {
  // const newContents = contents.reverse()

  return (
    <div className="Comments">
      {[...contents].reverse().map((content, idx) => (
        <CommentDetail
          key={"commealala" + idx}
          postId={postId}
          comment={content}
        />
      ))}
    </div>
  )
}

export default Comment
