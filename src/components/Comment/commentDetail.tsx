/* eslint-disable prettier/prettier */
import { Popper } from "@material-ui/core"
import { MoreHorizOutlined, ThumbUp } from "@material-ui/icons"
import { MouseEvent, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useHistory } from "react-router-dom"
import { CommentType } from "../../core/type"
import { RootState } from "../../store"
import { deleteComment, deleteReply, likeAndUnlikeComment } from "../../store"
import CommentForm from "../commentForm/commentForm"
import { getDaysBetween, useProfile } from "../../utitly"

interface ICommendDetail {
    comment: CommentType
    postId: string
}

const CommentDetail = ({ comment, postId }: ICommendDetail): JSX.Element => {
  const profile = useProfile()
  const dispatch = useDispatch()
  const [showRelies, setShowRelies] = useState(false)
  const [anchorEl, setAnchorEl] = useState < null | HTMLElement > (null)
  const history = useHistory()
  const status = useSelector((state: RootState) => state.blog.status)
  const open = Boolean(anchorEl)
  const id = open ? "simple-popper" : undefined

  function onShowReplies() {
    setShowRelies(!showRelies)
  }

  const handleClick = (event: MouseEvent<HTMLElement>) => {
    setAnchorEl(anchorEl ? null : event.currentTarget)
  }

  const handleDeleteComment = async () => {

    dispatch(deleteComment({ commentId: comment._id, postId: postId }))

  }

  const handleDeleteReply = async (commentId, postId, replyId) => {

    dispatch(deleteReply({ commentId: commentId, postId, replyId: replyId }))
  }

  const handleLike = async (commentId, postId) => {

    dispatch(likeAndUnlikeComment({ commentId: commentId, postId: postId }))

  }
  return (
    <div>
      <div className="Comments__Card">
	
        <div onClick={()=>history.push(`/content/profile/${comment.userId}`)} className={comment.avatar && "Comments__Image"}>
          {
            comment.avatar ? <img src={comment.avatar} />:
              <span className="avatar-title bg-soft-primary text-primary" style={{ borderRadius: "50%", width: "40px", height: "40px", marginRight:" 20px" }}>
                {comment.fullName.charAt(0)}                                                                                    
              </span>
          }
        </div>

        <div>
          <div style={{display:"flex",alignItems:"center"}}>
            <div className="Comments__Text__Container Comments__Text">
              {comment.fullName}
              <div className="Comments__Text">
                <p>{comment.comment}</p>
              </div>
              {
                comment.likesCount > 0 &&
            <div className='Comments__Text__Container__LikeCount'>
              <div><ThumbUp style={{fontSize:"12px"}}/></div>
              <p style={{marginRight:"2px"}}>{comment.likesCount}</p> 
            </div>
              }
        
            </div >
            <div aria-describedby={id} onClick={handleClick} className='Comments__Text__Container__More'> 
              <MoreHorizOutlined/>
              <Popper style={{ zIndex:2 }} id={id} open={open} anchorEl={anchorEl}>
             
                <div className='Comments__Text__Container__More__Dropdown'>
                  <ul>
                    {
                      comment.userId === profile.profile.user._id && <li onClick={()=> handleDeleteComment() }>Delete</li>
                    }
                    
                    {
                      comment.userId !== profile.profile.user._id && <li onClick={() => history.push(`/content/profile/${comment.userId}`)}>View Profile</li>
                    }
                  </ul>
                </div>
              </Popper>
            </div>

          </div>
   
          <div className="Comments__Action">
            <span onClick={() => handleLike(comment._id, postId)}>{ comment.likesCount } Likes</span>
            <span onClick={() => onShowReplies()}>{comment.replies.length } Replies</span>
            <p>{getDaysBetween(new Date(comment.dateCommented), new Date())}</p>
          </div>
        </div>
      </div>
      {
        showRelies &&
    <div style={{marginLeft:"60px"}}>
      {
        comment.replies &&
            comment.replies.map(({fullName,userId,avatar,reply,dateReplied,_id},idx) =>
              <div key={"jsmjms"+idx} style={{display:"flex"}}>
                <div className={avatar && "Comments__Image"} onClick={()=>history.push(`/content/profile/${userId}`)}>
                  {avatar ? <img src={avatar} />:
                    <span className="avatar-title bg-soft-primary text-primary" style={{ borderRadius: "50%", width: "30px", height: "30px", marginRight:" 20px" }}>
                      {fullName.charAt(0)}                                                                                    
                    </span>}
                </div>
                
                <div>
                  <div className="Comments__Text__Container Comments__Text">
                    {fullName}
                    <div className="Comments__Text">
                      <p>{reply}</p>
                    </div>
                    
                  </div>
                  <div className="Comments__Action">
                    {
                      userId === profile.profile.user._id && <span onClick={()=> handleDeleteReply(comment._id,postId, _id)}>Delete</span>
                    }
                    <p>{getDaysBetween(new Date(dateReplied), new Date())}</p>
                  </div>
                </div>
              </div>
                
            )
      }
      <div>
        <CommentForm commentId={comment._id} postId={postId} type='reply' fullname={comment.fullName}/>
      </div>
    </div>
      }
    </div>
  )
}

export default CommentDetail
