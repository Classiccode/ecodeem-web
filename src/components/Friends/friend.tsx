import React, { FunctionComponent } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useHistory } from "react-router-dom"
import { follow, FriendState, unfollow, RootState } from "../../store"
import { useProfile } from "../../utitly"
import Loader from "react-loader-spinner"
import "./friend.scss"

interface Notprops {
  avatar?: string
  username: string
  firstname: string
  lastname: string
  _id: string
}

const Friend = ({
  avatar,
  username,
  firstname,
  lastname,
  _id,
}: Notprops): JSX.Element => {
  const friends: FriendState = useSelector((state: RootState) => state.friends)
  const dispatch = useDispatch()
  // eslint-disable-next-line prettier/prettier
  const profile = useProfile()
  const history = useHistory()
  const checkIfFollowing = (_id) => {
    if (
      friends.following.filter(function (e) {
        return e.user._id === _id
      }).length > 0
    ) {
      return true
    }

    return false
  }

  return (
    <>
      <div className="friendBar">
        <div
          onClick={() => history.push(`/content/profile/${_id}`)}
          className="left"
        >
          {avatar ? (
            <img src={avatar} alt="" />
          ) : (
            <span
              className="avatar-title bg-soft-primary text-primary"
              style={{
                borderRadius: "50%",
                width: "40px",
                height: "40px",
                marginRight: " 20px",
              }}
            >
              {firstname.charAt(0)}
            </span>
          )}

          <p>
            @
            {username.length > 10
              ? `${username.substring(0, 10)}...`
              : username}
          </p>
          {/* <p className='ml-3'>{firstname} {lastname}</p> */}
        </div>
        <div className="right">
          {friends.status == "succeeded" ? (
            checkIfFollowing(_id) ? (
              <button onClick={() => dispatch(unfollow({ userId: _id }))}>
                {friends.followingState == "loading" ? (
                  <Loader
                    type="ThreeDots"
                    height="20px"
                    width="30px"
                    color="#FFFF"
                  />
                ) : (
                  "unfollow"
                )}
              </button>
            ) : (
              <button onClick={() => dispatch(follow({ userId: _id }))}>
                {friends.followingState == "loading" ? (
                  <Loader
                    type="ThreeDots"
                    height="20px"
                    width="30px"
                    color="#FFFF"
                  />
                ) : (
                  "follow"
                )}
              </button>
            )
          ) : (
            "error loading followers"
          )}
        </div>
      </div>
    </>
  )
}

export default Friend
