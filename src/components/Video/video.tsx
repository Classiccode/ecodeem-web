import React, { MouseEvent } from "react"
import "./video.scss"

interface IVideoprops {
  src?: string
  onClick?: (event: MouseEvent) => MouseEvent
}

const Video = ({ src, onClick }: IVideoprops): JSX.Element => {
  return (
    <video
      id=""
      className="Video"
      onClick={onClick}
      poster=""
      preload="metadata"
      controls
    >
      <source src={src} />
      Your browser does not support HTML5 video.
    </video>
  )
}

export default Video
