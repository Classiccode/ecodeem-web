import React, { FunctionComponent, MouseEvent } from "react"
import { ImagePlaceholder } from "../loaders"
import "./image.scss"

const BASE_URL =
  process.env.NODE_ENV === "development"
    ? process.env.USE_BASE_URL_ENV
    : process.env.USE_BASE_URL

interface Imageprops {
  source?: string
  interestType?: string
  onClick?: (event: MouseEvent) => MouseEvent
}

const Image = ({ source }: Imageprops): JSX.Element => {
  return (
    <>
      <div className="Image">
        <div
          style={{
            backgroundImage: `url(${source})`,
            height: "100%",
            width: "100%",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "center",
            backgroundSize: "cover",
            borderRadius: "10px",
          }}
        ></div>
      </div>
    </>
  )
}

export default Image
