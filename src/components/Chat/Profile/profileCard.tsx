import React, { FC } from "react"
import "../profileCard.scss"

type IProfileCardProps = {
  image: string
  title: string
  text?: string
  online?: boolean
}

const ProfileCard: FC<IProfileCardProps> = ({
  image,
  title,
  text,
  online,
}: any): any => {
  return (
    <>
      <div className="profileCard mb-3">
        <div className="d-flex align-items-center">
          {image ? (
            <img src={image} />
          ) : (
            <span
              className="avatar-title bg-soft-primary text-primary"
              style={{
                borderRadius: "50%",
                width: "40px",
                height: "40px",
                marginRight: " 20px",
              }}
            >
              {title.charAt(0)}
            </span>
          )}
          <span className="profileCard__text__main cursor-pointer">
            {title}
          </span>
        </div>

        <div className="profileCard__text">
          {online ? (
            <span
              style={{ backgroundColor: "green" }}
              className="red-dot"
            ></span>
          ) : (
            <span className="profileCard__text__sub">{text && ""}</span>
          )}
        </div>
      </div>
    </>
  )
}

export default ProfileCard
