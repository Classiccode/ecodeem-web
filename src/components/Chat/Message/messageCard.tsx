import React from "react"
import "./messageCard.scss"

const MessageCard = ({ image, inComing, message }: any): any => {
  return (
    <div className={`messageCard ${!inComing && "outgoing"}`}>
      {inComing && <img src={image} alt="" />}
      <div className="messageCard__message">{message}</div>
    </div>
  )
}

export default MessageCard
