import React from "react"
import { useLocation } from "react-router-dom"
import { ProductType } from "../../core/type"
import ProductItem from "./productItem"
import "./products.scss"

interface IProductProps {
  allProducts: ProductType[]
  onCartClick?: any
}
const ProductRow = ({ allProducts, onCartClick }: IProductProps) => {
  const { pathname } = useLocation()

  return (
    <>
      <div className="Products__Stack">
        {allProducts?.map((product) => (
          <ProductItem
            key={product._id}
            price={product.price}
            productName={product.name}
            productID={product._id}
            category={product.category?.title}
            img={product.image}
            rating={product.rating}
            isAuth={pathname.includes("/content")}
            onCartClick={(e: any) => onCartClick(e, product._id)}
          />
        ))}
        {/* <ProductItem
					price={allProducts[0]?.price}
					productName={allProducts[0]?.name}
					productID={allProducts[0]?._id}
					category={allProducts[0]?.category.title}
				/>
				<ProductItem
					category={allProducts[1]?.category.title}
					price={allProducts[1]?.price}
					productName={allProducts[1]?.name}
					productID={allProducts[1]?._id}
				/>
				<ProductItem
					// category="Appliances"
					// price="5000"
					// productName="Eco"
					// productID="12345"
					category={allProducts[2]?.category.title}
					price={allProducts[2]?.price}
					productName={allProducts[2]?.name}
					productID={allProducts[2]?._id}
				/> */}
      </div>
    </>
  )
}
export default ProductRow
