import "../../screens/unAuth/productDetails/productDetails.scss"
import { Button, CircularProgress } from "@material-ui/core"
import Rating from "@material-ui/lab/Rating"
import { numberWithCommas } from "../../utitly/string"
import { useState } from "react"
import { addCartItem } from "../../function"
import { toast } from "react-toastify"
import { useDispatch, useSelector } from "react-redux"
import { RootState, StoreType } from "../../store"

interface ProductInfoProps {
  name: string
  price: string | number
  description: string
  onCartClick: any
  rating: number
  brand: string
  reviews: any[]
  id?: string
}
const ProductInfo = ({
  name,
  price,
  description,
  reviews,
  onCartClick,
  rating,
  brand,
  id,
}: ProductInfoProps) => {
  const [loadingPurchase, setLoadingPurchaseState] = useState<boolean>()
  const { profile } = useSelector((state: RootState) => {
    return {
      profile: state.data.loginUserDetails,
    }
  })
  const dispatch = useDispatch()
  return (
    <>
      <div className="ProductDetails__Column__Middle__Info">
        {/* <div className="ProductDetails__Column__Middle__Info__FilterContainer">
					<img
						src={require("../../features/MarketPlace/Category/filter.svg")}
						className="ProductDetails__Column__Middle__Info__FilterContainer__Filter"
					/>
				</div> */}
        <div>
          <p className="ProductDetails__Column__Middle__Info__Name">{name}</p>
          <p className="ProductDetails__Column__Middle__Info__Price">
            ₦{numberWithCommas(price)}
          </p>
          <p style={{ marginTop: "12px" }}>
            Brand: <span style={{ color: "#2E8CCC" }}>{brand}</span>
          </p>
          <div className="ProductDetails__Column__Middle__Info__Stack">
            Ratings:{" "}
            <span>
              <Rating name="read-only" value={rating} readOnly />
            </span>
            <span>
              {" "}
              {reviews?.length === 0
                ? "(No reviews yet)"
                : `(${reviews?.length} reviews)`}
            </span>
          </div>
          {/* <p className="ProductDetails__Column__Middle__Info__Review">
						Reviews
					</p> */}
          {/* <div className="ProductDetails__Column__Middle__Info__TextBox">
						<p className="ProductDetails__Column__Middle__Info__TextBox__Product">
							Product Detail
						</p>
						<p className="ProductDetails__Column__Middle__Info__TextBox__Description">
							{description}
						</p>
					</div> */}
          <div style={{ height: "36px", marginTop: "64px" }}>
            {/* <Button
              text="Buy Now"
              type="navy"
              onClick={(e: any) => onCartClick(e)}
            /> */}
            <Button
              className="Button__navy btn"
              style={{
                backgroundColor: "#8CC64B",
                color: "white",
              }}
              onClick={async () => {
                setLoadingPurchaseState(true)
                await addCartItem({
                  productID: id ?? "",
                  userID: profile?.user._id ?? "",
                })
                  .then((_) => {
                    dispatch({
                      type: StoreType.cartCount,
                      value: _.length,
                    })
                    toast.success("Item has been added to your cart.")
                  })
                  .catch((err) => {
                    toast.error(err)
                  })
                  .finally(() => {
                    setLoadingPurchaseState(false)
                  })
              }}
            >
              {loadingPurchase ? (
                <CircularProgress color="inherit" />
              ) : (
                "Add To Cart"
              )}
            </Button>
          </div>

          {/* <div className="ProductDetails__Column__Middle__Info__SellerBox">
						<img
							src={require("./random-guy.jpg")}
							className="ProductDetails__Column__Middle__Info__SellerBox__SellerImage"
						/>
						<div className="ProductDetails__Column__Middle__Info__SellerBox__Aside">
							<div className="ProductDetails__Column__Middle__Info__SellerBox__SellerText">
								<p className="ProductDetails__Column__Middle__Info__SellerBox__SellerText__Name">
									Richard Reeds
								</p>
								<p className="ProductDetails__Column__Middle__Info__SellerBox__SellerText__Phone">
									+2347000000000
								</p>
							</div>
							<button className="ProductDetails__Column__Middle__Info__SellerBox__Button">
								Chat with seller
								<img
									src={require("./chat.svg")}
									className="ProductDetails__Column__Middle__Info__SellerBox__Button__Icon"
								/>
							</button>
						</div>
					</div> */}
        </div>
      </div>
    </>
  )
}
export default ProductInfo
