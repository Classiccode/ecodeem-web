import ProductRow from "./productRow"
import "./products.scss"
import { ProductType } from "../../core/type"
import { Link } from "react-router-dom"

interface IProductProps {
  allProducts: ProductType[]
  category: string
  id: string
  onCartClick?: any
}

const ProductHeader = ({
  allProducts,
  category,
  id,
  onCartClick,
}: IProductProps) => {
  const products = allProducts?.filter((product) => product.category?._id == id)
  return (
    <>
      {products.length > 0 ? (
        <div className="Products__ProductRow">
          <div className="Products__ProductRow__Title">
            <p className="Products__ProductRow__Title__Category">{category}</p>
            <Link to={`/marketplace/${id}`}>
              <p className="Products__ProductRow__Title__SeeAll">See All</p>
            </Link>
          </div>
          <ProductRow allProducts={products} onCartClick={onCartClick()} />
        </div>
      ) : (
        ""
      )}
    </>
  )
}
export default ProductHeader
