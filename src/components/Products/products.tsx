import "./products.scss"
import ProductHeader from "./productHeader"
import { CategoryType, ProductType } from "../../core/type"
import ProductRow from "./productRow"

interface IProductProps {
  categories: CategoryType[]
  allProducts: ProductType[]
  onCartClick?: any
}

const Products = ({ categories, allProducts, onCartClick }: IProductProps) => {
  return (
    <>
      <div className="Products">
        {categories?.map((prod) => (
          <ProductHeader
            allProducts={allProducts}
            category={prod.title}
            id={prod._id}
            key={prod._id}
            onCartClick={(e: any) => onCartClick(e, prod._id)}
          />
        ))}
      </div>
    </>
  )
}
export default Products
