/* eslint-disable indent */
import { Link } from "react-router-dom"
import { ProductItemProps } from "../../core/type"
import Rating from "@material-ui/lab/Rating"
import FavoriteBorderOutlinedIcon from "@material-ui/icons/FavoriteBorderOutlined"
import FavoriteOutlinedIcon from "@material-ui/icons/FavoriteOutlined"
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart"
import { FC, useState } from "react"
import { numberWithCommas } from "../../utitly/string"

const ProductItem: FC<ProductItemProps> = ({
  productName,
  price,
  category,
  productID,
  img,
  onCartClick,
  rating,
  isAuth = false,
}: ProductItemProps) => {
  const [like, setLike] = useState(false)
  const handleLike = (e: { preventDefault: () => void }) => {
    e.preventDefault()
    setLike(!like)
  }
  return (
    <>
      <Link
        to={
          isAuth
            ? `/content/market/${category
                .replace(/ /g, "_")
                .toLowerCase()}/${productID}`
            : `/market/${category
                .replace(/ /g, "_")
                .toLowerCase()}/${productID}`
        }
      >
        <div className="Products__Stack__Card">
          <div className="Products__Stack__Card__Item">
            <img src={img} className="Products__Stack__Card__Item__Image" />
            {/* <img src={like} className="Products__Stack__Card__Item__Heart" /> */}
            {like ? (
              <FavoriteOutlinedIcon
                fontSize="large"
                className="Products__Stack__Card__Item__Heart"
                onClick={handleLike}
              />
            ) : (
              <FavoriteBorderOutlinedIcon
                fontSize="large"
                className="Products__Stack__Card__Item__Heart"
                onClick={handleLike}
              />
            )}
          </div>
          <div>
            <p className="Products__Stack__Card__ProductName">{productName}</p>
            <div className="Products__Stack__Card__Block">
              <p className="Products__Stack__Card__Block__Price">
                ₦{numberWithCommas(price)}
              </p>
              <ShoppingCartIcon
                className="Products__Stack__Card__Block__Image"
                onClick={(e: any) => onCartClick(e)}
              />
            </div>
            <div className="Products__Stack__Card__Review">
              <Rating name="read-only" value={rating} readOnly />
            </div>
          </div>
        </div>
      </Link>
    </>
  )
}
export default ProductItem
