import { useCallback, useEffect, useState } from "react"
import { useHistory, useLocation } from "react-router-dom"
import { AnimatePresence, motion } from "framer-motion"
import { HashLoader } from "react-spinners"
import Config from "../config"
import { AxiosError } from "axios"
import { StoreType } from "../store"
import { useDispatch } from "react-redux"

export function Loader(): JSX.Element {
  const [isLoading, setLoadingState] = useState<boolean>(true)
  const { push } = useHistory()
  const { pathname } = useLocation()
  const dispatch = useDispatch()
  const fetchData = useCallback(async () => {
    const token = localStorage.getItem("token") ?? "empty"

    if (token === "empty" || pathname.includes("/auth")) {
      setTimeout(() => {
        setLoadingState(false)
      }, 1500)
    } else {
      await Config.apiMicroBlog
        .get("/user/profile/me")
        .then(() => {
          setTimeout(() => {
            setLoadingState(false)
          }, 1500)
        })
        .catch(
          ([error, error2]: [AxiosError<unknown>, AxiosError<unknown>]) => {
            if (error.response === undefined || error2.response === undefined) {
              push("/404")
              setTimeout(() => {
                setLoadingState(false)
              }, 2000)
              return
            }
            push("/auth/login")
            setTimeout(() => {
              setLoadingState(false)
            }, 2000)
          }
        )
    }
  }, [])
  useEffect(() => {
    fetchData()
  }, [fetchData])
  return (
    <AnimatePresence>
      {isLoading && (
        <motion.section
          id="loader"
          initial={{ y: 0, opacity: 1 }}
          exit={{ y: "-100vh", opacity: 0.2 }}
          transition={{ duration: 0.5, type: "spring" }}
        >
          <div className="content">
            <span className="logo" />
            <p className="txt">Loading...</p>
            <AnimatePresence>
              {isLoading && (
                <motion.div
                  className="loaderSpinner"
                  initial={{ opacity: 0, scale: 0.2 }}
                  transition={{ duration: 1 }}
                  exit={{ opacity: 0, scale: 0 }}
                  animate={{ opacity: 1, scale: 1 }}
                >
                  <HashLoader size="40px" color="#8CC64B" />
                </motion.div>
              )}
            </AnimatePresence>
          </div>
        </motion.section>
      )}
    </AnimatePresence>
  )
}
