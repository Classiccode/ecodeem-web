import React, {FC,MouseEvent} from "react";
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt';

import './horizontalist.scss';

interface IHorizontalistprops{
  title: string,
  contents: Array<any>,
  navigation: (event: MouseEvent) => void
}

const Horizontalist: FC<IHorizontalistprops> = ({title,navigation,contents}) => {
 

return(

	<div className="Horizontalist">
	   <div className="Horizontalist__Header">
			<h3>{title}</h3>
			<p onClick={navigation}><ArrowRightAltIcon fontSize="inherit" /></p>
	   </div>
	   <ul className="Horizontalist__Body">
{
contents.map((content,idx) => (
	<li key={idx} ><img src={content.avatar} /></li>
	
))
}
		
	   </ul>
	</div>
);
		  
}

export default Horizontalist;



