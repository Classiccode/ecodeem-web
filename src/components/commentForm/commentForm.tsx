import { Popper, TextField } from "@material-ui/core"
import {
  CameraAltOutlined,
  EmojiEmotionsOutlined,
  GifOutlined,
} from "@material-ui/icons"
import React, { MouseEvent, useRef, useState } from "react"
import "./commentForm.scss"
import Picker, { SKIN_TONE_MEDIUM_DARK } from "emoji-picker-react"
import { useDispatch, useSelector } from "react-redux"
import { addComment, addReply, getPost } from "../../store"
import { RootState } from "../../store"
import Loader from "react-loader-spinner"
import { useProfile } from "../../utitly"
import { useHistory } from "react-router-dom"
import SendIcon from "remixicon-react/SendPlane2LineIcon"

interface ICommentForm {
  fullname: string
  type: "comment" | "reply"
  commentId?: string
  postId?: string
}

const CommentForm = ({ type, fullname, commentId, postId }: ICommentForm) => {
  const profile = useProfile()
  const dispatch = useDispatch()
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null)
  const [chosenEmoji, setChosenEmoji] = useState(null)
  const [text, setText] = useState("")
  const inputRef = useRef<any>(null)
  const blog = useSelector((state: RootState) => state.blog)
  const history = useHistory()
  const onEmojiClick = (event, emojiObject) => {
    const cursor = inputRef.current!.selectionStart
    const newVal =
      text.slice(0, cursor) + emojiObject.emoji + text.slice(cursor)
    setText(newVal)
  }

  const handleClick = (event: MouseEvent<HTMLElement>) => {
    setAnchorEl(anchorEl ? null : event.currentTarget)
  }

  const handleSubmit = async (e) => {
    e.preventDefault()

    if (type == "comment") {
      const Apidata = {
        postId: postId,
        comment: text,
      }

      dispatch(addComment(Apidata))
    }

    if (type === "reply") {
      const Apidata = {
        postId: postId,
        commentId: commentId,
        reply: text,
      }

      dispatch(addReply(Apidata))
    }
    setText("")

    // await isDoneLoading(blog.status)
    //    if (blog.status === 'succeeded') {
    //         dispatch(getPost({token:profile.profile.token,id:postId}))
    //    }
  }

  const open = Boolean(anchorEl)
  const id = open ? "simple-popper" : undefined

  return (
    <div className="CommentForm">
      {profile.profile.user.avatar ? (
        <img
          onClick={() =>
            history.push(`/content/profile/${profile.profile.user._id}`)
          }
          className="rounded-circle"
          src={profile.profile.user.avatar}
          alt=""
        />
      ) : (
        <span
          onClick={() =>
            history.push(`/content/profile/${profile.profile.user._id}`)
          }
          className="avatar-title rounded-circle bg-soft-primary text-primary"
          style={{
            height: type == "comment" ? "3em" : "2em",
            width: type == "comment" ? "3em" : "2em",
          }}
        >
          {profile.profile.user.firstName.charAt(0)}
        </span>
      )}
      <form onSubmit={handleSubmit}>
        <input
          value={text}
          ref={inputRef}
          onChange={(e) => setText(e.target.value)}
          name=""
          placeholder={`${
            type == "comment"
              ? `Comment on ${fullname} post`
              : `Reply to ${fullname} comment`
          }`}
        />
        <ul>
          {blog.status === "loading" && (
            <Loader type="BallTriangle" color="black" height={30} width={30} />
          )}
          <li className="emoji-btn" aria-describedby={id} onClick={handleClick}>
            <EmojiEmotionsOutlined />
          </li>
          <li className="send" aria-describedby={id} onClick={handleSubmit}>
            <SendIcon />
          </li>
        </ul>
        <Popper
          className="bg-color2"
          style={{ zIndex: 2 }}
          id={id}
          open={open}
          anchorEl={anchorEl}
        >
          <Picker
            onEmojiClick={onEmojiClick}
            disableAutoFocus={true}
            skinTone={SKIN_TONE_MEDIUM_DARK}
            groupNames={{ smileys_people: "PEOPLE" }}
            native
          />
        </Popper>
      </form>
    </div>
  )
}

export default CommentForm
