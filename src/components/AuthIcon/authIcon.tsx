import React from "react"
import { useProfile } from "../../utitly"
import "./authIcon.scss"

interface IProps {
  width: string
  heigth: string
}

const AuthIcon = ({ width, heigth }: IProps): JSX.Element => {
  const profile = useProfile()
  return (
    <img
      style={{ width: width, height: heigth }}
      className="authIcon"
      src={profile?.profile?.user?.avatar ?? ""}
      alt=""
    />
  )
}

export default AuthIcon
