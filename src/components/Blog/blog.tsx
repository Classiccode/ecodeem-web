/* eslint-disable indent */
import { useState, MouseEvent, ChangeEvent, FormEvent } from "react"
import ShareOutlinedIcon from "@material-ui/icons/ShareOutlined"
import { Link, useHistory, useParams } from "react-router-dom"
import BlogProvider from "./provider"
import { PostType } from "../../core/type"
import { EscapeContent } from "../Escape"
import Video from "../Video/video"
import Image from "../Image/image"
import Card from "../Card/card"
import { ImagePlaceholder } from "../loaders"
import { checkMediaType } from "../../utitly"
import "./blog.scss"
import { useDispatch, useSelector } from "react-redux"
import {
  getInterestedDeems,
  likeDeem,
  removePost,
  sharePost,
  unfollow,
  unlikeDeem,
} from "../../store/slice"
import { RootState } from "../../store"
import {
  InputAdornment,
  Menu,
  MenuItem,
  Modal,
  TextareaAutosize,
  TextField,
} from "@material-ui/core"
import { makeStyles } from "@material-ui/core/styles"
import Button from "../Button/button"
import {
  ThumbUpAltOutlined,
  MonetizationOnOutlined,
  ThumbUp,
} from "@material-ui/icons"
import ProfileCard from "../Chat/Profile/profileCard"
import { useEffect } from "react"
import axios from "axios"
import { follow } from "../../store/slice"
import Config from "../../config"
import { useProfile } from "../../utitly/hooks"

import ChatIcon from "remixicon-react/Message3LineIcon"
import CloseEdit from "remixicon-react/CloseCircleLineIcon"
import ForwardIcon from "remixicon-react/ShareForwardLineIcon"
import Swal from "sweetalert2"
import { toast } from "react-toastify"
import { CopyToClipboard } from "react-copy-to-clipboard"
import HighlightOffIcon from "@material-ui/icons/HighlightOff"
import moment from "moment"

interface IBlogprops {
  Deem: PostType
  attributes?: "full" | "light"
  global: boolean
}

const useStyles = makeStyles((theme) => ({
  root: {
    padding: "2px 4px",
    display: "flex",
    alignItems: "center",
    width: "100%",
    marginTop: "30px",
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
    fontSize: "20px",
    width: "80%",
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
  noBorder: {
    border: "none",
  },
  padding: {
    padding: "13px 5px !important",
  },
}))

const Blog = ({ Deem, global = false }: IBlogprops): JSX.Element => {
  const { donating } = useSelector((state: RootState) => state.deem)
  const { following } = useSelector((state: RootState) => state.friends)
  const [edit, setEdit] = useState(false)
  const { profile } = useProfile()
  const dispatch = useDispatch()
  const classes = useStyles()
  const [amount, setAmount] = useState<number>()
  const [open, setOpen] = useState(false)
  const [loadingFund, setLoadingFund] = useState(true)
  const [donations, setDonations] = useState<Array<any>>([])
  const [min, setMin] = useState(Deem.fundRaise?.minAmount)
  const [max, setMax] = useState(Deem.fundRaise?.maxAmount)
  const [target, setTarget] = useState(Deem.fundRaise?.endDate)
  const [message, setMesage] = useState(Deem.postDesc)
  const [counter, setCounter] = useState(Deem.postDesc.length)
  const [loading, setLoading] = useState(false)
  const [like, setLike] = useState(
    Deem.likes?.find((userID: string) => {
      return userID === profile.user._id
    }) !== undefined
  )
  const [likeCoun, setLikeCoun] = useState(Deem?.counts?.likesCount)
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null)
  const [anchorElShare, setAnchorElShare] = useState<null | HTMLElement>(null)
  const { push } = useHistory()

  const likePost = () => {
    setLike(true)
    setLikeCoun(1)
  }

  const unlikePost = () => {
    setLike(false)
    setLikeCoun(0)
  }

  const { isComment } = useParams<Record<string, string | undefined>>()
  const checkIfFollowing = (_id) => {
    if (
      following.filter(function (e) {
        return e.user._id === _id
      }).length > 0
    ) {
      return true
    }

    return false
  }
  useEffect(() => {
    getFunding()
  }, [Deem])

  const deletePost = async (event: MouseEvent, post: PostType) => {
    event.preventDefault()

    const resp = await axios.delete(
      Config.apiUrl + "/post/delete/" + post._id,
      {
        data: {},
      }
    )
    return resp
  }

  const getFunding = async () => {
    const result = await axios.get(
      Config.apiUrl + "/donation/donors-post?postId=" + Deem._id
    )
    setLoadingFund(false)
    setDonations([...result.data])
  }

  const handleOpen = () => {
    setOpen(true)
  }

  const handleEditOpen = () => {
    setEdit(true)
  }

  const handleClickMenu = (event: MouseEvent<HTMLDivElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClickShare = (event: MouseEvent<HTMLTableElement>) => {
    setAnchorElShare(event.currentTarget)
  }

  const handleCloseMenu = () => {
    setAnchorEl(null)
  }

  const handleCloseShare = () => {
    setAnchorElShare(null)
  }

  const toggleOpen = () => {
    setOpen(!open)
  }

  const handleClose = () => {
    setOpen(false)
  }

  const handleEditClose = () => {
    setEdit(false)
  }

  const removeMedia = () => {
    console.log("remove")
  }

  const handleChange = (
    event: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
  ): void => {
    event.preventDefault()

    const { name, value } = event.target

    switch (name) {
      case "message":
        value.length <= 700 && setMesage(value || "")
        value.length <= 700 && setCounter(value.length)
        break
      case "min":
        setMin(Number(value) || undefined)
        break
      case "max":
        setMax(Number(value) || undefined)
        break
      case "target":
        setTarget(value || "")
        break
    }
  }
  let data = {}
  const handleEditPost = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    setLoading(true)
    if (Deem.postType == "raiseFund") {
      data = {
        postDesc: message,
        fundRaise: {
          minAmount: min,
          maxAmount: max,
          endDate: target,
        },
      }
    } else {
      data = {
        postDesc: message,
      }
    }
    try {
      await axios.put(`${Config.apiUrl}/post/edit/${Deem._id}`, data)
      dispatch(getInterestedDeems())
      setLoading(false)
      toast.success("Post edited", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      })
    } catch (error) {
      setLoading(false)
      toast.error("We can't edit your post at this moment", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      })
    }
  }
  return (
    <BlogProvider>
      {({ input, handlePayment, toggleAmountInput }) => (
        <>
          <div className="Blog">
            <Modal
              open={open}
              onClose={handleClose}
              aria-labelledby="simple-modal-title"
              aria-describedby="simple-modal-description"
            >
              <div className="h-100">
                <Card rounded className="Blog__Modal">
                  <div className="d-flex justify-content-between mb-3">
                    <h4>Donors</h4>
                    <CloseEdit
                      onClick={() => {
                        handleClose()
                      }}
                      className="cursor-pointer"
                    />
                  </div>
                  <div className="Blog__Modal__Header">
                    <ProfileCard
                      image={profile.user.avatar}
                      title={`${profile.user.firstName} ${profile.user.lastName}`}
                    />
                  </div>

                  <div className="Blog__Modal__Body">
                    <ul>
                      {loadingFund ? (
                        "loading..."
                      ) : donations.length < 1 ? (
                        <p>No donations yet</p>
                      ) : (
                        donations.map(
                          ({ _id, amountDonated, avatar, fullName }) => (
                            <li key={`skxss${_id}`}>
                              <ProfileCard image={avatar} title={fullName} />
                              <p>$+{amountDonated}</p>
                            </li>
                          )
                        )
                      )}
                    </ul>
                  </div>
                  <div className="Blog__Modal__Footer">
                    <TextField
                      fullWidth
                      placeholder="Enter amount you want to donate"
                      required
                      variant="outlined"
                      onChange={(e) => setAmount(Number(e.target.value))}
                      type="number"
                      style={{
                        backgroundColor: "#DEF4C5",
                        border: "none",
                        borderRadius: "10px",
                        color: "white",
                        paddingTop: "0px",
                      }}
                      InputProps={{
                        "aria-label": "naked",
                        classes: {
                          notchedOutline: classes.noBorder,
                          input: classes.padding,
                        },
                        startAdornment: (
                          <InputAdornment position="start">
                            <MonetizationOnOutlined />
                          </InputAdornment>
                        ),
                      }}
                    />
                    {donating == "loading" ? (
                      <Button text="loading..." disable type="dark" />
                    ) : amount ? (
                      <Button
                        text="Donate"
                        onClick={(e) => handlePayment(e, amount, Deem._id)}
                        type="dark"
                      />
                    ) : (
                      <div>
                        <Button text="Amount" disable type="dark" />
                      </div>
                    )}
                  </div>
                </Card>
              </div>
            </Modal>
            <Modal
              open={edit}
              onClose={handleEditOpen}
              aria-labelledby="simple-modal-title"
              aria-describedby="simple-modal-description"
            >
              <div className="h-100">
                <Card rounded className="Blog__Modal">
                  <form onSubmit={(event) => handleEditPost(event)}>
                    <div className="d-flex justify-content-between mb-3">
                      <h4>Edit Post</h4>
                      <CloseEdit
                        onClick={() => {
                          handleEditClose()
                        }}
                        className="cursor-pointer"
                      />
                    </div>

                    <div className="Blog__Modal__Header">
                      <ProfileCard
                        image={profile.user.avatar}
                        title={`${profile.user.firstName} ${profile.user.lastName}`}
                      />
                    </div>

                    <div className="Post__Wrapper__Content__scroll">
                      <TextareaAutosize
                        className="bg-color w-100 text-light p-2 mb-3"
                        style={{
                          borderRadius: "10px",
                        }}
                        rowsMin={4}
                        rowsMax={7}
                        maxLength={350}
                        name="message"
                        onChange={handleChange}
                        defaultValue={message}
                      />
                      <div className="Post__Wrapper__Content__Footer__Counter">
                        {counter}/350
                      </div>
                      {/* {Deem.postType == "image" && (
                      <div className="Post__Wrapper__Content__Footer__Media">
                        <span
                          onClick={removeMedia}
                          className="Post__Wrapper__Content__Footer__Media__Close"
                        >
                          <HighlightOffIcon fontSize="inherit" />
                        </span>
                        <img src={Deem.mediaUrl} />
                      </div>
                    )}

                    {Deem.postType == "video" && (
                      <div className="Post__Wrapper__Content__Footer__Media">
                        <span
                          onClick={removeMedia}
                          className="Post__Wrapper__Content__Footer__Media__Close"
                        >
                          <HighlightOffIcon fontSize="inherit" />
                        </span>

                        <div className="Post__Wrapper__Content__Footer__Media__Video">
                          <Video src={Deem.mediaUrl} />
                        </div>
                      </div>
                    )}
                    {Deem.postType == "raiseFund" &&
                    checkMediaType(Deem.mediaUrl) == "image" ? (
                      <div className="Post__Wrapper__Content__Footer__Media">
                        <span
                          onClick={removeMedia}
                          className="Post__Wrapper__Content__Footer__Media__Close"
                        >
                          <HighlightOffIcon fontSize="inherit" />
                        </span>
                        <img src={Deem.mediaUrl} />
                      </div>
                    ) : checkMediaType(Deem.mediaUrl) == "video" ? (
                      <div className="Post__Wrapper__Content__Footer__Media">
                        <span
                          onClick={removeMedia}
                          className="Post__Wrapper__Content__Footer__Media__Close"
                        >
                          <HighlightOffIcon fontSize="inherit" />
                        </span>

                        <div className="Post__Wrapper__Content__Footer__Media__Video">
                          <Video src={Deem.mediaUrl} />
                        </div>
                      </div>
                    ) : (
                      ""
                    )} */}
                      {Deem.postType == "raiseFund" && (
                        <div className="Post__Wrapper__Picker">
                          <div className="Post__Wrapper__Picker__Content">
                            <p>Edit your funding goals</p>
                            <section>
                              <div className="Post__Wrapper__Picker__Content__Amount">
                                <input
                                  id="min-amount"
                                  required={Deem.postType == "raiseFund"}
                                  className="Post__Wrapper__Picker__Content__Input bg-color font-color"
                                  type="number"
                                  name="min"
                                  onChange={handleChange}
                                  placeholder="Min"
                                  value={min}
                                />
                              </div>
                              <div className="Post__Wrapper__Picker__Content__Amount">
                                <input
                                  id="max-amount"
                                  required={Deem.postType == "raiseFund"}
                                  className="Post__Wrapper__Picker__Content__Input bg-color font-color"
                                  type="number"
                                  name="max"
                                  onChange={handleChange}
                                  placeholder="Max"
                                  value={max}
                                />
                              </div>
                              <div
                                className="Post__Wrapper__Picker__Content__Target"
                                style={{
                                  marginTop: "22px",
                                }}
                              >
                                <input
                                  id="target"
                                  className="Post__Wrapper__Picker__Content__Input bg-color font-color"
                                  type="date"
                                  name="target"
                                  required={Deem.postType == "raiseFund"}
                                  onChange={handleChange}
                                  placeholder="Funding target (e.g 30 days)"
                                  value={moment(target)
                                    .format("YYYY-MM-DD")
                                    .toString()}
                                />
                              </div>
                            </section>
                          </div>
                        </div>
                      )}
                    </div>
                    <div className="text-center">
                      {loading ? (
                        <Button text="loading..." disable type="dark" />
                      ) : (
                        <Button text="Save" submit="submit" type="dark" />
                      )}
                    </div>
                  </form>
                </Card>
              </div>
            </Modal>

            <div className="Blog__Header">
              <Link
                to={`/content/profile/${Deem.postedBy._id}`}
                style={{ width: "100%" }}
              >
                <table>
                  <tbody>
                    <tr>
                      <td style={{ width: "50px" }}>
                        {Deem.postedBy &&
                          (Deem.postedBy.avatar ? (
                            <div className="Blog__Header__Image">
                              <img
                                className="rounded-circle"
                                src={Deem.postedBy.avatar}
                                alt=""
                              />{" "}
                            </div>
                          ) : (
                            <span
                              className="avatar-title rounded-circle bg-soft-primary text-primary"
                              style={{ height: "3em", width: "3em" }}
                            >
                              {profile.user.firstName.charAt(0)}
                            </span>
                          ))}
                      </td>
                      <td>
                        <div className="Blog__Header__Text">
                          {`${Deem.postedBy.username}`} <br />
                          <span>{Deem.interest.interest}</span>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </Link>
              {/* <div
                onClick={() => ToggleCensor()}
                className="Blog__Header__More"
              >
                <i
                  style={{ fontSize: "20px" }}
                  className="fa fa-ellipsis-h"
                ></i>
              </div> */}
              <div
                aria-controls="simple-menu"
                aria-haspopup="true"
                onClick={handleClickMenu}
              >
                <i
                  style={{ fontSize: "20px" }}
                  className="fa fa-ellipsis-h"
                ></i>
              </div>
              {Deem.postedBy._id == profile.user._id ? (
                <Menu
                  id="simple-menu"
                  anchorEl={anchorEl}
                  keepMounted
                  open={Boolean(anchorEl)}
                  onClose={handleCloseMenu}
                >
                  <MenuItem
                    onClick={(e: any) => {
                      handleCloseMenu()
                      Swal.fire({
                        text: "Are you sure you want to delete this post",
                        showCancelButton: true,
                        focusConfirm: false,
                        background: "var(--background)",
                        customClass: {
                          confirmButton: "sweet-btn", //insert class here
                          cancelButton: "sweet-btn",
                          popup: "sweet-btn-rounded",
                        },
                      }).then((willDelete) => {
                        if (willDelete.isConfirmed) {
                          deletePost(e, Deem)
                            .then((data) => {
                              dispatch(removePost(Deem))
                              toast.success(
                                "Poof! Your post has been deleted!",
                                {
                                  position: "top-right",
                                  autoClose: 5000,
                                  hideProgressBar: false,
                                  closeOnClick: true,
                                  pauseOnHover: true,
                                  draggable: true,
                                  progress: undefined,
                                }
                              )
                            })
                            .catch((error) => {
                              toast.error(error, {
                                position: "top-right",
                                autoClose: 5000,
                                hideProgressBar: false,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true,
                                progress: undefined,
                              })
                            })
                        }
                      })
                    }}
                  >
                    Delete Post
                  </MenuItem>
                  <MenuItem
                    onClick={() => {
                      handleCloseMenu()
                      handleEditOpen()
                    }}
                  >
                    Edit Post
                  </MenuItem>
                  {Deem.postType === "raiseFund" && (
                    <MenuItem
                      onClick={() => {
                        handleCloseMenu()
                        toggleOpen()
                      }}
                    >
                      View Fund
                    </MenuItem>
                  )}
                </Menu>
              ) : (
                <Menu
                  id="simple-menu"
                  anchorEl={anchorEl}
                  keepMounted
                  open={Boolean(anchorEl)}
                  onClose={handleCloseMenu}
                >
                  {checkIfFollowing(Deem.postedBy._id) ? (
                    <MenuItem
                      onClick={() => {
                        handleCloseMenu()
                        Swal.fire({
                          text: `Are you sure you want to unfollow ${
                            Deem.postedBy.username ?? ""
                          }`,
                          showCancelButton: true,
                          background: "var(--background)",
                          focusConfirm: false,
                          customClass: {
                            confirmButton: "sweet-btn", //insert class here
                            cancelButton: "sweet-btn",
                            popup: "sweet-btn-rounded",
                          },
                        }).then((result) => {
                          if (result.isConfirmed) {
                            dispatch(unfollow({ userId: Deem.postedBy._id }))
                          }
                        })
                      }}
                    >
                      unfollow
                    </MenuItem>
                  ) : (
                    <MenuItem
                      onClick={() => {
                        handleCloseMenu()
                        Swal.fire({
                          text: `Are you sure you want to followw ${
                            Deem.postedBy.username ?? ""
                          }`,
                          background: "var(--background)",
                          showCancelButton: true,
                          focusConfirm: false,
                          customClass: {
                            confirmButton: "sweet-btn", //insert class here
                            cancelButton: "sweet-btn",
                            popup: "sweet-btn-rounded",
                          },
                        }).then((result) => {
                          if (result.isConfirmed) {
                            dispatch(follow({ userId: Deem.postedBy._id }))
                          }
                        })
                      }}
                    >
                      Follow
                    </MenuItem>
                  )}

                  {Deem.postType === "raiseFund" && (
                    <MenuItem
                      onClick={() => {
                        handleCloseMenu()
                        toggleOpen()
                      }}
                    >
                      View Fund
                    </MenuItem>
                  )}
                </Menu>
              )}
            </div>
            {Deem.postType == "image" && (
              <Link
                to={{
                  pathname: `${
                    global
                      ? `/preview/${Deem._id}/photo`
                      : `/content/post/${Deem._id}`
                  }`,
                  state: { modal: true },
                }}
              >
                <div className="Blog__Media">
                  <img height="100%" width="100%" src={Deem.mediaUrl} alt="" />
                  {/* <Image
                    source={Deem.mediaUrl}
                    interestType={Deem.interest.interest}
                  /> */}
                </div>
              </Link>
            )}
            {Deem.postType == "raiseFund" &&
            checkMediaType(Deem.mediaUrl) == "image" ? (
              <Link
                to={{
                  pathname: `${
                    global
                      ? `/preview/${Deem._id}/photo`
                      : `/content/post/${Deem._id}`
                  }`,
                  state: { modal: true },
                }}
              >
                <div className="Blog__Media">
                  <img height="100%" width="100%" src={Deem.mediaUrl} alt="" />
                  {/* <Image
                    source={Deem.mediaUrl}
                    interestType={Deem.interest.interest}
                  /> */}
                </div>
              </Link>
            ) : checkMediaType(Deem.mediaUrl) == "video" ? (
              <div className="Blog__Media">
                <Video src={Deem.mediaUrl} />
              </div>
            ) : (
              ""
            )}
            <Link className="font-color" to={`/content/post/${Deem._id}`}>
              <div className="Blog__Description">{Deem.postDesc}</div>
            </Link>

            <div className="Blog__Reactions">
              {global ? (
                <table>
                  <tbody>
                    <tr className="text-center">{Deem?.counts?.likesCount}</tr>
                    <tr className="hover">
                      {Deem?.likes &&
                      Deem.likes.filter(function (e) {
                        return e === profile.user._id
                      }).length > 0 ? (
                        <>
                          <td>
                            <div
                              onClick={() =>
                                dispatch(
                                  unlikeDeem({
                                    postId: Deem._id,
                                    token: profile.token,
                                    userId: profile.user._id,
                                  })
                                )
                              }
                              className="Blog__Reactions__Icon"
                            >
                              {/* <FavoriteBorderRoundedIcon fontSize='inherit' /> */}
                              <ThumbUp
                                style={{
                                  fontSize: "20px",
                                  color: "rgb(224, 36, 94)",
                                }}
                              />
                            </div>
                          </td>
                          <td>
                            <div className="Blog__Reactions__Text">likes</div>
                          </td>
                        </>
                      ) : (
                        <>
                          <td>
                            <div
                              onClick={() =>
                                dispatch(
                                  likeDeem({
                                    postId: Deem._id,
                                    token: profile.token,
                                    userId: profile.user._id,
                                  })
                                )
                              }
                              className="Blog__Reactions__Icon"
                            >
                              {/* <FavoriteBorderRoundedIcon fontSize='inherit' /> */}
                              <ThumbUpAltOutlined
                                style={{ fontSize: "20px" }}
                              />
                            </div>
                          </td>
                          <td>
                            <div className="Blog__Reactions__Text">likes</div>
                          </td>
                        </>
                      )}
                    </tr>
                  </tbody>
                </table>
              ) : (
                <table>
                  <tbody>
                    <tr className="text-center">{likeCoun}</tr>
                    <tr className="hover">
                      <td>
                        {like ? (
                          <div
                            onClick={() => {
                              dispatch(
                                unlikeDeem({
                                  postId: Deem._id,
                                  token: profile.token,
                                  userId: profile.user._id,
                                })
                              )
                              unlikePost()
                            }}
                            className="Blog__Reactions__Icon"
                          >
                            {/* <FavoriteBorderRoundedIcon fontSize='inherit' /> */}
                            <ThumbUp
                              style={{
                                fontSize: "20px",
                                color: "rgb(224, 36, 94)",
                              }}
                            />
                          </div>
                        ) : (
                          <div
                            onClick={() => {
                              dispatch(
                                likeDeem({
                                  postId: Deem._id,
                                  token: profile.token,
                                  userId: profile.user._id,
                                })
                              )
                              likePost()
                            }}
                            className="Blog__Reactions__Icon"
                          >
                            {/* <FavoriteBorderRoundedIcon fontSize='inherit' /> */}
                            <ThumbUpAltOutlined style={{ fontSize: "20px" }} />
                          </div>
                        )}
                      </td>
                      <td>
                        <div className="Blog__Reactions__Text">likes</div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              )}

              <table>
                <tbody onClick={() => push(`/content/post/${Deem._id}`)}>
                  <tr className="text-center">{Deem?.counts?.commentCount}</tr>
                  <tr className="hover">
                    <td>
                      {
                        <div className="Blog__Reactions__Icon">
                          {/* <CommentOutlinedIcon fontSize='inherit' color='inherit' /> */}
                          <ChatIcon />
                        </div>
                      }
                    </td>
                    <td>
                      <div className="Blog__Reactions__Text">comment</div>
                    </td>
                  </tr>
                </tbody>
              </table>
              {
                <table
                  aria-controls="simple-menu"
                  aria-haspopup="true"
                  onClick={handleClickShare}
                >
                  <tbody>
                    <tr className="text-center">{Deem?.counts?.shareCount}</tr>
                    <tr className="hover">
                      <td>
                        <div className="Blog__Reactions__Icon">
                          <ShareOutlinedIcon fontSize="inherit" />
                        </div>
                      </td>
                      <td>
                        <div className="Blog__Reactions__Text">share</div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              }
              <Menu
                id="simple-menu"
                anchorEl={anchorElShare}
                keepMounted
                open={Boolean(anchorElShare)}
                onClose={handleCloseShare}
              >
                {Deem.postedBy._id !== profile.user._id && (
                  <MenuItem
                    onClick={() => {
                      handleCloseShare()
                      Swal.fire({
                        text: "Are you sure you want to share the post",
                        showCancelButton: true,
                        showConfirmButton: true,
                        showCloseButton: false,
                        background: "var(--background)",
                        customClass: {
                          confirmButton: "sweet-btn", //insert class here
                          cancelButton: "sweet-btn",
                          popup: "sweet-btn-rounded",
                        },
                      }).then((result) => {
                        if (result.isConfirmed) {
                          dispatch(
                            sharePost({
                              postId: Deem._id,
                            })
                          )
                        }
                      })
                    }}
                  >
                    Share to timeline
                  </MenuItem>
                )}
                {navigator.share !== undefined && (
                  <MenuItem
                    onClick={() => {
                      handleCloseShare()
                      if (navigator.share != undefined) {
                        navigator.share({
                          title: "Hello check out my post on Ecodeem",
                          text: "Hello check out my post on Ecodeem",
                          url: `${Config.baseURL}/content/post/${Deem._id}`,
                        })
                      }
                    }}
                  >
                    Share to other apps
                  </MenuItem>
                )}
                <MenuItem onClick={() => handleCloseShare()}>
                  <CopyToClipboard
                    text={`${Config.baseURL}/content/post/${Deem._id}`}
                    onCopy={() => setAmount(22)}
                  >
                    <span>Copy link</span>
                  </CopyToClipboard>
                </MenuItem>
              </Menu>

              {Deem.postType == "raiseFund" && (
                <table onClick={handleOpen}>
                  <tbody>
                    <tr className="text-center">{Deem?.counts?.donorsCount}</tr>
                    <tr>
                      <td>
                        {
                          <div className="Blog__Reactions__Icon">
                            <MonetizationOnOutlined
                              fontSize="inherit"
                              color="inherit"
                            />
                          </div>
                        }
                      </td>
                      <td>
                        <div className="Blog__Reactions__Text">donor</div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              )}
            </div>

            {Deem.postType == "raiseFund" && (
              <div className="Blog__Funding">
                <div className="Blog__Funding__Progress">
                  <div
                    style={{ width: `${Deem?.fundRaise?.donationPercentage}%` }}
                    className="Blog__Funding__Progress__Inner"
                  ></div>
                </div>
                <div className="Blog__Funding__Info">
                  <div className="font-color">
                    Raised{" "}
                    {(Deem!.fundRaise!.donationPercentage! / 100) *
                      Deem!.fundRaise!.maxAmount!}{" "}
                    dollars of {Deem?.fundRaise?.maxAmount}
                  </div>
                  {Deem?.fundRaise?.donationPercentage == 100 ? (
                    <div className="Blog__Funding__Button-Inactive">
                      <table>
                        <tbody>
                          <tr>
                            <td style={{ width: "20px" }}></td>
                            <td>Fund Closed</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  ) : (
                    <Button
                      type="dark"
                      onClick={() => toggleAmountInput()}
                      className="Blog__Funding__Button-Active"
                      text="Donate"
                    />
                  )}
                </div>
              </div>
            )}

            {input.openAmountInpput && (
              <div className={classes.root}>
                <TextField
                  fullWidth
                  placeholder="Enter amount you want to donate"
                  required
                  variant="outlined"
                  onChange={(e) => setAmount(Number(e.target.value))}
                  type="number"
                  style={{
                    backgroundColor: "#DEF4C5",
                    border: "none",
                    borderRadius: "10px",
                    color: "white",
                  }}
                  InputProps={{
                    "aria-label": "naked",
                    classes: {
                      notchedOutline: classes.noBorder,
                      input: classes.padding,
                    },
                    startAdornment: (
                      <InputAdornment position="start">
                        <MonetizationOnOutlined />
                      </InputAdornment>
                    ),
                  }}
                />

                {donating == "loading" ? (
                  <Button text="loading..." disable type="dark" />
                ) : amount ? (
                  <Button
                    text="Donate"
                    onClick={(e) => handlePayment(e, amount, Deem._id)}
                    type="dark"
                  />
                ) : (
                  <div>
                    <Button text="Amount" disable type="dark" />
                  </div>
                )}
              </div>
            )}
          </div>
        </>
      )}
    </BlogProvider>
  )
}

export default Blog
