import { useState, MouseEvent, ReactNode } from "react"
import axios from "axios"

import { PostType } from "../../core/type"
import { useDispatch, useSelector } from "react-redux"
import { donate, RootState } from "../../store"
import { useProfile } from "../../utitly/hooks"
import Config from "../../config"

type Inputprops = {
  like: boolean
  save: boolean
  censor: boolean
  openAmountInpput: boolean
}
type ChildrenProps = {
  input: Inputprops
  toogleCensor: (event: MouseEvent) => void
  deletePost: (event: MouseEvent, post: PostType) => Promise<any>
  handlePayment: (event: MouseEvent, amount: number, postId: string) => void
  toggleAmountInput: () => void
}

interface IBlogprops {
  children(_: ChildrenProps): ReactNode
}

const BlogProvider = ({ children }: IBlogprops): JSX.Element => {
  const dispatch2 = useDispatch()
  const [like, setLike] = useState(false)
  const [save, setSave] = useState(false)
  const [censor, setCensor] = useState(false)
  const { profile } = useProfile()
  const { donating, action } = useSelector((state: RootState) => state.deem)
  const [openAmountInpput, setOpenAmountInpput] = useState(false)

  const toogleCensor = (event: MouseEvent) => {
    event.preventDefault()
    setCensor(!censor)
  }

  const toggleAmountInput = () => {
    setOpenAmountInpput(!openAmountInpput)
  }

  const deletePost = async (event: MouseEvent, post: PostType) => {
    event.preventDefault()

    const config = {
      headers: { Authorization: `Bearer ${profile.token}` },
    }

    const resp = await axios.delete(
      Config.apiUrl + "/post/delete/" + post._id,
      config
    )

    return resp
  }

  const handlePayment = async (
    event: MouseEvent,
    amount: number,
    postId: string
  ) => {
    event.preventDefault()
    dispatch2(donate({ postId: postId, amount: amount }))
  }

  const input = {
    like,
    save,
    censor,
    openAmountInpput,
  }

  return (
    <>
      {children({
        input,
        toogleCensor,
        deletePost,
        handlePayment,
        toggleAmountInput,
      })}
    </>
  )
}

export default BlogProvider
