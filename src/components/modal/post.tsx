import { Button, TextField } from "@material-ui/core"
import { useState } from "react"
import { ModalLayout } from "../../layout"
import ImageIcon from "remixicon-react/ImageLineIcon"
import VideoIcon from "remixicon-react/VidiconLineIcon"
import StoryIcon from "remixicon-react/BookletLineIcon"
import LiveIcon from "remixicon-react/LiveLineIcon"
import { AnimatePresence, motion } from "framer-motion"

export function PostModal(): JSX.Element {
  const [isPlainPost, setPostType] = useState<boolean>(true)
  const [postDesc, setPostDesc] = useState<string>("")
  const [minAmount, setMinAmount] = useState<string>("")
  const [maxAmount, setMaxAmount] = useState<string>("")
  return (
    <ModalLayout title="Create Post">
      <>
        <div className="user_detail">
          <span className="avatar" />
          <h5>Amos Daniel</h5>
        </div>
        <div className="post_picker">
          <span
            className={isPlainPost ? "indicator plain" : "indicator"}
          ></span>
          <span
            className={isPlainPost ? "post_type active" : "post_type"}
            onClick={() => {
              setPostType(true)
            }}
          >
            Make your move
          </span>
          <span
            className={!isPlainPost ? "post_type active" : "post_type"}
            onClick={() => {
              setPostType(false)
            }}
          >
            Raise fund
          </span>
        </div>
        <TextField
          multiline
          placeholder="What’s on your mind? John."
          variant="outlined"
          className="inputBox"
          minRows={7}
          maxRows={10}
          onChange={(e) => {
            setPostDesc(e.target.value)
          }}
          type="text"
          value={postDesc}
        />
        <AnimatePresence>
          {isPlainPost && (
            <motion.div
              exit={{ y: 100, opacity: 0 }}
              initial={{ y: 100, opacity: 0 }}
              animate={{ y: 0, opacity: 1 }}
              className="act"
              style={{
                width: "100%",
                display: "flex",
                flexDirection: "column",
              }}
            >
              <div className="action">
                <p className="txt">Attach:</p>
                <div className="action-layer">
                  <Button className="btn">
                    <ImageIcon /> Photo
                  </Button>
                  <Button className="btn">
                    <VideoIcon /> Video
                  </Button>
                  <Button className="btn">
                    <LiveIcon /> Live
                  </Button>
                  <Button className="btn">
                    <StoryIcon /> Story
                  </Button>
                </div>
              </div>
              <Button className="submit_btn">POST</Button>
            </motion.div>
          )}
        </AnimatePresence>

        <AnimatePresence>
          {!isPlainPost && (
            <motion.div
              exit={{ y: 100, opacity: 0 }}
              initial={{ y: 100, opacity: 0 }}
              animate={{ y: 0, opacity: 1 }}
              style={{
                width: "100%",
                display: "flex",
                flexDirection: "column",
              }}
            >
              <div className="action">
                <p className="txt">
                  Fund
                  <br />
                  Target:
                </p>
                <div className="action-layer">
                  <div className="col">
                    <TextField
                      placeholder="0.0"
                      type="text"
                      value={minAmount}
                      variant="outlined"
                      className="inputBox num"
                      onChange={(e) => {
                        if (/^[0-9]*$/.test(e.target.value)) {
                          setMinAmount(e.target.value)
                        }
                      }}
                    />
                    <span className="name">min</span>
                  </div>
                  <div className="col">
                    <TextField
                      placeholder="0.0"
                      type="text"
                      value={maxAmount}
                      variant="outlined"
                      className="inputBox num"
                      onChange={(e) => {
                        if (/^[0-9]*$/.test(e.target.value)) {
                          setMaxAmount(e.target.value)
                        }
                      }}
                    />
                    <span className="name">max</span>
                  </div>
                </div>
              </div>
              <div className="action">
                <p className="txt">Attach:</p>
                <div className="action-layer">
                  <Button className="btn">
                    <ImageIcon /> Photo
                  </Button>
                  <Button className="btn">
                    <VideoIcon /> Video
                  </Button>
                  <Button className="btn">
                    <LiveIcon /> Live
                  </Button>
                  <Button className="btn">
                    <StoryIcon /> Story
                  </Button>
                </div>
              </div>
              <Button className="submit_btn">POST</Button>
            </motion.div>
          )}
        </AnimatePresence>
      </>
    </ModalLayout>
  )
}
