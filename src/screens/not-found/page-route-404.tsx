import { motion } from "framer-motion"
import { Button } from "@material-ui/core"

export function Page404(): JSX.Element {
  return (
    <>
      <motion.section
        id="loader"
        initial={{ y: 0, opacity: 1 }}
        exit={{ y: "-100vh", opacity: 0.2 }}
        transition={{ duration: 0.5, type: "spring" }}
      >
        <div className="content error-404">
          <span className="illustrator" />
          <p className="txt">This page could not be found</p>
          <Button
            className="btn"
            onClick={() => {
              window.location.assign("/auth/login")
            }}
          >
            Go back
          </Button>
        </div>
      </motion.section>
    </>
  )
}
