import {
  TextField,
  InputAdornment,
  IconButton,
  Button,
  AppBar,
  Tabs,
  Tab,
  Typography,
} from "@material-ui/core"

import { AuthPageLayout } from "../../layout/auth"
import PhoneIcon from "remixicon-react/PhoneLineIcon"
import UserIcon from "remixicon-react/ShieldUserLineIcon"
import EmailIcon from "remixicon-react/MailLineIcon"
import LockIcon from "remixicon-react/LockLineIcon"
import VisibilityIcon from "remixicon-react/EyeLineIcon"
import VisibilityOffIcon from "remixicon-react/EyeOffLineIcon"
import { ChangeEvent, useState } from "react"
import { Link, useHistory } from "react-router-dom"
import { isEmail, isStrongPassword, isPhoneNumber } from "../../utitly"
import { toast } from "react-toastify"
import { useDispatch } from "react-redux"
import { StoreType } from "../../store"
import { PulseLoader } from "react-spinners"
import { AnimatePresence, motion } from "framer-motion"
import Config from "../../config"
import { setAuthorization } from "../../config/axios"

interface TabPanelProps {
  children?: React.ReactNode
  index: unknown
  value: unknown
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <>{children}</>}
    </div>
  )
}

function a11yProps(index: any) {
  return {
    id: `auth-tab-${index}`,
    "aria-controls": `auth-tabpanel-${index}`,
  }
}

export function LoginScreen(): JSX.Element {
  const [tabIndex, setValue] = useState(0)
  const [isVisible, setVisibleState] = useState<boolean>(false)
  const [isLoading, setLoading] = useState<boolean>(false)
  const [credential, setCredential] = useState<string>("")
  const [password, setPassword] = useState<string>("")
  const { push } = useHistory()

  const handleTabChange = (event: ChangeEvent<unknown>, newValue: number) => {
    setCredential("")
    setValue(newValue)
  }
  const [error, setError] = useState<{ isError: boolean; message?: string }>({
    isError: false,
    message: undefined,
  })
  const dispatch = useDispatch()

  const login = async (e: any) => {
    e.preventDefault()
    setError({
      isError: false,
      message: "",
    })
    setLoading(true)

    const urlencoded = new URLSearchParams()

    urlencoded.append(
      tabIndex === 0 ? "email" : tabIndex === 1 ? "phoneNumber" : "username",
      credential
    )
    urlencoded.append("password", password)

    const requestOptions = {
      method: "POST",
      body: urlencoded,
    }

    fetch(`${Config.apiUrl}/user/signin`, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        if (result.status == "error") {
          setError({
            isError: true,
            message: result.message,
          })
          toast.error(result.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          })
          if (
            (result.message as string).toLowerCase().includes("verified") &&
            tabIndex !== 0
          ) {
            setTimeout(() => {
              toast.dark(
                "Login with your email to start your verification process",
                {
                  position: "top-right",
                  autoClose: 5000,
                  hideProgressBar: true,
                  draggable: true,
                }
              )
            }, 3000)
          }

          if (
            (result.message as string).toLowerCase().includes("verified") &&
            tabIndex === 0
          ) {
            toast.dark("Your verification process would start automatically", {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: true,
              draggable: true,
            })
            setTimeout(() => {
              push(
                `/auth/verify?email=${credential.replaceAll(
                  " ",
                  ""
                )}&fromLogin=true`
              )
            }, 4000)
          }
        }

        if (result.status == "success") {
          dispatch({
            type: StoreType.updateLoginDetail,
            value: result.data,
          })
          setAuthorization(undefined, result.data.token)
          localStorage.setItem("userID", result.data.user._id)
          push("/content/post")
          toast.success("Login Successful", {
            hideProgressBar: true,
            pauseOnHover: false,
            onClose: () => {
              toast.dark("Welcome to Ecodeem", {
                hideProgressBar: true,
                pauseOnHover: false,
              })
            },
          })
        }
      })
      .catch((error) => {
        setLoading(false)
        setError({
          isError: true,
          message: error.message,
        })
        toast.error(error.message, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        })
      })
      .finally(() => {
        setLoading(false)
      })
  }
  return (
    <AuthPageLayout>
      <form onSubmit={(e) => login(e)}>
        <p className="title_txt">welcome back!</p>
        <AnimatePresence>
          {error.isError && (
            <motion.p
              className="error_txt"
              exit={{ y: 20, opacity: 0, scale: 0.5 }}
              initial={{ y: 20, opacity: 0, scale: 0.5 }}
              animate={{ y: 0, opacity: 1, scale: 1 }}
            >
              {error.message ?? ""}
            </motion.p>
          )}
        </AnimatePresence>
        <div className="center">
          <Typography variant="h6">Login With Your</Typography>
        </div>
        <AppBar position="relative" className="auth_appBar">
          <Tabs
            value={tabIndex}
            onChange={handleTabChange}
            aria-label="simple tabs example"
          >
            <Tab label="Email" className="auth_appTab" {...a11yProps(0)} />
            <Tab
              label="Phone number"
              className="auth_appTab"
              {...a11yProps(1)}
            />
            <Tab label="Username" className="auth_appTab" {...a11yProps(2)} />
          </Tabs>
        </AppBar>

        <TabPanel value={tabIndex} index={0}>
          <div className="inputBox">
            <label htmlFor="email">Email</label>
            <TextField
              placeholder="Enter your email"
              className="inputField"
              type="email"
              inputProps={{ id: "email" }}
              autoComplete="email"
              variant="outlined"
              value={credential}
              onChange={(e) => {
                setError({ isError: false, message: undefined })
                setCredential(e.target.value)
              }}
              error={!isEmail(credential)}
              helperText={
                credential.length < 1 || isEmail(credential)
                  ? ""
                  : "Please provide a valid email"
              }
              required
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start" className="btn">
                    <EmailIcon />
                  </InputAdornment>
                ),
              }}
            />
          </div>
          <div className="inputBox">
            <label htmlFor="password">Password</label>
            <TextField
              placeholder="Enter your password"
              className="inputField"
              type={isVisible ? "text" : "password"}
              inputProps={{ id: "password" }}
              autoComplete="current-password"
              variant="outlined"
              value={password}
              onChange={(e) => {
                setError({ isError: false, message: undefined })
                setPassword(e.target.value)
              }}
              required
              error={!isStrongPassword(password)}
              helperText={
                password.length < 1 || isStrongPassword(password)
                  ? ""
                  : "Password must contain a at least one number and special character"
              }
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start" className="btn">
                    <LockIcon />
                  </InputAdornment>
                ),
                endAdornment: (
                  <InputAdornment position="start">
                    <IconButton
                      onClick={() => {
                        setVisibleState(!isVisible)
                      }}
                      className="btn dark"
                    >
                      {isVisible ? <VisibilityOffIcon /> : <VisibilityIcon />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
          </div>
        </TabPanel>
        <TabPanel value={tabIndex} index={1}>
          <div className="inputBox">
            <label htmlFor="phoneNumber">Phone Number</label>
            <TextField
              placeholder="Enter your number (e.g: +234...)"
              className="inputField"
              type="tel"
              inputProps={{ id: "phoneNumber" }}
              autoComplete="tel"
              variant="outlined"
              value={credential}
              onChange={(e) => {
                setError({ isError: false, message: undefined })
                setCredential(e.target.value)
              }}
              error={!isPhoneNumber(credential)}
              helperText={
                credential.length < 1 || isPhoneNumber(credential)
                  ? ""
                  : "Please provide a valid phone number (e.g: 0901122334)"
              }
              required
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start" className="btn">
                    <PhoneIcon />
                  </InputAdornment>
                ),
              }}
            />
          </div>
          <div className="inputBox">
            <label htmlFor="password1">Password</label>
            <TextField
              placeholder="Enter your password"
              className="inputField"
              type={isVisible ? "text" : "password"}
              inputProps={{ id: "password1" }}
              autoComplete="current-password"
              variant="outlined"
              value={password}
              onChange={(e) => {
                setError({ isError: false, message: undefined })
                setPassword(e.target.value)
              }}
              required
              error={!isStrongPassword(password)}
              helperText={
                password.length < 1 || isStrongPassword(password)
                  ? ""
                  : "Password must contain a at least one number and special character"
              }
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start" className="btn">
                    <LockIcon />
                  </InputAdornment>
                ),
                endAdornment: (
                  <InputAdornment position="start">
                    <IconButton
                      onClick={() => {
                        setVisibleState(!isVisible)
                      }}
                      className="btn dark"
                    >
                      {isVisible ? <VisibilityOffIcon /> : <VisibilityIcon />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
          </div>
        </TabPanel>
        <TabPanel value={tabIndex} index={2}>
          <div className="inputBox">
            <label htmlFor="username">Username</label>
            <TextField
              placeholder="Enter your username"
              className="inputField"
              type="text"
              inputProps={{ id: "username" }}
              autoComplete="username"
              variant="outlined"
              value={credential}
              onChange={(e) => {
                setError({ isError: false, message: undefined })
                setCredential(e.target.value)
              }}
              error={credential.length < 3}
              helperText={
                credential.length > 3 || credential.length < 1
                  ? ""
                  : "Please provide a valid username"
              }
              required
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start" className="btn">
                    <UserIcon />
                  </InputAdornment>
                ),
              }}
            />
          </div>
          <div className="inputBox">
            <label htmlFor="password2">Password</label>
            <TextField
              placeholder="Enter your password"
              className="inputField"
              type={isVisible ? "text" : "password"}
              inputProps={{ id: "password2" }}
              autoComplete="current-password"
              variant="outlined"
              value={password}
              onChange={(e) => {
                setError({ isError: false, message: undefined })
                setPassword(e.target.value)
              }}
              required
              error={!isStrongPassword(password)}
              helperText={
                password.length < 1 || isStrongPassword(password)
                  ? ""
                  : "Password must contain a at least one number and special character"
              }
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start" className="btn">
                    <LockIcon />
                  </InputAdornment>
                ),
                endAdornment: (
                  <InputAdornment position="start">
                    <IconButton
                      onClick={() => {
                        setVisibleState(!isVisible)
                      }}
                      className="btn dark"
                    >
                      {isVisible ? <VisibilityOffIcon /> : <VisibilityIcon />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
          </div>
        </TabPanel>
        <p className="link">
          Forgot your password?{" "}
          <Link to="/auth/forgot-password">Click here</Link>
        </p>
        <div className="center">
          <Button type="submit" className="submit_btn" disabled={isLoading}>
            {isLoading ? (
              <PulseLoader size={10} margin={2} color="#fff9" />
            ) : (
              "Login"
            )}
          </Button>
        </div>
        <p className="link">
          {"Don't have an account? "}
          <Link to="/auth/register">Register here</Link>
        </p>
      </form>
    </AuthPageLayout>
  )
}
