/* eslint-disable no-mixed-operators */
import {
  TextField,
  InputAdornment,
  IconButton,
  Button,
  Input,
  MenuItem,
  Select,
  FormControl,
  makeStyles,
  createStyles,
  Theme,
} from "@material-ui/core"
import { AuthPageLayout } from "../../layout"
import UserIcon from "remixicon-react/User3LineIcon"
import EmailIcon from "remixicon-react/MailLineIcon"
import PhoneIcon from "remixicon-react/PhoneLineIcon"
import LockIcon from "remixicon-react/LockLineIcon"
import VisibilityIcon from "remixicon-react/EyeLineIcon"
import VisibilityOffIcon from "remixicon-react/EyeOffLineIcon"
import { FormEvent, useCallback, useEffect, useState } from "react"
import { Link, useHistory } from "react-router-dom"
import { PulseLoader } from "react-spinners"
import { toast } from "react-toastify"
import { isEmail, isStrongPassword } from "../../utitly"
import { useDispatch } from "react-redux"
import { InterestType } from "../../core/type"
import {
  getInterestById,
  getInterestsById,
  getInterestsFuncHandler,
  getInterestsId,
} from "../../function"
import { AnimatePresence, motion } from "framer-motion"
import Config from "../../config"
import { StoreType } from "../../store"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    chips: {
      display: "flex",
      flexWrap: "wrap",
    },
    chip: {
      margin: 2,
    },
    noLabel: {
      marginTop: theme.spacing(3),
    },
  })
)

const ITEM_HEIGHT = 48
const ITEM_PADDING_TOP = 8
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
}

export function RegisterScreen(): JSX.Element {
  const [isVisible, setVisibleState] = useState<boolean>(false)
  const [isLoading, setLoading] = useState<boolean>(false)
  const [email, setEmail] = useState<string>("")
  const [username, setUsername] = useState<string>("")
  const [firstName, setFirstName] = useState<string>("")
  const [lastName, setLastName] = useState<string>("")
  const [password, setPassword] = useState<string>("")
  const [interest, setInterest] = useState<Array<InterestType>>([])
  const [phoneNumber, setPhoneNumber] = useState<string>("")
  const [error, setError] = useState<{ isError: boolean; message?: string }>({
    isError: false,
    message: undefined,
  })
  const [rawInterests, setRawInterests] = useState<InterestType[]>([])
  const { push } = useHistory()
  const dispatch = useDispatch()
  const classes = useStyles()
  const getInterest = useCallback(async () => {
    const interest = await getInterestsFuncHandler()
    setRawInterests(interest.data?.interests ?? [])
  }, [])
  useEffect(() => {
    getInterest()
  }, [getInterest])

  const register = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    setLoading(true)
    const urlencoded = new URLSearchParams()
    urlencoded.append("firstName", firstName)
    urlencoded.append("lastName", lastName)
    urlencoded.append("username", username)
    urlencoded.append("email", email)
    urlencoded.append("password", password)
    urlencoded.append("phoneNumber", phoneNumber)
    const ints = getInterestsId(interest)
    ints.map((int) => urlencoded.append("interest", int))
    const requestOptions = {
      method: "POST",
      body: urlencoded,
    }

    fetch(Config.apiUrl + "/user/signup", requestOptions)
      .then((response) => response.json())
      .then((result) => {
        if (result.status == "error") {
          setLoading(false)
          setError({
            isError: true,
            message: `${result.message}`,
          })
          toast.error(result.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          })
        }

        if (result.status == "success") {
          setLoading(false)
          dispatch({
            type: StoreType.updateLoginDetail,
            value: result.data,
          })
          push(`/auth/verify?email=${email.trim()}`)
          toast.success("Your registration was successful", {
            hideProgressBar: true,
            pauseOnHover: false,
            delay: 3000,
            onClose: () => {
              toast.dark(
                "Please verify your account vai the code sent to your email.",
                {
                  hideProgressBar: true,
                  pauseOnHover: false,
                  delay: 10000,
                }
              )
            },
          })
        }
      })
      .catch((error) => {
        setLoading(false)
        toast.error(error.message, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        })
      })
  }

  return (
    <AuthPageLayout>
      <form onSubmit={register}>
        <p className="title_txt">get started with your account today</p>
        <AnimatePresence>
          {error.isError && (
            <motion.p
              className="error_txt"
              exit={{ y: 20, opacity: 0, scale: 0.5 }}
              initial={{ y: 20, opacity: 0, scale: 0.5 }}
              animate={{ y: 0, opacity: 1, scale: 1 }}
            >
              {error.message ?? ""}
            </motion.p>
          )}
        </AnimatePresence>
        <div className="inputBox">
          <label htmlFor="interest">Interests</label>
          <FormControl
            className={"inputField select"}
            required
            variant="outlined"
          >
            <Select
              labelId="interest"
              id="interests"
              multiple
              variant="outlined"
              required
              value={getInterestsId(interest)}
              onChange={(e) => {
                setInterest(
                  getInterestsById(rawInterests, e.target.value as string[])
                )
              }}
              input={<Input id="select-multiple-chip" />}
              renderValue={(selectedInterestsID) => (
                <div className={classes.chips}>
                  {(selectedInterestsID as string[]).map((value) => {
                    const selectedInterest = getInterestById(
                      rawInterests,
                      value
                    )
                    return (
                      <div className="chip_interest" key={selectedInterest._id}>
                        {selectedInterest.interest}
                      </div>
                    )
                  })}
                </div>
              )}
              MenuProps={MenuProps}
            >
              <MenuItem>
                <em>Select one or multiple interest</em>
              </MenuItem>
              {rawInterests.length <= 0 ? (
                <MenuItem>Loading Interests...</MenuItem>
              ) : (
                rawInterests.map((_interest) => (
                  <MenuItem key={_interest._id} value={_interest._id}>
                    {_interest.interest}
                  </MenuItem>
                ))
              )}
            </Select>
          </FormControl>
        </div>
        <div className="inputBox">
          <label htmlFor="firstName">First Name</label>
          <TextField
            placeholder="Enter your first name"
            className="inputField"
            type="first name"
            inputProps={{ id: "firstName" }}
            autoComplete="given-name"
            variant="outlined"
            value={firstName}
            onChange={(e) => {
              setFirstName(e.target.value)
            }}
            required
            InputProps={{
              startAdornment: (
                <InputAdornment position="start" className="btn">
                  <UserIcon />
                </InputAdornment>
              ),
            }}
          />
        </div>
        <div className="inputBox">
          <label htmlFor="lastName">Last Name</label>
          <TextField
            placeholder="Enter your last name"
            className="inputField"
            type="lastName"
            inputProps={{ id: "lastName" }}
            autoComplete="family-name"
            variant="outlined"
            value={lastName}
            required
            onChange={(e) => {
              setLastName(e.target.value)
            }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start" className="btn">
                  <UserIcon />
                </InputAdornment>
              ),
            }}
          />
        </div>
        <div className="inputBox">
          <label htmlFor="username">Username</label>
          <TextField
            placeholder="Enter your username"
            className="inputField"
            type="username"
            inputProps={{ id: "username" }}
            variant="outlined"
            value={username}
            required
            onChange={(e) => {
              setUsername(e.target.value)
            }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start" className="btn">
                  <UserIcon />
                </InputAdornment>
              ),
            }}
          />
        </div>
        <div className="inputBox">
          <label htmlFor="phoneNumber">Phone Number</label>
          <TextField
            placeholder="Enter your phoneNumber"
            className="inputField"
            type="tel"
            inputProps={{ id: "phoneNumber" }}
            autoComplete="tel"
            variant="outlined"
            value={phoneNumber}
            required
            onChange={(e) => {
              setPhoneNumber(e.target.value)
            }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start" className="btn">
                  <PhoneIcon />
                </InputAdornment>
              ),
            }}
          />
        </div>
        <div className="inputBox">
          <label htmlFor="email">Email</label>
          <TextField
            placeholder="Enter your email"
            className="inputField"
            type="email"
            inputProps={{ id: "email" }}
            autoComplete="email"
            variant="outlined"
            value={email}
            required
            onChange={(e) => {
              setEmail(e.target.value)
            }}
            error={!isEmail(email)}
            helperText={
              email.length < 1 || isEmail(email)
                ? ""
                : "Please provide a valid email"
            }
            InputProps={{
              startAdornment: (
                <InputAdornment position="start" className="btn">
                  <EmailIcon />
                </InputAdornment>
              ),
            }}
          />
        </div>
        <div className="inputBox">
          <label htmlFor="password">Password</label>
          <TextField
            placeholder="Enter your password"
            className="inputField"
            type={isVisible ? "text" : "password"}
            inputProps={{
              id: "password",
              pattern: ".*[A-Za-z].*[0-9][$&@#*].*",
            }}
            autoComplete="new-password"
            variant="outlined"
            value={password}
            required
            onChange={(e) => {
              setPassword(e.target.value)
            }}
            error={!isStrongPassword(password)}
            helperText={
              password.length < 1 || isStrongPassword(password)
                ? ""
                : "Password must contain a at least one number and special character"
            }
            InputProps={{
              startAdornment: (
                <InputAdornment position="start" className="btn">
                  <LockIcon />
                </InputAdornment>
              ),
              endAdornment: (
                <InputAdornment position="start">
                  <IconButton
                    onClick={() => {
                      setVisibleState(!isVisible)
                    }}
                    className="btn dark"
                  >
                    {isVisible ? <VisibilityOffIcon /> : <VisibilityIcon />}
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
          <Button
            type="submit"
            className="submit_btn"
            disabled={isLoading}
            style={{ marginTop: "20px" }}
          >
            {isLoading ? (
              <PulseLoader size={10} margin={2} color="#fff" />
            ) : (
              "Register"
            )}
          </Button>
          <p className="link">
            Already have an account?
            <Link to="/auth/login">Click here</Link>
          </p>
        </div>
      </form>
    </AuthPageLayout>
  )
}
