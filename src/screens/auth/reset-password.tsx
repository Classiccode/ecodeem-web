import React, { useState } from "react"
import { Link, useHistory, useParams } from "react-router-dom"
import {
  InputAdornment,
  makeStyles,
  TextField,
  Button,
} from "@material-ui/core"
import {
  LockOutlined,
  VisibilityOffOutlined,
  VisibilityOutlined,
} from "@material-ui/icons"
import { toast } from "react-toastify"
import Config from "../../config"
import { AuthPageLayout } from "../../layout"
import { PulseLoader } from "react-spinners"
import { isStrongPassword } from "../../utitly"

interface ErrorState {
  state: boolean
  message?: string
}

const useStyles = makeStyles(() => ({
  noBorder: {
    border: "none",
  },
}))

export const ResetPassword = (): JSX.Element => {
  const classes = useStyles()
  const { token } = useParams<Record<string, string | undefined>>()
  const history = useHistory()
  const [password, setPassword] = useState("")
  const [re_password, setRePassword] = useState("")
  const [loading, setLoading] = useState(false)
  const [showPassword, setShowPassword] = useState(false)

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword)
  }

  const reset = (event: React.FormEvent<EventTarget>): void => {
    event.preventDefault()

    if (password !== re_password) {
      return
    }

    setLoading(true)

    const urlencoded = new URLSearchParams()

    urlencoded.append("password", password)

    const requestOptions = {
      method: "POST",
      body: urlencoded,
    }

    fetch(`${Config.apiUrl}/user/reset_password/${token}`, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        if (result.status == "error") {
          setLoading(false)
          toast.error(result.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          })
        }

        if (result.status == "success") {
          setLoading(false)
          toast.success(result.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          })

          setTimeout(() => {
            history.push("/auth/login")
          }, 3000)
        }
      })
      .catch((error) => {
        setLoading(false)
      })
  }
  return (
    <AuthPageLayout>
      <form onSubmit={reset} className="text-center">
        <div className="inputBox">
          <label htmlFor="firstName">Password</label>
          <TextField
            fullWidth
            name="password"
            placeholder="enter new password"
            type={!showPassword ? "password" : "text"}
            required
            onChange={(e) => setRePassword(e.target.value)}
            variant="outlined"
            error={!isStrongPassword(password)}
            helperText={
              password.length < 1 || isStrongPassword(password)
                ? ""
                : "Password must contain a at least one number and special character"
            }
            style={{
              backgroundColor: "#F9F9F9",
              border: "none",
              borderRadius: "10px",
            }}
            InputProps={{
              "aria-label": "naked",
              classes: { notchedOutline: classes.noBorder },
              startAdornment: (
                <InputAdornment position="start">
                  <LockOutlined />
                </InputAdornment>
              ),
              endAdornment: (
                <InputAdornment position="end">
                  <div
                    style={{ cursor: "pointer" }}
                    onClick={() => togglePasswordVisibility()}
                  >
                    {showPassword ? (
                      <VisibilityOffOutlined />
                    ) : (
                      <VisibilityOutlined />
                    )}
                  </div>
                </InputAdornment>
              ),
            }}
          />
        </div>
        <div className="inputBox">
          <label htmlFor="firstName">Password</label>
          <TextField
            fullWidth
            name="password"
            placeholder="retype password"
            type={!showPassword ? "password" : "text"}
            required
            onChange={(e) => setPassword(e.target.value)}
            variant="outlined"
            error={!isStrongPassword(password)}
            helperText={
              password.length < 1 || isStrongPassword(password)
                ? ""
                : "Password must contain a at least one number and special character"
            }
            style={{
              backgroundColor: "#F9F9F9",
              border: "none",
              borderRadius: "10px",
            }}
            InputProps={{
              "aria-label": "naked",
              classes: { notchedOutline: classes.noBorder },
              startAdornment: (
                <InputAdornment position="start">
                  <LockOutlined />
                </InputAdornment>
              ),
              endAdornment: (
                <InputAdornment position="end">
                  <div
                    style={{ cursor: "pointer" }}
                    onClick={() => togglePasswordVisibility()}
                  >
                    {showPassword ? (
                      <VisibilityOffOutlined />
                    ) : (
                      <VisibilityOutlined />
                    )}
                  </div>
                </InputAdornment>
              ),
            }}
          />
        </div>

        <Button
          type="submit"
          className="submit_btn"
          disabled={loading}
          style={{ marginTop: "20px" }}
        >
          {loading ? (
            <PulseLoader size={10} margin={2} color="#fff" />
          ) : (
            "Reset Password"
          )}
        </Button>
        <p className="link">
          Already have an account?
          <Link to="/auth/login">Click here</Link>
        </p>
      </form>
    </AuthPageLayout>
  )
}
