import { FormEvent, useState } from "react"
import { Link } from "react-router-dom"
import {
  InputAdornment,
  makeStyles,
  TextField,
  Button,
} from "@material-ui/core"
import { EmailOutlined } from "@material-ui/icons"
import { toast } from "react-toastify"
import Config from "../../config"
import { AuthPageLayout } from "../../layout"
import { PulseLoader } from "react-spinners"

interface ErrorState {
  state: boolean
  message?: string
}

const useStyles = makeStyles(() => ({
  noBorder: {
    border: "none",
  },
}))

export const ForgetPassword = (): JSX.Element => {
  const classes = useStyles()
  const [email, setEmail] = useState("")
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState<ErrorState>()

  const forget = (e: FormEvent<HTMLFormElement>): void => {
    e.preventDefault()

    setLoading(true)

    const urlencoded = new URLSearchParams()

    urlencoded.append("email", email)
    urlencoded.append("passResetLink", `${Config.baseURL}/auth/reset-password`)

    const requestOptions = {
      method: "POST",
      body: urlencoded,
    }
    fetch(`${Config.apiUrl}/user/recover_password`, requestOptions)
      .then((response) => response.json())
      .then((result) => {
        if (result.status == "error") {
          setLoading(false)
          toast.error(result.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          })
        }

        if (result.status == "success") {
          setLoading(false)
          toast.success(result.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          })
        }
      })
      .catch((error) => {
        setLoading(false)
        setError({ state: true, message: error.message })
      })
  }

  return (
    <AuthPageLayout>
      <form onSubmit={forget} className="text-center">
        <div className="inputBox">
          <label htmlFor="firstName">Email</label>
          <TextField
            fullWidth
            name="email"
            placeholder="email"
            required
            onChange={(e) => setEmail(e.target.value)}
            type="email"
            variant="outlined"
            style={{
              backgroundColor: "#F9F9F9",
              border: "none",
            }}
            InputProps={{
              "aria-label": "naked",
              classes: { notchedOutline: classes.noBorder },
              startAdornment: (
                <InputAdornment position="start">
                  <EmailOutlined />
                </InputAdornment>
              ),
            }}
          />
        </div>
        <Button
          type="submit"
          className="submit_btn"
          disabled={loading}
          style={{ marginTop: "20px" }}
        >
          {loading ? (
            <PulseLoader size={10} margin={2} color="#fff" />
          ) : (
            "Reset Password"
          )}
        </Button>
        <p className="link">
          Already have an account?
          <Link to="/auth/login">Click here</Link>
        </p>
      </form>
    </AuthPageLayout>
  )
}
