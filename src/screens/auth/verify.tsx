import { Button } from "@material-ui/core"
import { AuthPageLayout } from "../../layout/auth"
import { useCallback, useEffect, useState } from "react"
import { Link, useHistory, useLocation } from "react-router-dom"
import { useDispatch } from "react-redux"
import { PulseLoader } from "react-spinners"
import { AnimatePresence, motion } from "framer-motion"
import PinInput from "react-pin-input"
import { resendOtp, verifyEmailFuncHandler } from "../../function"
import { parse } from "querystring"
import { StoreType } from "../../store"
import { toast } from "react-toastify"

export function VerifyScreen(): JSX.Element {
  const [isLoading, setLoading] = useState<boolean>(false)
  const [verifyCode, setVerifyCode] = useState<string>("")
  const [error, setError] = useState<{ isError: boolean; message?: string }>({
    isError: false,
    message: undefined,
  })
  const { push } = useHistory()
  const { search } = useLocation()
  const dispatch = useDispatch()
  const [loadingVer, setLoadingVer] = useState<boolean>(true)
  const callback = useCallback(async () => {
    const fromLogin = Boolean(parse(search)["fromLogin"])
    if (fromLogin) {
      await resendOtp({ email: parse(search)["?email"].toString() })
        .catch((error) => {
          toast.error(error)
        })
        .finally(() => {
          setLoadingVer(false)
        })
    }
  }, [parse(search)["fromLogin"]])
  useEffect(() => {
    callback()
  }, [callback])
  return (
    <AuthPageLayout>
      <form
        onSubmit={(e) => {
          e.preventDefault()
          if (isLoading) return
          verifyEmailFuncHandler({
            email: parse(search)["?email"].toString(),
            code: verifyCode,
          })
            .then((value) => {
              dispatch({
                type: StoreType.updateLoginDetail,
                value: {
                  token: value.data.token,
                  user: value.data.verifyUser,
                },
              })
              localStorage.setItem("userID", value.data.verifyUser._id)
              push("/content/post")
              toast.success("Your account has been verified", {
                hideProgressBar: true,
                pauseOnHover: false,
                delay: 2000,
                onClose: () => {
                  toast.dark("Welcome to Ecodeem", {
                    hideProgressBar: true,
                    pauseOnHover: false,
                  })
                },
              })
            })
            .catch((error: { message: string }) => {
              setError({
                isError: true,
                message: `Error: ${error.message}`,
              })
              toast.error(error.message, {
                hideProgressBar: true,
                pauseOnHover: false,
              })
            })
            .finally(() => {
              setLoading(false)
            })
          setLoading(true)
        }}
      >
        <p className="title_txt">Verify your account</p>
        <AnimatePresence>
          {error.isError && (
            <motion.p
              className="error_txt"
              exit={{ y: 20, opacity: 0, scale: 0.5 }}
              initial={{ y: 20, opacity: 0, scale: 0.5 }}
              animate={{ y: 0, opacity: 1, scale: 1 }}
            >
              {error.message ?? ""}
            </motion.p>
          )}
        </AnimatePresence>
        <div className="inputBox verify">
          <label>Enter your verification code sent to your email</label>
          <PinInput
            length={6}
            initialValue=""
            secret
            onChange={(value: string) => {
              setVerifyCode(value)
            }}
            type="numeric"
            inputMode="number"
            style={{ padding: "10px" }}
            inputStyle={{ borderColor: "#8CC64B" }}
            inputFocusStyle={{ borderColor: "#2E8CCC" }}
            autoSelect={true}
            regexCriteria={/^[0-9]*$/}
          />
          <p className="link">
            {"Didn't receive any email containing your verification code? "}
            <Link
              to="/auth/verify"
              onClick={async (e) => {
                e.preventDefault()
                setLoadingVer(true)
                await resendOtp({ email: parse(search)["?email"].toString() })
                  .then(() => {
                    toast.success("Your verification code has been resent.")
                  })
                  .catch((error) => {
                    toast.error(error)
                  })
                  .finally(() => {
                    setLoadingVer(true)
                  })
              }}
            >
              Click here to resend
            </Link>
          </p>
          <Button
            type="submit"
            className="submit_btn"
            disabled={isLoading || loadingVer}
            style={{ marginTop: "20px" }}
          >
            {isLoading ? (
              <PulseLoader size={10} margin={2} color="white" />
            ) : (
              "Verify"
            )}
          </Button>
          <p className="link">
            {"Already verified? "}
            <Link to="/auth/login">Click here</Link>
          </p>
        </div>
      </form>
    </AuthPageLayout>
  )
}
