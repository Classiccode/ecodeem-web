import { motion } from "framer-motion"
import { Button } from "@material-ui/core"

export function BadNetworkPage(): JSX.Element {
  return (
    <>
      <motion.section
        id="loader"
        initial={{ y: 0, opacity: 1 }}
        exit={{ y: "-100vh", opacity: 0.2 }}
        transition={{ duration: 0.5, type: "spring" }}
      >
        <div className="content error-404">
          <span className="illustrator" />
          <p className="txt">
            couldn&apos;t make a connection to Ecodeem, <br /> Please try again
            with better network
          </p>
          <Button
            className="btn"
            onClick={() => {
              window.location.assign("/content/post")
            }}
          >
            Retry
          </Button>
        </div>
      </motion.section>
    </>
  )
}
