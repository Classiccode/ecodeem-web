import { SocialShimmer } from "react-content-shimmer"
import {
  AppBar,
  Box,
  Button,
  IconButton,
  Tab,
  Tabs,
  Typography,
} from "@material-ui/core"
import SettingIcon from "remixicon-react/Settings3LineIcon"
import { ChangeEvent, ReactNode, useState } from "react"
import { useQuery } from "react-query"
import { getUserProfile } from "../../../function"
import { FavoriteBorderOutlined } from "@material-ui/icons"
import { useHistory, useParams } from "react-router-dom"
import Video from "../../../components/Video/video"
import { checkMediaType, useProfile } from "../../../utitly"
import {
  createChat,
  follow,
  FriendState,
  RootState,
  togglePost,
  unfollow,
} from "../../../store"
import { useDispatch, useSelector } from "react-redux"
import Loader from "react-loader-spinner"
import { Helmet } from "react-helmet"
import { capitalize } from "lodash"

interface TabPanelProps {
  children?: ReactNode
  index: number
  value: number
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`tabpanel-${index}`}
      aria-labelledby={`tab-${index}`}
      {...other}
      className="box-tab"
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  )
}

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  }
}
export function ProfileMainPage(): JSX.Element {
  const [value, setValue] = useState<number>(0)
  const friends: FriendState = useSelector((state: RootState) => state.friends)
  const history = useHistory()
  const dispatch = useDispatch()
  const { id } = useParams<Record<string, string | undefined>>()
  const { profile } = useProfile()

  const handleChange = (event: ChangeEvent, newValue: unknown) => {
    setValue(newValue as number)
  }
  const { data, isError, isFetchedAfterMount, isLoading } = useQuery(
    "profile",
    async () => {
      return (await getUserProfile(id!)).data
    }
  )

  const checkIfFollowing = (_id) => {
    if (
      friends.following.filter(function (e) {
        return e.user._id === _id
      }).length > 0
    ) {
      return true
    }

    return false
  }

  return (
    <>
      <Helmet>
        <title>
          {profile.user._id === data?._id
            ? "My Profile"
            : `${capitalize(data?.username ?? "")} Profile`}{" "}
          - Ecodeem Web
        </title>
      </Helmet>
      <section className="window_modal">
        <div className="container"></div>
      </section>
      <section className="Profile_page">
        {isError ? (
          <>
            <p className="error">Network Error</p>
          </>
        ) : isLoading && !isFetchedAfterMount ? (
          <SocialShimmer
            foreground="var(--background)"
            background="var(--background-2)"
          />
        ) : (
          <>
            <div className="title">
              <h4 className="title_txt">@{data?.username}</h4>
              {profile.user._id == data?._id && (
                <IconButton
                  onClick={() =>
                    history.push("/content/profile/account/setting")
                  }
                  className="btn"
                >
                  <SettingIcon />
                </IconButton>
              )}
            </div>
            <div className="profile_header">
              {!data?.avatar ? (
                <div className="avatar tin">
                  {data?.firstName[0]}
                  {data?.lastName[0]}
                </div>
              ) : (
                <img
                  className="rounded"
                  height="100%"
                  width="150"
                  src={data?.avatar}
                  alt=""
                />
              )}
              <div className="col_h">
                <div className="col_m">
                  <div className="col">
                    <span className="count">{data?.posts?.length ?? 0}</span>
                    <span className="name">Post</span>
                  </div>

                  <div
                    className={`col ${
                      profile.user._id == data?._id ? "link" : ""
                    }`}
                    role="a"
                    onClick={() => {
                      if (profile.user._id == data?._id) {
                        history.push("/content/profile/account/follower")
                      }
                    }}
                  >
                    <span className="count">{data?.followers ?? 0}</span>
                    <span className="name">Followers</span>
                  </div>

                  <div
                    className={`col ${
                      profile.user._id == data?._id ? "link" : ""
                    }`}
                    role="a"
                    onClick={() => {
                      if (profile.user._id == data?._id) {
                        history.push("/content/profile/account/following")
                      }
                    }}
                  >
                    <span className="count">{data?.following ?? 0}</span>
                    <span className="name">Following</span>
                  </div>
                </div>
                <div className="col_m">
                  <h3>{data?.reward.title}</h3>
                </div>
                <div className="col_m">
                  <h5>Level {data?.reward.level}</h5>
                </div>
              </div>
            </div>
            <div className="profile_desc">
              <h4 className="desc_title">
                {data?.firstName} {data?.lastName}
              </h4>
              <p className="desc_txt">{data?.bio ?? ""}</p>
            </div>
            {profile.user._id == data?._id ? (
              <div className="profile_action">
                <Button
                  onClick={() =>
                    history.push("/content/profile/account/setting")
                  }
                  className="btn"
                >
                  Edit Profile
                </Button>
                <Button
                  className="btn"
                  onClick={() => {
                    if (navigator?.share !== undefined) {
                      navigator.share({
                        title: "Invite A Friend/Family",
                        text: "Join ecodeem today",
                        url: `https://ecodeem.com/auth/signup?referralCode=${profile.user.referralCode}`,
                      })
                    } else {
                      console.log(profile.user.referralCode)
                    }
                  }}
                >
                  Refer A Friend
                </Button>
                <Button onClick={() => dispatch(togglePost())} className="btn">
                  Create Campaign
                </Button>
              </div>
            ) : (
              <div className="profile_action">
                <Button
                  className="btn"
                  style={{
                    width: "45%",
                  }}
                  onClick={() => {
                    dispatch(
                      createChat({
                        userId: data?._id,
                        ecodeemId: profile.user._id,
                      })
                    )
                    history.push("/content/chat")
                  }}
                >
                  Message
                </Button>
                {checkIfFollowing(id) ? (
                  <Button
                    onClick={() => dispatch(unfollow({ userId: id }))}
                    className="btn"
                    style={{
                      width: "45%",
                    }}
                  >
                    {friends.followingState == "loading" ? (
                      <Loader
                        type="ThreeDots"
                        height="20px"
                        width="30px"
                        color="black"
                      />
                    ) : (
                      "Unfollow"
                    )}
                  </Button>
                ) : (
                  <Button
                    onClick={() => dispatch(follow({ userId: id }))}
                    className="btn"
                    style={{
                      width: "45%",
                    }}
                  >
                    {friends.followingState == "loading" ? (
                      <Loader
                        type="ThreeDots"
                        height="20px"
                        width="30px"
                        color="black"
                      />
                    ) : (
                      "Follow"
                    )}
                  </Button>
                )}
              </div>
            )}

            <AppBar position="static" className="tab-bar">
              <Tabs
                value={value}
                onChange={(e, nv) => handleChange(e as ChangeEvent, nv)}
                aria-label="tabs"
              >
                <Tab label="My Posts" {...a11yProps(0)} />
                <Tab label="Saved Posts" {...a11yProps(1)} />
                <Tab label="Campaign" {...a11yProps(2)} />
              </Tabs>
            </AppBar>
            <TabPanel value={value} index={0}>
              {data?.posts && data?.posts.length > 0 ? (
                <div className="Gallery">
                  {data?.posts?.map((post, idx) => (
                    <div
                      key={`hhhhh${post._id}`}
                      className="Container"
                      onClick={() => history.push(`/content/post/${post._id}`)}
                    >
                      {post.mediaUrl ? (
                        checkMediaType(post.mediaUrl) == "image" ? (
                          <img src={post.mediaUrl} alt="post" />
                        ) : (
                          <Video src={post.mediaUrl} />
                        )
                      ) : (
                        <p className="Text">{post.postDesc}</p>
                      )}
                      <div className="Reaction">
                        <div>
                          <p>{post?.counts?.likesCount}</p>
                          <FavoriteBorderOutlined
                            style={{ fontSize: "25px" }}
                          />
                        </div>
                        <div
                          onClick={() =>
                            history.push(`/content/post/${post._id}`)
                          }
                        >
                          <p>{post?.counts?.commentCount}</p>
                          <i
                            style={{ fontSize: "25px" }}
                            className="ri-message-3-line"
                          ></i>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              ) : (
                <div className="PROFILE__Bottom__Content__Empty">
                  No Post Found
                </div>
              )}
            </TabPanel>
            <TabPanel value={value} index={1}>
              No Saved Posts
            </TabPanel>
            <TabPanel value={value} index={2}>
              {data?.posts && data?.posts.length > 0 ? (
                <div className="Gallery">
                  {data?.posts?.map(
                    (post, idx) =>
                      post.postType == "raiseFund" && (
                        <div
                          key={`hhhhh${post._id}`}
                          className="Container"
                          onClick={() =>
                            history.push(`/content/post/${post._id}`)
                          }
                        >
                          {post.mediaUrl ? (
                            checkMediaType(post.mediaUrl) == "image" ? (
                              <img src={post.mediaUrl} alt="post" />
                            ) : (
                              <Video src={post.mediaUrl} />
                            )
                          ) : (
                            <p className="Text">{post.postDesc}</p>
                          )}
                          <div className="Reaction">
                            <div>
                              <p>{post?.counts?.likesCount}</p>
                              <FavoriteBorderOutlined
                                style={{ fontSize: "25px" }}
                              />
                            </div>
                            <div
                              onClick={() =>
                                history.push(`/content/post/${post._id}`)
                              }
                            >
                              <p>{post?.counts?.commentCount}</p>
                              <i
                                style={{ fontSize: "25px" }}
                                className="ri-message-3-line"
                              ></i>
                            </div>
                          </div>
                        </div>
                      )
                  )}
                </div>
              ) : (
                <div className="PROFILE__Bottom__Content__Empty">
                  No Post Found
                </div>
              )}
            </TabPanel>
          </>
        )}
      </section>
    </>
  )
}
