import { useQuery } from "react-query"
import {
  getNotificationsFuncHandler,
  readNotificationFuncHandler,
} from "../../../function"

import { BulletListShimmer } from "react-content-shimmer"
import { useHistory } from "react-router-dom"
import { setAuthorization } from "../../../config/axios"
import { toast } from "react-toastify"
import { Avatar } from "@material-ui/core"
import moment from "moment"

export const useNotification = () => {
  const { push } = useHistory()
  return useQuery("notifications", async () => {
    setAuthorization(push, undefined)
    return (await getNotificationsFuncHandler()).data
  })
}

export function NotificationMainPage(): JSX.Element {
  const { push } = useHistory()
  const { data, isLoading, isError, isFetchedAfterMount } = useNotification()
  return (
    <>
      <section className="Notifications">
        <h3 className="title">Notifications</h3>
        <div className="container">
          <h4 className="sub_title">Today</h4>
          {isError ? (
            <ul>
              <li>
                <p>Network error, Please try again</p>
              </li>
            </ul>
          ) : isLoading && !isFetchedAfterMount ? (
            <>
              <BulletListShimmer
                rows={5}
                foreground="var(--background)"
                background="var(--background-2)"
              />
            </>
          ) : (
            <ul>
              {!data ? (
                <li>
                  <p>No notifications yet</p>
                </li>
              ) : data.length <= 0 ? (
                <li>
                  <p>No notifications yet</p>
                </li>
              ) : (
                data.map((notification) => {
                  return (
                    <li
                      key={notification._id}
                      className={`d-flex ${
                        !notification.isRead ? " active" : ""
                      }`}
                    >
                      <Avatar
                        onClick={() =>
                          push(`/content/profile/${notification.sender._id}`)
                        }
                        alt={notification.sender.firstName}
                        src={`${notification.sender.avatar}`}
                      />
                      <div className="w-100">
                        <p>{notification.content}</p>
                        <small
                          style={{
                            marginLeft: "15px",
                          }}
                          className="text-muted"
                        >
                          {moment(notification.createdAt).fromNow()}
                        </small>
                      </div>

                      <button
                        onClick={() => {
                          readNotificationFuncHandler(notification._id).catch(
                            (error) => {
                              toast.error("error", {
                                position: "top-right",
                                autoClose: 5000,
                                hideProgressBar: false,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true,
                                progress: undefined,
                              })
                            }
                          )
                          push(
                            `${
                              notification.type == "comment" ||
                              notification.type == "like" ||
                              notification.type == "share" ||
                              notification.type == "donation"
                                ? `/content/post/${notification.action}`
                                : `/content/profile/${notification.action}`
                            }`
                          )
                        }}
                      >
                        view
                      </button>
                    </li>
                  )
                })
              )}
            </ul>
          )}
        </div>
      </section>
    </>
  )
}
