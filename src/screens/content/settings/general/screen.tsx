import React, { useEffect, useState } from "react"

import { RootState } from "../../../../store"
import { useDispatch, useSelector } from "react-redux"
import { toggleDarkMode } from "../../../../store/action/theme"
import { Switch as muiSwitch } from "@material-ui/core"
import { withStyles } from "@material-ui/styles"
import { cloneDeep } from "lodash"
import { SaveSettings } from "../../../../function/settings"

const Switch = withStyles({
  switchBase: {
    color: "#AFCE8B",
    "&$checked": {
      color: "#8CC64B",
    },
    "&$checked + $track": {
      backgroundColor: "#8CC64B",
    },
  },
  checked: {},
  track: {},
})(muiSwitch)

export const GeneralSettingMainPage = (): JSX.Element => {
  const [settings, setDefaultSettings] = useState<
    | {
        canLoginWithBio: boolean
        canPush: boolean
        rememberLogin: boolean
        lowData: boolean
        backgroundMode: boolean
      }
    | any
  >()
  const [checkerSettings, setCheckerSettings] = useState<
    | {
        canLoginWithBio: boolean
        canPush: boolean
        rememberLogin: boolean
        lowData: boolean
        backgroundMode: boolean
      }
    | any
  >()
  const { isDarkMode, userDetails } = useSelector((state: RootState) => {
    return {
      isDarkMode: state.theme.isDarkMode,
      userDetails: state.data.loginUserDetails,
    }
  })
  const dispatch = useDispatch()
  const setIsDarkMode = () => {
    dispatch(toggleDarkMode())
  }
  useEffect(() => {
    setDefaultSettings(userDetails?.user.settings)
  }, [userDetails])
  useEffect(() => {
    ;(async () => {
      let checker = false
      const checkFunc = () =>
        new Promise<boolean>((res, rej) => {
          try {
            setTimeout(() => {
              if (
                checkerSettings?.backgroundMode === settings?.backgroundMode &&
                checkerSettings?.canLoginWithBio ===
                  settings?.canLoginWithBio &&
                checkerSettings?.canPush === settings?.canPush &&
                checkerSettings?.lowData === settings?.lowData &&
                checkerSettings?.rememberLogin === settings?.rememberLogin
              ) {
                checker = true
                res(true)
              } else {
                checker = false
                res(true)
              }
              setCheckerSettings(settings)
            }, 3000)
          } catch (error) {
            rej(error)
          }
        })
      await checkFunc()
      console.log(userDetails?.token ?? "")
      if (!checker) {
        await SaveSettings({ settings, token: userDetails?.token ?? "" }).catch(
          (error) => {
            console.error(error)
          }
        )
      }
    })()
  }, [settings])
  return (
    <>
      <div
        style={{
          marginTop: "100px",
          padding: "5px",
        }}
      >
        <h3 className="mb-5">General Settings</h3>
        <ul className="list-group-flush">
          <li className="mb-3 d-flex justify-content-between align-items-center">
            Dark Mode
            <Switch
              checked={isDarkMode}
              onChange={() => {
                setIsDarkMode()
              }}
              color="primary"
              name="canPush"
              inputProps={{ "aria-label": "canPush checkbox" }}
            />
          </li>
          <li className="mb-3 d-flex justify-content-between align-items-center">
            Allow push Notification
            <Switch
              checked={settings?.canPush}
              onChange={() => {
                setDefaultSettings((oldSetting) => {
                  return {
                    ...oldSetting,
                    canPush: !oldSetting?.canPush ?? false,
                  }
                })
              }}
              color="primary"
              name="canPush"
              inputProps={{ "aria-label": "canPush checkbox" }}
            />
          </li>
          <li className="mb-3 d-flex justify-content-between align-items-center">
            Remember Login
            <Switch
              checked={settings?.rememberLogin}
              onChange={() => {
                setDefaultSettings((oldSetting) => {
                  return {
                    ...oldSetting,
                    rememberLogin: !oldSetting?.rememberLogin ?? false,
                  }
                })
              }}
              color="primary"
              name="canPush"
              inputProps={{ "aria-label": "canPush checkbox" }}
            />
          </li>
          <li className="mb-3 d-flex justify-content-between align-items-center">
            Low Data Usage
            <Switch
              checked={settings?.lowData}
              onChange={() => {
                setDefaultSettings((oldSetting) => {
                  return {
                    ...oldSetting,
                    lowData: !oldSetting?.lowData ?? false,
                  }
                })
              }}
              color="primary"
              name="canPush"
              inputProps={{ "aria-label": "canPush checkbox" }}
            />
          </li>
        </ul>
      </div>
    </>
  )
}
