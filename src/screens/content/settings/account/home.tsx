import React, { Fragment, useState } from "react"
import { useHistory } from "react-router-dom"
import { useProfile } from "../../../../utitly"
import Config from "../../../../config"

import { UpdateInterests } from "./UpdateInterests"
import { ChangePassword } from "./ChangePassword"
import { EditProfile } from "./EditProfile"
import { LocalHospitalTwoTone } from "@material-ui/icons"

export const AccountSettingMainPage = () => {
  const [loggingOff, setLoggingOff] = useState(false)
  const [tab, setTab] = useState("edit-profile")
  const { profile } = useProfile()
  const History = useHistory()
  const logoutSession = (): void => {
    const myHeaders = new Headers()
    localStorage.clear()

    myHeaders.append("Authorization", `Bearer ${profile.token}`)

    const requestOptions = {
      method: "POST",
      headers: myHeaders,
    }

    setLoggingOff(true)

    fetch(Config.apiUrl + "/user/logout", requestOptions)
      .then((response) => response.json())
      .then((result) => {
        if (result.status == "success") {
          setLoggingOff(false)
          localStorage.clear()
          window.location.reload()
          History.push("/")
        } else {
          setLoggingOff(false)
        }
      })
      .catch((error) => console.error("error", error))
  }

  return (
    <>
      <div style={{ display: "flex" }}>
        <div style={{ marginTop: "50px" }} className="Account">
          <div className="Account__Top">
            <ul>
              <li
                onClick={() => setTab("edit-profile")}
                className={
                  tab == "edit-profile" ? `Account__Top__Tab__${tab}` : ""
                }
              >
                Edit Profile
              </li>
              <li
                onClick={() => setTab("change-password")}
                className={
                  tab == "change-password" ? `Account__Top__Tab__${tab}` : ""
                }
              >
                Change Password
              </li>
              <li
                onClick={() => setTab("update-interests")}
                className={
                  tab == "update-interests" ? `Account__Top__Tab__${tab}` : ""
                }
              >
                Update Interests
              </li>
            </ul>
          </div>
          <div className="Account__Right">
            {tab == "edit-profile" && <EditProfile />}
            {tab == "change-password" && <ChangePassword />}
            {tab == "update-interests" && <UpdateInterests />}
          </div>
        </div>
      </div>
    </>
  )
}
