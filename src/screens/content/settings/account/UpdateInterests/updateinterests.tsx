import React, { Fragment } from "react"
import { useHistory } from "react-router-dom"

import UpdateInterestsProvider from "./provider"
import Button from "../../../../../components/Button/button"
import { ApiLoadState } from "../../../../../components/loaders"
import { truncateWords } from "../../../../../utitly"

export const UpdateInterests = () => {
  return (
    <UpdateInterestsProvider>
      {({ input, interests, handleSubmit, addUpInterest }) => (
        <Fragment>
          <div
            className="Account__Right__Wrapper"
            style={{ padding: "10px 30px" }}
          >
            <div className="Account__Right__Interests">
              <div className="Account__Right__Interests__Title">
                Selected ({input.selectedInterests?.length}/{interests?.length})
              </div>

              <div className="Account__Right__Interests__Wrapper">
                {interests &&
                  interests.map((interest, idx) => (
                    <div
                      key={idx}
                      onClick={() => addUpInterest(interest._id)}
                      className="Account__Right__Interests__Interest"
                    >
                      <div
                        className={
                          input.selectedInterests.includes(
                            interest._id.toLowerCase()
                          )
                            ? "Account__Right__Interests__Interest__Active"
                            : ""
                        }
                      >
                        <img src={`${interest.image}`} />
                      </div>
                      <h6>{truncateWords(interest.interest, 12)}</h6>
                    </div>
                  ))}
              </div>

              <div className="Account__Right__Interests__Button">
                {input.showLoader ? (
                  <ApiLoadState />
                ) : (
                  <Button onClick={handleSubmit} type="dark" text="Update" />
                )}
              </div>
            </div>
          </div>
        </Fragment>
      )}
    </UpdateInterestsProvider>
  )
}
