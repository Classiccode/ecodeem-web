import React, {
  useEffect,
  useState,
  FC,
  ReactNode,
  useReducer,
  MouseEvent,
  ChangeEvent,
} from "react"
import axios from "axios"
import { useProfile } from "../../../../../utitly"
import { InterestType } from "../../../../../core/type"
import {
  getInterestsFuncHandler,
  interestsUpdateFuncHandler,
} from "../../../../../function"
import { toast } from "react-toastify"

type Inputprops = {
  selectedInterests: string[]
  showLoader: boolean
}

type ChildrenProps = {
  input: Inputprops
  interests: InterestType[] | undefined
  addUpInterest: (value: string) => void
  handleSubmit: (event: MouseEvent) => Promise<null | void>
}

interface IUpdateInterestprops {
  interestState?: InterestType[]
  children(_: ChildrenProps): ReactNode
}

const UpdateInterestsProvider = ({
  children,
  interestState,
}: IUpdateInterestprops): JSX.Element => {
  const [selectedInterests, setSelectedInterests] = useState<Array<string>>([])
  const [interests, setInterests] = useState<Array<InterestType>>()
  const [showLoader, setLoader] = useState(false)

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  const addUpInterest = (value: string): void => {
    if (selectedInterests.includes(value)) {
      const filtered = selectedInterests.filter(function (value) {
        return value !== value
      })
      setSelectedInterests([...filtered])
    } else {
      setSelectedInterests([...selectedInterests, value])
    }
  }

  const handleSubmit = async (event: MouseEvent): Promise<null | void> => {
    event.preventDefault()
    setLoader(true)
    interestsUpdateFuncHandler({ interests: selectedInterests })
      .then(({ data }) => {
        setLoader(false)
        toast.success("Interest updated!", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        })
      })
      .catch((error) => {
        setLoader(false)
        toast.error("error updating interest!", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        })
      })
  }

  useEffect(() => {
    getInterestsFuncHandler()
      .then(({ data }) => {
        setInterests([...data.interests])
      })
      .catch((error) => {
        toast.error(error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        })
      })
  }, [])

  const input = {
    selectedInterests,
    showLoader,
  }

  return (
    <>
      {children({
        input,
        interests,
        addUpInterest,
        handleSubmit,
      })}
    </>
  )
}

export default UpdateInterestsProvider
