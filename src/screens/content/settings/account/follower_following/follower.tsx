import { Button, CircularProgress, IconButton } from "@material-ui/core"
import { ArrowBack } from "@material-ui/icons"
import axios, { AxiosError } from "axios"
import { useCallback, useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useHistory } from "react-router"
import { toast } from "react-toastify"
import { apiUrl } from "../../../../../config/api"
import { createChat, followRes, RootState } from "../../../../../store"
import GroupIcon from "remixicon-react/GroupLineIcon"
import { BeatLoader } from "react-spinners"
import { ErrorOutline } from "@material-ui/icons"

export function AccountMyFollower(): JSX.Element {
  const [isLoading, setLoadingState] = useState<boolean>(true)
  const [error, setError] = useState<string>("")
  const [follower, setFollower] = useState<
    {
      user: {
        avatar: string
        _id: string
        firstName: string
        lastName: string
        username: string
      }
    }[]
  >([])
  const { push } = useHistory()
  const { token, myID } = useSelector((state: RootState) => {
    return {
      token: state.data.loginUserDetails?.token ?? "",
      myID: state.data.loginUserDetails?.user._id ?? "",
    }
  })
  const loadMyFollowing = useCallback(async () => {
    if (token === "") return
    if (token === undefined) return
    await axios
      .get(`${apiUrl}/follow/followers`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(({ data }) => {
        setFollower(data)
      })
      .catch((error: AxiosError) => {
        setError(error.response?.data?.message ?? "Network Error!")
        toast.error(error.response?.data?.message ?? "Network Error!")
      })
      .finally(() => {
        setLoadingState(false)
      })
  }, [token])
  useEffect(() => {
    loadMyFollowing()
  }, [loadMyFollowing])
  return (
    <>
      <section className="follow_container">
        <div className="main_head">
          <IconButton
            className="btn"
            onClick={() => {
              push(`/content/profile/${myID}`)
            }}
          >
            <ArrowBack />
          </IconButton>
          <h3 className="main_txt">My Followers</h3>
        </div>
        {isLoading ? (
          <>
            <div className="center">
              <CircularProgress color="inherit" />
            </div>
          </>
        ) : error !== "" ? (
          <>
            <div className="center">
              <ErrorOutline className="error_icon" />
              <span className="error_txt">{error}</span>
            </div>
          </>
        ) : follower.length <= 0 ? (
          <>
            <div className="center">
              <GroupIcon className="_icon" />
              <span className="txt">Your have no follower yet.</span>
            </div>
          </>
        ) : (
          follower.map((user) => {
            return (
              <FollowContainer
                myID={myID}
                setFollowing={(val) => {
                  setFollower(val)
                }}
                token={token}
                user={user}
                key={user.user._id}
                users={follower}
              />
            )
          })
        )}
      </section>
    </>
  )
}

function FollowContainer({
  user,
  myID,
  token,
  setFollowing,
}: {
  user: {
    user: {
      avatar: string
      _id: string
      firstName: string
      lastName: string
      username: string
    }
  }
  setFollowing: (
    value: {
      user: {
        avatar: string
        _id: string
        firstName: string
        lastName: string
        username: string
      }
    }[]
  ) => void
  myID: string
  token: string
  users: {
    user: {
      avatar: string
      _id: string
      firstName: string
      lastName: string
      username: string
    }
  }[]
}): JSX.Element {
  const dispatch = useDispatch()
  const { push } = useHistory()
  const [actionLoading, setActionLoadingState] = useState<boolean>(false)

  return (
    <div className="content" key={user.user._id}>
      <span className="avatar">
        {user.user.firstName[0]}
        {user.user.lastName[0]}
      </span>
      <div className="detail">
        <h4>
          {user.user.firstName} {user.user.lastName}
        </h4>
      </div>
      <div className="action">
        <Button
          className="btn"
          onClick={() => {
            push(`/content/profile/${user.user._id}`)
          }}
        >
          View Account
        </Button>
        <Button
          className="btn"
          onClick={async () => {
            setActionLoadingState(true)
            await followRes({ userID: user.user._id, token })
              .then(
                ({
                  data: {
                    data: { followers },
                  },
                }) => {
                  setFollowing(followers)
                }
              )
              .catch((error: AxiosError) => {
                toast.error(error.response?.data?.message ?? "Network Error!")
              })
              .finally(() => {
                setActionLoadingState(false)
              })
          }}
        >
          {actionLoading ? <BeatLoader /> : "Follow Account"}
        </Button>
        <Button
          className="btn"
          onClick={() => {
            dispatch(
              createChat({
                userId: user.user?._id,
                ecodeemId: myID,
              })
            )
            push("/content/chat")
          }}
        >
          Message Account
        </Button>
      </div>
    </div>
  )
}
