import React, { Fragment, useState } from "react"
import Button from "../../../../../components/Button/button"
import {
  IconButton,
  Input,
  InputAdornment,
  makeStyles,
  TextField,
} from "@material-ui/core"
import VisibilityIcon from "remixicon-react/EyeLineIcon"
import VisibilityOffIcon from "remixicon-react/EyeOffLineIcon"
import { Fingerprint, LockOutlined } from "@material-ui/icons"
import Config from "../../../../../config"
import { ApiLoadState } from "../../../../../components/loaders"
import { passwordUpdateFuncHandler } from "../../../../../function"
import { toast } from "react-toastify"
import { isStrongPassword } from "../../../../../utitly"

const useStyles = makeStyles(() => ({
  noBorder: {
    border: "none",
  },
}))
export const ChangePassword = (): JSX.Element => {
  const classes = useStyles()
  const [password, setPassword] = useState<string>("")
  const [repassword, setRepassword] = useState("")
  const [error, setError] = useState("")
  const [loading, setLoading] = useState(false)
  const [isVisible, setVisibleState] = useState<boolean>(false)
  const handleClick = async () => {
    if (password !== repassword) {
      setError("password does not match try again")

      return
    }

    setLoading(true)

    try {
      await passwordUpdateFuncHandler({ password })
      setLoading(false)
      setError("")
      toast.success("Password Updated Successfully", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      })
    } catch (error) {
      setLoading(false)
      toast.error("Password could not be updated", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      })
    }
  }
  return (
    <Fragment>
      <div className="Account__Right__Wrapper" style={{ padding: "10px 30px" }}>
        <div className="row">
          <div className="col-md-12 mb-5">
            <TextField
              fullWidth
              name="password"
              placeholder="new password"
              type={isVisible ? "text" : "password"}
              required
              label="New Password"
              onChange={(e) => setPassword(e.target.value)}
              error={!isStrongPassword(password)}
              helperText={
                password.length < 1 || isStrongPassword(password)
                  ? ""
                  : "Password must contain a at least one number and special character"
              }
              variant="outlined"
              style={{
                backgroundColor: "#F9F9F9",
                border: "none",
              }}
              InputProps={{
                "aria-label": "naked",
                classes: { notchedOutline: classes.noBorder },
                startAdornment: (
                  <InputAdornment position="start">
                    <LockOutlined />
                  </InputAdornment>
                ),
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      onClick={() => {
                        setVisibleState(!isVisible)
                      }}
                      className="btn dark"
                    >
                      {isVisible ? <VisibilityOffIcon /> : <VisibilityIcon />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
          </div>
        </div>

        <div className="row">
          <div className="col-md-12 mb-5">
            <TextField
              fullWidth
              variant="outlined"
              type={isVisible ? "text" : "password"}
              name="repassword"
              placeholder="re-type password"
              label="Config Password"
              onChange={(e) => setRepassword(e.target.value)}
              error={!isStrongPassword(password)}
              helperText={
                password.length < 1 || isStrongPassword(password)
                  ? ""
                  : "Password must contain a at least one number and special character"
              }
              style={{
                backgroundColor: "#F9F9F9",
                border: "none",
              }}
              InputProps={{
                "aria-label": "naked",
                classes: { notchedOutline: classes.noBorder },
                startAdornment: (
                  <InputAdornment position="start">
                    <LockOutlined />
                  </InputAdornment>
                ),
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      onClick={() => {
                        setVisibleState(!isVisible)
                      }}
                      className="btn dark"
                    >
                      {isVisible ? <VisibilityOffIcon /> : <VisibilityIcon />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
          </div>
        </div>

        <div className="Account__Right__Row">
          <div className="Account__Right__Row__Left"></div>
          <div className="Account__Right__Row__Right">
            <div className="Account__Right__Row__Right__Buttons">
              <div className="Account__Right__Row__Right__Buttons__Button">
                {loading ? (
                  <ApiLoadState />
                ) : (
                  <Button onClick={handleClick} type="dark" text="Change" />
                )}
              </div>
              <div className="Account__Right__Row__Right__Buttons__Button"></div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  )
}
