import React, {
  Fragment,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react"
import Button from "../../../../../components/Button/button"
import {
  Input as Input2,
  InputAdornment,
  makeStyles,
  TextField,
} from "@material-ui/core"
import {
  AccountCircleOutlined,
  AlternateEmailOutlined,
  CameraAlt,
  EmailOutlined,
  PhoneAndroidOutlined,
} from "@material-ui/icons"
import Config from "../../../../../config"
import { useProfile } from "../../../../../utitly"
import { ApiLoadState } from "../../../../../components/loaders"
import Avata from "../../../../../assets/images/user.png"
import {
  profilePicFuncHandler,
  profileUpdateFuncHandler,
} from "../../../../../function"
import { toast } from "react-toastify"
import { useDispatch } from "react-redux"
import { StoreType } from "../../../../../store"
import { Autocomplete } from "@material-ui/lab"
import { countryAndStates } from "../../../../../assets/country"

// import { profile } from "console";

const useStyles = makeStyles(() => ({
  noBorder: {
    border: "none",
  },
}))

export const EditProfile = () => {
  const classes = useStyles()
  const { profile } = useProfile()
  const [firstName, setFirstname] = useState<string | undefined>()
  const [lastName, setLastname] = useState<string | undefined>()
  const [country, setCountry] = useState<string>("")
  const [state, setState] = useState<string>("")
  const [username, setUsername] = useState<string | undefined>()
  const [bio, setBio] = useState<string | undefined>()
  const [phoneNumber, setPhone] = useState<string | undefined>()
  const [avata, setAvata] = useState(profile.user.avatar)
  const [mediaFile, setMediaFile] = useState<File | undefined>()
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState<{ status: boolean; text: string }>()
  const {
    profile: { token },
  } = useProfile()
  const errorRef = useRef<HTMLDivElement>(null)
  const dispatch = useDispatch()
  useEffect(() => {
    if (error?.status && errorRef.current) {
      errorRef.current.scrollIntoView({
        behavior: "smooth",
        block: "end",
        inline: "nearest",
      })
    }
  }, [error])

  const handleFileChange = async (selectorFiles: FileList | null) => {
    if (selectorFiles) {
      if (selectorFiles[0].size > 10000000) {
        setAvata("")
        setError({ status: true, text: "image cannot be more than 10mb" })
        return
      }

      setAvata(URL.createObjectURL(selectorFiles[0]))
      setMediaFile(selectorFiles[0])

      // console.log(avata);

      if (selectorFiles[0]) {
        try {
          const resp = await profilePicFuncHandler(selectorFiles[0])
          dispatch({
            type: StoreType.updatePic,
            value: resp.data,
          })
          toast.success("Profile picture updated", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          })
        } catch (error) {
          toast.error(`${error}`, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          })
        }
      }
    }
  }

  async function update() {
    setLoading(true)

    if (!firstName && !lastName && !username && !bio && !phoneNumber) {
      setError({ status: true, text: "you did not change any value" })
      setLoading(false)

      return
    }

    const resp = await profileUpdateFuncHandler({
      username,
      firstName,
      lastName,
      phoneNumber,
      bio,
    })
      .then(({ data }) => {
        setLoading(false)
        toast.success("Profile updated", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        })
        dispatch({
          type: StoreType.updateAfterEdit,
          value: data,
        })
      })
      .catch((error) => {
        console.error(error)
        setLoading(false)
        toast.error("Error updating profile", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        })
      })

    // const urlencoded = new URLSearchParams()
    // if (firstname) urlencoded.append("firstName", firstname)
    // if (lastname) urlencoded.append("lastName", lastname)
    // if (username) urlencoded.append("username", username)

    // console.log(username)

    // const requestOptions = {
    //   method: "PUT",
    //   body: urlencoded,
    // }

    // fetch(Config.apiUrl + "/user/update/", requestOptions)
    //   .then((response) => response.json())
    //   .then((result) => {
    //     console.log("====================================")
    //     console.log(result)
    //     console.log("====================================")

    //     const data: any = localStorage.getItem("user")
    //     const p = JSON.parse(data)

    //     const newData = {
    //       ...p,
    //       user: result.data,
    //     }

    //     localStorage.setItem("user", JSON.stringify(newData))

    //     console.log("====================================")
    //     console.log(newData)
    //     console.log("====================================")

    //     setLoading(false)

    //     // setToast({ type: result.status, text: result.message })

    //     setTimeout(() => {
    //       //   setToast(null)
    //     }, 2000)
    //   })
    //   .catch((error) => console.log("error", error))
  }

  const hiddenFileInput = useRef<HTMLInputElement>(null)

  const handleClick = (event) => {
    hiddenFileInput?.current?.click()
  }
  const callback = useCallback(() => {
    setCountry(profile.user.country)
    setState(profile.user.state)
  }, [profile])
  useEffect(() => {
    callback()
  }, [callback])

  return (
    <Fragment>
      <div className="Account__Right__Wrapper" style={{ padding: "10px 30px" }}>
        {error?.status && (
          <div className="text-danger text-center mb-3" ref={errorRef}>
            {error.text}
          </div>
        )}
        <div className="m-auto Setting__Holder mb-5">
          <span onClick={handleClick} className="Setting__Holder__IconHolder">
            <CameraAlt />
          </span>
          <img
            height="100px 100px rounded"
            src={avata == "" ? Avata : avata}
            alt=""
          />
          <input
            ref={hiddenFileInput}
            style={{ display: "none" }}
            type="file"
            onChange={(e) => {
              handleFileChange(e.target.files)
              e.target.value = ""
            }}
          />
        </div>
        <div className="row">
          <div className="col-md-12 mb-5">
            <TextField
              fullWidth
              name="firstname"
              placeholder="Enter your first name"
              required
              label="First Name"
              variant="outlined"
              value={firstName}
              onChange={(e) => setFirstname(e.target.value)}
              type="text"
              style={{
                backgroundColor: "#F9F9F9",
                border: "none",
              }}
              InputProps={{
                "aria-label": "naked",
                defaultValue: profile.user.firstName,
                classes: { notchedOutline: classes.noBorder },
                startAdornment: (
                  <InputAdornment position="start">
                    <AccountCircleOutlined />
                  </InputAdornment>
                ),
              }}
            />
          </div>
        </div>

        <div className="row">
          <div className="col-md-12 mb-5">
            <TextField
              fullWidth
              name="lastname"
              placeholder="Enter your last name"
              required
              label="Last Name"
              variant="outlined"
              value={lastName}
              onChange={(e) => setLastname(e.target.value)}
              type="text"
              style={{
                backgroundColor: "#F9F9F9",
                border: "none",
              }}
              InputProps={{
                "aria-label": "naked",
                defaultValue: profile.user.lastName,
                classes: { notchedOutline: classes.noBorder },
                startAdornment: (
                  <InputAdornment position="start">
                    <AccountCircleOutlined />
                  </InputAdornment>
                ),
              }}
            />
          </div>
        </div>

        <hr />

        <div className="row">
          <div className="col-md-12 mb-5">
            <TextField
              fullWidth
              name="email"
              placeholder="Enter your email address"
              required
              label="Email"
              disabled
              variant="outlined"
              type="text"
              style={{
                backgroundColor: "#F9F9F9",
                border: "none",
              }}
              InputProps={{
                "aria-label": "naked",
                defaultValue: profile.user.email,
                classes: { notchedOutline: classes.noBorder },
                startAdornment: (
                  <InputAdornment position="start">
                    <EmailOutlined />
                  </InputAdornment>
                ),
              }}
            />
          </div>
        </div>

        <div className="row">
          <div className="col-md-12 mb-5">
            <TextField
              fullWidth
              name="phone"
              placeholder="Enter your phone number"
              onChange={(e) => setPhone(e.target.value)}
              required
              variant="outlined"
              type="text"
              label="Phone number"
              style={{
                backgroundColor: "#F9F9F9",
                border: "none",
              }}
              InputProps={{
                "aria-label": "naked",
                defaultValue: profile.user.phoneNumber,
                classes: { notchedOutline: classes.noBorder },
                startAdornment: (
                  <InputAdornment position="start">
                    <PhoneAndroidOutlined />
                  </InputAdornment>
                ),
              }}
            />
          </div>
        </div>

        <div className="row">
          <div className="col-md-12 mb-5">
            <Autocomplete
              id="countries"
              options={countryAndStates}
              getOptionLabel={(option) => option.name}
              placeholder="Enter your Country"
              className="inputField"
              style={{
                backgroundColor: "#F9F9F9",
                border: "none",
              }}
              value={countryAndStates.find(
                (_country) =>
                  _country.name.toLowerCase() === country.toLowerCase()
              )}
              onChange={(e, i) => {
                setCountry(i?.name ?? "")
              }}
              renderInput={(params) => (
                <TextField {...params} variant="outlined" required />
              )}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-md-12 mb-5">
            <Autocomplete
              id="state"
              options={
                countryAndStates.find(
                  (_country) =>
                    _country.name.toLowerCase() === country.toLowerCase()
                )?.states ?? []
              }
              getOptionLabel={(option) => option.name}
              placeholder="Enter your state"
              className="inputField"
              style={{
                backgroundColor: "#F9F9F9",
                border: "none",
              }}
              value={countryAndStates
                .find(
                  (_country) =>
                    _country.name.toLowerCase() === country.toLowerCase()
                )
                ?.states.find(
                  (_state) => _state.name.toLowerCase() === state.toLowerCase()
                )}
              onChange={(e, i) => {
                setState(i?.name ?? "")
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  variant="outlined"
                  required
                  helperText={
                    state.length < 1 ||
                    countryAndStates
                      .find(
                        (_country) =>
                          _country.name.toLowerCase() === country.toLowerCase()
                      )
                      ?.states.find(
                        (_state) =>
                          _state.name.toLowerCase() === state.toLowerCase()
                      ) !== undefined
                      ? ""
                      : "Please enter your state of residence"
                  }
                  error={
                    state.length < 1 ||
                    countryAndStates
                      .find(
                        (_country) =>
                          _country.name.toLowerCase() === country.toLowerCase()
                      )
                      ?.states.find(
                        (_state) =>
                          _state.name.toLowerCase() === state.toLowerCase()
                      ) === undefined
                  }
                />
              )}
            />
          </div>
        </div>

        <div className="row">
          <div className="col-md-12 mb-5">
            <TextField
              fullWidth
              name="bio"
              placeholder="Enter your bio"
              required
              rows={10}
              label="Bio"
              onChange={(e) => setBio(e.target.value)}
              multiline
              variant="outlined"
              type="text"
              style={{
                backgroundColor: "#F9F9F9",
                border: "none",
              }}
              InputProps={{
                "aria-label": "naked",

                defaultValue: profile.user.bio,

                classes: { notchedOutline: classes.noBorder },
              }}
            />
          </div>
        </div>

        <div className="Account__Right__Row">
          <div className="Account__Right__Row__Left"></div>
          <div className="Account__Right__Row__Right">
            <div className="Account__Right__Row__Right__Buttons">
              <div className="Account__Right__Row__Right__Buttons__Button">
                {loading ? (
                  <ApiLoadState />
                ) : (
                  <Button onClick={update} type="dark" text="Change" />
                )}
              </div>
              <div className="Account__Right__Row__Right__Buttons__Button"></div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  )
}
