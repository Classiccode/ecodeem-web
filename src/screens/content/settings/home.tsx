import {
  AccountCircleOutlined,
  ArrowRightAltOutlined,
  CameraAlt,
  ExitToAppOutlined,
  HistoryOutlined,
  PaymentOutlined,
  PhoneAndroidOutlined,
} from "@material-ui/icons"
import { useHistory } from "react-router-dom"
import { useProfile } from "../../../utitly"
import Config from "../../../config"
import AboutIcon from "remixicon-react/ErrorWarningLineIcon"

import Avata from "../../../assets/images/user.png"
export const SettingsMainPage = (): JSX.Element => {
  const profile = useProfile()
  const history = useHistory()

  const logoutSession = (): void => {
    const myHeaders = new Headers()

    myHeaders.append("Authorization", `Bearer ${profile.profile.token}`)

    const requestOptions = {
      method: "POST",
      headers: myHeaders,
    }

    // setLoggingOff(true)

    fetch(Config.apiUrl + "/user/logout", requestOptions)
      .then((response) => response.json())
      .then((result) => {
        if (result.status == "success") {
          // setLoggingOff(false)

          localStorage.clear()
          window.location.reload()
          history.push("/")
        } else {
          // setLoggingOff(false)
        }
      })
      .catch((error) => console.error("error", error))
  }

  return (
    <>
      <div className="Setting" style={{ display: "flex" }}>
        <div className="w-100  mt-5 pr-5 pl-5">
          <div className="m-auto Setting__Holder">
            <span className="Setting__Holder__IconHolder">
              <CameraAlt />
            </span>
            <img
              height="100px 100px rounded"
              src={
                profile.profile.user.avatar == ""
                  ? Avata
                  : profile.profile.user.avatar
              }
              alt=""
            />
          </div>
          <div className="Setting__Body">
            <p>Welcome</p>
            <h2 className="font-color">{`${profile.profile.user.firstName} ${profile.profile.user.lastName}`}</h2>

            <ul>
              <li
                onClick={() =>
                  history.push("/content/profile/account/setting/general")
                }
              >
                <div className="rigth">
                  <div className="icon">
                    <PhoneAndroidOutlined fontSize="default" />
                  </div>
                  <p>General</p>
                </div>
                <div className="left">
                  <ArrowRightAltOutlined />
                </div>
              </li>
              <li
                onClick={() =>
                  history.push("/content/profile/account/setting/account")
                }
              >
                <div className="rigth">
                  <div className="icon">
                    <AccountCircleOutlined fontSize="default" />
                  </div>
                  <p>Account Settings</p>
                </div>
                <div className="left">
                  <ArrowRightAltOutlined />
                </div>
              </li>
              <li>
                <div className="rigth">
                  <div className="icon">
                    <PaymentOutlined fontSize="default" />
                  </div>
                  <p>My Cards</p>
                </div>
                <div className="left">
                  <ArrowRightAltOutlined />
                </div>
              </li>
              <li
                onClick={() => {
                  history.push("/content/market/orders")
                }}
              >
                <div className="rigth">
                  <div className="icon">
                    <HistoryOutlined fontSize="default" />
                  </div>
                  <p>Order History</p>
                </div>
                <div className="left">
                  <ArrowRightAltOutlined />
                </div>
              </li>
              <li>
                <div className="rigth">
                  <div className="icon">
                    <AboutIcon fontSize="default" />
                  </div>
                  <p>About Us</p>
                </div>
                <div className="left">
                  <ArrowRightAltOutlined />
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </>
  )
}
