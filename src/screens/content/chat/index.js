import React, { useEffect, useState, useRef } from "react"

//Import Components
import ChatLeftSidebar from "./ChatLeftSidebar"
import UserChat from "./UserChat/"
import { connect, useSelector } from "react-redux"
import { ContentLayout } from "../../../layout"
import AuthLayout from "../../../layout/chat"
// import useChat from '../../helpers/useChat';
// import useTyping from '../../helpers/useTyping';

export const ChatPageMain = () => {
  const { chats } = useSelector((state) => state.Chat)
  return (
    <>
      <AuthLayout>
        <div className="d-lg-flex">
          {/* chat left sidebar */}
          <ChatLeftSidebar recentChatList={chats} />

          {/* user chat */}
          {chats.length > 0 && <UserChat recentChatList={chats} />}
        </div>
      </AuthLayout>
    </>
  )
}
