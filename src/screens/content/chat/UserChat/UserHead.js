import React, { useState, useEffect } from 'react';
import { Dropdown, DropdownMenu, DropdownItem, DropdownToggle, Button, Input, Row, Col, Modal, ModalBody } from "reactstrap";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import { openUserSidebar, setFullUser } from "../../../../store/action/index";

//import images
import { getChatter } from '../../../../helpers/common';

function UserHead(props) {
    const [dropdownOpen, setDropdownOpen] = useState(false);
    const [dropdownOpen1, setDropdownOpen1] = useState(false);
    const [Callmodal, setCallModal] = useState(false);
    const [Videomodal, setVideoModal] = useState(false);

    const toggle = () => setDropdownOpen(!dropdownOpen);
    const toggle1 = () => setDropdownOpen1(!dropdownOpen1);
    const toggleCallModal = () => setCallModal(!Callmodal);
    const toggleVideoModal = () => setVideoModal(!Videomodal);

    const openUserSidebar = (e) => {
        e.preventDefault();
        props.openUserSidebar();
    }

    function closeUserChat(e) {
        e.preventDefault();
        var userChat = document.getElementsByClassName("user-chat");
        if (userChat) {
            userChat[0].classList.remove("user-chat-show");
        }
    }

    function deleteMessage() {
        let allUsers = props.users;
        let copyallUsers = allUsers;
        copyallUsers[props.active_user].messages = [];

        props.setFullUser(copyallUsers);
    }

    return (
        <React.Fragment>
            <div className="p-3 p-lg-4 border-bottom">
                <Row className="align-items-center">
                    <Col sm={4} xs={8}>
                        <div className="d-flex align-items-center">
                            <div className="d-block d-lg-none me-2 ms-0">
                                <Link to="#" onClick={(e) => closeUserChat(e)} className="user-chat-remove text-muted font-size-16 p-2">
                                    <i className="ri-arrow-left-s-line"></i></Link>
                            </div>
                            {props.loadingRecentConversation == true ? 'loading...' :
                                props.chats[props.active_user].type === 'private' ?
                                    getChatter(props.chats[props.active_user].users, props.user).avatar ?
                                        <div className="me-3 ms-0">
                                            <img src={getChatter(props.chats[props.active_user].users, props.user).avatar} className="rounded-circle avatar-xs" alt="chatvia" />
                                        </div>
                                        : <div className="chat-user-img align-self-center me-3">
                                            <div className="avatar-xs">
                                                <span className="avatar-title rounded-circle bg-soft-primary text-primary">
                                                    {props.loadingRecentConversation == true ? 'loading' : getChatter(props.chats[props.active_user].users, props.user).firstName.charAt(0)}
                                                </span>
                                            </div>
                                        </div> : ''
                            }
                            {
                                props.loadingRecentConversation == true ? 'loading...' :
                                    props.chats[props.active_user].type === 'group' ?
                                        props.chats[props.active_user].avatar ?
                                            <div className="me-3 ms-0">
                                                <img src={props.chats[props.active_user].avatar} className="rounded-circle avatar-xs" alt="chatvia" />
                                            </div>
                                            : <div className="chat-user-img align-self-center me-3">
                                                <div className="avatar-xs">
                                                    <span className="avatar-title rounded-circle bg-soft-primary text-primary">
                                                        {props.chats[props.active_user].name.charAt()}
                                                    </span>
                                                </div>
                                            </div> : ''
                            }
                            <div className="flex-1 overflow-hidden">
                                <h5 className="font-size-16 mb-0 text-truncate">
                                    <Link to="#" onClick={(e) => openUserSidebar(e)} className="text-reset user-profile-show">
                                        {
                                            props.loadingRecentConversation == true ? 'loading...' :
                                                props.chats[props.active_user].type === 'group' ? `${props.chats[props.active_user].name}` : `${getChatter(props.chats[props.active_user].users, props.user).firstName} ${getChatter(props.chats[props.active_user].users, props.user).lastName}`

                                        }
                                    </Link>
                                </h5>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        </React.Fragment>
    );
}


const mapStateToProps = (state) => {
    const { chats, users, active_user, loadingRecentConversation } = state.Chat;
    const { user } = state.data.loginUserDetails || {};
    return { ...state.Layout, users, active_user, chats, loadingRecentConversation, user };

};

export default connect(mapStateToProps, { openUserSidebar, setFullUser })(UserHead);