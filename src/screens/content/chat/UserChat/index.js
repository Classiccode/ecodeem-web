import React, { useState, useEffect, useRef } from "react"
import {
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
  UncontrolledDropdown,
  Modal,
  ModalHeader,
  ModalBody,
  CardBody,
  Button,
  ModalFooter,
} from "reactstrap"
import { connect, useDispatch } from "react-redux"
import socketIOClient from "socket.io-client"
import SimpleBar from "simplebar-react"
import moment from "moment"

import { withRouter } from "react-router-dom"

//Import Components
import UserProfileSidebar from "../../../../components/chat-components/UserProfileSidebar"
import UserHead from "./UserHead"
import ImageList from "./ImageList"
import ChatInput from "./ChatInput"
import FileList from "./FileList"

//actions
import {
  openUserSidebar,
  sendMessage,
  setFullUser,
  markAsRead,
} from "../../../../store/action/index"

//i18n
import { useTranslation } from "react-i18next"
// import { useProfile } from '../../../../../components/reducers/profile';
import useTyping from "../../../../helpers/useTyping"
import useChat from "../../../../helpers/useChat"
import Config from "../../../../config"
import axios from "axios"
import { useProfile } from "../../../../utitly"

function UserChat(props) {
  const { isTyping, startTyping, stopTyping, cancelTyping } = useTyping()
  const {
    typingUsers,
    startTypingMessage,
    stopTypingMessage,
    socketRef,
    joinRoom,
    message,
  } = useChat()
  const dispatch = useDispatch()

  const ref = useRef()
  const scrollRef = useRef(null)

  const profile = useProfile()
  const [modal, setModal] = useState(false)

  /* intilize t variable for multi language implementation */
  const { t } = useTranslation()

  //demo conversation messages
  //userType must be required
  const [conversation, setCurrentConversations] = useState([])
  const [chatMessages, setchatMessages] = useState([])
  const [chatId, setChatId] = useState()
  const [currentUser, setCurrentUser] = useState()
  const [page, setPage] = useState(1)

  // socketRef.current.on("connect", () => {
  //     console.log('user connected '+socketRef.current.id);
  // socketRef.current.emit('join', 'kkmdkmkdmvskd')npm
  //   });

  // joinRoom('jnjnsjncs')

  useEffect(() => {
    if (
      props.loadingRecentConversation == false &&
      message &&
      props.recentChatList[props.active_user]._id == message.chatRoomId
    ) {
      setchatMessages([...chatMessages, message])
    }
  }, [message, props.active_user, props.recentChatList])

  useEffect(() => {
    if (props.loadingRecentConversation == false) {
      props.recentChatList.forEach(({ _id }) => {
        joinRoom(_id)
      })
    }
  }, [props.recentChatList, props.loadingRecentConversation])
  useEffect(() => {
    if (props.loadingRecentConversation == false) {
      dispatch(
        markAsRead(
          profile.profile.user._id,
          props.recentChatList[props.active_user]._id
        )
      )
    }
  }, [props.active_user, props.recentChatList, props.loadingRecentConversation])

  useEffect(() => {
    if (props.loadingRecentConversation == false) {
      setchatMessages(props.recentChatList[props.active_user].recentMessages)
      setChatId(props.recentChatList[props.active_user]._id)
      setCurrentUser(props.recentChatList[props.active_user].users)
      setCurrentConversations(props.recentChatList)
      ref.current.recalculate()
      if (ref.current.el) {
        ref.current.getScrollElement().scrollTop =
          ref.current.getScrollElement().scrollHeight
      }
    }
  }, [props.active_user, props.recentChatList, props.loadingRecentConversation])

  useEffect(() => {
    if (props.loadingRecentConversation == false) {
      if (isTyping)
        startTypingMessage(
          props.recentChatList[props.active_user]._id,
          props.user.firstName,
          props.user._id
        )
      else
        stopTypingMessage(
          props.recentChatList[props.active_user]._id,
          props.user.firstName,
          props.user._id
        )
    }
  }, [
    isTyping,
    props.active_user,
    props.recentChatList,
    props.loadingRecentConversation,
  ])

  function scrolltoBottom() {
    if (scrollRef.current) {
      scrollRef.current.scrollIntoView({
        behavior: "smooth",
      })
    }
  }

  useEffect(() => {
    scrolltoBottom()
  }, [props.active_user])

  useEffect(() => {
    scrolltoBottom()
  }, [])

  const addMessage = (message, type) => {
    //  let postObj =  {
    //     chatRoomId: chatId,
    //     createdAt: Date.now(),
    //     link: type message,
    //     message: message,
    //     postedBy: {
    //         _id: profile.profile.user._id,
    //         firstName: profile.profile.user.firstName,
    //         lastName: profile.profile.user.lastName,
    //         ecodeemId: profile.profile.user._id,
    //         username: profile.profile.user.username,
    //         avatar: profile.profile.user.avatar
    //     },
    //     postedByUser: "60d459b17926232a60ec71ab",
    //     type: type,
    // }
    var messageObj = null

    let d = new Date()
    var n = d.getSeconds()

    //matches the message type is text, file or image, and create object according to it
    switch (type) {
      case "text":
        messageObj = {
          roomId: chatId,
          ecodeemId: profile.profile.user._id,
          messageText: message,
          type: "text",
        }
        break

      case "document":
        messageObj = {
          roomId: chatId,
          ecodeemId: profile.profile.user._id,
          messageText: message,
          type: "document",
          link: message,
        }
        break

      case "image":
        messageObj = {
          roomId: chatId,
          ecodeemId: profile.profile.user._id,
          messageText: message,
          type: "document",
          link: message,
        }
        break

      default:
        break
    }

    //add message object to chat
    // setchatMessages([...chatMessages, postObj]);

    // let copyallUsers = [...allUsers];
    // copyallUsers[props.active_user].messages = [...chatMessages, messageObj];
    // copyallUsers[props.active_user].isTyping = false;
    // props.setFullUser(copyallUsers);

    dispatch(sendMessage(messageObj))

    scrolltoBottom()
  }

  const deleteMessage = (id) => {
    let conversation = chatMessages

    var filtered = conversation.filter(function (item) {
      return item.id !== id
    })

    setchatMessages(filtered)
  }

  const getMoreMessages = async () => {
    try {
      let result = await axios.get(
        `${Config.chatApiUrl}/room/${chatId}?page=${page}&limit=20`
      )

      // setchatMessages(messages => messages.push(result.messages))

      let reversed = result.messages.reverse()

      setchatMessages([...reversed, ...chatMessages])
      setPage(page + 1)
    } catch (error) {
      console.error(error)
    }
  }

  return (
    <React.Fragment>
      <div className="user-chat">
        <div className="d-lg-flex">
          <div className={props.userSidebar ? "w-70" : "w-100"}>
            {/* render user head */}
            <UserHead />

            <SimpleBar
              style={{ maxHeight: "100%" }}
              ref={ref}
              className="chat-conversation p-3 p-lg-4"
              id="messages"
            >
              <ul className="list-unstyled mb-0">
                <li>
                  <Button
                    onClick={() => getMoreMessages()}
                    className="text-decoration-none text-center text-muted pr-1"
                    type="button"
                  >
                    load more
                  </Button>
                </li>

                {props.loadingRecentConversation == false &&
                  chatMessages.map((chat, key) => (
                    // <li>{chat._id}</li>
                    // chat.isToday && chat.isToday === true ? <li key={"dayTitle" + key}>
                    //     <div className="chat-day-title">
                    //         <span className="title">Today</span>
                    //     </div>
                    // </li> :
                    // (props.recentChatList[props.active_user].type === 'group') ?

                    <li
                      key={key}
                      className={
                        chat.postedBy.ecodeemId === profile.profile.user._id
                          ? "right"
                          : ""
                      }
                    >
                      <div className="conversation-list">
                        <div className="chat-avatar">
                          {!chat.postedBy.avatar ? (
                            <div className="chat-user-img align-self-center me-3">
                              <div className="avatar-xs">
                                <span className="avatar-title rounded-circle bg-soft-primary text-primary">
                                  {chat.postedBy.firstName.charAt(0)}
                                </span>
                              </div>
                            </div>
                          ) : (
                            <img src={chat.postedBy.avatar} alt="chatvia" />
                          )}
                        </div>

                        <div
                          className="user-chat-content"
                          style={{
                            width: chat.type == "image" ? "min-content" : "",
                          }}
                        >
                          <div className="ctext-wrap">
                            <div className="ctext-wrap-content">
                              {chat.type == "text" && (
                                <p className="mb-0">{chat.message}</p>
                              )}
                              {chat.type == "image" && (
                                // image list component
                                <ImageList message={chat} />
                              )}
                              {/* {
                                                                    chat.type == 'document' &&
                                                                        //file input component
                                                                        <FileList fileName={chat} fileSize={chat.size} />
                                                                } */}
                              {
                                <p className="chat-time mb-0">
                                  <i className="ri-time-line align-middle"></i>{" "}
                                  <span className="align-middle">
                                    {moment(chat.createdAt).fromNow()}
                                  </span>
                                </p>
                              }
                            </div>
                            {chat.postedBy.ecodeemId == props.user._id && (
                              <UncontrolledDropdown className="align-self-start">
                                <DropdownToggle tag="a">
                                  <i className="ri-more-2-fill"></i>
                                </DropdownToggle>
                                <DropdownMenu>
                                  <DropdownItem
                                    onClick={() => deleteMessage(chat.id)}
                                  >
                                    Delete{" "}
                                    <i className="ri-delete-bin-line float-end text-muted"></i>
                                  </DropdownItem>
                                </DropdownMenu>
                              </UncontrolledDropdown>
                            )}
                          </div>
                          {chat.type == "private" && (
                            <div className="conversation-name">
                              {chat.postedBy.firstName}
                            </div>
                          )}
                        </div>
                      </div>
                    </li>
                  ))}
                {typingUsers.filter(function (e) {
                  return e.conversationId === chatId
                }).length > 0 && (
                  <li>
                    <div className="conversation-list">
                      <div className="user-chat-content">
                        <div className="ctext-wrap">
                          <div className="ctext-wrap-content">
                            <p className="mb-0">
                              typing
                              <span className="animate-typing">
                                <span className="dot ms-1"></span>
                                <span className="dot ms-1"></span>
                                <span className="dot ms-1"></span>
                              </span>
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                )}
              </ul>
            </SimpleBar>

            <ChatInput
              handleStartTyping={startTyping}
              handleStopTyping={stopTyping}
              chatId={chatId}
              onaddMessage={addMessage}
            />
            <div style={{ float: "left", clear: "both" }} ref={scrollRef}></div>
          </div>
          {props.loadingRecentConversation == false ? (
            <UserProfileSidebar
              conversation={props.recentChatList[props.active_user]}
              user={props.user}
            />
          ) : (
            ""
          )}
        </div>
      </div>
    </React.Fragment>
  )
}

const mapStateToProps = (state) => {
  const { active_user, loadingRecentConversation } = state.Chat
  const { user } = state.data.loginUserDetails || {}
  const { userSidebar } = state.Layout
  return { user, active_user, userSidebar, loadingRecentConversation }
}

export default withRouter(
  connect(mapStateToProps, {
    sendMessage,
    openUserSidebar,
    setFullUser,
    markAsRead,
  })(UserChat)
)
