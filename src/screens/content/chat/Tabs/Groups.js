import React, { Component } from "react"
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  UncontrolledTooltip,
  Form,
  Label,
  Input,
  Collapse,
  CardHeader,
  CardBody,
  Alert,
  InputGroup,
  Card,
  Badge,
} from "reactstrap"
import { Link } from "react-router-dom"
import { connect } from "react-redux"

import { withTranslation } from "react-i18next"

//simple bar
import SimpleBar from "simplebar-react"

//components
import SelectContact from "../../../../components/chat-components/SelectContact"

//actions
import {
  createGroup,
  getGroups,
  activeUser,
  getChats,
} from "../../../../store/action/index"

class Groups extends Component {
  constructor(props) {
    super(props)
    this.state = {
      modal: false,
      isOpenCollapse: false,
      groups: this.props.groups,
      selectedContact: [],
      isOpenAlert: false,
      message: "",
      groupName: "",
      groupDesc: "",
    }
    this.toggle = this.toggle.bind(this)
    this.toggleCollapse = this.toggleCollapse.bind(this)
    this.createGroup = this.createGroup.bind(this)
    this.handleCheck = this.handleCheck.bind(this)
    this.handleChangeGroupName = this.handleChangeGroupName.bind(this)
    this.handleChangeGroupDesc = this.handleChangeGroupDesc.bind(this)
    this.openUserChat = this.openUserChat.bind(this)
  }

  toggle() {
    this.setState({ modal: !this.state.modal })
  }

  toggleCollapse() {
    this.setState({ isOpenCollapse: !this.state.isOpenCollapse })
  }

  componentDidMount() {
    this.props.getGroups(this.props.user._id)
  }

  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) {
      this.setState({
        groups: this.props.groups,
      })
    }
  }

  createGroup() {
    if (this.state.selectedContact.length > 0) {
      // gourpId : 5, name : "#Project-aplha", profilePicture : "Null", isGroup : true, unRead : 0, isNew : true, desc : "project related Group",
      var obj = {
        // gourpId: this.state.groups.length + 1,
        groupname: this.state.groupName,
        // avatar: "Null",
        ecodeemId: this.props.user._id,
        type: "group",
        // isGroup: true,
        // unRead: 0,
        // isNew: true,
        groupdescription: this.state.groupDesc,
        userIds: this.state.selectedContact,
      }

      //call action for creating a group
      this.props.createGroup(obj)
      this.toggle()

      // } else if (this.state.selectedContact.length === 1) {
      //     this.setState({ message: "Minimum 2 members required!!!", isOpenAlert: true });
    } else {
      this.setState({ message: "Please Select Members!!!", isOpenAlert: true })
    }
    setTimeout(
      function () {
        this.setState({ isOpenAlert: false })
      }.bind(this),
      3000
    )
  }

  handleChangeGroupDesc(e) {
    this.setState({ groupDesc: e.target.value })
  }

  handleCheck(e, contactId) {
    var selected = this.state.selectedContact
    var obj
    if (e.target.checked) {
      obj = {
        id: contactId,
        name: e.target.value,
      }
      selected.push(contactId)
      this.setState({ selectedContact: selected })
    }
  }

  handleChangeGroupName(e) {
    this.setState({ groupName: e.target.value })
  }

  createGroup() {
    if (this.state.selectedContact.length > 0) {
      // gourpId : 5, name : "#Project-aplha", profilePicture : "Null", isGroup : true, unRead : 0, isNew : true, desc : "project related Group",
      var obj = {
        // gourpId: this.state.groups.length + 1,
        groupname: this.state.groupName,
        // avatar: "Null",
        ecodeemId: this.props.user._id,
        type: "group",
        // isGroup: true,
        // unRead: 0,
        // isNew: true,
        groupdescription: this.state.groupDesc,
        userIds: this.state.selectedContact,
      }

      //call action for creating a group
      this.props.createGroup(obj)
      this.toggle()

      // } else if (this.state.selectedContact.length === 1) {
      //     this.setState({ message: "Minimum 2 members required!!!", isOpenAlert: true });
    } else {
      this.setState({ message: "Please Select Members!!!", isOpenAlert: true })
    }
    setTimeout(
      function () {
        this.setState({ isOpenAlert: false })
      }.bind(this),
      3000
    )
  }

  openUserChat(e, chat) {
    e.preventDefault()

    //find index of current chat in array
    // var index = this.props.chats.indexOf(chat);
    const index = this.props.chats.findIndex((item) => item._id === chat._id)

    // set activeUser
    this.props.activeUser(index)

    var chatList = document.getElementById("chat-list")
    var clickedItem = e.target
    var currentli = null

    if (chatList) {
      var li = chatList.getElementsByTagName("li")
      //remove coversation user
      for (var i = 0; i < li.length; ++i) {
        if (li[i].classList.contains("active")) {
          li[i].classList.remove("active")
        }
      }
      //find clicked coversation user
      for (var k = 0; k < li.length; ++k) {
        if (li[k].contains(clickedItem)) {
          currentli = li[k]
          break
        }
      }
    }

    //activation of clicked coversation user
    if (currentli) {
      currentli.classList.add("active")
    }

    var userChat = document.getElementsByClassName("user-chat")

    if (userChat) {
      userChat[0].classList.add("user-chat-show")
    }

    //removes unread badge if user clicks
    var unread = document.getElementById("unRead" + chat._id)
    if (unread) {
      unread.style.display = "none"
    }
  }

  render() {
    const { t } = this.props

    // console.log('====================================');
    // console.log(this.props.groups,this.props.loadingChats);
    // console.log('====================================');

    return (
      <React.Fragment>
        <div>
          <div className="p-4">
            <div className="user-chat-nav float-end">
              <div id="create-group">
                {/* Button trigger modal */}
                <Button
                  onClick={this.toggle}
                  type="button"
                  color="link"
                  className="text-decoration-none text-muted font-size-18 py-0"
                >
                  <i className="ri-group-line me-1"></i>
                </Button>
              </div>
              <UncontrolledTooltip target="create-group" placement="bottom">
                Create group
              </UncontrolledTooltip>
            </div>
            <h4 className="mb-4">{t("Groups")}</h4>

            {/* Start add group Modal */}
            <Modal isOpen={this.state.modal} centered toggle={this.toggle}>
              <ModalHeader
                tag="h5"
                className="modal-title font-size-14"
                toggle={this.toggle}
              >
                {t("Create New Group")}
              </ModalHeader>
              <ModalBody className="p-4">
                <Form>
                  <div className="mb-4">
                    <Label className="form-label" htmlFor="addgroupname-input">
                      {t("Group Name")}
                    </Label>
                    <Input
                      type="text"
                      className="form-control"
                      id="addgroupname-input"
                      value={this.state.groupName}
                      onChange={(e) => this.handleChangeGroupName(e)}
                      placeholder="Enter Group Name"
                    />
                  </div>
                  <div className="mb-4">
                    <Label className="form-label">{t("Group Members")}</Label>
                    <Alert isOpen={this.state.isOpenAlert} color="danger">
                      {this.state.message}
                    </Alert>
                    <div className="mb-3">
                      <Button
                        color="light"
                        size="sm"
                        type="button"
                        onClick={this.toggleCollapse}
                      >
                        {t("Select Members")}
                      </Button>
                    </div>

                    <Collapse
                      isOpen={this.state.isOpenCollapse}
                      id="groupmembercollapse"
                    >
                      <Card className="border">
                        <CardHeader>
                          <h5 className="font-size-15 mb-0">{t("Contacts")}</h5>
                        </CardHeader>
                        <CardBody className="p-2">
                          <SimpleBar style={{ maxHeight: "150px" }}>
                            {/* contacts */}
                            <div id="addContacts">
                              <SelectContact handleCheck={this.handleCheck} />
                            </div>
                          </SimpleBar>
                        </CardBody>
                      </Card>
                    </Collapse>
                  </div>
                  <div>
                    <Label
                      className="form-label"
                      htmlFor="addgroupdescription-input"
                    >
                      Description
                    </Label>
                    <textarea
                      className="form-control"
                      id="addgroupdescription-input"
                      value={this.state.groupDesc}
                      onChange={(e) => this.handleChangeGroupDesc(e)}
                      rows="3"
                      placeholder="Enter Description"
                    ></textarea>
                  </div>
                </Form>
              </ModalBody>
              <ModalFooter>
                <Button type="button" color="link" onClick={this.toggle}>
                  {t("Close")}
                </Button>
                <Button
                  type="button"
                  color="primary"
                  onClick={this.createGroup}
                >
                  Create Group
                </Button>
              </ModalFooter>
            </Modal>
            {/* End add group Modal */}

            <div className="search-box chat-search-box">
              <InputGroup size="lg" className="bg-light rounded-lg">
                <Button
                  color="link"
                  className="text-decoration-none text-muted pr-1"
                  type="button"
                >
                  <i className="ri-search-line search-icon font-size-18"></i>
                </Button>
                <Input
                  type="text"
                  className="form-control bg-light"
                  placeholder="Search groups..."
                />
              </InputGroup>
            </div>
            {/* end search-box */}
          </div>

          {/* Start chat-group-list */}
          <SimpleBar
            style={{ maxHeight: "100%" }}
            className="p-4 chat-message-list chat-group-list"
          >
            <ul className="list-unstyled chat-list">
              {this.props.loadingChats && this.props.loadingRecentConversation
                ? "loading"
                : this.props.groups.length < 1
                ? "no groups yet start by creating one"
                : this.props.groups.map((group, key) => (
                    <li key={"group-list" + key}>
                      <Link to="#" onClick={(e) => this.openUserChat(e, group)}>
                        <div className="d-flex align-items-center">
                          <div className="chat-user-img me-3 ms-0">
                            <div className="avatar-xs">
                              <span className="avatar-title rounded-circle bg-soft-primary text-primary">
                                {group.name.charAt(0)}
                              </span>
                            </div>
                          </div>
                          <div className="flex-1 overflow-hidden">
                            <h5 className="text-truncate font-size-14 mb-0">
                              {"#" + group.name}
                              {group.unRead !== 0 ? (
                                <Badge
                                  color="none"
                                  pill
                                  className="badge-soft-danger float-end"
                                >
                                  {group.unRead >= 20
                                    ? group.unRead + "+"
                                    : group.unRead}
                                </Badge>
                              ) : null}

                              {group.isNew && (
                                <Badge
                                  color="none"
                                  pill
                                  className="badge-soft-danger float-end"
                                >
                                  New
                                </Badge>
                              )}
                            </h5>
                          </div>
                        </div>
                      </Link>
                    </li>
                  ))}
            </ul>
          </SimpleBar>
          {/* End chat-group-list */}
        </div>
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state) => {
  const {
    groups,
    active_user,
    loadingChats,
    chats,
    loadingRecentConversation,
  } = state.Chat
  const { user } = state.data.loginUserDetails || {}
  return {
    groups,
    active_user,
    loadingChats,
    user,
    chats,
    loadingRecentConversation,
  }
}

export default connect(mapStateToProps, {
  createGroup,
  getGroups,
  activeUser,
  getChats,
})(withTranslation()(Groups))
