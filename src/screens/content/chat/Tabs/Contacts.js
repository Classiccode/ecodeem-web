import React, { Component } from "react"
import {
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  UncontrolledTooltip,
  Form,
  Label,
  Input,
  InputGroup,
} from "reactstrap"
import SimpleBar from "simplebar-react"

import { connect } from "react-redux"
// import { Button:Btn } from '../../../components/Button/button'
import { withTranslation } from "react-i18next"
import { getFollowers } from "../../../../store/slice/friendSlice"
import { createChat } from "../../../../store/action/chat"

//use sortedContacts variable as global variable to sort contacts
let sortedContacts = [
  {
    group: "A",
    children: [{ name: "Demo" }],
  },
]

class Contacts extends Component {
  constructor(props) {
    super(props)
    this.state = {
      modal: false,
      contacts: [],
      followers: [],
      loading: true,
    }
    this.toggle = this.toggle.bind(this)
    // this.sortContact = this.sortContact.bind(this);
    // this.getFollowers = this.getFollowers.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) {
      this.setState({
        contacts: this.props.contacts,
      })
    }
  }

  // getFollowers = async () => {
  // 	try {
  // 		const config = {
  // 			headers: { Authorization: `Bearer ${this.props.token}` }
  // 		};
  // 		const resp = await axios.get(`${Config.AUTH_URL}/follow/followers`, config)

  //         this.setState({followers:[...resp.data]})
  //         this.setState({loading:false})

  //         this.sortContact();

  // 	} catch (error) {
  // 		console.log(error);
  // 		setLoading(false)

  // 	}

  // }

  toggle() {
    this.setState({ modal: !this.state.modal })
  }

  // sortContact() {
  //     let data = this.state.followers.reduce((r, e) => {
  //         try {
  //             // get first letter of name of current element
  //             let group = e.firstName[0];
  //             // if there is no property in accumulator with this letter create it
  //             if (!r[group]) r[group] = { group, children: [e] }
  //             // if there is push current element to children array for that letter
  //             else r[group].children.push(e);
  //         } catch (error) {
  //             return sortedContacts;
  //         }
  //         // return accumulator
  //         return r;
  //     }, {})

  //     // since data at this point is an object, to get array of values
  //     // we use Object.values method
  //     let result = Object.values(data);
  //     this.setState({ contacts: result });
  //     sortedContacts = result;
  //     return result;
  // }

  componentDidMount() {
    // this.props.getFollowers({ token: this.props.token })
  }

  componentWillUnmount() {
    // this.props.getFollowers({ token: this.props.token })
  }

  render() {
    const { t } = this.props

    // console.log('====================================');
    // console.log(this.props.contacts);
    // console.log('====================================');
    return (
      <React.Fragment>
        <div>
          <div className="p-4">
            <div className="user-chat-nav float-end"></div>
            <h4 className="mb-4">Followers</h4>

            <div className="search-box chat-search-box">
              <InputGroup size="lg" className="bg-light rounded-lg">
                <Button
                  color="link"
                  className="text-decoration-none text-muted pr-1"
                  type="button"
                >
                  <i className="ri-search-line search-icon font-size-18"></i>
                </Button>
                <Input
                  type="text"
                  className="form-control bg-light "
                  placeholder={t("Search users..")}
                />
              </InputGroup>
            </div>
            {/* End search-box */}
          </div>
          {/* end p-4 */}

          {/* Start contact lists */}
          <SimpleBar
            style={{ maxHeight: "100%" }}
            id="chat-room"
            className="p-4 chat-message-list chat-group-list"
          >
            {/* {
                            this.state.contacts.length < 1 ? 'you do not have any followers yet ':
                            this.state.contacts.map((contact, key) =>
                                <div key={key} className={key + 1 === 1 ? "" : "mt-3"}>
                                    <div className="p-3 fw-bold text-primary">
                                        {contact.group}
                                    </div>

                                    <ul className="list-unstyled contact-list">
                                        {
                                            contact.children.map((child, key) =>
                                                <li key={key} >
                                                    <div className="d-flex align-items-center">
                                                        <div className="flex-1">
                                                            <h5 className="font-size-14 m-0">{child.name}</h5>
                                                        </div>
                                                        <UncontrolledDropdown>
                                                            <DropdownToggle tag="a" className="text-muted">
                                                                <i className="ri-more-2-fill"></i>
                                                            </DropdownToggle>
                                                            <DropdownMenu className="dropdown-menu-end">
                                                                <DropdownItem>{t('message')} <i className="ri-share-line float-end text-muted"></i></DropdownItem>
                                                            </DropdownMenu>
                                                        </UncontrolledDropdown>
                                                    </div>
                                                </li>
                                            )
                                        }
                                    </ul>
                                </div>
                            )
                        } */}

            {this.props.friends.status == "succeeded" ? (
              this.props.friends.followers.length > 0 ? (
                <ul className="list-unstyled contact-list">
                  {this.props.friends.followers.map(
                    ({ avatar, firstName, lastName, username, _id }, key) => (
                      <li key={key}>
                        <div className="d-flex align-items-center">
                          <div className="flex-1">
                            <h5 className="font-size-14 m-0">{`${firstName} ${lastName}`}</h5>
                          </div>
                          <UncontrolledDropdown>
                            <DropdownToggle tag="a" className="text-muted">
                              <i className="ri-more-2-fill"></i>
                            </DropdownToggle>
                            <DropdownMenu className="dropdown-menu-end">
                              <DropdownItem
                                onClick={() =>
                                  this.props.createChat({
                                    userId: _id,
                                    ecodeemId: this.props.user._id,
                                  })
                                }
                              >
                                {t("message")}{" "}
                                <i className="ri-share-line float-end text-muted"></i>
                              </DropdownItem>
                            </DropdownMenu>
                          </UncontrolledDropdown>
                        </div>
                      </li>
                    )
                  )}
                </ul>
              ) : (
                <p className="mt-5 text-center">No one is following you yet</p>
              )
            ) : this.props.friends.status == "failed" ? (
              <div class="text-center">
                <p style={{ margin: "20px" }}>
                  Looks like you lost your connection. Please check it and try
                  again.
                </p>
                {/* <Button onClick={() => this.props.getFollowers({ token: this.props.token })} className="text-decoration-none text-center text-muted pr-1" type="button">
                                        refresh
                                    </Button> */}
                {/* <div style={{width: '50%', margin:'auto', height:'40px'}}>
                                        <Btn text='try again'  type='dark' onClick={() => this.props.getFollowers({token:profile.profile.token})}/>
                                    </div> */}
                <br />
              </div>
            ) : (
              // <ApiloadStateMain />
              <p>loading...</p>
            )}
          </SimpleBar>
          {/* end contact lists */}
        </div>
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state) => {
  const { contacts } = state.Chat
  const friends = state.friends
  const { user, token } = state.data.loginUserDetails || {}
  return { contacts, friends, user, token }
}

export default connect(mapStateToProps, { getFollowers, createChat })(
  withTranslation()(Contacts)
)
