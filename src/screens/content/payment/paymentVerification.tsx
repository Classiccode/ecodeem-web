import React, { useEffect } from "react"
import { Redirect, useLocation, useParams } from "react-router-dom"
import { toast } from "react-toastify"
import { verifyDonateFuncHandler } from "../../../function"
import queryString from "query-string"

export const PaymentVerification = () => {
  const { search } = useLocation()

  const params = queryString.parse(search)

  const verify = async (transaction_id) => {
    try {
      const resp = await verifyDonateFuncHandler(transaction_id)
      toast.success(resp, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      })
    } catch (error) {
      toast.error("Payment could not be confirmed", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      })
    }
  }

  useEffect(() => {
    verify(params.transaction_id)
  }, [params.transaction_id])

  return <Redirect to="/content/post" />
}
