/* eslint-disable indent */
import { createRef, MouseEvent, useEffect, useState } from "react"
import { useHistory, useLocation, useParams } from "react-router-dom"
import Close from "@material-ui/icons/CloseOutlined"
import { PostType } from "../../../core/type"
import { useProfile } from "../../../utitly"
import ShareOutlinedIcon from "@material-ui/icons/ShareOutlined"
import CommentOutlinedIcon from "@material-ui/icons/CommentOutlined"
import {
  likeDeem,
  unlikeDeem,
  RootState,
  donate,
  sharePost,
} from "../../../store"
import { useDispatch, useSelector } from "react-redux"
import {
  MonetizationOnOutlined,
  ThumbUp,
  ThumbUpAltOutlined,
} from "@material-ui/icons"
import {
  makeStyles,
  Menu,
  MenuItem,
  Modal,
  TextField,
  useMediaQuery,
} from "@material-ui/core"
import { InputAdornment } from "@material-ui/core"
import Button from "../../../components/Button/button"
import CommentForm from "../../../components/commentForm/commentForm"
import Comment from "../../../components/Comment/comment"
import { getPost } from "../../../store"
import { ApiloadStateMain } from "../../../components/loaders"
import Swal from "sweetalert2"
import Config from "../../../config"
import Card from "../../../components/Card/card"
import ProfileCard from "../../../components/Chat/Profile/profileCard"
import axios from "axios"

const useStyles = makeStyles((theme) => ({
  root: {
    padding: "2px 4px",
    display: "flex",
    alignItems: "center",
    width: "100%",
    marginTop: "30px",
  },
  noBorder: {
    border: "none",
  },
  input: {
    padding: "13px 5px !important",
  },
  padding: {
    padding: "13px 5px !important",
  },
}))

interface Prop {
  isModal: boolean
  match?: any
}

const BASE_URL =
  process.env.NODE_ENV === "development"
    ? process.env.USE_BASE_URL_ENV
    : process.env.USE_BASE_URL

const ImagePreview = ({ match, isModal }: Prop) => {
  // const savedPost = useSelector((state: RootState) => state.account.userPost.savedPosts)

  const { donating } = useSelector((state: RootState) => state.deem)
  const dispatch = useDispatch()
  const { profile } = useProfile()
  const [showSidePan, setShowSidePan] = useState(true)
  const location = useLocation()
  const [showLoader, setLoader] = useState(true)
  const [post, setPost] = useState<PostType>()
  const history = useHistory()
  const modalRef = createRef<HTMLDivElement>()
  const { id } = useParams<Record<string, string | undefined>>()
  const [anchorElShare, setAnchorElShare] = useState<null | HTMLElement>(null)
  const [open, setOpen] = useState(false)
  const [loadingFund, setLoadingFund] = useState(true)
  const [donations, setDonations] = useState<Array<any>>([])

  const isMobile = useMediaQuery("(max-width: 950px)")

  useEffect(() => {
    if (isMobile) {
      setShowSidePan(false)
    }
  }, [isMobile])
  const allpost: PostType[] = useSelector(
    (state: RootState) => state.blog.posts
  )
  const { deem, status, message } = useSelector(
    (state: RootState) => state.deem
  )
  const [share, setShare] = useState(false)
  const [amount, setAmount] = useState<number>()
  const classes = useStyles()

  const handlePayment = async (amount: number, postId: string) => {
    dispatch(donate({ postId: postId, amount: amount }))
  }
  function toggleShare() {
    setShare(!share)
  }

  const handleOpen = () => {
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  const handleClickShare = (event: MouseEvent<HTMLTableElement>) => {
    setAnchorElShare(event.currentTarget)
  }

  const handleCloseShare = () => {
    setAnchorElShare(null)
  }

  useEffect(() => {
    dispatch(getPost({ id: id }))
  }, [id])

  useEffect(() => {
    setLoader(true)

    if (id) {
      const deem = allpost.find((inst: any) => inst._id == id)
      setPost(deem)

      setLoader(false)
    }

    // if (isMobile) {
    //     setShowSidePan(false)
    // }
  }, [id, setLoader, setShowSidePan, allpost])

  if (!isModal) {
    history.push("/microblog")
  }

  // console.log(savedPost);

  const toggleSidePan = () => {
    setShowSidePan(!showSidePan)
  }

  const getFunding = async () => {
    const result = await axios.get(
      Config.apiUrl + "/donation/donors-post?postId=" + post?._id
    )
    setLoadingFund(false)
    setDonations([...result.data])
  }

  useEffect(() => {
    getFunding()
  }, [post])

  return (
    <>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <div>
          <Card rounded className="Blog__Modal">
            <h3>Donors</h3>
            <div className="Blog__Modal__Header">
              <ProfileCard
                image={profile.user.avatar}
                title={`${profile.user.firstName} ${profile.user.lastName}`}
              />
            </div>

            <div className="Blog__Modal__Body">
              <ul>
                {loadingFund ? (
                  "loading..."
                ) : donations.length < 1 ? (
                  <p>No donations yet</p>
                ) : (
                  donations.map(({ _id, amountDonated, avatar, fullName }) => (
                    <li key={`skxss${_id}`}>
                      <ProfileCard image={avatar} title={fullName} />
                      <p>$+{amountDonated}</p>
                    </li>
                  ))
                )}
              </ul>
            </div>
            <div className="Blog__Modal__Footer">
              <TextField
                fullWidth
                placeholder="enter amount you want to donate"
                required
                variant="outlined"
                onChange={(e) => setAmount(Number(e.target.value))}
                type="number"
                style={{
                  backgroundColor: "#DEF4C5",
                  border: "none",
                  borderRadius: "10px",
                  color: "white",
                  paddingTop: "0px",
                }}
                InputProps={{
                  "aria-label": "naked",
                  classes: {
                    notchedOutline: classes.noBorder,
                    input: classes.padding,
                  },
                  startAdornment: (
                    <InputAdornment position="start">
                      <MonetizationOnOutlined />
                    </InputAdornment>
                  ),
                }}
              />
              {donating == "loading" ? (
                <Button text="loading..." disable type="dark" />
              ) : amount ? (
                <Button
                  text="Donate"
                  onClick={(e) => handlePayment(amount, post!._id)}
                  type="dark"
                />
              ) : (
                <div>
                  <Button text="Amount" disable type="dark" />
                </div>
              )}
            </div>
          </Card>
        </div>
      </Modal>
      <div className="imagepreview">
        <div className="imagepreview__rightcontainer">
          <div
            className="pa imagepreview__rightcontainer__button imagepreview__rightcontainer__close"
            onClick={() => history.goBack()}
          >
            <Close fontSize="large" />
          </div>

          <div
            className="pa imagepreview__rightcontainer__button imagepreview__rightcontainer__expand"
            onClick={() => toggleSidePan()}
          >
            {showSidePan ? (
              <i className="fa fa-angle-double-right"></i>
            ) : (
              <i className="fa fa-angle-double-left"></i>
            )}
          </div>

          {/* <CommentOutlinedIcon fontSize='inherit' /> */}

          {showLoader ? (
            "loading"
          ) : (
            <>
              <div
                className="imagepreview__rightcontainer__image"
                style={{ backgroundImage: `url(${post?.mediaUrl})` }}
              ></div>
              <div className="imagepreview__rightcontainer__downbutton">
                {post &&
                post.likes!.filter(function (e) {
                  return e === profile.user._id
                }).length > 0 ? (
                  <div
                    onClick={(e) =>
                      dispatch(
                        unlikeDeem({
                          postId: post._id,
                          token: profile.token,
                          userId: profile.user._id,
                        })
                      )
                    }
                    className="Blog__Reactions__Icon"
                  >
                    {/* <FavoriteBorderRoundedIcon fontSize='inherit' /> */}
                    <ThumbUp
                      style={{ fontSize: "20px", color: "rgb(224, 36, 94)" }}
                    />
                  </div>
                ) : (
                  <div
                    onClick={(e) =>
                      dispatch(
                        // eslint-disable-next-line indent
                        likeDeem({
                          postId: post!._id,
                          token: profile.token,
                          userId: profile.user._id,
                        })
                      )
                    }
                    className="Blog__Reactions__Icon"
                  >
                    {/* <FavoriteBorderRoundedIcon fontSize='inherit' /> */}
                    <ThumbUpAltOutlined style={{ fontSize: "20px" }} />
                  </div>
                )}

                <div
                  className=" imagepreview__rightcontainer__downbutton__comment imagepreview__rightcontainer__downbutton__button"
                  onClick={() => history.push(`/content/post/${post?._id}`)}
                >
                  <CommentOutlinedIcon fontSize="small" color="inherit" />
                </div>
                <div
                  className=" imagepreview__rightcontainer__downbutton__share imagepreview__rightcontainer__downbutton__button"
                  aria-controls="simple-menu"
                  aria-haspopup="true"
                  onClick={handleClickShare}
                >
                  <ShareOutlinedIcon fontSize="small" color="inherit" />
                </div>
                <Menu
                  id="simple-menu"
                  anchorEl={anchorElShare}
                  keepMounted
                  open={Boolean(anchorElShare)}
                  onClose={handleCloseShare}
                >
                  <MenuItem
                    onClick={() => {
                      handleCloseShare()
                      Swal.fire({
                        text: "Are you sure you want to share the post",
                        showCancelButton: true,
                        showConfirmButton: true,
                        showCloseButton: false,
                        background: "var(--background)",
                        customClass: {
                          confirmButton: "sweet-btn", //insert class here
                          cancelButton: "sweet-btn",
                          popup: "sweet-btn-rounded",
                        },
                      }).then((result) => {
                        if (result.isConfirmed) {
                          dispatch(
                            sharePost({
                              postId: post?._id,
                            })
                          )
                        }
                      })
                    }}
                  >
                    Share to timeline
                  </MenuItem>
                  <MenuItem
                    onClick={() => {
                      handleCloseShare()
                      navigator.share({
                        title: "Hello check out my post on Ecodeem",
                        text: "Hello check out my post on Ecodeem",
                        url: `${Config.baseURL}/content/post/${post?._id}`,
                      })
                    }}
                  >
                    Share to other apps
                  </MenuItem>
                </Menu>
                {post?.postType == "raiseFund" && (
                  <div className="imagepreview__rightcontainer__downbutton__share imagepreview__rightcontainer__downbutton__button">
                    <MonetizationOnOutlined
                      fontSize="inherit"
                      color="inherit"
                      onClick={handleOpen}
                    />
                  </div>
                )}
              </div>
            </>
          )}
        </div>

        {showSidePan && (
          <div className="imagepreview__leftcontainer">
            {!showLoader ? (
              <>
                <div className="imagepreview__leftcontainer__profilecontainer">
                  {profile?.user.avatar ? (
                    <div
                      onClick={() =>
                        history.push(`/content/profile/${profile.user._id}`)
                      }
                      className="imagepreview__leftcontainer__profilecontainer__image"
                    >
                      <img
                        className="rounded-circle"
                        src={profile?.user.avatar}
                        alt=""
                      />
                    </div>
                  ) : (
                    <span
                      onClick={() =>
                        history.push(`/content/profile/${profile.user._id}`)
                      }
                      className="avatar-title rounded-circle bg-soft-primary text-primary"
                      style={{ height: "3em", width: "3em" }}
                    >
                      {profile?.user.firstName.charAt(0)}
                    </span>
                  )}

                  <div className="imagepreview__leftcontainer__profilecontainer__text">
                    <p>{post?.postedBy.username}</p>
                    <span>{post?.interest.interest}</span>
                  </div>
                </div>
                <div className="imagepreview__leftcontainer__posttext">
                  {post?.postDesc && <p>{post?.postDesc}</p>}
                </div>
                {/* <div className='imagepreview__leftcontainer__date'>
									{post && moment(post?.).format('MMMM Do YYYY, h:mm a')}
								</div> */}
                <div className="imagepreview__leftcontainer__count">
                  <strong>{post?.counts?.commentCount}</strong>{" "}
                  <span className="text-muted"> comments</span>
                  <strong> {post?.counts?.likesCount}</strong>{" "}
                  <span className="text-muted"> likes</span>
                  <strong> {post?.counts?.shareCount}</strong>{" "}
                  <span className="text-muted"> shares</span>
                  {post?.postType == "raiseFund" && (
                    <>
                      <strong> {post?.counts?.donorsCount}</strong>{" "}
                      <span className="text-muted"> donors</span>
                    </>
                  )}
                  {/* {`$ comments ${post?.likesCount} likes`} */}
                </div>
                <hr />
                {post?.postType == "raiseFund" && (
                  <div className={classes.root}>
                    <TextField
                      fullWidth
                      placeholder="enter amount you want to donate"
                      required
                      variant="outlined"
                      onChange={(e) => setAmount(Number(e.target.value))}
                      type="number"
                      style={{
                        backgroundColor: "#DEF4C5",
                        border: "none",
                        borderRadius: "10px",
                        color: "white",
                      }}
                      inputProps={{}}
                      InputProps={{
                        "aria-label": "naked",
                        classes: {
                          notchedOutline: classes.noBorder,
                          input: classes.input,
                        },
                        startAdornment: (
                          <InputAdornment position="start">
                            <MonetizationOnOutlined />
                          </InputAdornment>
                        ),
                      }}
                    />

                    {donating == "loading" ? (
                      <Button text="loading..." disable type="dark" />
                    ) : amount ? (
                      <Button
                        text="Donate"
                        onClick={(e) => handlePayment(amount, post!._id)}
                        type="dark"
                      />
                    ) : (
                      <div>
                        <Button text="Donate" disable type="dark" />
                      </div>
                    )}
                  </div>
                )}
                <hr />

                <div className="imagepreview__leftcontainer__buttons">
                  {post &&
                  post?.likes!.filter(function (e) {
                    return e === profile.user._id
                  }).length > 0 ? (
                    <div
                      onClick={(e) =>
                        dispatch(
                          unlikeDeem({
                            postId: post._id,
                            token: profile.token,
                            userId: profile.user._id,
                          })
                        )
                      }
                      className="Blog__Reactions__Icon"
                    >
                      {/* <FavoriteBorderRoundedIcon fontSize='inherit' /> */}
                      <ThumbUp
                        style={{ fontSize: "20px", color: "rgb(224, 36, 94)" }}
                      />
                    </div>
                  ) : (
                    <div
                      onClick={(e) =>
                        dispatch(
                          likeDeem({
                            postId: post!._id,
                            token: profile.token,
                            userId: profile.user._id,
                          })
                        )
                      }
                      className="Blog__Reactions__Icon"
                    >
                      {/* <FavoriteBorderRoundedIcon fontSize='inherit' /> */}
                      <ThumbUpAltOutlined style={{ fontSize: "20px" }} />
                    </div>
                  )}
                  <div
                    className="Blog__Reactions__Icon"
                    onClick={() => history.push(`/content/post/${post?._id}`)}
                  >
                    <CommentOutlinedIcon fontSize="small" color="inherit" />
                  </div>
                  <div
                    className="Blog__Reactions__Icon"
                    aria-controls="simple-menu"
                    aria-haspopup="true"
                    onClick={handleClickShare}
                  >
                    <ShareOutlinedIcon fontSize="small" color="inherit" />
                  </div>
                  <Menu
                    id="simple-menu"
                    anchorEl={anchorElShare}
                    keepMounted
                    open={Boolean(anchorElShare)}
                    onClose={handleCloseShare}
                  >
                    <MenuItem
                      onClick={() => {
                        handleCloseShare()
                        Swal.fire({
                          text: "Are you sure you want to share the post",
                          showCancelButton: true,
                          showConfirmButton: true,
                          showCloseButton: false,
                          background: "var(--background)",
                          customClass: {
                            confirmButton: "sweet-btn", //insert class here
                            cancelButton: "sweet-btn",
                            popup: "sweet-btn-rounded",
                          },
                        }).then((result) => {
                          if (result.isConfirmed) {
                            dispatch(
                              sharePost({
                                postId: post?._id,
                              })
                            )
                          }
                        })
                      }}
                    >
                      Share to timeline
                    </MenuItem>
                    <MenuItem
                      onClick={() => {
                        handleCloseShare()
                        navigator.share({
                          title: "Hello check out my post on Ecodeem",
                          text: "Hello check out my post on Ecodeem",
                          url: `${Config.baseURL}/content/post/${post?._id}`,
                        })
                      }}
                    >
                      Share to other apps
                    </MenuItem>
                  </Menu>
                  {post?.postType == "raiseFund" && (
                    <div className="Blog__Reactions__Icon" onClick={handleOpen}>
                      <MonetizationOnOutlined
                        fontSize="small"
                        color="inherit"
                      />
                    </div>
                  )}
                </div>
                <hr />
                {post && (
                  <CommentForm
                    type="comment"
                    fullname={post.postedBy.username}
                    postId={post!._id}
                  />
                )}

                {status == "succeeded" ? (
                  deem && deem!.comments!.length > 0 ? (
                    <Comment postId={post!._id} contents={deem!.comments!} />
                  ) : (
                    <div className="MicroBlog__Column__Middle__Nocontent">
                      no comments yet
                    </div>
                  )
                ) : status == "failed" ? (
                  <div className="MicroBlog__Column__Middle__Nocontent">
                    <div>
                      <p style={{ margin: "20px" }}>
                        Looks like you lost your connection. Please check it and
                        try again.
                      </p>
                      <div
                        style={{ width: "50%", margin: "auto", height: "40px" }}
                      >
                        <Button
                          text="try again"
                          type="dark"
                          onClick={() =>
                            dispatch(
                              getPost({
                                id: profile.user._id,
                              })
                            )
                          }
                        />
                      </div>
                      <br />
                    </div>
                  </div>
                ) : (
                  <ApiloadStateMain />
                )}
              </>
            ) : (
              "loading"
            )}
          </div>
        )}
      </div>
    </>
  )
}

export default ImagePreview
