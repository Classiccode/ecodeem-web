import React, { Fragment, useState, MouseEvent } from "react"
import { withRouter, useHistory, Link } from "react-router-dom"
import MicroBlogProvider from "./provider"
import Interests from "../../../components/Card/Interests/interests"
import Blog from "../../../components/Blog/blog"
import Post from "../../../components/Post/post"
import SendOutlined from "@material-ui/icons/SendOutlined"
import "./microBlog.scss"
import { useDispatch, useSelector } from "react-redux"
import { getInterestedDeems, togglePost } from "../../../store/slice"
import { RootState } from "../../../store"
import Config from "../../../config"
import { useProfile } from "../../../utitly/hooks"
import { toast } from "react-toastify"
import { ApiloadStateMain } from "../../../components/loaders"

import { Button } from "@material-ui/core"

export const PostPage = (): JSX.Element => {
  //handle Alert & Logout

  const profile = useProfile()
  const {
    profile: { token },
  } = useProfile()
  const postStatus = useSelector((state: RootState) => state.blog.status)
  const History = useHistory()
  const dispatch = useDispatch()

  const [showAlert, setShowAlert] = useState(false)
  const [alertMessage, setAlertMessage] = useState("")
  const [loggingOff, setLoggingOff] = useState(false)

  const toggleAlert = (event: MouseEvent): void => {
    event.preventDefault()
    setShowAlert(!showAlert)
    setAlertMessage("This would log you out. Continue?")
  }

  const logoutSession = (event: MouseEvent): void => {
    const myHeaders = new Headers()

    myHeaders.append("Authorization", `Bearer ${token}`)

    const requestOptions = {
      method: "POST",
      headers: myHeaders,
    }

    setLoggingOff(true)

    fetch(Config.apiUrl + "/user/logout", requestOptions)
      .then((response) => response.json())
      .then((result) => {
        if (result.status == "success") {
          setLoggingOff(false)
          localStorage.clear()
          window.location.reload()
          History.push("/")
        } else {
          setLoggingOff(false)
        }
      })
      .catch(() => {
        setLoggingOff(false)
        toast.error("error logging out", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        })
      })
  }

  return (
    <MicroBlogProvider>
      {({ input, allpost, posts, getPerInterestDeems }) => (
        <>
          <div className="mt-5 ">
            <div className="MicroBlog">
              <div className="MicroBlog__Bracer">
                <div className="MicroBlog__Column">
                  <div
                    className="MicroBlog__Column__Middle"
                    style={{ width: "100%" }}
                  >
                    <Link
                      to={`/content/profile/${profile.profile.user._id}`}
                      className="d-flex mb-3 px-4 py-3 rounded bg-color2"
                      style={{
                        borderRadius: "0.8rem",
                      }}
                    >
                      {profile.profile.user.avatar ? (
                        <img
                          height="30px"
                          width="30px"
                          className="rounded-circle"
                          src={profile.profile.user.avatar}
                          alt=""
                        />
                      ) : (
                        <span
                          className="avatar-title rounded-circle bg-soft-primary text-primary"
                          style={{ height: "3em", width: "3em" }}
                        >
                          {profile.profile.user.firstName.charAt(0)}
                        </span>
                      )}

                      <input
                        onFocus={() => dispatch(togglePost())}
                        style={{
                          color: "#909090",
                          border: "1px solid gray",
                          borderRadius: "25px",
                        }}
                        className="w-100 bg-color2"
                        name=""
                        placeholder="      Create a deem"
                      />
                    </Link>
                    <Interests
                      type={input.interestTab}
                      onClick={(e, value) => getPerInterestDeems(value)}
                    />
                    {/* <Live/>  */}
                    <div>
                      {postStatus == "succeeded" ? (
                        allpost && allpost.length > 0 ? (
                          allpost.map((post, idx) => (
                            <Blog key={idx} Deem={post} global={true} />
                          ))
                        ) : (
                          <div className="MicroBlog__Column__Middle__Nocontent">
                            You do not have any post on your timeline yet
                          </div>
                        )
                      ) : postStatus == "failed" ? (
                        <div className="MicroBlog__Column__Middle__Nocontent">
                          <div>
                            <p style={{ margin: "20px" }}>
                              Looks like you lost your connection. Please check
                              it and try again.
                            </p>
                            <div
                              style={{
                                width: "50%",
                                margin: "auto",
                                height: "40px",
                              }}
                            >
                              <Button
                                size="small"
                                variant="outlined"
                                onClick={() => dispatch(getInterestedDeems())}
                              >
                                Try again
                              </Button>
                            </div>
                            <br />
                          </div>
                        </div>
                      ) : (
                        <ApiloadStateMain />
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      )}
    </MicroBlogProvider>
  )
}
