import React, { useEffect, useState, FC, MouseEvent, ReactNode } from "react"
import { useDispatch, useSelector } from "react-redux"
import axios from "axios"

import { AuthUser, PostType } from "../../../core/type"
import { useProfile } from "../../../utitly"
import { RootState } from "../../../store"
import {
  getInterestedDeems,
  getPerInterestDeems as getInterestDem,
} from "../../../store"

type Inputprops = {
  showPost: boolean
  showLoader: boolean
  interestTab: string
  like: boolean
}
type ChildrenProps = {
  input: Inputprops
  posts: PostType[] | undefined
  allpost: PostType[] | undefined
  getPerInterestDeems: (interest: string) => void
}

type Props = {
  children(_: ChildrenProps): ReactNode
}

const MicroBlogProvider = ({ children }: Props): JSX.Element => {
  const [showPost, setShowPost] = useState(false)
  const { profile } = useProfile()
  const [showLoader, setLoader] = useState(false)
  const [posts, setPosts] = useState<PostType[]>([])
  const [interestTab, setInterestTab] = useState("timeline")
  const [like, setLike] = useState(false)

  const account = useSelector((state: RootState) => state.data.loginUserDetails)
  const dispatch2 = useDispatch()

  //   const posts2 = useSelector(selectAllPosts)

  const postStatus = useSelector((state: RootState) => state.blog.status)
  const allpost: PostType[] = useSelector(
    (state: RootState) => state.blog.posts
  )

  useEffect(() => {
    if (postStatus === "idle") {
      dispatch2(getInterestedDeems())
    }
  }, [dispatch2, postStatus])

  const getPerInterestDeems = async (interest: string) => {
    setInterestTab(interest)

    if (interest == "timeline") {
      dispatch2(getInterestedDeems())
      return
    }

    dispatch2(getInterestDem(interest))
  }

  const input = {
    showPost,
    showLoader,
    interestTab,
    like,
  }

  return (
    <>
      {children({
        input,
        allpost,
        posts,
        getPerInterestDeems,
      })}
    </>
  )
}

export default MicroBlogProvider
