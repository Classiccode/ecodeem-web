import { FC, Fragment, useEffect } from "react"
import Blog from "../../../components/Blog/blog"
import { ApiloadStateMain } from "../../../components/loaders"
import { useDispatch, useSelector } from "react-redux"
import { getPost, RootState } from "../../../store"
import Comment from "../../../components/Comment/comment"
import CommentForm from "../../../components/commentForm/commentForm"
import Loader from "react-loader-spinner"

import { useParams } from "react-router-dom"

export const SinglePostPage: FC = () => {
  const blog = useSelector((state: RootState) => state.blog)
  const post = useSelector((state: RootState) => state.deem.deem)
  const { status } = useSelector((state: RootState) => state.deem)
  const { id } = useParams<{ id?: string }>()
  const dispatch = useDispatch()
  useEffect(() => {
    console.log("hi")
    dispatch(getPost({ id: id }))
  }, [id])
  return (
    <>
      <div className="mt-25 w-100" style={{ marginTop: "100px" }}>
        {blog.status === "loading" && (
          <div
            style={{
              position: "fixed",
              left: "50%",
              top: "50%",
              transform: "translate(-50%,-50%)",
              zIndex: 4,
            }}
          >
            <Loader
              type="BallTriangle"
              color="#2E8CCB"
              height={150}
              width={150}
            />
          </div>
        )}
        <div className="MicroBlog__Column__Middle" style={{ width: "100%" }}>
          {post && status == "succeeded" && (
            <Blog global={false} attributes="full" Deem={post} />
          )}
          {status == "succeeded" && (
            <div>
              <CommentForm
                type="comment"
                fullname={post!.postedBy.username}
                postId={post!._id}
              />

              {post?.comments && post.comments.length < 1 ? (
                "no comment"
              ) : (
                <Comment postId={post!._id} contents={post!.comments!} />
              )}
            </div>
          )}

          {status == "loading" && (
            <div className="w-100">
              <ApiloadStateMain />
            </div>
          )}
        </div>
      </div>
    </>
  )
}
