import { useEffect, ReactNode, FC, useState, MouseEvent } from "react"
import axios from "axios"

import { useSelector, useDispatch } from "react-redux"
import { RootState } from "../../../../store"
import Config from "../../../../config"
import {
  GetProductsByCategories,
  GetCategories,
  AddProductToCart,
  GetSingleProduct,
  ReviewProduct,
} from "../../../../config/endpoints"
import { useParams, useHistory } from "react-router-dom"
import {
  getProductsByCategory,
  getCategory,
  getSingleProduct,
} from "../../../../store/slice/productSlice"
import { addToCart } from "../../../../store/slice/cartSlice"

type ChildrenProps = {
  onCartClick: (e: any, productID: string) => void
  addCartLoader: boolean
  toggleTab: (event: MouseEvent, value: string) => void
  setComment: any
  setRating: any
  handleReview: () => void
  singleProduct: any
  comment: string
  rating: number | null
  message: string
  tab: string
}

type Props = {
  children(_: ChildrenProps): ReactNode
}

const ProductDetailsProvider: FC<Props> = ({ children }) => {
  const dispatch = useDispatch()
  const history = useHistory()
  const [addCartLoader, setAddCartLoader] = useState(false)

  interface RouteParams {
    productDetails: string
  }
  const { productDetails } = useParams<RouteParams>()

  const [tab, setTab] = useState("description")

  const toggleTab = (event: MouseEvent, value: string) => {
    event.preventDefault()
    setTab(value)
  }

  const [showLoader, setLoader] = useState(false)

  const singleProduct: any = useSelector(
    (state: RootState) => state.product.singleProduct
  )

  useEffect(() => {
    getSingle()
    getProductCategories()
  }, [])

  const getProductCategories = async () => {
    try {
      const response: any = await axios.get(
        Config.marketplaceApiUrl + GetCategories()
      )
      const { data, status } = response
      if (status != 200) {
        console.error(data.message)
      }
      dispatch(getCategory(data))
    } catch (e) {
      console.error(e)
    }
  }

  const getSingle = async () => {
    setLoader(true)

    try {
      const response: any = await axios.get(
        Config.marketplaceApiUrl + GetSingleProduct(productDetails)
      )
      const { data, status } = response
      if (status != 200) {
        console.error(data.message)
        setLoader(false)
      }
      dispatch(getSingleProduct(data))
      setLoader(false)
    } catch (e) {
      setLoader(false)
    }
  }

  const [comment, setComment] = useState("")
  const [rating, setRating] = useState<number | null>(0)
  const [message, setMessage] = useState("")

  const userID: any = localStorage.getItem("userID")

  const postReview = async () => {
    setLoader(true)

    const payload = {
      productId: singleProduct._id,
      rating,
      comment,
    }

    try {
      const response: any = await axios.post(
        Config.marketplaceApiUrl + ReviewProduct(userID),
        payload
      )
      const { data, status, message } = response
      if (status !== 201) {
        setMessage("You already")
        setLoader(false)
      }
      getSingle()
      setComment("")
      setRating(0)
      setLoader(false)
    } catch (e) {
      setLoader(false)
    }
  }

  const handleReview = () => {
    postReview()
  }

  const user: any = localStorage.getItem("userID")
  const addCartItem = async (productID: string) => {
    setAddCartLoader(true)
    try {
      const response: any = await axios.post(
        Config.marketplaceApiUrl + AddProductToCart(user),
        {
          productId: productID,
        }
      )
      const { data, status } = response
      if (status != 200) {
        console.error(data.message)
        setAddCartLoader(false)
      }
      dispatch(addToCart(data.cart))
      setAddCartLoader(false)
      history.push("/content/market/cart")
    } catch (e) {
      setAddCartLoader(false)
    }
  }
  const onCartClick = (e: any, productID: string) => {
    e.preventDefault()
    if (!user) {
      history.push("/auth/login")
      return
    }
    addCartItem(productID)
  }
  return (
    <>
      {children({
        onCartClick,
        addCartLoader,
        toggleTab,
        setComment,
        setRating,
        handleReview,
        singleProduct,
        comment,
        rating,
        message,
        tab,
      })}
    </>
  )
}

export default ProductDetailsProvider
