/* eslint-disable indent */
import { AuthPageLayout } from "../../../../layout"
import ProductDetailsProvider from "./provider"
import "./productDetails.scss"
import { useState } from "react"
import Rating from "@material-ui/lab/Rating"
import FavoriteBorderOutlinedIcon from "@material-ui/icons/FavoriteBorderOutlined"
import FavoriteOutlinedIcon from "@material-ui/icons/FavoriteOutlined"
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart"
import Moment from "react-moment"
import ProductInfo from "../../../../components/Products/productInfo"
import Button from "../../../../components/Button/button"
import { reviewProduct } from "../../../../function/marketPlace/reviewProduct"
import { toast } from "react-toastify"
import { RootState } from "../../../../store"
import { useSelector } from "react-redux"

export function ProductDetails(): JSX.Element {
  const [like, setLike] = useState(false)
  const handleLike = (e) => {
    e.preventDefault()
    setLike(!like)
  }
  const [rating, setRating] = useState<number>(0)
  const [comment, setComment] = useState<string>("")
  const { product, profile } = useSelector((state: RootState) => {
    return {
      product: state.data.currentProductView,
      profile: state.data.loginUserDetails,
    }
  })
  return (
    <AuthPageLayout>
      <ProductDetailsProvider>
        {({
          onCartClick,
          addCartLoader,
          toggleTab,
          setComment,
          setRating,
          handleReview,
          singleProduct,
          comment,
          rating,
          message,
          tab,
        }) => (
          <section className="MarketPlace">
            <div className="ProductDetails__Column__Middle">
              <div className="ProductDetails__Column__Mid">
                <div className="ProductDetails__Column__Middle__Pictures">
                  <div className="ProductDetails__Column__Middle__Pictures__Big">
                    <img
                      src={singleProduct?.image}
                      className="ProductDetails__Column__Middle__Pictures__Big__Pix"
                    />
                    {like ? (
                      <FavoriteOutlinedIcon
                        fontSize="large"
                        className="ProductDetails__Column__Middle__Pictures__Big__Heart"
                        onClick={handleLike}
                      />
                    ) : (
                      <FavoriteBorderOutlinedIcon
                        fontSize="large"
                        className="ProductDetails__Column__Middle__Pictures__Big__Heart"
                        onClick={handleLike}
                      />
                    )}
                  </div>
                  <div className="ProductDetails__Column__Middle__Pictures__Small">
                    <div className="ProductDetails__Column__Middle__Pictures__Small__Pix">
                      <img
                        src={singleProduct?.image}
                        className="ProductDetails__Column__Middle__Pictures__Small__Pix__Img"
                      />
                    </div>
                    <div className="ProductDetails__Column__Middle__Pictures__Small__Pix">
                      <img
                        src={singleProduct?.image}
                        className="ProductDetails__Column__Middle__Pictures__Small__Pix__Img"
                      />
                    </div>
                    <div className="ProductDetails__Column__Middle__Pictures__Small__Pix">
                      <img
                        src={singleProduct?.image}
                        className="ProductDetails__Column__Middle__Pictures__Small__Pix__Img"
                      />
                    </div>
                    <div className="ProductDetails__Column__Middle__Pictures__Small__Pix">
                      <img
                        src={singleProduct?.image}
                        className="ProductDetails__Column__Middle__Pictures__Small__Pix__Img"
                      />
                    </div>
                  </div>
                </div>
                <ProductInfo
                  name={singleProduct?.name}
                  description={singleProduct?.description}
                  price={singleProduct?.price}
                  onCartClick={(e: any) => onCartClick(e, singleProduct._id)}
                  rating={singleProduct?.rating}
                  brand={singleProduct?.brand}
                  reviews={singleProduct.reviews}
                  id={""}
                />
              </div>
              <div className="ProductDetails__Column__Middle__Bottom">
                <div className="ProductDetails__Column__Middle__Bottom__Header">
                  <ul>
                    <li
                      onClick={(e) => toggleTab(e, "description")}
                      className={`${
                        tab == "description"
                          ? "ProductDetails__Column__Middle__Bottom__Header__Active"
                          : "ProductDetails__Column__Middle__Bottom__Header__Inactive"
                      }`}
                    >
                      DESCRIPTION
                    </li>
                    <li
                      onClick={(e) => toggleTab(e, "review")}
                      className={`${
                        tab == "review"
                          ? "ProductDetails__Column__Middle__Bottom__Header__Active"
                          : "ProductDetails__Column__Middle__Bottom__Header__Inactive"
                      }`}
                    >
                      REVIEWS
                    </li>
                  </ul>
                </div>
                <div className="ProductDetails__Column__Middle__Bottom__Content">
                  {tab === "description" ? (
                    <div style={{ marginTop: "24px", marginBottom: "24px" }}>
                      {singleProduct?.description}
                    </div>
                  ) : null}
                  {tab === "review" ? (
                    singleProduct.reviews &&
                    singleProduct.reviews.length > 0 ? (
                      singleProduct.reviews.map((review) => (
                        <div
                          style={{
                            marginTop: "24px",
                            borderBottom: "1px solid #f3f3f3",
                          }}
                          key={review._id}
                        >
                          <p>{review.name}</p>
                          <div style={{ display: "flex" }}>
                            <Rating
                              name="read-only"
                              value={review.rating}
                              readOnly
                              size="small"
                            />
                            <p style={{ marginLeft: "12px" }}>
                              <Moment format="Do MMMM YYYY">
                                {review.reviewedOn}
                              </Moment>
                            </p>
                          </div>
                          <p>{review.comment}</p>
                        </div>
                      ))
                    ) : (
                      <div>
                        <p>No reviews yet for this product.</p>
                      </div>
                    )
                  ) : null}
                  {tab === "review" ? (
                    <div>
                      <p style={{ fontWeight: "bold", marginTop: "24px" }}>
                        Leave a review
                      </p>
                      <p>{message !== "" ? message : null}</p>
                      <label>Comment</label>
                      <br />
                      <textarea
                        className="ProductDetails__Column__Middle__Input"
                        value={comment}
                        onChange={(e) => setComment(e.target.value)}
                      />
                      <div style={{ marginBottom: "24px" }}>
                        Rating:{" "}
                        <Rating
                          name="rating"
                          value={rating}
                          onChange={(event, newValue) => {
                            setRating(newValue)
                          }}
                          size="small"
                        />
                      </div>
                      <Button
                        type="nav"
                        text="Add Review"
                        onClick={async () => {
                          await reviewProduct({
                            comment: comment,
                            productId: singleProduct?._id ?? "",
                            rating: rating ?? 0,
                            userID: profile?.user._id ?? "",
                          })
                            .then(() => {
                              toast.success("Your review has been added.")
                            })
                            .catch((error) => {
                              toast.error(error)
                            })
                        }}
                      />
                    </div>
                  ) : null}
                </div>
              </div>
            </div>
          </section>
        )}
      </ProductDetailsProvider>
    </AuthPageLayout>
  )
}
