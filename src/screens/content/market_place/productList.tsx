/* eslint-disable indent */
import { CircularProgress, IconButton } from "@material-ui/core"
import { ArrowBackIos } from "@material-ui/icons"
import { useSelector } from "react-redux"
import { Link, useHistory, useLocation } from "react-router-dom"
import type { RootState } from "../../../store"
import { ShoppingBasket } from "@material-ui/icons"
import { useParams } from "react-router-dom"
import { useCallback, useEffect, useState } from "react"
import Config from "../../../config"
import { toast } from "react-toastify"
import { AxiosError } from "axios"
import { useInfiniteQuery } from "react-query"
import { ProductReturnType } from "../../../function"
import { ProductContent } from "."

export function ProductList(): JSX.Element {
  const { sub_categoryID } = useParams<{
    sub_categoryID: string
  }>()
  const { profile, subCategories } = useSelector((state: RootState) => {
    return {
      categories: state.data.categories,
      subCategories: state.data.subCategories,
      profile: state.data.loginUserDetails,
    }
  })
  const [page, setPage] = useState<number>(0)
  const {
    data,
    isLoading: productLoading,
    isError,
    error,
    fetchNextPage,
    hasNextPage,
  } = useInfiniteQuery(
    "products",
    async ({ pageParam = 1 }) => {
      const res = await (
        await Config.apiMarketPlace.get("/product/shop", {
          params: {
            pageNumber: pageParam,
            filterBySubCategory: sub_categoryID,
          },
        })
      ).data
      setPage(res.data.pages as number)
      return res.data as {
        products: ProductReturnType[]
        page: number
        productCount: number
        pages: number
      }
    },
    {
      keepPreviousData: true,
      getNextPageParam: (lastPage) => {
        return lastPage.page !== lastPage.pages
          ? undefined
          : {
              pageParam: Math.min(lastPage.pages, lastPage.page + 1),
            }
      },
    }
  )
  const { pathname } = useLocation()
  const { goBack, push } = useHistory()
  const [isLoading, setLoading] = useState<boolean>(true)
  useEffect(() => {
    if (
      subCategories.filter(
        (s) => s._id === pathname.split("/")[pathname.split("/").length - 1]
      ).length <= 0
    ) {
      push("/not-found")
    }
  }, [subCategories])
  const [products, setProducts] = useState<ProductReturnType[]>([])
  useEffect(() => {
    if (data !== undefined) {
      const _p: ProductReturnType[] = []
      for (let i = 0; i < data.pages.length; i++) {
        const { products } = data.pages[i]
        _p.push(...products)
      }
      setProducts(_p)
    }
  }, [data])
  return (
    <>
      <section
        className="MarketPlace"
        style={{
          position: "relative",
          marginLeft: 0,
          marginTop: "20px",
          height: "calc(100vh - 100px)",
          overflowY: "auto",
          minHeight: "calc(100vh - 100px)",
          top: "100px",
          width: "100%",
          minWidth: "100%",
        }}
      >
        <div className="market_place_header">
          <IconButton
            className="btn"
            onClick={() => {
              goBack()
            }}
          >
            <ArrowBackIos />
          </IconButton>
          <h4 className="s_title">
            {subCategories.find(
              (s) =>
                s._id === pathname.split("/")[pathname.split("/").length - 1]
            )?.title ?? ""}
          </h4>
        </div>
        {productLoading ? (
          <>
            <div className="center">
              <CircularProgress color="inherit" />
            </div>
          </>
        ) : products === undefined || products.length <= 0 ? (
          <>
            <div className="center">
              <h3 className="txt">No Product List</h3>
            </div>
          </>
        ) : (
          <>
            <div className="container">
              {products.map((product) => {
                return <ProductContent product={product} key={product._id} />
              })}
            </div>
          </>
        )}
      </section>
    </>
  )
}
