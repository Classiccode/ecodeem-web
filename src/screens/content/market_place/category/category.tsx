import { AuthPageLayout } from "../../../../layout"
import ProductItem from "../../../../components/Products/productItem"
import CategoryProvider from "./provider"

export function Category(): JSX.Element {
  return (
    <AuthPageLayout>
      <CategoryProvider>
        {({ productsByCategory, onCartClick }) => (
          <section className="MarketPlace">
            <h3>{productsByCategory?.[0]?.category.title}</h3>
            <div className="Products__Stack">
              {productsByCategory?.map((product: any) => (
                <ProductItem
                  key={product._id}
                  price={product.price}
                  productName={product.name}
                  productID={product._id}
                  category={product.category?.title}
                  img={product.image}
                  rating={product.rating}
                  onCartClick={(e: any) => onCartClick(e, product._id)}
                />
              ))}
            </div>
          </section>
        )}
      </CategoryProvider>
    </AuthPageLayout>
  )
}
