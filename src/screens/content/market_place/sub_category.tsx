/* eslint-disable indent */
import { IconButton } from "@material-ui/core"
import { ArrowBackIos } from "@material-ui/icons"
import { useSelector } from "react-redux"
import { Link, useHistory, useLocation } from "react-router-dom"
import type { RootState } from "../../../store"
import { ShoppingBasket } from "@material-ui/icons"

export function SubCategory(): JSX.Element {
  const { subCategories, profile, sectionalCategories } = useSelector(
    (state: RootState) => {
      return {
        subCategories: state.data.subCategories,
        sectionalCategories: state.data.sectionalCategories,
        profile: state.data.loginUserDetails,
      }
    }
  )
  const { pathname } = useLocation()
  const { goBack } = useHistory()
  return (
    <>
      <section
        className="MarketPlace"
        style={{
          position: "relative",
          marginLeft: 0,
          marginTop: "20px",
          height: "calc(100vh - 100px)",
          overflowY: "auto",
          minHeight: "calc(100vh - 100px)",
          top: "100px",
          width: "100%",
          minWidth: "100%",
        }}
      >
        <div className="market_place_header">
          <IconButton
            className="btn"
            onClick={() => {
              goBack()
            }}
          >
            <ArrowBackIos />
          </IconButton>
          <h4 className="s_title">
            {sectionalCategories.find(
              (s) =>
                s._id === pathname.split("/")[pathname.split("/").length - 1]
            )?.title ?? ""}
          </h4>
        </div>
        {subCategories.filter(
          (s) =>
            s.sectionalCategory._id ===
            pathname.split("/")[pathname.split("/").length - 1]
        ).length <= 0 ? (
          <>
            <div className="center">
              <ShoppingBasket className="_icon" />
              <span className="txt">No sections yet.</span>
            </div>
          </>
        ) : (
          subCategories
            .filter(
              (s) =>
                s.sectionalCategory._id ===
                pathname.split("/")[pathname.split("/").length - 1]
            )
            .map((sub) => {
              return (
                <Link
                  to={
                    profile?.token !== undefined
                      ? `/content/market/product_list/${sub._id}`
                      : `/market/product_list/${sub._id}`
                  }
                  className="sub_list"
                  key={sub._id}
                >
                  <div className="image">
                    <span style={{ backgroundImage: `url(${sub.image})` }} />
                  </div>
                  {sub.title}
                </Link>
              )
            })
        )}
      </section>
    </>
  )
}
