import CartProvider from "./provider"
import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import CircularProgress from "@material-ui/core/CircularProgress"
import DeleteIcon from "@material-ui/icons/Delete"
import RemoveOutlinedIcon from "@material-ui/icons/RemoveOutlined"
import AddOutlinedIcon from "@material-ui/icons/AddOutlined"
import Button from "../../../../components/Button/button"
import { Button as ButtonM } from "@material-ui/core"
import ShoppingCartOutlinedIcon from "@material-ui/icons/ShoppingCartOutlined"
import Address from "./Address"
import "./cart.scss"
import { numberWithCommas } from "../../../../utitly/string"
import { useQuery } from "react-query"
import { getCartItems } from "../../../../function/marketPlace/getCartItem"
import { RootState } from "../../../../store"
import { CartProductContent } from "../screen"
import Config from "../../../../config"
import { Link } from "react-router-dom"

interface CartProps {
  name: string
  quantity: number
  price: number
  image: string
  total: string
  onDelete: () => void
  onIncrement: () => void
  onDecrement: () => void
  deleteLoading?: boolean
  incrementLoading: boolean
  decrementLoading: boolean
  productID?: string
}

const Cart = () => {
  const [open, setOpen] = useState(false)
  const dispatch = useDispatch()
  const { profile } = useSelector((state: RootState) => {
    return {
      profile: state.data.loginUserDetails,
    }
  })
  const { data, isLoading, refetch } = useQuery("cart_products", async () => {
    return getCartItems({ userID: profile?.user._id ?? "" })
  })
  const [total, setTotal] = useState<number>(0)
  useEffect(() => {
    let _total = 0
    if (data !== undefined && data.length >= 1) {
      for (let i = 0; i < data.length; i++) {
        const t = data[i].price * data[i].quantity
        _total = t + _total
      }
      if (_total !== total) {
        setTotal(_total)
      }
    }
  }, [data])
  return (
    <>
      <section
        className="MarketPlace"
        style={{
          position: "relative",
          marginLeft: 0,
          marginTop: "40px",
          height: "calc(100vh - 100px)",
          overflowY: "auto",
          minHeight: "calc(100vh - 100px)",
          top: "100px",
          width: "100%",
          minWidth: "100%",
          paddingBottom: "100px",
        }}
      >
        <div className="container_m" style={{ paddingBottom: "100px" }}>
          {isLoading ? (
            <>
              <div className="center">
                <CircularProgress color="inherit" />
              </div>
            </>
          ) : data === undefined || data.length <= 0 ? (
            <>
              <div className="center">
                <h3 className="txt">No Items in Cart, Continue Shopping</h3>
              </div>
            </>
          ) : (
            data.map((cartProduct, i) => {
              return (
                <CartProductContent
                  key={i}
                  cartProduct={cartProduct}
                  refresh={refetch}
                />
              )
            })
          )}
        </div>
        <div className="checkout_m">
          <h3 className="total">
            {Config.nairaSign}
            {total.toLocaleString()}
          </h3>
          <ButtonM>
            <Link to="/content/market/new-order">Check Out</Link>
          </ButtonM>
        </div>
      </section>
    </>
  )
}
export default Cart
