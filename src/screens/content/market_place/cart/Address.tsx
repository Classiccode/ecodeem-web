import React, { useState } from "react"
import "./Address.scss"
import Input from "@material-ui/core/Input"
import Select from "@material-ui/core/Select"
import Button from "../../../../components/Button/button"
import Modal from "../../../../components/Dialog"
import { CreateOrder } from "../../../../config/endpoints"
import Config from "../../../../config"
import axios from "axios"
import { useDispatch, useSelector } from "react-redux"
import { createOrder } from "../../../../store/slice/orderSlice"
import { withRouter } from "react-router-dom"
import { RootState } from "../../../../store"

const Address = (props: any): JSX.Element => {
  const { closeModal } = props

  const dispatch = useDispatch()

  // const history = useHistory();

  const [state, setState] = useState({
    shippingType: "",
    address: "",
    city: "",
    state: "",
    postalCode: "",
    country: "",
  })

  const [loading, setLoading] = useState(false)

  const handleChange = (e: React.ChangeEvent<any>) => {
    const { name, value } = e.target

    setState({ ...state, [name]: value })
  }

  const handleSubmit = (e: React.ChangeEvent<any>) => {
    e.preventDefault()

    // props.history.push("/marketplace/checkout");

    createNewOrder()
  }

  const push = () => {
    props.history.push("/content/market/checkout")
  }

  const cart = useSelector((state: RootState) => state.cart)
  const user: any = localStorage.getItem("userID")

  const createNewOrder = async () => {
    setLoading(true)
    try {
      const response: any = await axios.post(
        Config.marketplaceApiUrl + CreateOrder(user),
        {
          shippingType: state.shippingType,
          shippingAddress: {
            address: state.address,
            city: state.city,
            state: state.state,
            postalCode: state.postalCode,
            country: state.country,
          },
        }
      )
      const { data, status } = response
      if (status != 201) {
        console.error(data.message)
        setLoading(false)
      }
      dispatch(createOrder(data))
      setLoading(false)
      push()
    } catch (e) {
      console.error(e)
      setLoading(false)
    }
  }

  return (
    <>
      <Modal closeModal={closeModal} title="Shipping Details">
        <form className="Address" onSubmit={handleSubmit}>
          <div className="Address__InputRow">
            <Select
              native
              value={state.shippingType}
              onChange={handleChange}
              name="shippingType"
              style={{ width: "inherit", fontSize: "14px" }}
              required
            >
              <option aria-label="None" value="">
                Select shipping type
              </option>
              {cart?.shippingType?.map((option: any) => (
                <option value={option._id} key={option._id}>
                  {option.title + ", " + option.timeline + ", " + option.price}
                </option>
              ))}

              {/* <option value={20}>Twenty</option>
							<option value={30}>Thirty</option> */}
            </Select>
          </div>
          <div className="Address__InputRow">
            <Input
              placeholder="Address"
              style={{ width: "inherit", fontSize: "14px" }}
              value={state.address}
              onChange={handleChange}
              name="address"
              required
            />
          </div>
          <div className="Address__InputRow">
            <Input
              placeholder="City"
              className="Address__InputRow__Input"
              value={state.city}
              onChange={handleChange}
              style={{ fontSize: "14px" }}
              name="city"
              required
            />
            <Input
              placeholder="Postal Code"
              className="Address__InputRow__Input"
              value={state.postalCode}
              onChange={handleChange}
              name="postalCode"
              style={{ fontSize: "14px" }}
              required
            />
          </div>
          <div className="Address__InputRow">
            <Input
              placeholder="State"
              className="Address__InputRow__Input"
              value={state.state}
              onChange={handleChange}
              name="state"
              style={{ fontSize: "14px" }}
              required
            />
            <Input
              placeholder="Country"
              value={state.country}
              onChange={handleChange}
              style={{ fontSize: "14px" }}
              className="Address__InputRow__Input"
              name="country"
              required
            />
          </div>
          <div className="Address__Button">
            <Button
              text={loading ? "Submitting" : "Submit"}
              type="navy"
              disable={loading ? true : false}
            />
          </div>
        </form>
      </Modal>
    </>
  )
}
export default withRouter(Address)
