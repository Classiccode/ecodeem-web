import { useEffect, ReactNode, FC, useState } from "react"

import axios from "axios"
import { RootState } from "../../../../store"
import Config from "../../../../config"
import { useDispatch, useSelector } from "react-redux"
import { getCategory } from "../../../../store/slice/productSlice"
import {
  getCart,
  removeFromCart,
  getTotalPrice,
  addToCart,
  decrementFromCart,
  getShippingType,
} from "../../../../store/slice/cartSlice"
import {
  GetProductsInCart,
  RemoveProductFromCart,
  AddProductToCart,
  DecrementFromCart,
  GetShippingType,
  GetCategories,
} from "../../../../config/endpoints"

type ChildrenProps = {
  cartLoader: boolean
  cart: any
  onDeleteClick: (ProductID: string) => void
  onIncrement: (ProductID: string) => void
  onDecrement: (ProductID: string) => void
  removeCartLoader: boolean
  addCartLoader: boolean
  decrementLoader: boolean
  shippingLoader: boolean
}

type Props = {
  children(_: ChildrenProps): ReactNode
}

const CartProvider: FC<Props> = ({
  children,
}: {
  children: ((_: ChildrenProps) => ReactNode) & ReactNode
}) => {
  const [cartLoader, setCartLoader] = useState(false)
  const [removeCartLoader, setRemoveCartLoader] = useState(false)
  const [addCartLoader, setAddCartLoader] = useState(false)
  const [decrementLoader, setDecrementLoader] = useState(false)
  const [shippingLoader, setShippingLoader] = useState(false)
  const [categoryLoader, setCategoryLoader] = useState(false)

  const dispatch = useDispatch()

  const cart = useSelector((state: RootState) => state.cart)

  useEffect(() => {
    allCartItems()
    allShippingType()
    if (categories.length === 0) {
      getProductCategories()
    }
  }, [])

  const user: any = localStorage.getItem("userID")
  const categories = useSelector((state: RootState) => state.product.categories)

  const getProductCategories = async () => {
    setCategoryLoader(true)
    try {
      const response: any = await axios.get(
        Config.marketplaceApiUrl + GetCategories()
      )
      const { data, status } = response

      //if error

      if (status != 200) {
        console.error(data.message)
        setCategoryLoader(false)
      }
      dispatch(getCategory(data))

      // setProducts(data.data.products);

      setCategoryLoader(false)
    } catch (e) {
      console.error(e)
      setCategoryLoader(false)
    }
  }

  const allCartItems = async () => {
    setCartLoader(true)
    try {
      const response: any = await axios.get(
        Config.marketplaceApiUrl + GetProductsInCart(user)
      )
      const { data, status } = response
      if (status != 200) {
        console.error(data.message)
        setCartLoader(false)
      }
      dispatch(getCart(data.myCart))
      dispatch(getTotalPrice())
      setCartLoader(false)
    } catch (e) {
      console.error(e)
      setCartLoader(false)
    }
  }

  const allCartItem = async () => {
    try {
      const response: any = await axios.get(
        Config.marketplaceApiUrl + GetProductsInCart(user)
      )
      const { data, status } = response
      if (status != 200) {
        console.error(data.message)
      }
      dispatch(getCart(data.myCart))
      dispatch(getTotalPrice())
    } catch (e) {
      console.error(e)
    }
  }

  const allShippingType = async () => {
    setShippingLoader(true)
    try {
      const response: any = await axios.get(
        Config.marketplaceApiUrl + GetShippingType()
      )
      const { data, status } = response
      if (status != 200) {
        console.error(data.message)
        setShippingLoader(false)
      }
      dispatch(getShippingType(data))
      setShippingLoader(false)
    } catch (e) {
      console.error(e)
      setShippingLoader(false)
    }
  }

  const removeItem = async (ProductID: string) => {
    setRemoveCartLoader(true)
    try {
      const response: any = await axios.patch(
        Config.marketplaceApiUrl + RemoveProductFromCart(ProductID, user)
      )
      const { data, status } = response
      if (status != 200) {
        console.error(data.message)
        setRemoveCartLoader(false)
      }
      dispatch(removeFromCart(data.cart))
      allCartItem()
      dispatch(getTotalPrice())
      setRemoveCartLoader(false)
    } catch (e) {
      console.error(e)
      setRemoveCartLoader(false)
    }
  }

  const decrementItem = async (ProductID: string) => {
    setDecrementLoader(true)
    try {
      const response: any = await axios.patch(
        Config.marketplaceApiUrl + DecrementFromCart(ProductID, user)
      )
      const { data, status } = response
      if (status != 200) {
        console.error(data.message)
        setDecrementLoader(false)
      }
      dispatch(decrementFromCart(data.cart))
      allCartItem()
      dispatch(getTotalPrice())
      setDecrementLoader(false)
    } catch (e) {
      console.error(e)
      setDecrementLoader(false)
    }
  }

  const addCartItem = async (productID: string) => {
    setAddCartLoader(true)
    try {
      const response: any = await axios.post(
        Config.marketplaceApiUrl + AddProductToCart(user),
        {
          productId: productID,
        }
      )
      const { data, status } = response
      if (status != 200) {
        console.error(data.message)
        setAddCartLoader(false)
      }
      dispatch(addToCart(data.cart))
      allCartItem()
      dispatch(getTotalPrice())
      setAddCartLoader(false)
    } catch (e) {
      console.error(e)
      setAddCartLoader(false)
    }
  }

  const onDeleteClick = (ProductID: string) => {
    removeItem(ProductID)
  }

  const onDecrement = (ProductID: string) => {
    decrementItem(ProductID)
  }

  const onIncrement = (ProductID: string) => {
    addCartItem(ProductID)
  }

  return (
    <>
      {children({
        cartLoader,
        cart,
        onDeleteClick,
        onIncrement,
        onDecrement,
        removeCartLoader,
        addCartLoader,
        decrementLoader,
        shippingLoader,
      })}
    </>
  )
}
export default CartProvider
