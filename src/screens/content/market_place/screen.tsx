import {
  AppBar,
  Box,
  CircularProgress,
  Tab,
  Tabs,
  Button,
} from "@material-ui/core"
import { ReactNode, useCallback, useEffect } from "react"
import { ChangeEvent } from "react"
import { useState } from "react"

import CartIcon from "remixicon-react/Book2LineIcon"
import ArrowBackIosOutlinedIcon from "@material-ui/icons/ArrowBackIosOutlined"
import ArrowForwardIosOutlinedIcon from "@material-ui/icons/ArrowForwardIosOutlined"
import MarketPlaceIcon from "remixicon-react/Store3LineIcon"
import {
  AllCampaignsReturnType,
  getAllCampaignsFuncHandler,
} from "../../../function"
import Blog from "../../../components/Blog/blog"
import Products from "../../../components/Products/products"
import ProductProvider from "../../unAuth/provider"
import banner3 from "../../../assets/images/bannerImage3.png"
import banner2 from "../../../assets/images/bannerImage2.png"
import banner1 from "../../../assets/images/bannerImage1.png"
import { useDispatch, useSelector } from "react-redux"
import { RootState, StoreType } from "../../../store"
import { useHistory, useLocation } from "react-router"
import { Link } from "react-router-dom"
import Config from "../../../config"
import ReactStars from "react-rating-stars-component"
import { toast } from "react-toastify"
import {
  NewArrival,
  MonthlyDeal,
  TodaysDeal,
  TopRatingProducts,
  TopSellingProducts,
  WeeksDeal,
  CartType,
  addCartItem,
} from "../../../function/marketPlace"
import type { ProductReturnType } from "../../../function/marketPlace"
import {
  decrementCartItem,
  removeFromCartItem,
} from "../../../function/marketPlace/decreaseCartItem"

interface TabPanelProps {
  children?: ReactNode
  index: number
  value: number
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`tabpanel-${index}`}
      aria-labelledby={`tab-${index}`}
      {...other}
      className="box-tab"
    >
      {value === index && <Box p={1}>{children}</Box>}
    </div>
  )
}

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  }
}

export function MarketPlaceMain(): JSX.Element {
  const [value, setValue] = useState<number>(0)
  const [campsData, setCampsData] = useState<AllCampaignsReturnType>()
  const [marketPlaceContent, setContent] = useState<{
    newArrival: ProductReturnType[]
    weekDeals: ProductReturnType[]
    dailyDeals: ProductReturnType[]
    monthlyDeals: ProductReturnType[]
    topSelling: ProductReturnType[]
    topRated: ProductReturnType[]
  }>({
    newArrival: [],
    weekDeals: [],
    dailyDeals: [],
    monthlyDeals: [],
    topSelling: [],
    topRated: [],
  })

  const setCamps = useCallback(() => {
    getAllCampaignsFuncHandler()
      .then(({ data }) => {
        data && setCampsData(data)
      })
      .catch((error) => {
        console.error(error)
      })
  }, [])

  useEffect(() => {
    setCamps()
  }, [setCamps])
  const handleChange = (event: ChangeEvent, newValue: unknown) => {
    setValue(newValue as number)
  }

  const images = [banner3, banner2, banner1]

  const [index, setIndex] = useState(0)

  const handleForward = () => {
    if (index === images.length - 1) {
      setIndex(0)
    } else setIndex(index + 1)
  }

  const handleBackward = () => {
    if (index === 0) {
      setIndex(images.length - 1)
    } else setIndex(index - 1)
  }

  const { categories } = useSelector((state: RootState) => {
    return {
      categories: state.data.categories,
    }
  })
  const { pathname } = useLocation()
  const [loading, setLoadingState] = useState(true)
  const callback = useCallback(async () => {
    await Promise.all([
      await NewArrival(),
      await WeeksDeal(),
      await MonthlyDeal(),
      await TodaysDeal(),
      await TopRatingProducts(),
      await TopSellingProducts(),
    ])
      .then((response) => {
        setContent({
          newArrival: response[0],
          weekDeals: response[1],
          monthlyDeals: response[2],
          dailyDeals: response[3],
          topRated: response[4],
          topSelling: response[5],
        })
      })
      .catch(() => {
        toast.error("Network Error!")
      })
      .finally(() => {
        setLoadingState(false)
      })
  }, [])
  useEffect(() => {
    callback()
  }, [callback])
  useEffect(() => {
    const interval = setInterval(() => {
      if (index === images.length - 1) {
        setIndex(0)
      } else setIndex(index + 1)
    }, 10000)
    return () => clearInterval(interval)
  }, [index])
  return (
    <>
      <section
        className="MarketPlace"
        style={{
          position: "relative",
          marginLeft: 0,
          marginTop: "70px",
          height: "calc(100vh - 100px)",
          overflowY: "auto",
          minHeight: "calc(100vh - 100px)",
          top: "100px",
          width: "100%",
          minWidth: "100%",
          paddingBottom: "100px",
        }}
      >
        <AppBar position="fixed" className="tab-bar">
          <Tabs
            value={value}
            onChange={(e, nv) => handleChange(e as ChangeEvent, nv)}
            aria-label="tabs"
          >
            <Tab icon={<MarketPlaceIcon />} label="Market" {...a11yProps(0)} />
            <Tab icon={<CartIcon />} label="Campaign" {...a11yProps(1)} />
          </Tabs>
        </AppBar>
        <TabPanel value={value} index={0}>
          <h2>MarketPlace</h2>
          <div
            style={{
              width: "100%",
              position: "relative",
              marginBottom: "24px",
            }}
            className="MarketPlace__Slider"
          >
            <div className="MarketPlace__Column">
              <ArrowBackIosOutlinedIcon
                onClick={handleBackward}
                fontSize="large"
                style={{
                  background: "#f7f7f7",
                  borderRadius: "50%",
                  padding: "4px",
                  cursor: "pointer",
                }}
              />
              <ArrowForwardIosOutlinedIcon
                onClick={handleForward}
                fontSize="large"
                style={{
                  background: "#f7f7f7",
                  borderRadius: "50%",
                  padding: "4px",
                  cursor: "pointer",
                }}
              />
            </div>
            <img src={images[index]} className="MarketPlace__HeaderImage" />
          </div>
          <h3 className="sub_title category_min">Categories</h3>
          <div className="category_list category_min">
            {categories.map((category) => {
              return (
                <Link
                  to={`${pathname}/sectional/${category._id}`}
                  className="content_content"
                  key={category._id}
                >
                  <div className="image">
                    <span
                      style={{ backgroundImage: `url(${category.image})` }}
                    />
                  </div>
                  <p className="txt_name">{category.title}</p>
                </Link>
              )
            })}
          </div>
          {loading ? (
            <div className="center">
              <CircularProgress color="inherit" />
            </div>
          ) : (
            <>
              {marketPlaceContent.newArrival.length >= 1 && (
                <div className="m_container">
                  <h3 className="sub_title">New Arrivals</h3>
                  <MarketPlaceContent content={marketPlaceContent.newArrival} />
                </div>
              )}
              {marketPlaceContent.dailyDeals.length >= 1 && (
                <div className="m_container">
                  <h3 className="sub_title">Today&apos;s Deals</h3>
                  <MarketPlaceContent content={marketPlaceContent.dailyDeals} />
                </div>
              )}
              {marketPlaceContent.weekDeals.length >= 1 && (
                <div className="m_container">
                  <h3 className="sub_title">Weekly Deals</h3>
                  <MarketPlaceContent content={marketPlaceContent.weekDeals} />
                </div>
              )}
              {marketPlaceContent.topSelling.length >= 1 && (
                <div className="m_container">
                  <h3 className="sub_title">Top Selling</h3>
                  <MarketPlaceContent content={marketPlaceContent.topSelling} />
                </div>
              )}
              {marketPlaceContent.topRated.length >= 1 && (
                <div className="m_container">
                  <h3 className="sub_title">Top Rated</h3>
                  <MarketPlaceContent content={marketPlaceContent.topRated} />
                </div>
              )}
              {marketPlaceContent.monthlyDeals.length >= 1 && (
                <div className="m_container">
                  <h3 className="sub_title">This Month&apos;s Deals</h3>
                  <MarketPlaceContent
                    content={marketPlaceContent.monthlyDeals}
                  />
                </div>
              )}
            </>
          )}

          {/* <ProductProvider>
            {({ allProducts, onCartClick }) => (
              <>
                <Products
                  categories={allProducts?.categories}
                  allProducts={allProducts.allProducts}
                  onCartClick={() => onCartClick}
                />
              </>
            )}
          </ProductProvider> */}
        </TabPanel>
        <TabPanel value={value} index={1}>
          <div
            style={{
              maxWidth: " 700px",
              margin: "auto",
            }}
          >
            {campsData && campsData.allCamps.length > 0 ? (
              campsData.allCamps.map((post, idx) => {
                if (post.postedBy) {
                  return <Blog global={false} key={idx} Deem={post} />
                }
              })
            ) : (
              <div>No campaigns yet</div>
            )}
          </div>
        </TabPanel>
      </section>
    </>
  )
}

export function MarketPlaceContent({
  content,
}: {
  content: ProductReturnType[]
}): JSX.Element {
  return (
    <div className="m_content">
      {content.map((con) => {
        return <ProductContent product={con} key={con._id} />
      })}
    </div>
  )
}

export function ProductContent({
  product,
}: {
  product: ProductReturnType
}): JSX.Element {
  const { profile } = useSelector((state: RootState) => {
    return {
      profile: state.data.loginUserDetails,
    }
  })
  const dispatch = useDispatch()
  return (
    <Link
      to={`${
        profile?.token !== undefined
          ? `/content/market/product/${product._id}}`
          : `/market/product/${product._id}}`
      }`}
      className="content_m"
      key={product._id}
      onClick={() => {
        dispatch({
          type: StoreType.setProductInView,
          value: product,
        })
      }}
    >
      <div className="image">
        <span style={{ backgroundImage: `url(${product.image})` }} />
      </div>
      <div className="details">
        <p className="name">{product.name}</p>
        <p className="price">
          {Config.nairaSign}
          {(product.price - product.discount.amount).toLocaleString()}{" "}
          {product.discount.active && (
            <span className="discountPrice">
              {Config.nairaSign}
              {(product.price - product.discount.amount).toLocaleString()}
            </span>
          )}
        </p>
        <ReactStars
          count={5}
          value={product.rating}
          size={18}
          className="star_rating"
          edit={false}
          activeColor="#FFC107"
        />
      </div>
    </Link>
  )
}

export function CartProductContent({
  cartProduct,
  refresh,
}: {
  cartProduct: CartType
  refresh: () => any
}): JSX.Element {
  const { profile } = useSelector((state: RootState) => {
    return {
      profile: state.data.loginUserDetails,
    }
  })
  const [loading, setLoading] = useState<boolean>(false)
  const dispatch = useDispatch()
  return (
    <div className="content_m" key={cartProduct._id}>
      <Link
        to={`/content/market/product/${cartProduct._id}`}
        className="link"
      />
      <div className={`center ${loading && "loading"}`}>
        <CircularProgress color="inherit" />
      </div>
      <div className="image">
        <span style={{ backgroundImage: `url(${cartProduct.image})` }} />
      </div>
      <div className="details">
        <p className="name">{cartProduct.name}</p>
        <p className="price">
          {Config.nairaSign}
          {cartProduct.price.toLocaleString()}
        </p>
      </div>
      <div className="counter">
        <Button
          onClick={async () => {
            setLoading(true)
            if (cartProduct.quantity <= 1) {
              await removeFromCartItem({
                productID: cartProduct.productId,
                userID: profile?.user._id ?? "",
              })
                .then(() => {
                  refresh()
                })
                .catch((err) => {
                  toast.error(err)
                })
                .finally(() => {
                  setLoading(false)
                })
            } else {
              await decrementCartItem({
                productID: cartProduct.productId,
                userID: profile?.user._id ?? "",
              })
                .then(() => {
                  refresh()
                })
                .catch((err) => {
                  toast.error(err)
                })
                .finally(() => {
                  setLoading(false)
                })
            }
          }}
        >
          -
        </Button>
        <span className="count">{cartProduct.quantity}</span>
        <Button
          onClick={async () => {
            setLoading(true)
            await addCartItem({
              productID: cartProduct.productId,
              userID: profile?.user._id ?? "",
            })
              .then(() => {
                refresh()
              })
              .catch((err) => {
                toast.error(err)
              })
              .finally(() => {
                setLoading(false)
              })
          }}
        >
          +
        </Button>
      </div>
    </div>
  )
}
