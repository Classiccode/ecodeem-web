/* eslint-disable indent */
import { IconButton } from "@material-ui/core"
import { ArrowBackIos } from "@material-ui/icons"
import { useSelector } from "react-redux"
import { Link, useHistory, useLocation } from "react-router-dom"
import type { RootState } from "../../../store"
import { ShoppingBasket } from "@material-ui/icons"

export function SectionalCategory(): JSX.Element {
  const { categories, profile, sectionalCategories } = useSelector(
    (state: RootState) => {
      return {
        categories: state.data.categories,
        sectionalCategories: state.data.sectionalCategories,
        profile: state.data.loginUserDetails,
      }
    }
  )
  const { pathname } = useLocation()
  const { goBack } = useHistory()
  return (
    <>
      <section
        className="MarketPlace"
        style={{
          position: "relative",
          marginLeft: 0,
          marginTop: "20px",
          height: "calc(100vh - 100px)",
          overflowY: "auto",
          minHeight: "calc(100vh - 100px)",
          top: "100px",
          width: "100%",
          minWidth: "100%",
        }}
      >
        <div className="market_place_header">
          <IconButton
            className="btn"
            onClick={() => {
              goBack()
            }}
          >
            <ArrowBackIos />
          </IconButton>
          <h4 className="s_title">
            {categories.find(
              (s) =>
                s._id === pathname.split("/")[pathname.split("/").length - 1]
            )?.title ?? ""}
          </h4>
        </div>
        {sectionalCategories.filter(
          (s) =>
            s.category._id ===
            pathname.split("/")[pathname.split("/").length - 1]
        ).length <= 0 ? (
          <>
            <div className="center">
              <ShoppingBasket className="_icon" />
              <span className="txt">No sections yet.</span>
            </div>
          </>
        ) : (
          sectionalCategories
            .filter(
              (s) =>
                s.category._id ===
                pathname.split("/")[pathname.split("/").length - 1]
            )
            .map((sectional) => {
              return (
                <Link
                  to={
                    profile?.token !== undefined
                      ? `/content/market/sub_category/${sectional._id}`
                      : `/market/sub_category/${sectional._id}`
                  }
                  className="sub_list"
                  key={sectional._id}
                >
                  {sectional.title}
                </Link>
              )
            })
        )}
      </section>
    </>
  )
}
