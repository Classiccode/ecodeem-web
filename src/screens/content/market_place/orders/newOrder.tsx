import { useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import CircularProgress from "@material-ui/core/CircularProgress"
import {
  Button as ButtonM,
  FormControl,
  InputAdornment,
  MenuItem,
  Select,
  TextField,
} from "@material-ui/core"
import { useQuery } from "react-query"
import { RootState } from "../../../../store"
import { Explore, MyLocation, PersonPin } from "@material-ui/icons"
import { Autocomplete } from "@material-ui/lab"
import { countryAndStates } from "../../../../assets/country"
import { getShippingTypes } from "../../../../function/marketPlace/getShippingTypes"
import { createShippingOrder } from "../../../../function/marketPlace/createOrder"
import { toast } from "react-toastify"
import { payNow } from "../../../../function"

const NewOrder = (): JSX.Element => {
  const dispatch = useDispatch()
  const { profile } = useSelector((state: RootState) => {
    return {
      profile: state.data.loginUserDetails,
    }
  })
  const [shippingType, setShippingType] = useState<string>()
  const [isLoadingReq, setLoadingState] = useState<boolean>(false)
  const [address, setAddress] = useState<string>("")
  const [city, setCity] = useState<string>("")
  const [state, setState] = useState<string>("")
  const [postalCode, setPostalCode] = useState<string>("")
  const [country, setCountry] = useState<string>("")
  const { data, isFetchedAfterMount, isLoading } = useQuery(
    "shippingType",
    async () => {
      return getShippingTypes()
    }
  )
  return (
    <>
      <section
        className="MarketPlace"
        style={{
          position: "relative",
          marginLeft: 0,
          marginTop: "40px",
          height: "calc(100vh - 100px)",
          overflowY: "auto",
          minHeight: "calc(100vh - 100px)",
          top: "100px",
          width: "100%",
          minWidth: "100%",
          paddingBottom: "100px",
        }}
      >
        <h2>Shipping Info</h2>
        <form
          onSubmit={async (e) => {
            e.preventDefault()
            setLoadingState(true)
            await createShippingOrder({
              shippingAddress: {
                address,
                city,
                country,
                postalCode: parseInt(postalCode),
                state,
              },
              shippingType: shippingType ?? "",
              userID: profile?.user._id ?? "",
            })
              .then(async (res) => {
                await payNow({
                  userID: profile?.user._id ?? "",
                  orderId: res.data._id,
                })
                  .then((link) => {
                    window.open(link)
                  })
                  .catch((err) => {
                    toast.error(err)
                  })
                  .finally(() => {
                    setLoadingState(false)
                  })
              })
              .catch((err) => {
                setLoadingState(false)
                toast.error(err)
              })
          }}
        >
          <div className="inputBox">
            <label htmlFor="address">Address*</label>
            <TextField
              className="inputBox"
              type="address"
              id="address"
              required
              variant="outlined"
              value={address}
              onChange={({ target: { value } }) => {
                setAddress(value)
              }}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <PersonPin />
                  </InputAdornment>
                ),
              }}
            />
          </div>
          <div className="inputBox">
            <label htmlFor="city">City*</label>
            <TextField
              className="inputBox"
              id="city"
              type="text"
              required
              variant="outlined"
              value={city}
              onChange={({ target: { value } }) => {
                setCity(value)
              }}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <MyLocation />
                  </InputAdornment>
                ),
              }}
            />
          </div>
          <div className="inputBox">
            <label htmlFor="countries">Country*</label>

            <Autocomplete
              id="countries"
              options={countryAndStates}
              getOptionLabel={(option) => option.name}
              placeholder="Enter your Country"
              className="inputField"
              value={countryAndStates.find(
                (_country) =>
                  _country.name.toLowerCase() === country.toLowerCase()
              )}
              onChange={(e, i) => {
                setCountry(i?.name ?? "")
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  variant="outlined"
                  required
                  label="Country"
                  helperText={
                    state.length < 1 ||
                    countryAndStates.find(
                      (_country) =>
                        _country.name.toLowerCase() === country.toLowerCase()
                    ) !== undefined
                      ? ""
                      : "Please enter your country"
                  }
                  error={
                    state.length < 1 ||
                    countryAndStates.find(
                      (_country) =>
                        _country.name.toLowerCase() === country.toLowerCase()
                    ) !== undefined
                  }
                />
              )}
            />
          </div>
          <div className="inputBox">
            <label htmlFor="state">State*</label>
            <Autocomplete
              id="state"
              options={
                countryAndStates.find(
                  (_country) =>
                    _country.name.toLowerCase() === country.toLowerCase()
                )?.states ?? []
              }
              getOptionLabel={(option) => option.name}
              placeholder="Enter your Country"
              className="inputField"
              value={countryAndStates
                .find(
                  (_country) =>
                    _country.name.toLowerCase() === country.toLowerCase()
                )
                ?.states.find(
                  (_state) => _state.name.toLowerCase() === state.toLowerCase()
                )}
              onChange={(e, i) => {
                setState(i?.name ?? "")
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  variant="outlined"
                  required
                  label="State"
                  helperText={
                    state.length < 1 ||
                    countryAndStates
                      .find(
                        (_country) =>
                          _country.name.toLowerCase() === country.toLowerCase()
                      )
                      ?.states.find(
                        (_state) =>
                          _state.name.toLowerCase() === state.toLowerCase()
                      ) !== undefined
                      ? ""
                      : "Please enter your state of residence"
                  }
                  error={
                    state.length < 1 ||
                    countryAndStates
                      .find(
                        (_country) =>
                          _country.name.toLowerCase() === country.toLowerCase()
                      )
                      ?.states.find(
                        (_state) =>
                          _state.name.toLowerCase() === state.toLowerCase()
                      ) !== undefined
                  }
                />
              )}
            />
          </div>
          <div className="inputBox">
            <label htmlFor="postal">Postal Code*</label>

            <TextField
              className="inputBox"
              id="postal"
              type="number"
              required
              variant="outlined"
              value={postalCode}
              onChange={({ target: { value } }) => {
                setPostalCode(value)
              }}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <Explore />
                  </InputAdornment>
                ),
              }}
            />
          </div>
          <div className="inputBox">
            <label htmlFor="shippingType">Shipping Type</label>
            <FormControl required variant="outlined">
              <Select
                value={shippingType}
                onChange={({ target }) => {
                  setShippingType(target.value as string)
                }}
              >
                {!isFetchedAfterMount && isLoading ? (
                  <MenuItem>
                    <em>Loading available shipping types</em>
                  </MenuItem>
                ) : data === undefined || data?.length <= 0 ? (
                  <MenuItem>No Shipping available</MenuItem>
                ) : (
                  data?.map((shipping) => {
                    return (
                      <MenuItem value={shipping._id} key={shipping._id}>
                        {shipping.title},{shipping.location}
                      </MenuItem>
                    )
                  })
                )}
              </Select>
            </FormControl>
          </div>
          <ButtonM
            type="submit"
            className="submit_btn"
            disabled={isLoadingReq}
            style={{ marginTop: "20px" }}
          >
            {isLoadingReq ? <CircularProgress color="inherit" /> : "Pay Out"}
          </ButtonM>
        </form>
      </section>
    </>
  )
}
export default NewOrder
