import { useState, useEffect } from "react"
import axios from "axios"
import { VerifyPayment } from "../../../../config/endpoints"
import Config from "../../../../config"
import { useLocation, Link } from "react-router-dom"
import CircularProgress from "@material-ui/core/CircularProgress"
import CheckCircleOutlineOutlinedIcon from "@material-ui/icons/CheckCircleOutlineOutlined"
import Button from "../../../../components/Button/button"

function useQuery() {
  return new URLSearchParams(useLocation().search)
}

const PaymentConfirmation = () => {
  const [verifyLoading, setVerifyLoading] = useState(false)
  const [verify, setVerify] = useState("")
  const user: any = localStorage.getItem("userID")

  const query = useQuery()

  const TransId = query?.get("transaction_id") || ""

  useEffect(() => {
    payVerify(TransId)
  }, [])

  const payVerify = async (TransID: string) => {
    setVerifyLoading(true)
    try {
      const response: any = await axios.post(
        Config.marketplaceApiUrl + VerifyPayment(user, TransID)
      )
      const { data, status } = response
      if (status != 200) {
        console.error(data.message)
        setVerifyLoading(false)
        return null
      }
      setVerifyLoading(false)
      setVerify("Verified")

      // window.location.href = data.data.data.link;
    } catch (e) {
      setVerifyLoading(false)
    }
  }
  return (
    <>
      <>
        <section>
          <div className="Order">
            <div className="Order__Box">
              {verifyLoading ? (
                <div>
                  <div>
                    <CircularProgress />
                  </div>
                  <p style={{ fontSize: "24px", marginTop: "24px" }}>
                    Verifying Payment
                  </p>
                </div>
              ) : (
                ""
              )}
              {verify === "Verified" ? (
                <div>
                  <div className="Order__Box__Icon__IconBox">
                    <CheckCircleOutlineOutlinedIcon
                      className="Order__Box__IconBox__Icon"
                      fontSize="large"
                    />
                  </div>
                  <p style={{ fontSize: "24px" }}>Payment Completed</p>
                  <Link to="/marketplace">
                    <div className="Order__Box__Button">
                      <Button text="Continue to MarketPlace" type="secondary" />
                    </div>
                  </Link>
                </div>
              ) : (
                ""
              )}
            </div>
          </div>
        </section>
      </>
    </>
  )
}
export default PaymentConfirmation
