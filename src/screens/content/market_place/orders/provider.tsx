import { useEffect, ReactNode, FC, useState } from "react"

import axios from "axios"
import { RootState } from "../../../../store"
import Config from "../../../../config"
import { useDispatch, useSelector } from "react-redux"
import {
  cancelOrder,
  getOrders,
  trackOrder,
} from "../../../../store/slice/orderSlice"
import { getCategory } from "../../../../store/slice/productSlice"
import {
  CancelOrder,
  GetOrder,
  TrackOrder,
  MakePayment,
  GetCategories,
} from "../../../../config/endpoints"

type ChildrenProps = {
  Orders: any
  orderLoading: boolean
  onCancelClick: (OrderID: string) => void
  cancelLoader: boolean
  deleteId: string
  checkoutLoading: boolean
  onCheckoutClick: (OrderID: string) => void
  onTrackOrder: (OrderID: string) => void
}

type Props = {
  children(_: ChildrenProps): ReactNode
}

const OrderProvider: FC<Props> = ({
  children,
}: {
  children: ((_: ChildrenProps) => ReactNode) & ReactNode
}) => {
  const [orderLoading, setOrderLoading] = useState(false)
  const [cancelLoader, setCancelLoader] = useState(false)
  const [deleteId, setDeleteId] = useState("")
  const [checkoutLoading, setCheckoutloading] = useState(false)
  const [categoryLoader, setCategoryLoader] = useState(false)
  const [verifyLoading, setVerifyLoading] = useState(false)
  const [trackLoading, setTrackLoading] = useState(false)

  const dispatch = useDispatch()

  useEffect(() => {
    getAllOrders()
    if (categories.length === 0) {
      getProductCategories()
    }
  }, [])

  const user: any = localStorage.getItem("userID")

  const Orders = useSelector((state: RootState) => state.orders)
  const categories = useSelector((state: RootState) => state.product.categories)

  const getProductCategories = async () => {
    setCategoryLoader(true)
    try {
      const response: any = await axios.get(
        Config.marketplaceApiUrl + GetCategories()
      )
      const { data, status } = response

      //if error

      if (status != 200) {
        console.error(data.message)
        setCategoryLoader(false)
      }
      dispatch(getCategory(data))

      // setProducts(data.data.products);

      setCategoryLoader(false)
    } catch (e) {
      setCategoryLoader(false)
    }
  }

  const getAllOrders = async () => {
    setOrderLoading(true)
    try {
      const response: any = await axios.get(
        Config.marketplaceApiUrl + GetOrder(user)
      )
      const { data, status } = response
      if (status != 200) {
        console.error(data.message)
        setOrderLoading(false)
      }
      dispatch(getOrders(data.orders))
      setOrderLoading(false)
    } catch (e) {
      setOrderLoading(false)
    }
  }

  const cancelAllOrders = async (OrderID: string) => {
    setCancelLoader(true)
    try {
      const response: any = await axios.patch(
        Config.marketplaceApiUrl + CancelOrder(OrderID, user)
      )
      const { data, status } = response
      if (status != 200) {
        console.error(data.message)
        setCancelLoader(false)
      }

      dispatch(cancelOrder(data))
      getAllOrders()
      setCancelLoader(false)
      setDeleteId("")
    } catch (e) {
      setCancelLoader(false)
    }
  }

  const completeCheckout = async (OrderID: string) => {
    setCheckoutloading(true)
    try {
      const response: any = await axios.post(
        Config.marketplaceApiUrl + MakePayment(user, OrderID)
      )
      const { data, status } = response
      if (status != 200) {
        console.error(data.message)
        setCheckoutloading(false)
      }
      setCheckoutloading(false)
      window.location.href = data.data.link
    } catch (e) {
      setCheckoutloading(false)
    }
  }

  const trackMyOrder = async (OrderID: string) => {
    const payload = {
      orderID: OrderID,
    }
    setTrackLoading(true)
    try {
      const response: any = await axios.get(
        Config.marketplaceApiUrl + TrackOrder(),
        {
          data: payload,
        }
      )
      const { data, status } = response
      if (status != 200) {
        console.error(data.message)
        setTrackLoading(false)
      }
      setTrackLoading(false)
    } catch (e) {
      setTrackLoading(false)
    }
  }

  const onCancelClick = (OrderID: string) => {
    cancelAllOrders(OrderID)
    setDeleteId(OrderID)
  }

  const onCheckoutClick = (OrderID: string) => {
    completeCheckout(OrderID)
  }

  const onTrackOrder = (OrderID: string) => {
    trackMyOrder(OrderID)
  }
  return (
    <>
      {children({
        orderLoading,
        Orders,
        onCancelClick,
        cancelLoader,
        deleteId,
        onCheckoutClick,
        checkoutLoading,
        onTrackOrder,
      })}
    </>
  )
}
export default OrderProvider
