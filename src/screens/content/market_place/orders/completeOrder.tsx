import OrderProvider from "./provider"
import { useDispatch } from "react-redux"
import Button from "../../../../components/Button/button"
import "./order.scss"
import { numberWithCommas } from "../../../../utitly/string"

interface OrderProps {
  name: string
  quantity: number
  price: number
  image: string
  total: string
  onCancel: () => void
  cancelLoading?: boolean
  status: string
  deleteID: string
  currentID: string
}

const OrderItem = ({
  name,
  quantity,
  price,
  image,
  total,
  onCancel,
  status,
  cancelLoading,
  deleteID,
  currentID,
}: OrderProps) => {
  return (
    <>
      <div className="Order__Column__Middle__Item">
        <div className="Order__Column__Middle__Item__One">
          <div className="Order__Column__Middle__Item__One__ImageBox">
            <img
              src={image}
              className="Order__Column__Middle__Item__One__ImageBox__Image"
            />
          </div>
          <p>{name}</p>
        </div>
        <div className="Order__Column__Middle__Item__Two">
          <div>
            <p className="Order__Column__Middle__Item__Two__Price__MainPrice">
              ₦{numberWithCommas(total)}
            </p>
            <p className="Order__Column__Middle__Item__Two__Price__OtherPrice">
              ₦{numberWithCommas(price)} x {quantity} item
            </p>
          </div>
          {/* <div className="Order__Column__Middle__Item__Two__Quantity"> */}

          {/* </div> */}
          <div className="Order__Column__Middle__Item__Two__Button">
            {/* {deleteLoading ? (
								<CircularProgress />
							) : (
								<DeleteIcon
									htmlColor="#bbbbbb"
									style={{ fontSize: "32px", cursor: "pointer" }}
									onClick={() => onDelete()}
								/>
			)} */}
            <Button
              type="nav"
              text={
                cancelLoading && currentID === deleteID
                  ? "Cancelling..."
                  : "Cancel Order"
              }
              onClick={() => onCancel()}
            />
          </div>
        </div>
      </div>
    </>
  )
}

const CompleteOrder = () => {
  const dispatch = useDispatch()
  return (
    <>
      <>
        <OrderProvider>
          {({
            orderLoading,
            Orders,
            onCancelClick,
            cancelLoader,
            deleteId,
            onCheckoutClick,
          }) => (
            <section>
              <div className="Order">
                <div className="Order__Column">
                  <div className="Order__Column__Middle">
                    <p className="Order__Column__Middle__Header">Checkout</p>
                    {/* <div> //====I might add delivery address later when I am in a good mood====
                                <p>Delivery Details</p>
                            </div> */}
                    {Orders?.create.length < 1 && (
                      <p>You have no pending orders</p>
                    )}
                    {/* // onIncrement={() => onIncrement(car.productId)}
                    // onDecrement={() => onDecrement(car.productId)} */}
                    {Orders?.create?.orderItems?.map((car: any) => (
                      <OrderItem
                        name={car.name}
                        total={car.total}
                        image={car.image}
                        price={car.price}
                        quantity={car.quantity}
                        key={car._id}
                        onCancel={() => onCancelClick(car._id)}
                        cancelLoading={cancelLoader}
                        status={car.status}
                        deleteID={deleteId}
                        currentID={car._id}
                      />
                    ))}
                    {Orders?.create?.orderItems?.length > 0 && (
                      <div className="Cart__Column__Middle__Checkbox">
                        <div>
                          {/* <p className="Cart__Column__Middle__Checkbox__Total">
												Total
											</p> */}
                          <p className="Cart__Column__Middle__Checkbox__Total">
                            Item Price: ₦
                            {numberWithCommas(
                              Orders.create.orderDetails.itemPrice
                            )}
                          </p>
                          <p className="Cart__Column__Middle__Checkbox__Total">
                            Shipping Cost: ₦
                            {numberWithCommas(
                              Orders.create.orderDetails.shippingPrice
                            )}
                          </p>
                          <p className="Cart__Column__Middle__Checkbox__Price">
                            Total: ₦
                            {numberWithCommas(
                              Orders.create.orderDetails.totalPrice
                            )}
                          </p>
                        </div>
                        <div className="Cart__Column__Middle__Checkbox__Button">
                          <Button
                            text="Proceed to pay"
                            type="navy"
                            className="nav"
                            onClick={() => onCheckoutClick(Orders.create._id)}
                          />
                        </div>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </section>
          )}
        </OrderProvider>
      </>
    </>
  )
}
export default CompleteOrder
