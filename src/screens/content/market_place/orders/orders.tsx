import { numberWithCommas } from "../../../../utitly"
import OrderProvider from "./provider"
import { useState } from "react"
import { sum } from "lodash"
import { useSelector } from "react-redux"

// import Review from "../../../components/Modals/Review/review"

import Chip from "@material-ui/core/Chip"
import Button from "../../../../components/Button/button"
import CircularProgress from "@material-ui/core/CircularProgress"
import "./order.scss"
import { useQuery } from "react-query"
import { getMyOrder } from "../../../../function/marketPlace/getMyOrders"
import { RootState } from "../../../../store"
import { trackMyOrder } from "../../../../function/marketPlace/trackOrder"
import { CancelOrder } from "../../../../function/marketPlace/cancelOrder"
import { toast } from "react-toastify"

interface OrderProps {
  name: string
  quantity: number
  price: number
  image: string
  total: string
  onCancel: () => Promise<string>
  cancelLoading?: boolean
  status: string
  deleteID: string
  currentID: string
  trackOrder: () => Promise<string>
}

const OrderItem = ({
  name,
  quantity,
  price,
  image,
  total,
  onCancel,
  status,
  deleteID,
  currentID,
  trackOrder,
}: OrderProps) => {
  const [l, setL] = useState<boolean>(false)
  const [cancelLoading, setCancelLoading] = useState<boolean>(false)
  return (
    <>
      <div className="Order__Column__Middle__Item">
        <div className="Order__Column__Middle__Item__One">
          <div className="Order__Column__Middle__Item__One__ImageBox">
            <img
              src={image}
              className="Order__Column__Middle__Item__One__ImageBox__Image"
            />
          </div>
          <p>{name}</p>
        </div>
        <div className="Order__Column__Middle__Item__Two">
          <div>
            <p className="Order__Column__Middle__Item__Two__Price__MainPrice">
              ₦{numberWithCommas(total)}
            </p>
            <p className="Order__Column__Middle__Item__Two__Price__OtherPrice">
              ₦{numberWithCommas(price)} x {quantity} item
            </p>
          </div>
          {/* <div className="Order__Column__Middle__Item__Two__Quantity"> */}
          <div
            className={
              status === "cancelled"
                ? "Order__Column__Middle__Item__Two__Buttonn"
                : "Order__Column__Middle__Item__Two__Buttony"
            }
          >
            {/* // className={
              // status === "cancelled"
              // ? "Order__Column__Middle__Item__Two__Buttonn"
              // : ""
              // } */}
            <Chip
              label={status}
              style={{
                fontSize: "14px",
                background: "#979797",
                height: "21px",
              }}
            />
            <p
              onClick={async () => {
                setL(true)
                await trackOrder()
                  .catch((err) => {
                    toast.error(err)
                  })
                  .finally(() => {
                    setL(false)
                  })
              }}
              style={{ cursor: "pointer" }}
            >
              {l ? "Tracking Order..." : "Track Order"}
            </p>
          </div>
          {/* {open && <Review closeModal={closeModal} />} */}
          {/* </div> */}
          <div className="Order__Column__Middle__Item__Two__Button">
            {/* {deleteLoading ? (
								<CircularProgress />
							) : (
								<DeleteIcon
									htmlColor="#bbbbbb"
									style={{ fontSize: "32px", cursor: "pointer" }}
									onClick={() => onDelete()}
								/>
							)} */}
            {status === "pending" ? (
              <Button
                type="nav"
                text={
                  cancelLoading && currentID === deleteID
                    ? "Cancelling..."
                    : "Cancel Order"
                }
                onClick={async () => {
                  setCancelLoading(true)
                  await onCancel()
                    .catch((err) => {
                      toast.error(err)
                    })
                    .finally(() => {
                      setCancelLoading(false)
                    })
                }}
              />
            ) : (
              ""
            )}
          </div>
        </div>
      </div>
    </>
  )
}

const Orders = (): JSX.Element => {
  const { profile } = useSelector((state: RootState) => {
    return {
      profile: state.data.loginUserDetails,
    }
  })
  const {
    data: Orders,
    isLoading: orderLoading,
    refetch,
  } = useQuery("orders", async () => {
    return getMyOrder({ userID: profile?.user._id ?? "" })
  })
  return (
    <>
      <section>
        <div className="Order">
          <div className="Order__Column">
            <div className="Order__Column__Middle">
              <p className="Order__Column__Middle__Header">All Orders</p>
              {orderLoading ? (
                <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    height: "50%",
                  }}
                >
                  <CircularProgress />
                </div>
              ) : (
                ""
              )}
              {/* // onIncrement={() => onIncrement(car.productId)}
                    // onDecrement={() => onDecrement(car.productId)} */}
              {Orders?.map((order) => (
                <>
                  <OrderItem
                    name={`${profile?.user.firstName ?? ""} ${
                      profile?.user.lastName ?? ""
                    }`}
                    total={order.orderDetails.totalPrice.toLocaleString()}
                    image={order.orderItems[0].image}
                    price={Math.max(...order.orderItems.map((i) => i.price))}
                    quantity={sum(order.orderItems.map((i) => i.quantity))}
                    key={order._id}
                    onCancel={async () =>
                      CancelOrder({
                        userID: profile?.user._id ?? "",
                        orderID: order._id,
                      }).then((r) => {
                        refetch()
                        return r
                      })
                    }
                    cancelLoading={false}
                    status={order.status}
                    deleteID={""}
                    currentID={order._id}
                    trackOrder={async () =>
                      trackMyOrder({ orderID: order._id }).then((r) => {
                        refetch()
                        return r
                      })
                    }
                  />
                </>
              ))}
            </div>
          </div>
        </div>
      </section>
    </>
  )
}
export default Orders
