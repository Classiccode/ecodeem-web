import { useEffect, ReactNode, FC, useState } from "react"
import { useHistory } from "react-router-dom"

import axios from "axios"
import { RootState } from "../../../store"
import {
  GetAllProducts,
  GetCategories,
  AddProductToCart,
  GetProductsInCart,
} from "../../../config/endpoints"
import { ProductType } from "../../../core/type"
import Config from "../../../config"
import { useDispatch, useSelector } from "react-redux"
import { getAllProducts, getCategory } from "../../../store/slice/productSlice"
import {
  addToCart,
  getTotalPrice,
  getCart,
} from "../../../store/slice/cartSlice"

type ChildrenProps = {
  addLike: (event: MouseEvent, productId: string | number) => void
  removeLike: (event: MouseEvent, productId: string | number) => void
  products: ProductType[] | undefined
  searchProducts: any
  allProducts: any
  showLoader: boolean
  categoryLoader: boolean
  onCartClick: (e: any, productID: string) => void
}

type Props = {
  children(_: ChildrenProps): ReactNode
}

const ProductProvider: FC<Props> = ({ children }) => {
  const [like, setLike] = useState(false)
  const [showLoader, setLoader] = useState(false)
  const [products, setProducts] = useState<ProductType[]>([])
  const [categoryLoader, setCategoryLoader] = useState(false)
  const [addCartLoader, setAddCartLoader] = useState(false)

  const dispatch = useDispatch()

  const history = useHistory()

  const allProducts = useSelector((state: RootState) => state.product)

  const searchProducts = useSelector(
    (state: RootState) => state.product.searchResult
  )

  useEffect(() => {
    getProducts()
    getProductCategories()
    allCartItems()
  }, [])

  const allCartItems = async () => {
    try {
      const response: any = await axios.get(
        Config.marketplaceApiUrl + GetProductsInCart(user)
      )
      const { data, status } = response
      if (status != 200) {
        console.error(data.message)
        return null
      }
      dispatch(getCart(data.data.myCart))
      dispatch(getTotalPrice())
    } catch (e) {
      console.error(e)
    }
  }

  const getProducts = async () => {
    setLoader(true)

    try {
      const response: any = await axios.get(
        Config.marketplaceApiUrl + GetAllProducts()
      )
      const { data, status } = response
      if (status != 200) {
        console.error(data.message)
        setLoader(false)
      }
      dispatch(getAllProducts(data.products))
      setProducts(data.products)
      setLoader(false)
    } catch (e) {
      console.error(e)
      setLoader(false)
    }
  }

  const getProductCategories = async () => {
    setCategoryLoader(true)
    try {
      const response: any = await axios.get(
        Config.marketplaceApiUrl + GetCategories()
      )
      const { data, status } = response
      if (status != 200) {
        console.error(data.message)
        setCategoryLoader(false)
      }
      dispatch(getCategory(data))
      setCategoryLoader(false)
    } catch (e) {
      console.error(e)
      setCategoryLoader(false)
    }
  }

  const addLike = async (event: MouseEvent, productId: string | number) => {
    event.preventDefault()
    setLike(true)
  }

  const removeLike = async (event: MouseEvent, productId: string | number) => {
    event.preventDefault()
    setLike(false)
  }

  const user: any = localStorage.getItem("userID")

  const addCartItem = async (productID: string) => {
    setAddCartLoader(true)
    try {
      const response: any = await axios.post(
        Config.marketplaceApiUrl + AddProductToCart(user),
        {
          productId: productID,
        }
      )
      const { data, status } = response
      if (status != 200) {
        console.error(data.message)
        setAddCartLoader(false)
      }
      dispatch(addToCart(data.cart))
      setAddCartLoader(false)
      history.push("/marketPlace/cart")
    } catch (e) {
      console.error(e)
      setAddCartLoader(false)
    }
  }

  const onCartClick = (e: any, productID: string) => {
    e.preventDefault()
    if (!user) {
      history.push("/auth/login")
      return
    }
    addCartItem(productID)
  }

  return (
    <>
      {children({
        products,
        addLike,
        removeLike,
        allProducts,
        showLoader,
        categoryLoader,
        onCartClick,
        searchProducts,
      })}
    </>
  )
}

export default ProductProvider
