import { ContentLayout, UnAuthLayout } from "../../../layout"
import ProductItem from "../../../components/Products/productItem"
import CategoryProvider from "./provider"
import { AnimatePresence } from "framer-motion"
import { ClipLoader } from "react-spinners"

export function Category(): JSX.Element {
  return (
    <UnAuthLayout>
      <CategoryProvider>
        {({ productsByCategory, onCartClick }) => (
          <section className="MarketPlace">
            <h3>{productsByCategory?.[0]?.category.title}</h3>
            <div className="Products__Stack">
              {productsByCategory?.map((product: any) => (
                <ProductItem
                  key={product._id}
                  price={product.price}
                  productName={product.name}
                  productID={product._id}
                  category={product.category?.title}
                  img={product.image}
                  rating={product.rating}
                  onCartClick={(e: any) => onCartClick(e, product._id)}
                />
              ))}
            </div>
          </section>
        )}
      </CategoryProvider>
    </UnAuthLayout>
  )
}

export function CategoryAuth(): JSX.Element {
  return (
    <>
      <CategoryProvider>
        {({ productsByCategory, onCartClick, showLoader }) => {
          return (
            <section className="MarketPlace">
              <AnimatePresence>
                {showLoader ? (
                  <div className="center_loader">
                    <ClipLoader color="#8CC64B" />
                  </div>
                ) : productsByCategory?.length <= 0 ? (
                  <>
                    <h3 className="center_loader">No products to display</h3>
                  </>
                ) : (
                  <>
                    <h3>{productsByCategory?.[0]?.category.title}</h3>
                    <div className="Products__Stack">
                      {productsByCategory?.map((product: any) => (
                        <ProductItem
                          key={product._id}
                          price={product.price}
                          productName={product.name}
                          productID={product._id}
                          category={product.category?.title}
                          img={product.image}
                          rating={product.rating}
                          isAuth={true}
                          onCartClick={(e: any) => onCartClick(e, product._id)}
                        />
                      ))}
                    </div>
                  </>
                )}
              </AnimatePresence>
            </section>
          )
        }}
      </CategoryProvider>
    </>
  )
}
