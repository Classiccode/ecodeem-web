import { useEffect, ReactNode, FC, useState } from "react"
import axios, { AxiosResponse } from "axios"

import { useSelector, useDispatch } from "react-redux"
import { RootState } from "../../../store"
import Config from "../../../config"
import {
  GetProductsByCategories,
  GetCategories,
  AddProductToCart,
} from "../../../config/endpoints"
import { useParams, useHistory } from "react-router-dom"
import {
  getProductsByCategory,
  getCategory,
} from "../../../store/slice/productSlice"
import { addToCart } from "../../../store/slice/cartSlice"

type ChildrenProps = {
  productsByCategory: any
  showLoader: boolean
  priceLoader: boolean
  onCartClick: (e: any, productID: string) => void
  addCartLoader: boolean
}

type Props = {
  children(_: ChildrenProps): ReactNode
}

const CategoryProvider: FC<Props> = ({
  children,
}: {
  children: ((_: ChildrenProps) => ReactNode) & ReactNode
}) => {
  const { category } = useParams<RouteParams>()
  const history = useHistory()

  interface RouteParams {
    category: string
  }

  const [showLoader, setLoader] = useState(false)
  const [priceLoader, setPriceLoader] = useState(false)
  const [addCartLoader, setAddCartLoader] = useState(false)

  const dispatch = useDispatch()

  const productsByCategory: any = useSelector(
    (state: RootState) => state.product.productsByCategory
  )

  useEffect(() => {
    getProductByCat()
    getProductCategories()
  }, [category])

  const getProductByCat = async () => {
    setLoader(true)
    try {
      const response: any = await axios.get(
        Config.marketplaceApiUrl + GetProductsByCategories(category)
      )
      const { data, status } = response
      if (status != 200) {
        console.error(data.message)
        setLoader(false)
      }
      dispatch(getProductsByCategory(data.products))
      setLoader(false)
    } catch (e: unknown) {
      console.error((e as { message: string }).message)
      setLoader(false)
    }
  }

  const getProductCategories = async () => {
    try {
      const response: AxiosResponse<unknown> = await axios.get(
        Config.marketplaceApiUrl + GetCategories()
      )
      const { data, status } = response
      if (status != 200) {
        console.error((data as { message: string }).message)
      }
      dispatch(getCategory(data))
      setLoader(false)
    } catch (e: unknown) {
      console.error((e as { message: string }).message)
    }
  }

  const user: any = localStorage.getItem("userID")
  const addCartItem = async (productID: string) => {
    setAddCartLoader(true)
    try {
      const response: any = await axios.post(
        Config.marketplaceApiUrl + AddProductToCart(user),
        {
          productId: productID,
        }
      )
      const { data, status } = response
      if (status != 200) {
        console.error(data.message)
        setAddCartLoader(false)
      }
      dispatch(addToCart(data.cart))
      setAddCartLoader(false)
      history.push("/marketPlace/cart")
    } catch (e) {
      console.error((e as { message: string }).message)
      setAddCartLoader(false)
    }
  }

  const onCartClick = (e: any, productID: string) => {
    e.preventDefault()
    if (!user) {
      history.push("/auth/login")
      return
    }
    addCartItem(productID)
  }
  return (
    <>
      {children({
        showLoader,
        priceLoader,
        onCartClick,
        productsByCategory,
        addCartLoader,
      })}
    </>
  )
}

export default CategoryProvider
