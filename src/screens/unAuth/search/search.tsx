import { UnAuthLayout } from "../../../layout"
import MarketplaceProvider from "../provider"
import ProductItem from "../../../components/Products/productItem"

const Search = (): JSX.Element => {
  return (
    <>
      <UnAuthLayout>
        <MarketplaceProvider>
          {({ onCartClick, searchProducts }) => (
            <section className="MarketPlace">
              <div className="Category__Column__Middle">
                <div className="Category__Column__Middle__Header">
                  <p className="Category__Column__Middle__Header__Text">
                    Search Result
                  </p>
                </div>
                <div className="Category__Column__Middle__Main">
                  <div className="Products__Stack">
                    {searchProducts?.map((product: any) => (
                      <ProductItem
                        key={product._id}
                        price={product.price}
                        productName={product.name}
                        productID={product._id}
                        category={product.category?.title}
                        img={product.image}
                        rating={product.rating}
                        onCartClick={(e: any) => onCartClick(e, product._id)}
                      />
                    ))}
                    {searchProducts.length == 0 && (
                      <div>No result for your search</div>
                    )}
                  </div>
                </div>
              </div>
            </section>
          )}
        </MarketplaceProvider>
      </UnAuthLayout>
    </>
  )
}

export const SearchAuth = (): JSX.Element => {
  return (
    <>
      <MarketplaceProvider>
        {({ onCartClick, searchProducts }) => (
          <section className="MarketPlace">
            <div className="Category__Column__Middle">
              <div className="Category__Column__Middle__Header">
                <p className="Category__Column__Middle__Header__Text">
                  Search Result
                </p>
              </div>
              <div className="Category__Column__Middle__Main">
                <div className="Products__Stack">
                  {searchProducts?.map((product: any) => (
                    <ProductItem
                      key={product._id}
                      price={product.price}
                      productName={product.name}
                      productID={product._id}
                      category={product.category?.title}
                      img={product.image}
                      rating={product.rating}
                      onCartClick={(e: any) => onCartClick(e, product._id)}
                    />
                  ))}
                  {searchProducts.length == 0 && (
                    <div>No result for your search</div>
                  )}
                </div>
              </div>
            </div>
          </section>
        )}
      </MarketplaceProvider>
    </>
  )
}
export default Search
