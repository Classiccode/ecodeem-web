import { UnAuthLayout } from "../../layout"
import { useState, useEffect, useCallback } from "react"
import ArrowBackIosOutlinedIcon from "@material-ui/icons/ArrowBackIosOutlined"
import ArrowForwardIosOutlinedIcon from "@material-ui/icons/ArrowForwardIosOutlined"
import banner3 from "../../assets/images/bannerImage3.png"
import banner2 from "../../assets/images/bannerImage2.png"
import banner1 from "../../assets/images/bannerImage1.png"
import Products from "../../components/Products/products"
import ProductProvider from "./provider"
import {
  MonthlyDeal,
  NewArrival,
  ProductReturnType,
  TodaysDeal,
  TopRatingProducts,
  TopSellingProducts,
  WeeksDeal,
} from "../../function"
import { useLocation } from "react-router"
import { toast } from "react-toastify"
import { useSelector } from "react-redux"
import { RootState } from "../../store"
import { Link } from "react-router-dom"
import { CircularProgress } from "@material-ui/core"
import { MarketPlaceContent } from ".."

export function MarketPlaceUnAuth(): JSX.Element {
  const images = [banner3, banner2, banner1]

  const [index, setIndex] = useState(0)
  const [marketPlaceContent, setContent] = useState<{
    newArrival: ProductReturnType[]
    weekDeals: ProductReturnType[]
    dailyDeals: ProductReturnType[]
    monthlyDeals: ProductReturnType[]
    topSelling: ProductReturnType[]
    topRated: ProductReturnType[]
  }>({
    newArrival: [],
    weekDeals: [],
    dailyDeals: [],
    monthlyDeals: [],
    topSelling: [],
    topRated: [],
  })

  const { categories } = useSelector((state: RootState) => {
    return {
      categories: state.data.categories,
    }
  })
  const { pathname } = useLocation()
  const [loading, setLoadingState] = useState(true)
  const callback = useCallback(async () => {
    await Promise.all([
      await NewArrival(),
      await WeeksDeal(),
      await MonthlyDeal(),
      await TodaysDeal(),
      await TopRatingProducts(),
      await TopSellingProducts(),
    ])
      .then((response) => {
        setContent({
          newArrival: response[0],
          weekDeals: response[1],
          monthlyDeals: response[2],
          dailyDeals: response[3],
          topRated: response[4],
          topSelling: response[5],
        })
      })
      .catch(() => {
        toast.error("Network Error!")
      })
      .finally(() => {
        setLoadingState(false)
      })
  }, [])
  useEffect(() => {
    callback()
  }, [callback])

  const handleForward = () => {
    if (index === images.length - 1) {
      setIndex(0)
    } else setIndex(index + 1)
  }

  const handleBackward = () => {
    if (index === 0) {
      setIndex(images.length - 1)
    } else setIndex(index - 1)
  }

  useEffect(() => {
    const interval = setInterval(() => {
      if (index === images.length - 1) {
        setIndex(0)
      } else setIndex(index + 1)
    }, 10000)
    return () => clearInterval(interval)
  }, [index])

  return (
    <UnAuthLayout>
      <section
        className="MarketPlace"
        style={{
          position: "relative",
          marginLeft: 0,
          marginTop: "70px",
          height: "calc(100vh - 100px)",
          overflowY: "auto",
          minHeight: "calc(100vh - 100px)",
          top: "100px",
          width: "100%",
          minWidth: "100%",
          paddingBottom: "100px",
        }}
      >
        <h2>MarketPlace</h2>
        <div
          style={{
            width: "100%",
            position: "relative",
            marginBottom: "24px",
          }}
          className="MarketPlace__Slider"
        >
          <div className="MarketPlace__Column">
            <ArrowBackIosOutlinedIcon
              onClick={handleBackward}
              fontSize="large"
              style={{
                background: "#f7f7f7",
                borderRadius: "50%",
                padding: "4px",
                cursor: "pointer",
              }}
            />
            <ArrowForwardIosOutlinedIcon
              onClick={handleForward}
              fontSize="large"
              style={{
                background: "#f7f7f7",
                borderRadius: "50%",
                padding: "4px",
                cursor: "pointer",
              }}
            />
          </div>
          <img src={images[index]} className="MarketPlace__HeaderImage" />
        </div>
        <h3 className="sub_title category_min">Categories</h3>
        <div className="category_list category_min">
          {categories.map((category) => {
            return (
              <Link
                to={`${pathname}/sectional/${category._id}`}
                className="content_content"
                key={category._id}
              >
                <div className="image">
                  <span style={{ backgroundImage: `url(${category.image})` }} />
                </div>
                <p className="txt_name">{category.title}</p>
              </Link>
            )
          })}
        </div>
        {loading ? (
          <div className="center">
            <CircularProgress color="inherit" />
          </div>
        ) : (
          <>
            {marketPlaceContent.newArrival.length >= 1 && (
              <div className="m_container">
                <h3 className="sub_title">New Arrivals</h3>
                <MarketPlaceContent content={marketPlaceContent.newArrival} />
              </div>
            )}
            {marketPlaceContent.dailyDeals.length >= 1 && (
              <div className="m_container">
                <h3 className="sub_title">Today&apos;s Deals</h3>
                <MarketPlaceContent content={marketPlaceContent.dailyDeals} />
              </div>
            )}
            {marketPlaceContent.weekDeals.length >= 1 && (
              <div className="m_container">
                <h3 className="sub_title">Weekly Deals</h3>
                <MarketPlaceContent content={marketPlaceContent.weekDeals} />
              </div>
            )}
            {marketPlaceContent.topSelling.length >= 1 && (
              <div className="m_container">
                <h3 className="sub_title">Top Selling</h3>
                <MarketPlaceContent content={marketPlaceContent.topSelling} />
              </div>
            )}
            {marketPlaceContent.topRated.length >= 1 && (
              <div className="m_container">
                <h3 className="sub_title">Top Rated</h3>
                <MarketPlaceContent content={marketPlaceContent.topRated} />
              </div>
            )}
            {marketPlaceContent.monthlyDeals.length >= 1 && (
              <div className="m_container">
                <h3 className="sub_title">This Month&apos;s Deals</h3>
                <MarketPlaceContent content={marketPlaceContent.monthlyDeals} />
              </div>
            )}
          </>
        )}
      </section>
    </UnAuthLayout>
  )
}
