/* eslint-disable lines-around-comment */
/* eslint-disable indent */
/* eslint-disable prettier/prettier */
import { UnAuthLayout } from "../../../layout"
import ProductDetailsProvider from "./provider"
import "./productDetails.scss"
import { useEffect, useState } from "react"
import Rating from "@material-ui/lab/Rating"
import FavoriteBorderOutlinedIcon from "@material-ui/icons/FavoriteBorderOutlined"
import FavoriteOutlinedIcon from "@material-ui/icons/FavoriteOutlined"
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart"
import Moment from "react-moment"
import ProductInfo from "../../../components/Products/productInfo"
import Button from "../../../components/Button/button"
import { Button as ButtonM } from "@material-ui/core"
import { useQuery } from "react-query"
import { RouteComponentProps, useHistory } from "react-router"
import Config from "../../../config"
import { useSelector } from "react-redux"
import { RootState } from "../../../store"
import { reviewProduct } from "../../../function/marketPlace/reviewProduct"
import { toast } from "react-toastify"
import { CircularProgress } from "@material-ui/core"

export function ProductDetails(): JSX.Element {
  const [like, setLike] = useState(false)
  const handleLike = (e) => {
    e.preventDefault()
    setLike(!like)
  }

  const [tab, setTab] = useState<string>("description")
  const { product, profile } = useSelector((state: RootState) => {
    return {
      product: state.data.currentProductView,
      profile: state.data.loginUserDetails,
    }
  })
  const { push } = useHistory()
  const [rating, setRating] = useState<number>(0)
  const [comment, setComment] = useState<string>("")
  useEffect(() => {
    if (product === undefined || product === null) {
      push("/content/market")
    }
  }, [product])
  return (
    <>
      <section className="MarketPlace">
        <div className="ProductDetails__Column__Middle">
          <div className="ProductDetails__Column__Mid">
            <div className="ProductDetails__Column__Middle__Pictures">
              <div className="ProductDetails__Column__Middle__Pictures__Big">
                <img
                  src={product?.image}
                  className="ProductDetails__Column__Middle__Pictures__Big__Pix"
                />
                {like ? (
                  <FavoriteOutlinedIcon
                    fontSize="large"
                    className="ProductDetails__Column__Middle__Pictures__Big__Heart"
                    onClick={handleLike}
                  />
                ) : (
                  <FavoriteBorderOutlinedIcon
                    fontSize="large"
                    className="ProductDetails__Column__Middle__Pictures__Big__Heart"
                    onClick={handleLike}
                  />
                )}
              </div>
              <div className="ProductDetails__Column__Middle__Pictures__Small">
                <div className="ProductDetails__Column__Middle__Pictures__Small__Pix">
                  <img
                    src={product?.image}
                    className="ProductDetails__Column__Middle__Pictures__Small__Pix__Img"
                  />
                </div>
                <div className="ProductDetails__Column__Middle__Pictures__Small__Pix">
                  <img
                    src={product?.image}
                    className="ProductDetails__Column__Middle__Pictures__Small__Pix__Img"
                  />
                </div>
                <div className="ProductDetails__Column__Middle__Pictures__Small__Pix">
                  <img
                    src={product?.image}
                    className="ProductDetails__Column__Middle__Pictures__Small__Pix__Img"
                  />
                </div>
                <div className="ProductDetails__Column__Middle__Pictures__Small__Pix">
                  <img
                    src={product?.image}
                    className="ProductDetails__Column__Middle__Pictures__Small__Pix__Img"
                  />
                </div>
              </div>
            </div>
            <ProductInfo
              name={product?.name ?? ""}
              description={product?.description ?? ""}
              price={product?.price ?? 0}
              onCartClick={(e: any) => console.log("hello")}
              rating={product?.rating ?? 0}
              brand={product?.brand ?? ""}
              reviews={product?.reviews ?? []}
              id={product?._id ?? ""}
            />
          </div>
          <div className="ProductDetails__Column__Middle__Bottom">
            <div className="ProductDetails__Column__Middle__Bottom__Header">
              <ul>
                <li
                  onClick={() => {
                    setTab("description")
                  }}
                  className={`${
                    tab == "description"
                      ? "ProductDetails__Column__Middle__Bottom__Header__Active"
                      : "ProductDetails__Column__Middle__Bottom__Header__Inactive"
                  }`}
                >
                  DESCRIPTION
                </li>
                <li
                  onClick={() => {
                    setTab("review")
                  }}
                  className={`${
                    tab == "review"
                      ? "ProductDetails__Column__Middle__Bottom__Header__Active"
                      : "ProductDetails__Column__Middle__Bottom__Header__Inactive"
                  }`}
                >
                  REVIEWS
                </li>
              </ul>
            </div>
            <div className="ProductDetails__Column__Middle__Bottom__Content">
              {tab === "description" ? (
                <div style={{ marginTop: "24px", marginBottom: "24px" }}>
                  {product?.description}
                </div>
              ) : null}
              {tab === "review" ? (
                product?.reviews && product.reviews.length > 0 ? (
                  product.reviews.map((review) => (
                    <div
                      style={{
                        marginTop: "24px",
                        borderBottom: "1px solid #f3f3f3",
                      }}
                      key={review._id}
                    >
                      <p>{review.name}</p>
                      <div style={{ display: "flex" }}>
                        <Rating
                          name="read-only"
                          value={review.rating}
                          readOnly
                          size="small"
                        />
                        <p style={{ marginLeft: "12px" }}>
                          <Moment format="Do MMMM YYYY">
                            {review.reviewedOn}
                          </Moment>
                        </p>
                      </div>
                      <p>{review.comment}</p>
                    </div>
                  ))
                ) : (
                  <div>
                    <p>No reviews yet for this product.</p>
                  </div>
                )
              ) : null}
              {tab === "review" ? (
                <div>
                  <p style={{ fontWeight: "bold", marginTop: "24px" }}>
                    Leave a review
                  </p>
                  {/* <p>{message !== "" ? message : null}</p>  */}
                  <label>Comment</label>
                  <br />
                  <textarea
                    className="ProductDetails__Column__Middle__Input"
                    value={comment}
                    onChange={(e) => {
                      setComment(e.target.value)
                    }}
                  />
                  <div style={{ marginBottom: "24px" }}>
                    Rating:{" "}
                    <Rating
                      name="rating"
                      value={rating ?? 0}
                      onChange={(event, newValue) => {
                        setRating(newValue ?? 0)
                      }}
                      size="small"
                    />
                  </div>
                  <Button
                    type="nav"
                    text="Add Review"
                    onClick={async () => {
                      await reviewProduct({
                        comment: comment,
                        productId: product?._id ?? "",
                        rating: rating,
                        userID: profile?.user._id ?? "",
                      })
                        .then(() => {
                          toast.success("Your review has been added.")
                        })
                        .catch((error) => {
                          toast.error(error)
                        })
                    }}
                  />
                </div>
              ) : null}
            </div>
          </div>
        </div>
      </section>
      {/* )} */}
    </>
  )
}

export function ProductDetailsAuth(): JSX.Element {
  const [like, setLike] = useState(false)
  const handleLike = (e) => {
    e.preventDefault()
    setLike(!like)
  }
  const [tab, setTab] = useState<string>("description")
  const { product, profile } = useSelector((state: RootState) => {
    return {
      product: state.data.currentProductView,
      profile: state.data.loginUserDetails,
    }
  })
  const [rating, setRating] = useState<number>(5)
  const [comment, setComment] = useState<string>("")
  const [loadingReview, setLoadingReviewState] = useState<boolean>(false)
  const { push } = useHistory()
  useEffect(() => {
    if (product === undefined || product === null) {
      push("/content/market")
    }
  }, [product])
  return (
    <>
      <section className="MarketPlace">
        <div className="ProductDetails__Column__Middle">
          <div className="ProductDetails__Column__Mid">
            <div className="ProductDetails__Column__Middle__Pictures">
              <div className="ProductDetails__Column__Middle__Pictures__Big">
                <img
                  src={product?.image}
                  className="ProductDetails__Column__Middle__Pictures__Big__Pix"
                />
                {like ? (
                  <FavoriteOutlinedIcon
                    fontSize="large"
                    className="ProductDetails__Column__Middle__Pictures__Big__Heart"
                    onClick={handleLike}
                  />
                ) : (
                  <FavoriteBorderOutlinedIcon
                    fontSize="large"
                    className="ProductDetails__Column__Middle__Pictures__Big__Heart"
                    onClick={handleLike}
                  />
                )}
              </div>
              <div className="ProductDetails__Column__Middle__Pictures__Small">
                <div className="ProductDetails__Column__Middle__Pictures__Small__Pix">
                  <img
                    src={product?.image}
                    className="ProductDetails__Column__Middle__Pictures__Small__Pix__Img"
                  />
                </div>
                <div className="ProductDetails__Column__Middle__Pictures__Small__Pix">
                  <img
                    src={product?.image}
                    className="ProductDetails__Column__Middle__Pictures__Small__Pix__Img"
                  />
                </div>
                <div className="ProductDetails__Column__Middle__Pictures__Small__Pix">
                  <img
                    src={product?.image}
                    className="ProductDetails__Column__Middle__Pictures__Small__Pix__Img"
                  />
                </div>
                <div className="ProductDetails__Column__Middle__Pictures__Small__Pix">
                  <img
                    src={product?.image}
                    className="ProductDetails__Column__Middle__Pictures__Small__Pix__Img"
                  />
                </div>
              </div>
            </div>
            <ProductInfo
              name={product?.name ?? ""}
              description={product?.description ?? ""}
              price={product?.price ?? 0}
              onCartClick={(e: any) => console.log("hello")}
              rating={product?.rating ?? 0}
              brand={product?.brand ?? ""}
              reviews={product?.reviews ?? []}
              id={product?._id ?? ""}
            />
          </div>
          <div className="ProductDetails__Column__Middle__Bottom">
            <div className="ProductDetails__Column__Middle__Bottom__Header">
              <ul>
                <li
                  onClick={() => {
                    setTab("description")
                  }}
                  className={`${
                    tab == "description"
                      ? "ProductDetails__Column__Middle__Bottom__Header__Active"
                      : "ProductDetails__Column__Middle__Bottom__Header__Inactive"
                  }`}
                >
                  DESCRIPTION
                </li>
                <li
                  onClick={() => {
                    setTab("review")
                  }}
                  className={`${
                    tab == "review"
                      ? "ProductDetails__Column__Middle__Bottom__Header__Active"
                      : "ProductDetails__Column__Middle__Bottom__Header__Inactive"
                  }`}
                >
                  REVIEWS
                </li>
              </ul>
            </div>
            <div className="ProductDetails__Column__Middle__Bottom__Content">
              {tab === "description" ? (
                <div style={{ marginTop: "24px", marginBottom: "24px" }}>
                  {product?.description}
                </div>
              ) : null}
              {tab === "review" ? (
                product?.reviews && product.reviews.length > 0 ? (
                  product.reviews.map((review) => (
                    <div
                      style={{
                        marginTop: "24px",
                        borderBottom: "1px solid #f3f3f3",
                      }}
                      key={review._id}
                    >
                      <p>{review.name}</p>
                      <div style={{ display: "flex" }}>
                        <Rating
                          name="read-only"
                          value={review.rating}
                          readOnly
                          size="small"
                        />
                        <p style={{ marginLeft: "12px" }}>
                          <Moment format="Do MMMM YYYY">
                            {review.reviewedOn}
                          </Moment>
                        </p>
                      </div>
                      <p>{review.comment}</p>
                    </div>
                  ))
                ) : (
                  <div>
                    <p>No reviews yet for this product.</p>
                  </div>
                )
              ) : null}
              {tab === "review" ? (
                <div>
                  <p style={{ fontWeight: "bold", marginTop: "24px" }}>
                    Leave a review
                  </p>
                  {/* <p>{message !== "" ? message : null}</p>  */}
                  <label>Comment</label>
                  <br />
                  <textarea
                    className="ProductDetails__Column__Middle__Input"
                    value={comment}
                    onChange={(e) => {
                      setComment(e.target.value)
                    }}
                  />
                  <div style={{ marginBottom: "24px" }}>
                    Rating:{" "}
                    <Rating
                      name="rating"
                      value={rating ?? 0}
                      onChange={(event, newValue) => {
                        setRating(newValue ?? 0)
                      }}
                      size="small"
                    />
                  </div>

                  <ButtonM
                    className="Button__nav btn"
                    style={{
                      backgroundColor: "#2E8CCC",
                      color: "white",
                      minWidth: "150px",
                    }}
                    onClick={async () => {
                      setLoadingReviewState(true)
                      if (comment === "") {
                        toast.dark("Provide your comment.")
                        return
                      }
                      await reviewProduct({
                        comment: comment,
                        productId: product?._id ?? "",
                        rating: rating ?? 0,
                        userID: profile?.user._id ?? "",
                      })
                        .then(() => {
                          toast.success("Your review has been added.")
                        })
                        .catch((error) => {
                          toast.error(error)
                        })
                        .finally(() => {
                          setLoadingReviewState(false)
                        })
                    }}
                  >
                    {loadingReview ? (
                      <CircularProgress
                        color="inherit"
                        style={{ height: "40px" }}
                      />
                    ) : (
                      "Add Review"
                    )}
                  </ButtonM>
                </div>
              ) : null}
            </div>
          </div>
        </div>
      </section>
      {/* )} */}
    </>
  )
}
