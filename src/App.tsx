import { memo, useCallback, useEffect } from "react"
import {
  Route,
  Switch,
  Redirect,
  useHistory,
  useLocation,
  useRouteMatch,
} from "react-router-dom"
import { ToastContainer } from "react-toastify"
import {
  LoginScreen,
  RegisterScreen,
  PostPage,
  Page404,
  VerifyScreen,
  MarketPlaceMain,
  ProfileMainPage,
  ChatPageMain,
  NotificationMainPage,
  MarketPlaceUnAuth,
  SinglePostPage,
  ForgetPassword,
  ResetPassword,
  SettingsMainPage,
  AccountSettingMainPage,
  PaymentVerification,
  Category,
  ProductDetails,
  Search,
  Cart,
  Orders,
  PaymentConfirm,
  CompleteOrder,
  BadNetworkPage,
  CategoryAuth,
  ProductDetailsAuth,
  AccountSettingViewMyInterest,
  AccountMyFollowing,
  AccountMyFollower,
  SectionalCategory,
  SubCategory,
  ProductList,
} from "./screens"

// styles

import "./global.scss"
import "./styles/components/loader.scss"
import "./styles/auth/auth.scss"
import "./assets/chat-assets/scss/themes.scss"
import "./assets/chat-assets/css/icons.css"
import "./styles/components/components.scss"
import "./styles/screen/screen.scss"
import "react-toastify/dist/ReactToastify.min.css"
import { Loader } from "./components"
import { AnimatePresence } from "framer-motion"
import { setAuthorization } from "./config/axios"
import ImagePreview from "./screens/content/blog/imagePreview"
import { GeneralSettingMainPage } from "./screens/content/settings/general"
import { RootState, StoreType } from "./store"
import { useDispatch, useSelector } from "react-redux"
import { ContentLayout } from "./layout"
import { SearchAuth } from "./screens/unAuth/search/search"
import NewOrder from "./screens/content/market_place/orders/newOrder"
import Config from "./config"
import { AxiosError } from "axios"

export default function Ecodeem(): JSX.Element {
  const { isDarkMode } = useSelector((state: RootState) => state.theme)
  const { push } = useHistory()
  const location = useLocation()
  const dispatch = useDispatch()
  const setToken = useCallback(() => {
    if (!location.pathname.includes("/auth/reset-password")) {
      setAuthorization(push, undefined)
    }
  }, [])
  useEffect(() => {
    setToken()
  }, [setToken])
  useEffect(() => {
    isDarkMode
      ? document.documentElement.setAttribute("data-theme", "deem")
      : document.documentElement.setAttribute("data-theme", "light")
  }, [isDarkMode])
  useEffect(() => {
    const vh = window.innerHeight * 0.01
    document.documentElement.style.setProperty("--vh", `${vh}px`)
  }, [])
  const marketPlaceInit = useCallback(async () => {
    await Promise.all([
      await Config.apiMarketPlace.get("/category/all"),
      await Config.apiMarketPlace.get("/s-category/all"),
      await Config.apiMarketPlace.get("/sub-category/all"),
    ])
      .then(
        ([
          {
            data: {
              data: { categories },
            },
          },
          {
            data: {
              data: { sectionalCategories },
            },
          },
          {
            data: {
              data: { subCategories },
            },
          },
        ]) => {
          dispatch({
            type: StoreType.updateFullCategoryList,
            value: {
              categories,
              sectionalCategories,
              subCategories,
            },
          })
        }
      )
      .catch(([error, error2]: [AxiosError<unknown>, AxiosError<unknown>]) => {
        if (error.response === undefined || error2.response === undefined) {
          push("/503")
          return
        }
        push("/503")
      })
  }, [])
  useEffect(() => {
    marketPlaceInit()
  }, [marketPlaceInit])
  return (
    <>
      <ToastContainer position="top-right" />
      <Loader />
      <AnimatePresence>
        <Switch location={location} key={location.key}>
          <Route path="/content">
            <ContentRoutes />
          </Route>
          <Route path="/auth">
            <AuthRoutes />
          </Route>
          <Route path="/market">
            <UnAuthMarketPlaceRoutes />
          </Route>
          <Route
            path="/503"
            component={BadNetworkPage}
            exact
            key={"network-error"}
          />
          <Route
            path="/not-found"
            component={Page404}
            exact
            key="page-redirect"
          />
          <Route
            path="/checkout/payment-confirmation"
            component={PaymentConfirm}
            exact
            key="market_place_confirm"
          />
          <Route
            path="/payment-callback"
            component={PaymentVerification}
            exact
            key="paymentVerification"
          />
          <Route exact path="/preview/:id/photo">
            <ImagePreview isModal={true} />
          </Route>
          <Redirect from="/" to="/content/post" exact key="not-found-plain" />
          <Redirect from="*" to="/not-found" exact key="not-found" />
        </Switch>
      </AnimatePresence>
    </>
  )
}

const UnAuthMarketPlaceRoutes = memo(function i(): JSX.Element {
  const { path } = useRouteMatch()
  return (
    <AnimatePresence>
      <Switch>
        <Route
          path={`${path}`}
          component={MarketPlaceUnAuth}
          exact
          key="market_place_un_auth"
        />
        <Route
          path={`${path}/search`}
          component={Search}
          exact
          key="market_search"
        />
        <Route
          path={`${path}/:category`}
          component={Category}
          exact
          key="category"
        />
        <Route
          exact
          path={`${path}/:category/:productDetails`}
          component={ProductDetails}
          key="product_details"
        />
      </Switch>
    </AnimatePresence>
  )
})

const AuthRoutes = memo(function i(): JSX.Element {
  const { path } = useRouteMatch()
  return (
    <AnimatePresence>
      <Switch>
        <Route
          path={`${path}/register`}
          component={RegisterScreen}
          exact
          key={"register"}
        />
        <Route
          path={`${path}/forgot-password`}
          component={ForgetPassword}
          exact
          key={"forgot-password"}
        />
        <Route
          path={`${path}/verify`}
          component={VerifyScreen}
          exact
          key={"verify"}
        />
        <Route
          path={`${path}/login`}
          component={LoginScreen}
          exact
          key={"login"}
        />
        <Route
          path={`${path}/reset-password/:token`}
          component={ResetPassword}
          exact
          key={"reset-password"}
        />
      </Switch>
    </AnimatePresence>
  )
})

const ContentRoutes = memo(function i(): JSX.Element {
  const { path } = useRouteMatch()
  return (
    <AnimatePresence>
      <ContentLayout>
        <Switch>
          <Route path={`${path}/post`} exact key="posts">
            <PostPage />
          </Route>
          <Route
            path={`${path}/payment-callback`}
            component={PaymentVerification}
            exact
            key="paymentVerification"
          />
          <Route
            path={`${path}/post/:id`}
            component={SinglePostPage}
            exact
            key="post"
          />
          <Route
            path={`${path}/chat`}
            component={ChatPageMain}
            exact
            key="chat"
          />
          <Route
            path={`${path}/notification`}
            component={NotificationMainPage}
            exact
            key="notification"
          />
          <Route
            path={`${path}/profile/:id`}
            component={ProfileMainPage}
            exact
            key="profile"
          />
          <Route
            path={`${path}/profile/account/setting`}
            component={SettingsMainPage}
            exact
            key="setting"
          />
          <Route
            path={`${path}/profile/account/follower`}
            component={AccountMyFollower}
            exact
            key="followers"
          />
          <Route
            path={`${path}/profile/account/following`}
            component={AccountMyFollowing}
            exact
            key="followers"
          />
          <Route
            path={`${path}/profile/account/setting/account`}
            component={AccountSettingMainPage}
            exact
            key="account"
          />
          <Route
            path={`${path}/profile/account/setting/interest`}
            component={AccountSettingViewMyInterest}
            exact
            key="interest"
          />
          <Route
            path={`${path}/profile/account/setting/general`}
            component={GeneralSettingMainPage}
            exact
            key="general"
          />
          <Route path={`${path}/market`}>
            <MarketPlaceRoutes />
          </Route>
        </Switch>
      </ContentLayout>
    </AnimatePresence>
  )
})

const MarketPlaceRoutes = memo(function i(): JSX.Element {
  const { path } = useRouteMatch()
  return (
    <AnimatePresence>
      <Switch>
        <Route
          path={`${path}`}
          component={MarketPlaceMain}
          exact
          key="market_place"
        />
        <Route
          path={`${path}/search`}
          component={SearchAuth}
          exact
          key="market_search"
        />
        <Route
          path={`${path}/new-order`}
          component={NewOrder}
          exact
          key="market_place"
        />
        <Route
          path={`${path}/cart`}
          component={Cart}
          exact
          key="market_place_cart"
        />
        <Route
          path={`${path}/orders`}
          component={Orders}
          exact
          key="market_place_orders"
        />
        <Route
          path={`${path}/checkout`}
          component={CompleteOrder}
          exact
          key="market_place_orders"
        />
        <Route
          path={`${path}/:category`}
          component={CategoryAuth}
          exact
          key="category"
        />
        <Route
          path={`${path}/sectional/:categoryID`}
          component={SectionalCategory}
          exact
          key="sectional_category"
        />
        <Route
          path={`${path}/product_list/:sub_categoryID`}
          component={ProductList}
          exact
          key="sectional_category"
        />
        <Route
          path={`${path}/sub_category/:sectionalCategoryID`}
          component={SubCategory}
          exact
          key="sub_category"
        />
        <Route
          exact
          path={`${path}/product/:productID`}
          component={ProductDetailsAuth}
          key="product_details"
        />
        {/* <Route
          exact
          path={`${path}/:category/:productDetails`}
          component={ProductDetailsAuth}
          key="product_details"
        /> */}
      </Switch>
    </AnimatePresence>
  )
})
