/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { UserType } from "./../function/auth/type"
import { RootState } from "../store/store"
import { useSelector } from "react-redux"

interface useProfileReturnType {
  profile: {
    token: string
    user: UserType
  }
}
export const useProfile = (): useProfileReturnType => {
  const { token, user } = useSelector((state: RootState) => {
    return {
      token: state.data.loginUserDetails!.token,
      user: state.data.loginUserDetails!.user,
    }
  })

  const profile = {
    token,
    user,
  }
  return { profile }
}
