export function isEmail(value: string): boolean {
  return /^([A-Za-z0-9_\-.]){1,}@([A-Za-z0-9_\-.]){1,}\.([A-Za-z]){2,4}$/.test(
    value
  )
}
export function isPhoneNumber(value: string): boolean {
  return /^[0-9]*$/.test(value)
}

export function isStrongPassword(value: string): boolean {
  return /[!@#$%^&*(),.?":{}|<>]/.test(value) && /[1-9]/.test(value)
}

export function numberWithCommas(x: number | undefined | string) {
  return x?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}
