import Swal from "sweetalert2"
export const getDaysBetween = (date1: Date, date2: Date): string => {
  const d1 = +date1
  const d2 = date2

  let diff = (d2.getTime() - d1) / 1000
  diff /= 60 * 60 * 24

  if (diff > 365) {
    diff /= 365
    const vd = Math.abs(diff).toFixed(1).split(".").shift()
    return vd + " years ago"
  }

  if (diff >= 30 && diff <= 365) {
    diff /= 30
    const vd = Math.abs(diff).toFixed(1).split(".").shift()
    return vd + " months ago"
  }

  if (diff <= 30) {
    const vd = Math.abs(diff).toFixed(1).split(".").shift()
    return vd === "0" ? "today" : vd + " days ago"
  }

  return ""
}

export const isDoneLoading = function (status: string): Promise<unknown> {
  return new Promise(function (resolve) {
    const interval = setInterval(function () {
      if (status !== "loading") {
        resolve(status)
        clearInterval(interval)
      }
    }, 2000)
  })
}

export const checkMediaType = (url: string | undefined): string => {
  if (url) {
    const urlArray = url.split("/")

    return urlArray[4]
  }
  return ""
}

export const openInNewTab = (url: string): void => {
  const newWindow = window.open(url, "_blank", "noopener,noreferrer")
  if (newWindow) newWindow.opener = null
}

export const truncateWords = (chars: string, num: number): string => {
  const kk = chars.length > num ? "..." : ""
  return `${chars.slice(0, num)}${kk}`
}

export const deDupArray = (arr: Array<any>, key: string): Array<any> => {
  return arr.filter(
    (x, index, newArray) =>
      index === newArray.findIndex((t) => t[key] === x[key])
  )
}

export const toastMixin = Swal.mixin({
  toast: true,
  icon: "success",
  title: "General Title",
  position: "top-right",
  showConfirmButton: false,
  timer: 5000,
  timerProgressBar: true,
  didOpen: (toast) => {
    toast.addEventListener("mouseenter", Swal.stopTimer)
    toast.addEventListener("mouseleave", Swal.resumeTimer)
  },
})
