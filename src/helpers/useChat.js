import { useEffect, useRef, useState } from "react"
import socketIOClient from "socket.io-client"
import axios from "axios"
import { onNewMessage, onTyping } from "../store/action/index"
import { useDispatch } from "react-redux"
import { useProfile } from "../utitly"
import { toast } from "react-toastify"
import Config from "../config"

const USER_JOIN_CHAT_EVENT = "USER_JOIN_CHAT_EVENT"
const USER_LEAVE_CHAT_EVENT = "USER_LEAVE_CHAT_EVENT"
const NEW_CHAT_MESSAGE_EVENT = "NEW_CHAT_MESSAGE_EVENT"
const START_TYPING_MESSAGE_EVENT = "START_TYPING_MESSAGE_EVENT"
const STOP_TYPING_MESSAGE_EVENT = "STOP_TYPING_MESSAGE_EVENT"
const SOCKET_SERVER_URL = Config.chatApiUrl

const useChat = () => {
  const [message, setMessage] = useState()
  const [users, setUsers] = useState([])
  const [typingUsers, setTypingUsers] = useState([])
  const [user, setUser] = useState()
  const socketRef = useRef()
  const profile = useProfile()
  const dispatch = useDispatch()

  useEffect(() => {
    socketRef.current = socketIOClient(SOCKET_SERVER_URL, {
      forceNew: true,
      transports: ["websocket"],
    })

    socketRef.current.on("connect", () => {
      console.log("user connected " + socketRef.current.id)
    })

    socketRef.current.on(USER_JOIN_CHAT_EVENT, (user) => {
      if (user.id === socketRef.current.id) return
      setUsers((users) => [...users, user])
    })

    socketRef.current.on(USER_LEAVE_CHAT_EVENT, (user) => {
      setUsers((users) => users.filter((u) => u.id !== user.id))
    })

    socketRef.current.on(NEW_CHAT_MESSAGE_EVENT, ({ message }) => {
      dispatch(onNewMessage(message))
      if (profile.profile.user._id !== message.postedBy.ecodeemId) {
        toast.success(
          `you have a new message from ${message.postedBy.firstName}`,
          {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          }
        )
      }

      // setMessage({...message})
      // console.log(message);
    })

    socketRef.current.on("join", (room) => {
      socket.current.join(room)
      console.log(`Socket joining ${room}`)
    })

    socketRef.current.on(START_TYPING_MESSAGE_EVENT, (typingInfo) => {
      const info = typingInfo
      setTypingUsers((users) => [...users, info])

      dispatch(onTyping([...typingUsers, typingInfo]))
    })

    socketRef.current.on(STOP_TYPING_MESSAGE_EVENT, (typingInfo) => {
      const info = typingInfo
      setTypingUsers((users) => users.filter((u) => u.id !== info.id))
      dispatch(onTyping(typingUsers.filter((u) => u.id !== info.id)))
    })

    return () => {
      socketRef.current.disconnect()
    }
  }, [])

  const sendMessage = (messageBody) => {
    if (!socketRef.current) return
    socketRef.current.emit(NEW_CHAT_MESSAGE_EVENT, {
      body: messageBody,
      senderId: socketRef.current.id,
      user: user,
    })
  }

  const startTypingMessage = (roomId, firstName, id) => {
    if (!socketRef.current) return
    socketRef.current.emit(START_TYPING_MESSAGE_EVENT, {
      conversationId: roomId,
      name: firstName,
      id: id,
    })
  }

  const stopTypingMessage = (roomId, firstName, id) => {
    if (!socketRef.current) return
    socketRef.current.emit(STOP_TYPING_MESSAGE_EVENT, {
      conversationId: roomId,
      name: firstName,
      id: id,
    })
  }

  const joinRoom = (roomId) => {
    socketRef.current.emit(USER_JOIN_CHAT_EVENT, roomId)
  }

  const newMessage = () => {}

  return {
    typingUsers,
    joinRoom,
    sendMessage,
    startTypingMessage,
    stopTypingMessage,
    socketRef,
    message,
  }
}

export default useChat
