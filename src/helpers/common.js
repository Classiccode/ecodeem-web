const getChatter = (users, user) => {
    let nonAuth = users.filter(e => e.ecodeemId !== user._id)
    return nonAuth[0]
}

const getImageSize = (url) => {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.responseType = "arraybuffer";
    xhr.onreadystatechange = function () {
        if (this.readyState == this.DONE) {
            // console.log(this.response.byteLength);
            return this.response.byteLength
            // alert("Image size = " + this.response.byteLength + " bytes.");
            // alert("Image size = " + this.response.byteLength + " bytes.");
            if (callback) callback(this.response.byteLength);
        }
    };
    xhr.send(null);
}

const truncateWords = (chars, num) => {
    let kk = chars.length > num ? '...' : '';
    return `${chars.slice(0, num)}${kk}`;
}

export { getChatter, getImageSize, truncateWords }