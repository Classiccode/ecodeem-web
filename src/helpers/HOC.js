import React from 'react';
import useChat from './useChat';

export const withSocketEvent = (Component) => {
  return (props) => {
    const socketMethods = useChat();

    return <Component socketMethods={socketMethods} {...props} />;
  };
};